<img src="logo.png" alt="AbstractSwarm Logo" width="215">

# AbstractSwarm

AbstractSwarm is a graphical multi-agent modeling and simulation environment, especially suitable for applying AI to logistics. 
It is currently further developed as an open source project under the GPL license (Version 3) and it is planned to be applied to the optimization of hospital processes. 

## Main Ideas 

- Graphical modeling of logistics and related problems on an agent level. 
- Implementation of agents that are able to cooperatively find solutions to the modeled problems.
- Use of machine learning and related artificial intelligence (AI) or computational intelligence (CI) technologies to create agents that are not only able to find solutions to a single model but also to several different models and even new models, that are not known in advance.

**With AbstractSwarm it is also intended to stimulate AI/CI research by means of a comptition: See the [AbstractSwarm Multi-Agent Logistics Competition](https://abstractswarm.gitlab.io/abstractswarm_competition) website for test scenarios, tutorials and further information!**

## Setup and Getting Started

- Download the latest release build of AbstractSwarm from the [Releases](https://gitlab.com/abstractswarm/abstractswarm/-/releases) page, place it in a folder with read/write access and unzip.
- **Windows**: 
    - Make sure you have a Java JDK ([OpenJDK](http://openjdk.java.net/projects/jdk/) is recommended) properly available on your system (path to its "bin" subfolder must be included in the "Path" variable).
    - Run AbstractSwarm.exe, load a demo scenario from the Graphs subfolder (e.g., MultiAgentShortestPaths.xml), choose an agent implementation from the Agents menu (default is random behavior) and press the play button.
- **Linux**: 

    ***Method 1***
    - Make sure you have Wine installed.
    - Download a compatible [Java JDK](https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u265-b01/OpenJDK8U-jdk_x86-32_windows_hotspot_8u265b01.zip) and put it in an eligible folder of the Wine environment (e.g., "c:\Program Files").
    - Set the path variable accordingly by running `wine regedit` in a terminal, navigate to "HKEY_CURRENT_USER" --> "Environment" and add the folder pointing to the "bin" subfolder of the Java JDK to the "Path" variable (or create the "Path" varialbe with the value "c:\\\windows;c:\\\windows\\\system;..." replacing "..." with the path to the "bin" subfolder of the Java JDK, without trailing slashes). Close the "regedit" window.
    - Navigate to the AbstractSwarm main folder in the terminal and run AbstractSwarm.exe with Wine (`wine AbstractSwarm.exe`).
    - Load a demo scenario from the Graphs subfolder (e.g., MultiAgentShortestPaths.xml), choose an agent implementation from the Agents menu (default is random behavior) and press the play button.  

    ***Method 2***
    - Install [WineHQ](https://www.winehq.org/) for your distribution.
    - Download a [JDK-Installer](https://www.oracle.com/java/technologies/javase-downloads.html) for Windows.
    - Install that JDK using *wine*
      ```shell
      wine jdk-XX-windows-ARCH-bin.exe
      ```
      **Note the location the jdk is installed to.**
    - Set the Windows PATH variable using *WINEPATH*
      ```shell
      export WINEPATH="C:\Program Files\Java\jdk-16\bin"
      ```
      The Path depends on the JDK version you are using. Here it is JDK 16 x64
    - Run the application using *wine*
      ```shell
      wine AbstractSwarm.exe
      ```

      Note that the WINEPATH is temporarily set. Persist it in your environment or use `wine regedit` to set the windows PATH permanently.

- In AbstractSwarm's Agents menu, click on "Agent Editor" to open the source file of the currently selected agent implementation. Alternatively, you can navigate in the "Agents" subfolder and open an agent folder or add a new one there. You can also put .jar files in your agent folder, in case you want to include a java library in your agent implementation.
(If Eclipse is installed, clicking on "Agent Editor" in the "Agents" menu will open the Eclipse IDE with the current selected agent implementation.)  
