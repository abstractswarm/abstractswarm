/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "AgentType.h"
#include "./../Simulation/Components/Agent.hpp"  // Included from simulation!

#include <sstream>





using namespace std;





//------------------------------------------------------------
// Static Init
//------------------------------------------------------------


int CAgentType::noOfInstances = 0;


//------------------------------------------------------------
// Construction
//------------------------------------------------------------


CAgentType::CAgentType( const CPerspective& owner, TfpAddAgentToSimulation fpAddAgentToSimulation, TfpRemoveAgentFromSimulation fpRemoveAgentFromSimulation ) : CComponentType( owner )
{
	// Count instances
	noOfInstances++;

	// Store pointer to function to add agent to simulation
	this->fpAddAgentToSimulation = fpAddAgentToSimulation;

	// Store pointer to function to remove agent from simulation
	this->fpRemoveAgentFromSimulation = fpRemoveAgentFromSimulation;

	// Set the name
	ostringstream ossNoOfInstances; 
	ossNoOfInstances << getNoOfInstances();
	setName( string( "AgentType" )+ossNoOfInstances.str() );

	// Add at least one agent to agent type
	agents.push_back( new CAgent( *this ) );

	// Init optional agent type attributes
	hasCapacity = false;
	capacity = 1;
	hasSize = false;
	size = 1;
	hasSpeed = false;
	speed = 1;
}


//------------------------------------------------------------
// Destruction
//------------------------------------------------------------


CAgentType::~CAgentType()
{
	deleteAgents();

    // Update the components of all connected component types (cannot be done in super class since
	// this would cause a pure virtual function call because "updateConnectedComponents()" uses 
	// late binding calls which are not possible during destruction)
	updateConnectedComponents();
}


//------------------------------------------------------------
// Getter
//------------------------------------------------------------


int CAgentType::getNoOfInstances() const
{
	return( noOfInstances );
}


int CAgentType::getNoOfComponents() const
{
	int count = agents.size();
	return( count );
}


TfpAddAgentToSimulation CAgentType::getAddAgentToSimulation() const
{
	return( fpAddAgentToSimulation );
}


TfpRemoveAgentFromSimulation CAgentType::getRemoveAgentFromSimulation() const
{
	return( fpRemoveAgentFromSimulation );
}


const vector<CComponent*>& CAgentType::getComponents() const
{
	return( reinterpret_cast<const vector<CComponent*>&>( agents ) );
}


const vector<CAgent*>& CAgentType::getAgents() const
{
	return( agents );
}


//------------------------------------------------------------
// Optional agent type attribute getter
//------------------------------------------------------------


bool CAgentType::getHasAttribs() const
{
	return( (CComponentType::getHasAttribs() || hasCapacity || hasSize || hasSpeed) );
}


int CAgentType::getNoOfAttribs() const
{
	int attribsCount = CComponentType::getNoOfAttribs();

	if( hasCapacity )
		attribsCount++;
	if( hasSize )
		attribsCount++;
	if( hasSpeed )
		attribsCount++;

	return( attribsCount );
}


bool CAgentType::getHasCapacity() const
{
	return( hasCapacity );
}


int CAgentType::getCapacity() const
{
	return( capacity );
}


bool CAgentType::getHasSize() const
{
	return( hasSize );
}


int CAgentType::getSize() const
{
	return( size );
}


bool CAgentType::getHasSpeed() const
{
	return( hasSpeed );
}


int CAgentType::getSpeed() const
{
	return( speed );
}


//------------------------------------------------------------
// Setter
//------------------------------------------------------------


void CAgentType::setNoOfComponents( int noOfComponents )
{
	// Free the old agents from memory first and clear the agents list
	deleteAgents();

	// And finally create the new agents
	int i = 0;
	for( i = 0; i < noOfComponents; i++ )
		agents.push_back( new CAgent( *this ) );

	// Update all attributes, etc. of the components of all connected component types
	updateConnectedComponents();
}


//------------------------------------------------------------
// Optional agent type attribute setter
//------------------------------------------------------------


void CAgentType::setHasCapacity( bool hasCapacity )
{
	this->hasCapacity = hasCapacity;
	capacity = 1;

	// Resize component type
	resize();		
}


void CAgentType::setCapacity( int capacity )
{
	this->capacity = capacity;
}


void CAgentType::setHasSize( bool hasSize )
{
	this->hasSize = hasSize;
	size = 1;

	// Resize component type
	resize();		
}


void CAgentType::setSize( int size )
{
	this->size = size;
}


void CAgentType::setHasSpeed( bool hasSpeed )
{
	this->hasSpeed = hasSpeed;
	speed = 1;

	// Resize component type
	resize();		
}


void CAgentType::setSpeed( int speed )
{
	this->speed = speed;
}


//------------------------------------------------------------
// Clean up
//------------------------------------------------------------


void CAgentType::deleteAgents()
{
	vector<CAgent*>::iterator itAgent;
	for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
		delete( *itAgent );
	
	// Clear the agent list for this agent type
	agents.clear();
}


//------------------------------------------------------------
// Helper
//------------------------------------------------------------


void CAgentType::updateComponents()
{
	// THIS IS SOME KIND OF DUMMY IMPLEMENTATION! 
	// NORMALLY IT SHOULD BE CHECKED HERE IF SOMETHING AND WHAT EXAKTLY HAS CHANGED
	// INSTEAD OF REINITIALIZING ALL COMPONENTS!
	// ALTERNATIVELY ALL CALLS TO THIS METHOD SHOULD BE REPLACED BY THE CORRESPONDING
	// BEHAVIOR THAT HAS TO BE EXECUTED TO UPDATE THE COMPONENTS THROUGH THE SPECIFIC CHANGE
	// (IN THE LATTER CASE MAY BE THIS METHOD COULD BE PARAMETERIZED WITH THE SPECIFIC CHANGE 
	// AND THE METHOD SHOULD BE CALLED THEN EXPLICITLY EVEN IN THE HAS-ATTRIBUTE-METHODS)

	vector<CAgent*>::iterator itAgent;
	for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
		(*itAgent)->initialize();
}
