/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined EditorObj_h
	#define EditorObj_h





class CEditorObj
{
	//friend method MLoadXMLFile (not implemented yet in C-mol Compiler :( )

	public:
		
		// Construction
		CEditorObj();

		// Destruction
		virtual ~CEditorObj();

		// Getter
		int getId() const;
		bool getIsSelected() const;

		// Setter
		void setIsSelected( bool isSelected );

		// Setter (should only be set by method MLoadXmlFile hierarchy in case of loading a file)
		void setId( int id );

	private:	
		
		// Class attributes
		static int nextId;
		
		// Attributes
		int id;
		bool isSelected;
};





#endif
