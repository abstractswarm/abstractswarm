/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined CheckPosition_h
	#define CheckPosition_h

#include <list>





using namespace std;





class CGraphObj;
class CPerspective;
class CStationType;
class CAgentType;





//method MCheckPosition( list<CGraphObj*>& objectsToCheck, int maxWidth, int maxHeight, int newX, int newY)
class MCheckPosition
{
#define METHOD_PARAMS_CHECKPOSITION list<CGraphObj*>& objectsToCheck, int maxWidth, int maxHeight, int newX, int newY

public:

    // Prologue
//	MCheckPosition();
    static void prologue( CGraphObj& that, METHOD_PARAMS_CHECKPOSITION );

	// Handlings
//	bool <CPerspective>();
    static bool handling( CPerspective& that, METHOD_PARAMS_CHECKPOSITION );
//	bool <CStationType, CAgentType>();
    static bool handling( CStationType& that, METHOD_PARAMS_CHECKPOSITION );
    static bool handling( CAgentType& that, METHOD_PARAMS_CHECKPOSITION );
};





#endif
