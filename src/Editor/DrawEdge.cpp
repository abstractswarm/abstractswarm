/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Constants.h"

// Needed for platform dependent(!) visualization
#include "windows.h"

#include "VisitEdge.h"
#include "PlaceEdge.h"
#include "TimeEdge.h"
#include <sstream>
#include <cmath>
#include <typeinfo>
//#pragma c-mol_hdrstop

#include "DrawEdge.hpp"
#include "CalcEdgeDrawOffset.hpp"
#include "GetEdgeLabel.hpp"





using namespace std;





//------------------------------------------------------------
// Data
//------------------------------------------------------------


int zoomedX2 = 0;
int zoomedY2 = 0;

int xOffset1 = 0;
int yOffset1 = 0;

int xOffset2 = 0;
int yOffset2 = 0;

double edgeAngle = 0;





//------------------------------------------------------------
// Prologue
//------------------------------------------------------------


//MDrawEdge::MDrawEdge()
void MDrawEdge::prologue( CEdge* that, METHOD_PARAMS_DRAWEDGE )
{
    zoomedX2 = static_cast<int>( screenX+(that->getX2()-that->getX1())*GRID_XSIZE*zoomFactor );
    zoomedY2 = static_cast<int>( screenY+(that->getY2()-that->getY1())*GRID_YSIZE*zoomFactor );

	// Calculate the draw offset needed for non overlapping edges for first connected component type
	xOffset1 = 0;
	yOffset1 = 0;
	bool isConnected1 = that->getConnector1().getIsConnected();
	if( isConnected1 )
	{
		const CComponentType* pComponentType1 = &(that->getConnector1().getComponentType());
//		MCalcEdgeDrawOffset( zoomedX2-screenX, zoomedY2-screenY, zoomFactor, xOffset1, yOffset1 )°(const_cast<CComponentType*>( pComponentType1 ))();
        MCalcEdgeDrawOffset::handling( const_cast<CComponentType*>( pComponentType1 ), zoomedX2-screenX, zoomedY2-screenY, zoomFactor, xOffset1, yOffset1 );
    }
	
	// Calculate the draw offset needed for non overlapping edges for second connected component type
	xOffset2 = 0;
	yOffset2 = 0;
	bool isConnected2 = that->getConnector2().getIsConnected();
	if( isConnected2 )
	{
		const CComponentType* pComponentType2 = &(that->getConnector2().getComponentType());
//		MCalcEdgeDrawOffset( (screenX-zoomedX2), (screenY-zoomedY2), zoomFactor, xOffset2, yOffset2 )°(const_cast<CComponentType*>( pComponentType2 ))();
        MCalcEdgeDrawOffset::handling( const_cast<CComponentType*>( pComponentType2 ), (screenX-zoomedX2), (screenY-zoomedY2), zoomFactor, xOffset2, yOffset2 );
    }
}


//------------------------------------------------------------
// Epilogue
//------------------------------------------------------------


//MDrawEdge::~MDrawEdge()
void MDrawEdge::epilogue( CEdge* that, METHOD_PARAMS_DRAWEDGE )
{
	// Calculate edge angle
	double aLeg = zoomedX2-screenX;
	double oLeg = zoomedY2-screenY;

	// Calculate the angle of edge due relative to horizontal base line
	edgeAngle = atan2( oLeg, aLeg );
	if( (fabs( edgeAngle ) > (PI/2.0)) )
		edgeAngle += PI;

	// Modify current selected font with angle
	HFONT hCurFont;
    hCurFont = static_cast<HFONT>( GetCurrentObject( graphicContext, OBJ_FONT ) );
	LOGFONT logFont;
	GetObject( hCurFont, sizeof( LOGFONT ), &logFont );
    logFont.lfEscapement = static_cast<long>( -((edgeAngle*180)/PI)*10 );  // angle of escapement

	// Create an select new modified font
	HFONT hFont = CreateFontIndirect( &logFont );
    HFONT hFontOld = static_cast<HFONT>( SelectObject( graphicContext, hFont ) );

	// Draw label text
//	string sEdgeLabel = MGetEdgeLabel()°that();
    string sEdgeLabel = MGetEdgeLabel::handling( that );
    wchar_t wcEdgeLabel[ 512 ];  // NOTE THAT THE Edge LABEL IS NOT ALLLOWED TO BE LONGER THAN 512
    mbstowcs( wcEdgeLabel, sEdgeLabel.c_str(), 512 );

    // WORKAROUND: This fixes an encoding error for the edge labels (e. g., Â«visitÂ» to «visit») without changing the lenght of the string (which would cause further code pollution)
    wcEdgeLabel[ 0 ] = ' ';
    wcEdgeLabel[ sEdgeLabel.length() - 2 ] = wcEdgeLabel[ sEdgeLabel.length() - 1 ];
    wcEdgeLabel[ sEdgeLabel.length() - 1 ] = ' ';

	// Calculate egde label size on screen
	SIZE labelSize;
    GetTextExtentPoint32( graphicContext, wcEdgeLabel, static_cast<int>( sEdgeLabel.length() ), &labelSize );

	// Calculate egde label text offset on screen
	double hyp1 = -(EDGE_DIAMONDSIZE/2.0)*zoomFactor+(labelSize.cy/4.0);
	double aLeg1 = cos( -edgeAngle )*hyp1;
	double oLeg1 = sin( -edgeAngle )*hyp1;

	double hyp2 = (labelSize.cx/2.0);
	double aLeg2 = cos( -edgeAngle )*hyp2;
	double oLeg2 = sin( -edgeAngle )*hyp2;

	double textDeltaX = aLeg2+oLeg1;
	double textDeltaY = aLeg1-oLeg2;

	// Draw the label Text
	if( that->getIsLabelVisible() )
        TextOut( graphicContext, static_cast<int>( screenX+(aLeg/2.0)-textDeltaX ), static_cast<int>( screenY+(oLeg/2.0)-textDeltaY ), wcEdgeLabel, static_cast<int>( sEdgeLabel.length() ) );

	// Release new modified font
	SelectObject( graphicContext, hFontOld );
	DeleteObject( hFont );
}


//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MDrawEdge::<CVisitEdge*>()
void MDrawEdge::handling( CVisitEdge* that, METHOD_PARAMS_DRAWEDGE )
{
    prologue( that, graphicContext, screenX, screenY, zoomFactor );

    HPEN hPen = nullptr;
    HPEN hPenOld = nullptr;

	// If is bold visit edge, modify pen
	if( that->getIsBold() )
	{
		// Modify current selected pen by thickness
		HPEN hCurPen;
        hCurPen = static_cast<HPEN>( GetCurrentObject( graphicContext, OBJ_PEN ) );
		LOGPEN logPen;
		GetObject( hCurPen, sizeof( LOGPEN ), &logPen );
		logPen.lopnWidth.x = 3;

		hPen = CreatePenIndirect( &logPen );
        hPenOld = static_cast<HPEN>( SelectObject( graphicContext, hPen ) );
	}
	
	// Draw line
    MoveToEx( graphicContext, screenX+xOffset1, screenY+yOffset1, nullptr );
	LineTo( graphicContext, zoomedX2+xOffset2, zoomedY2+yOffset2 );

	// If is bold visit edge, release the modified pen
	if( that->getIsBold() )
	{
		// Release pen
		SelectObject( graphicContext, hPenOld );
		DeleteObject( hPen );
	}

    epilogue( that, graphicContext, screenX, screenY, zoomFactor );
}


