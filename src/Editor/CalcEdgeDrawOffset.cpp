/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------

#include "Constants.h"
#include "StationType.h"
#include "AgentType.h"
#include <math.h>
#include <typeinfo>
//#pragma c-mol_hdrstop

#include "CalcEdgeDrawOffset.hpp"





//------------------------------------------------------------
// Static data
//------------------------------------------------------------


static double zoomedWidth;
static double zoomedHeight;

static const double accuracy = 100.0;

static const double stepX = 1/accuracy;  // Defines the horizontal step size
static double stepY = 0;                 // Defines the vertical gradient (will be calculated in prologue)





//------------------------------------------------------------
// Prologue
//------------------------------------------------------------


//MCalcEdgeDrawOffset::MCalcEdgeDrawOffset()
void MCalcEdgeDrawOffset::prologue( CComponentType* that, METHOD_PARAM_CALCEDGEDRAWOFFSET )
{
	// Calculate zoomed width and height of component type
	zoomedWidth = COMPONENTTYPE_WIDTH*GRID_XSIZE*zoomFactor;
	zoomedHeight = COMPONENTTYPE_HEIGHT*GRID_YSIZE*zoomFactor;

	// Calculate gradient
	
	// If there is no horizontal difference between component types, augmentation would be infinite
	// => only set the vertical offset and leave! 
	if( edgeRelX == 0 )
	{
		xOffset = 0;
		
		// Set the vertical offset, depending on if it's positive or negative
		if( edgeRelY < 0 )
            yOffset = static_cast<int>( -(zoomedHeight/2.0) );
		else
            yOffset = static_cast<int>(  zoomedHeight/2.0 );
		
		return;
	}
	else
		stepY = (static_cast<double>( edgeRelY )/edgeRelX)/accuracy;
}


//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MCalcEdgeDrawOffset::<CStationType*>()
void MCalcEdgeDrawOffset::handling( CStationType* that, METHOD_PARAM_CALCEDGEDRAWOFFSET )
{
    prologue( that, edgeRelX, edgeRelY, zoomFactor, xOffset, yOffset );

	// Init walk variables
	double curX = 0;
	double curY = 0;

	// If connected to left-bottom, walk to left-bottom
	if( (edgeRelX < 0) && (edgeRelY <= 0) )
	{
		// As long as x and y are inside the station type rectangle (starting from the middle)
		while( (curX > -(zoomedWidth/2.0)) && (curY < zoomedHeight/2.0) )
		{
			curX = curX-stepX;
			curY += stepY;
		}

		// Set the calculated offset
        xOffset = static_cast<int>( curX );
        yOffset = static_cast<int>( -curY );
	}

	// Else if connected to left-top, walk to left-top
	else if( (edgeRelX < 0) && (edgeRelY >= 0) )
	{
		// As long as x and y are inside the station type rectangle (starting from the middle)
		while( (curX > -(zoomedWidth/2.0)) && (curY > -(zoomedHeight/2.0)) )
		{
			curX = curX-stepX;
			curY += stepY;
		}

		// Set the calculated offset
        xOffset = static_cast<int>( curX );
        yOffset = static_cast<int>( -curY );
	}

	// Else if connected to right-bottom, walk to right-bottom
	else if( (edgeRelX > 0) && (edgeRelY >= 0) )
	{
		// As long as x and y are inside the station type rectangle (starting from the middle)
		while( (curX < zoomedWidth/2.0) && (curY < zoomedHeight/2.0) )
		{
			curX += stepX;
			curY += stepY;
		}

		// Set the calculated offset
        xOffset = static_cast<int>( curX );
        yOffset = static_cast<int>( curY );
	}

	// Else if connected to right-top, walk to right-top
	else if( (edgeRelX > 0) && (edgeRelY <= 0) )
	{
		// As long as x and y are inside the station type rectangle (starting from the middle)
		while( (curX < zoomedWidth/2.0) && (curY > -(zoomedHeight/2.0)) )
		{
			curX += stepX;
			curY += stepY;
		}

		// Set the calculated offset
        xOffset = static_cast<int>( curX );
        yOffset = static_cast<int>( curY );
	}
}


//void MCalcEdgeDrawOffset::<CAgentType*>()
void MCalcEdgeDrawOffset::handling( CAgentType* that, METHOD_PARAM_CALCEDGEDRAWOFFSET )
{
    prologue( that, edgeRelX, edgeRelY, zoomFactor, xOffset, yOffset );

	// Init walk variables
	double curX = 0;
	double curY = 0;

	// If connected to left-bottom, walk to left-bottom
	if( (edgeRelX < 0) && (edgeRelY <= 0) )
	{
		// As long as x and y are inside the agent type circle (starting from the middle)
		double curRadius = 0;
		while( curRadius < (zoomedWidth/2.0) )
		{
			curX = curX-stepX;
			curY += stepY;

			curRadius = sqrt( (curX*curX)+(curY*curY) );
		}

		// Set the calculated offset
        xOffset = static_cast<int>( curX );
        yOffset = static_cast<int>( -curY );
	}

	// Else if connected to left-top, walk to left-top
	else if( (edgeRelX < 0) && (edgeRelY >= 0) )
	{
		// As long as x and y are inside the agent type circle (starting from the middle)
		double curRadius = 0;
		while( curRadius < (zoomedWidth/2.0) )
		{
			curX = curX-stepX;
			curY += stepY;

			curRadius = sqrt( (curX*curX)+(curY*curY) );
		}

		// Set the calculated offset
        xOffset = static_cast<int>( curX );
        yOffset = static_cast<int>( -curY );
	}

	// Else if connected to right-bottom, walk to right-bottom
	else if( (edgeRelX > 0) && (edgeRelY >= 0) )
	{
		// As long as x and y are inside the agent type circle (starting from the middle)
		double curRadius = 0;
		while( curRadius < (zoomedWidth/2.0) )
		{
			curX += stepX;
			curY += stepY;

			curRadius = sqrt( (curX*curX)+(curY*curY) );
		}

		// Set the calculated offset
        xOffset = static_cast<int>( curX );
        yOffset = static_cast<int>( curY );
	}

	// Else if connected to right-top, walk to right-top
	else if( (edgeRelX > 0) && (edgeRelY <= 0) )
	{
		// As long as x and y are inside the agent type circle (starting from the middle)
		double curRadius = 0;
		while( curRadius < (zoomedWidth/2.0) )
		{
			curX += stepX;
			curY += stepY;

			curRadius = sqrt( (curX*curX)+(curY*curY) );
		}

		// Set the calculated offset
        xOffset = static_cast<int>( curX );
        yOffset = static_cast<int>( curY );
	}
}
