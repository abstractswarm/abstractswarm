/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "EdgeConnector.h"
#include "ComponentType.h"
#include "Edge.h"
#include <list>





#if !defined NULL
	#define NULL 0
#endif





using namespace std;





//------------------------------------------------------------
// Construction
//------------------------------------------------------------


CEdgeConnector::CEdgeConnector()  
{
    pComponentType = nullptr;
    pEdge = nullptr;

	isDirected = false;
	isAndConnection = false;
}


//------------------------------------------------------------
// Destruction
//------------------------------------------------------------


CEdgeConnector::~CEdgeConnector()  
{
	// If Connector is connected to a component type, remove it from component type's edge connector list
    if( pComponentType != nullptr )
		pComponentType->removeEdgeConnector( *this );
}


//------------------------------------------------------------
// Getter
//------------------------------------------------------------


bool CEdgeConnector::getIsConnected() const
{
	bool isConnected = false;

    if( pComponentType == nullptr )
		isConnected = false;
	else
		isConnected = true;

	return( isConnected );
}


bool CEdgeConnector::getIsDirected() const
{
	return( isDirected );
}


bool CEdgeConnector::getIsAndConnection() const
{
	return( isAndConnection );
}


const CComponentType& CEdgeConnector::getComponentType() const
{
	return( *pComponentType );
}


const CEdge& CEdgeConnector::getEdge() const
{
	return( *pEdge );
}


const CEdgeConnector& CEdgeConnector::getLinkedConnector() const
{
	CEdgeConnector& connector1 = getEdge().getConnector1();
	CEdgeConnector& connector2 = getEdge().getConnector2();

	// Check if this connector is the first one or the second of the edge
	if( this == &connector1 )
		return( connector2 );
	else
		return( connector1 );
}


//------------------------------------------------------------
// Setter
//------------------------------------------------------------


void CEdgeConnector::setIsDirected( bool isDirected )
{
	// Being directed and and-connection on opposite side doesn't make sense
	if( isDirected )
	{
		if( getLinkedConnector().getIsConnected() )
			const_cast<CEdgeConnector&>( getLinkedConnector() ).setIsAndConnection( false );
	}
	
	this->isDirected = isDirected;
}


void CEdgeConnector::setIsAndConnection( bool isAndConnection )
{
	// Being directed and and-connection on opposite side doesn't make sense
	if( isAndConnection )
	{
		if( getLinkedConnector().getIsConnected() )
			const_cast<CEdgeConnector&>( getLinkedConnector() ).setIsDirected( false );
	}
	
	this->isAndConnection = isAndConnection;
}
