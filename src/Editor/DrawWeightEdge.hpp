/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined DrawWeightEdge_hmol
	#define DrawWeightEdge_hmol


#include <typeinfo>
#include "windows.h"
#include "PlaceEdge.h"
#include "TimeEdge.h"





class CAgent;





//method MDrawWeightEdge( HDC graphicContext, int screenX, int screenY, double zoomFactor )
class MDrawWeightEdge
{
#define METHOD_PARAMS_DRAWWEIGHTEDGE HDC graphicContext, int screenX, int screenY, double zoomFactor

public:

    // Prologue
//	MDrawWeightEdge();
    static void prologue( CWeightEdge* that, METHOD_PARAMS_DRAWWEIGHTEDGE );

    // Epilogue
//	~MDrawWeightEdge();
    static void epilogue( CWeightEdge* that, METHOD_PARAMS_DRAWWEIGHTEDGE );


    //
	// Handlings
    //

    /**
     * @brief Draws either a place edge OR an agent moving along that place edge (if provided).
     *        (Agents moving along the place edge are drawn here to be able to reuse the edge's geometry calculation.)
     *
     * @param pAgentOnEdge           the agent to be drawn; nullptr if no agent is provided
     * @param pTargetEdgeConnector   the edge connector for the agents next target component type that is traversed (not necessarily the agent's final target); nullptr if no edge connector is provided
     * @param coveredDistanceOnEdge  the agent's distance currently covered on its current edge (that is connected to the provided edge connector); 0 if no covered distance is provided
     */
//	virtual void <CPlaceEdge*>();
    static void handling( CPlaceEdge* that, METHOD_PARAMS_DRAWWEIGHTEDGE, const CAgent* pAgentOnEdge = nullptr, const CEdgeConnector* pTargetEdgeConnector = nullptr, int coveredDistanceOnEdge = 0 );

//    virtual void <CTimeEdge*>();
    static void handling( CTimeEdge* that, METHOD_PARAMS_DRAWWEIGHTEDGE );

    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline void handling( void* vp_that, METHOD_PARAMS_DRAWWEIGHTEDGE )
    {
        if( typeid( *(static_cast<CPlaceEdge*>( vp_that ))) == typeid( CPlaceEdge ) )
            handling( static_cast<CPlaceEdge*>( vp_that ), graphicContext, screenX, screenY, zoomFactor );
        else if( typeid( *(static_cast<CTimeEdge*>( vp_that ))) == typeid( CTimeEdge ) )
            handling( static_cast<CTimeEdge*>( vp_that ), graphicContext, screenX, screenY, zoomFactor );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }
};





#endif
