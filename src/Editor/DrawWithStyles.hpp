/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined DrawWithStyles_hmol
	#define DrawWithStyles_hmol





#include "DrawComponentType.hpp"
#include "DrawEdge.hpp"





class CPerspective;
class CGrid;
class CColor;





//method MDrawWithStyles( HDC graphicContext,
//					    int screenX,
//						int screenY,
//						double zoomFactor,
//						const CColor& penColor,
//						const CColor& brushColor,
//						bool isSelected ) : MDrawComponentType( HDC graphicContext,
//						                                        int screenX,
//													 		    int screenY,
//															    double zoomFactor ),
//										    MDrawEdge( HDC graphicContext,
//						                               int screenX,
//										               int screenY,
//												  	   double zoomFactor )
class MDrawWithStyles : public MDrawComponentType, public MDrawEdge
{
#define METHOD_PARAMS_DRAWWITHSTYLES HDC graphicContext, int screenX, int screenY, double zoomFactor, const CColor& penColor, const CColor& brushColor, bool isSelected

public:

	// Prologue
//	MDrawWithStyles();
    static void prologue( CEditorObj* that, METHOD_PARAMS_DRAWWITHSTYLES );
    static void prologue( CGrid& that, METHOD_PARAMS_DRAWWITHSTYLES );

	// Epilogue
//	~MDrawWithStyles();
    static void epilogue( CEditorObj* that, METHOD_PARAMS_DRAWWITHSTYLES );
    static void epilogue( CGrid& that, METHOD_PARAMS_DRAWWITHSTYLES );

	// Handlings
//	void <CPerspective>();
    static void handling( CPerspective& that, METHOD_PARAMS_DRAWWITHSTYLES );

//	void <CGrid>( int width, int height );
    static void handling( CGrid& that, METHOD_PARAMS_DRAWWITHSTYLES, int width, int height );


    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline void handling( void* vp_that, METHOD_PARAMS_DRAWWITHSTYLES )
    {
        if( typeid( *(static_cast<CAgentType*>( vp_that ))) == typeid( CAgentType ) )
            handling( static_cast<CAgentType*>( vp_that ), graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );
        else if( typeid( *(static_cast<CStationType*>( vp_that ))) == typeid( CStationType ) )
            handling( static_cast<CStationType*>( vp_that ), graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );
        else if( typeid( *(static_cast<CVisitEdge*>( vp_that ))) == typeid( CVisitEdge ) )
            handling( static_cast<CVisitEdge*>( vp_that ), graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );
        else if( typeid( *(static_cast<CPlaceEdge*>( vp_that ))) == typeid( CPlaceEdge ) )
            handling( static_cast<CPlaceEdge*>( vp_that ), graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );
        else if( typeid( *(static_cast<CTimeEdge*>( vp_that ))) == typeid( CTimeEdge ) )
            handling( static_cast<CTimeEdge*>( vp_that ), graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }


    //
    // C-MOL INHERITANCE MECHANISM
    // (DO NOT FORGET TO OVERWRITE ALL INHERITED HANDLINGS HERE FOR SUPER-PROLOGUE/-EPILOGUE CALLS!)
    //

    static inline void handling( CStationType* that, METHOD_PARAMS_DRAWWITHSTYLES )
    {
        prologue( that, graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );
        MDrawComponentType::handling( that, graphicContext, screenX, screenY, zoomFactor );
        epilogue( that, graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );
    }

    static inline void handling( CAgentType* that, METHOD_PARAMS_DRAWWITHSTYLES )
    {
        prologue( that, graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );
        MDrawComponentType::handling( that, graphicContext, screenX, screenY, zoomFactor );
        epilogue( that, graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );
    }

    static inline void handling( CVisitEdge* that, METHOD_PARAMS_DRAWWITHSTYLES )
    {
        prologue( that, graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );
        MDrawEdge::handling( that, graphicContext, screenX, screenY, zoomFactor );
        epilogue( that, graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );
    }

    static inline void handling( CPlaceEdge* that, METHOD_PARAMS_DRAWWITHSTYLES, const CAgent* pAgentOnEdge = nullptr, const CEdgeConnector* pTargetEdgeConnector = nullptr, int coveredDistanceOnEdge = 0 )
    {
        prologue( that, graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );
        MDrawEdge::handling( that, graphicContext, screenX, screenY, zoomFactor, pAgentOnEdge, pTargetEdgeConnector, coveredDistanceOnEdge );
        epilogue( that, graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );
    }

    static inline void handling( CTimeEdge* that, METHOD_PARAMS_DRAWWITHSTYLES )
    {
        prologue( that, graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );
        MDrawEdge::handling( that, graphicContext, screenX, screenY, zoomFactor );
        epilogue( that, graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );
    }
};





#endif
