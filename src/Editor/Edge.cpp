/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Edge.h"
#include "ComponentType.h"
#include "Perspective.h"
#include "Constants.h"





#if !defined NULL
	#define NULL 0
#endif





//------------------------------------------------------------
// Construction
//------------------------------------------------------------


CEdge::CEdge()
{
	// Set this edge as owner of these connectors
	connector1.pEdge = this;
	connector2.pEdge = this;
	
	this->tempX = 0;
	this->tempY = 0;

	// Default is a visible label
	isLabelVisible = true;
}


//------------------------------------------------------------
// Destruction
//------------------------------------------------------------


CEdge::~CEdge()
{
}


//------------------------------------------------------------
// Getter
//------------------------------------------------------------


int CEdge::getX1() const
{
	// Return connected component type's position, or a temporary drag position if not connected
    if( connector1.pComponentType != nullptr )
	{
		int perspectiveX = connector1.pComponentType->getOwner().getX();
		int componentTypeX = connector1.pComponentType->getX();
		bool hasAttribs = connector1.pComponentType->getHasAttribs(); 
		int offset = static_cast<int>( connector1.pComponentType->getWidth()-(hasAttribs ? 1 : 0)-(COMPONENTTYPE_WIDTH/2.0) );
		return( perspectiveX + componentTypeX + offset );
	}
	else
		return( tempX );
}


int CEdge::getY1() const
{
	// Return connected component type's position, or a temporary drag position if not connected
    if( connector1.pComponentType != nullptr )
	{
		int perspectiveY = connector1.pComponentType->getOwner().getY();
		int componentTypeY = connector1.pComponentType->getY();
		int offset = static_cast<int>( connector1.pComponentType->getHeight()-(COMPONENTTYPE_HEIGHT/2.0) );
		return( perspectiveY + componentTypeY + offset );
	}
	else
		return( tempY );
}


int CEdge::getX2() const
{
	// Return connected component type's position, or a temporary drag position if not connected
    if( connector2.pComponentType != nullptr )
	{
		int perspectiveX = connector2.pComponentType->getOwner().getX();
		int componentTypeX = connector2.pComponentType->getX();
		bool hasAttribs = connector2.pComponentType->getHasAttribs();
		int offset = static_cast<int>( connector2.pComponentType->getWidth()-(hasAttribs ? 1 : 0)-(COMPONENTTYPE_WIDTH/2.0) );
		return( perspectiveX + componentTypeX + offset );
	}
	else
		return( tempX );
}


int CEdge::getY2() const
{
	// Return connected component type's position, or a temporary drag position if not connected
    if( connector2.pComponentType != nullptr )
	{
		int perspectiveY = connector2.pComponentType->getOwner().getY();
		int componentTypeY = connector2.pComponentType->getY();
		int offset = static_cast<int>( connector2.pComponentType->getHeight()-(COMPONENTTYPE_HEIGHT/2.0) );
		return( perspectiveY + componentTypeY + offset );
	}
	else
		return( tempY );
}


CEdgeConnector& CEdge::getConnector1() const
{
	return( const_cast<CEdgeConnector&>( connector1 ) );
}


CEdgeConnector& CEdge::getConnector2() const
{
	return( const_cast<CEdgeConnector&>( connector2 ) );
}


CEdgeConnector& CEdge::getFreeConnector() const
{
    if( connector1.pComponentType == nullptr )
		return( const_cast<CEdgeConnector&>( connector1 ) );
    else if( connector2.pComponentType == nullptr )
		return( const_cast<CEdgeConnector&>( connector2 ) );
	else
		throw( "CEdge: No free connector!" );
}


CEdgeConnector& CEdge::getConnectedConnector() const
{
    if( connector1.pComponentType != nullptr )
		return( const_cast<CEdgeConnector&>( connector1 ) );
    else if( connector2.pComponentType != nullptr )
		return( const_cast<CEdgeConnector&>( connector2 ) );
	else
		throw( "CEdge: No connected connector!" );
}


bool CEdge::getIsLabelVisible() const
{
	return( isLabelVisible );
}


//------------------------------------------------------------
// Setter
//------------------------------------------------------------


void CEdge::setTempX( int tempX )
{
	this->tempX = tempX;
}


void CEdge::setTempY( int tempY )
{
	this->tempY = tempY;
}


void CEdge::setIsLabelVisible( bool isVisible )
{
	isLabelVisible = isVisible;
}
