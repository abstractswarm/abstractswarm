/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Constants.h"
#include "Perspective.h"
#include "StationType.h"
#include "AgentType.h"
#include <list>
#include <typeinfo>
//#pragma c-mol_hdrstop

//#include "DragGraphObj.hmol"
#include "DragGraphObj.hpp"





using namespace std; 





//------------------------------------------------------------
// Epilogue
//------------------------------------------------------------


//MDragGraphObj::~MDragGraphObj()
void MDragGraphObj::epilogue( CGraphObj* that, METHOD_PARAMS_DRAGGRAPHOBJ )
{
	// If no side is dragged, drag the whole object
	if( !(isDraggedLeft || isDraggedTop || isDraggedRight || isDraggedBottom) )
	{
		int x = that->getX();
		that->setX( x + dragAmountX );
		
		int y = that->getY();
		that->setY( y + dragAmountY );
	}
}


//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MDragGraphObj::<CStationType*, CAgentType*>()
void MDragGraphObj::handling( CStationType* that, METHOD_PARAMS_DRAGGRAPHOBJ )
{
    epilogue( that, dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
}
void MDragGraphObj::handling( CAgentType* that, METHOD_PARAMS_DRAGGRAPHOBJ )
{
    epilogue( that, dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
}


//void MDragGraphObj::<CPerspective*>()
void MDragGraphObj::handling( CPerspective* that, METHOD_PARAMS_DRAGGRAPHOBJ )
{
	// Drag behavior, dependant on which side is dragged
	if( isDraggedRight )
	{
		int width = that->getWidth();
		that->setWidth( width + dragAmountX );
	}
	if( isDraggedBottom )
	{
		int height = that->getHeight();
		that->setHeight( height + dragAmountY );
	}
	if( isDraggedLeft )
	{
		int x = that->getX();
		that->setLeft( x + dragAmountX );
    }
	if( isDraggedTop )
	{

		int y = that->getY();
		that->setTop( y + dragAmountY );
    }

	// Swap dragged sides if frame size gets smaller than width/height
	int width = that->getWidth();
	if( width < 0 )
	{
		if( isDraggedRight )
		{
			int x = that->getX(); 
			int width = that->getWidth();
			that->setX( x + width + 1 );
			that->setWidth( -width );
		}
		else if( isDraggedLeft )
		{
			int x = that->getX(); 
			int width = that->getWidth();
			that->setX( x + width - 1 );
			that->setWidth( -width );
		}

		bool swapDummy = isDraggedRight;
		isDraggedRight = isDraggedLeft;
		isDraggedLeft = swapDummy;
	}

	int height = that->getHeight();
	if( height < 0 )
	{
		if( isDraggedBottom )
		{
			int y = that->getY(); 
			int height = that->getHeight();
			that->setY( y + height + 1 );
			that->setHeight( -height );
		}
		else if( isDraggedTop )
		{
			int y = that->getY(); 
			int height = that->getHeight();
			that->setY( y + height - 1 );
			that->setHeight( -height );
		}

		bool swapDummy = isDraggedBottom;
		isDraggedBottom = isDraggedTop;
		isDraggedTop = swapDummy;
	}

    epilogue( that, dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
}
