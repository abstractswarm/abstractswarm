/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined Editor_h
	#define Editor_h





// Needed for platform dependent(!) visualization
#include "windows.h"

#include "EditorTypes.h"
#include "EditorObj.h"
#include "GraphObj.h"
#include "Perspective.h"
#include "AgentType.h"
#include "StationType.h"
#include "Edge.h"
#include "Grid.h"





using namespace std;





class CEditor 
{
	public: 

		// Construction
		CEditor();

		// Destruction
		virtual ~CEditor();

        // Events
        ENEditorMessage click();
        void doubleClick();
        void mouseDown( ENMouseButton mouseButton );
        void mouseMove( int x, int y );
        ENEditorMessage mouseUp( ENMouseButton mouseButton );
        void keyPress( char key );
        void keyDown( ENKey key );
        void keyUp( ENKey key );

//		// Layout independant functionality and state setters
        void clear();
        bool load( char* pFileName );
        void save( char* pFileName );
        void setSelectedTool( ENTool tool );
        void setScrollX( int percentScrollX );
        void setScrollY( int percentScrollY );
        double setZoom( double zoomFactor, int& percentScrollX, int& percentScrollY );
        void setVisibleFrameSize( int width, int height );
        void scrollToSceneMiddle();

//		// UI data transfer and advanced communication
        ENEditorObjType getSelectedObjData( UEditorObjData selectedObjData[] ) const;
        void setSelectedObjResults(  UEditorObjData selectedObjData[] );

//		/**
//		 * Connects the editor to the simulation module by storing the given function pointers.
//		 * <p>
//		 * Every time a component is created or removed through the editor, this is done in the
//		 * simulation as well through the given functions.
//		 *
//		 * @param fpAddStationToSimulation        pointer to the function that adds a station to the simulation
//		 * @param fpRemoveStationFromSimulation   pointer to the function that removes a station from the simulation
//		 * @param fpAddAgentToSimulation          pointer to the function that adds an agent to the simulation
//		 * @param fpRemoveAgentFromSimulation     pointer to the function that removes an agent from the simulation
//		 */
        void connectToSimulation( TfpAddStationToSimulation fpAddStationToSimulation, TfpRemoveStationFromSimulation fpRemoveStationFromSimulation, TfpAddAgentToSimulation fpAddAgentToSimulation, TfpRemoveAgentFromSimulation fpRemoveAgentFromSimulation );


        // HELPER

        // Resolv an id to an object (needed by load module)
        CEditorObj* getEditorObjFromId( int id );
		
		
		// Platform dependent(!) interface for visualization
		void draw( HDC graphicContext );

		
		// Containees
		list<CPerspective*> perspectives;
		list<CEdge*> edges;

	private:

		// Attributes
		int curMouseX;
		int curMouseY;
		int curSelectedTool;
		int curScreenX;
		int curScreenY;
		double curZoomFactor;
		int visibleFrameWidth;
		int visibleFrameHeight;

		// Drag behavior attributes
		bool isDraggedLeft; 
		bool isDraggedTop; 
		bool isDraggedRight; 
		bool isDraggedBottom; 
		list<CEditorObj*> draggedObjects;
		const void* pDraggedObjectsOwner;  // The object where the currently dragged objects belong to

		// Selected objects
		list<CEditorObj*> selectedObjects;
		
		// Mouse and key handling attributes
		list<ENMouseButton> mouseButtonsDown;
		list<ENKey> keysDown;

		// Containee
		CGrid grid;

		/** The function pointer to the function that adds a station to the simulation. */
		TfpAddStationToSimulation fpAddStationToSimulation;

		/** The function pointer to the function that removes a station from the simulation. */
		TfpRemoveStationFromSimulation fpRemoveStationFromSimulation;

		/** The function pointer to the function that adds an agent to the simulation. */
		TfpAddAgentToSimulation fpAddAgentToSimulation;

		/** The function pointer to the function that removes an agent from the simulation. */
		TfpRemoveAgentFromSimulation fpRemoveAgentFromSimulation;


		// HELPER
		
		// Transform object positions to screen positions
		int xToScreenX( double x ) const;  // Input parameter is double for more precise scrolling behavior
		int yToScreenY( double y ) const;  // Input parameter is double for more precise scrolling behavior
		double screenXToX( int x ) const;  // Return type is double for more precise scrolling behavior
		double screenYToY( int y ) const;  // Return type is double for more precise scrolling behavior

		// Get object located on the given screen coordinates and where the object is hit, returns NULL if nothing is hit 
		CPerspective* getPerspectiveAtScreenPos( int screenX, int screenY, bool isCheckWithFrame, const void*& pObjOwner, bool& isHitLeft, bool& isHitTop, bool& isHitRight, bool& isHitBottom ) const;
		CComponentType* getComponentTypeAtScreenPos( int screenX, int screenY, bool isCheckWithFrame, const void*& pObjOwner ) const;
		CEdge* getEdgeAtScreenPos( int screenX, int screenY, bool& isHitEnd1, bool& isHitEnd2 ) const;

		// Release all selected objects
		void releaseSelectedObjects();
};





#endif
