/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined StationType_h
	#define StationType_h

#include "ComponentType.h"
#include "EditorTypes.h"





using namespace std;





class CStationType : public CComponentType
{
	public:

		// Construction
        CStationType( const CPerspective& owner, TfpAddStationToSimulation fpAddStationToSimulation = NULL, TfpRemoveStationFromSimulation fpRemoveStationFromSimulation = nullptr );

		/**
		 * Does the clean up work for station types.
		 */
		~CStationType();

		// Getter
		int getNoOfInstances() const;
		int getNoOfComponents() const;

		/**
		 * Returns the pointer to a function to add a station to simulation.
		 *
		 * @return the pointer to a function to add a station to simulation
		 */
		TfpAddStationToSimulation getAddStationToSimulation() const;

		/**
		 * Returns the pointer to a function to remove a station to simulation.
		 *
		 * @return the pointer to a function to remove a station to simulation
		 */
		TfpRemoveStationFromSimulation getRemoveStationFromSimulation() const;

		/** 
		 * Returns all agents belonging to this agent type.
		 *
		 * @return the agents belonging to this agent type
		 */
		const vector<CStation*>& getStations() const;

		// Optional station type attribute getter
		bool getHasAttribs() const;
		int getNoOfAttribs() const;
		bool getHasItem() const;
		int getItem() const;
		bool getHasSpace() const;
		int getSpace() const;

		// Setter
		void setNoOfComponents( int noOfComponents );

		// Optional station type attribute setter
		void setHasItem( bool hasItem );
		void setItem( int item );
		void setHasSpace( bool hasSpace );
		void setSpace( int space );

		/**
		 * Updates all stations of the station type (by recreating its stations; 
		 * must be changed later to allow dynamic changes during simulation). 
		 * This method should be called everytime some of these changes are made to a 
		 * station type: An attribute is added or changed, an edge is added or changed.
		 */
		void updateComponents();

	protected:
		
		/**
		 * Deletes the stations belonging to this station type 
		 * and clears the stations list.
		 */
		void deleteStations();


	private:

		static int noOfInstances;

		/** Pointer to a function to add a station to the simulation. */
		void (*fpAddStationToSimulation)( CStation* );

		/** Pointer to a function to remove a station from the simulation. */
		void (*fpRemoveStationFromSimulation)( CStation* );

		// Containee
		vector<CStation*> stations;

		// Optional component type attributes
		bool hasItem;
		int item;
		bool hasSpace;
		int space;


		/** 
		 * Returns all stations belonging to this station type as components.
		 * <p>
		 * This method should not be used here. Use the method getStations() instead,
		 * which returns the stations belonging to this station type as stations!
		 * This method only provides the possibility to access the components 
		 * from a component type when not knowing the specific type of it.
		 *
		 * @return the components belonging to this station type as components
		 */
		const vector<CComponent*>& getComponents() const;
};	





#endif
