/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Editor.h"
#include "Constants.h"
#include "AgentType.h"
#include "StationType.h"
#include "Color.h"
#include "VisitEdge.h"
#include "TimeEdge.h"
#include "PlaceEdge.h"
#include ".//..//Simulation//Simulation.h"   // TEST: Simulation test code!
#include ".//..//Simulation//Components//Agent.hpp"   // TEST: Simulation test code!
#include ".//..//Simulation//Components//Station.hpp"   // TEST: Simulation test code!

#include <cmath>
#include <cstdlib>
#include <string>
#include <fstream>
//#pragma c-mol_hdrstop

// Platform dependant drawing interface
#include "DrawWithStyles.hpp"

//#include "Drag.hmol"
#include "Drag.hpp"
//#include "Drop.hmol"
#include "Drop.hpp"
//#include "GetEditorObjType.hmol"
#include "GetEditorObjType.hpp"
//#include "GetEditorObjData.hmol"
#include "GetEditorObjData.hpp"
//#include "SetEditorObjData.hmol"
#include "SetEditorObjData.hpp"
#include "LoadXmlFile.hpp"
//#include "WriteXmlFile.hmol"
#include "WriteXmlFile.hpp"





using namespace std;





//------------------------------------------------------------
// TEST: Simulation test code
//------------------------------------------------------------

static CSimulation* pSimulation;


//------------------------------------------------------------
// Construction
//------------------------------------------------------------


CEditor::CEditor() : grid( GRID_XSIZE, GRID_YSIZE )
{
	// Set default values for editor
	curMouseX = 0;
	curMouseY = 0;
	curSelectedTool = tlNone;
	curScreenX = 0;
	curScreenY = 0;
	curZoomFactor = 1;
	
	// Default is maximum of editor size; smaller sizes will increase runtime speed
	visibleFrameWidth = EDITOR_WIDTH*GRID_XSIZE;
	visibleFrameHeight = EDITOR_HEIGHT*GRID_YSIZE;

	// Drag behavior attributes
	isDraggedLeft = false; 
	isDraggedTop = false; 
	isDraggedRight = false; 
	isDraggedBottom = false; 

    pDraggedObjectsOwner = nullptr;

	// Initialize function pointers for connection to simulation
    fpAddStationToSimulation = nullptr;
    fpRemoveStationFromSimulation = nullptr;
    fpAddAgentToSimulation = nullptr;
    fpRemoveAgentFromSimulation = nullptr;
}





//------------------------------------------------------------
// Destruction
//------------------------------------------------------------


CEditor::~CEditor()
{
	// Delete all Perspectives
	list<CPerspective*>::iterator itPerspective;
	for( itPerspective = perspectives.begin(); itPerspective != perspectives.end(); itPerspective++ )
	{
		delete (*itPerspective);
	}

	// Delete all Edges
	list<CEdge*>::iterator itEdge;
	for( itEdge = edges.begin(); itEdge != edges.end(); itEdge++ )
	{
		delete (*itEdge);
	}
}





//------------------------------------------------------------
// Events
//------------------------------------------------------------


ENEditorMessage CEditor::click()
{
    // If station type tool or agent type tool is selected...
    if( (curSelectedTool == tlStationType) || (curSelectedTool == tlAgentType) )
    {
        // Unselect all selected objects.
        releaseSelectedObjects();

        const void* pObjOwner = nullptr;
        bool isHitLeft = false;
        bool isHitTop = false;
        bool isHitRight = false;
        bool isHitBottom = false;

        const bool isCheckWithFrame = false;
		
        CPerspective* pPerspective = getPerspectiveAtScreenPos( curMouseX, curMouseY, isCheckWithFrame, pObjOwner, isHitLeft, isHitTop, isHitRight, isHitBottom );
		
        // If no perspective is hit, then nothing to do, so leave!
        if( pPerspective == nullptr )
			
            // No message required to send back to UI
            return( emsgNone );
		
        // Create component type dependant on which is selected
        CComponentType* pComponentType;
        if( curSelectedTool == tlStationType )
            pComponentType = new CStationType( *pPerspective, fpAddStationToSimulation, fpRemoveStationFromSimulation );
        else
            pComponentType = new CAgentType( *pPerspective, fpAddAgentToSimulation, fpRemoveAgentFromSimulation );

        // Calculate component type's position relative to perspective's position
        int mouseX = static_cast<int>( screenXToX( curMouseX ) );
        int mouseY = static_cast<int>( screenYToY( curMouseY ) );
        int perspectiveX = pPerspective->getX();
        int perspectiveY = pPerspective->getY();
        pComponentType->setX( mouseX - perspectiveX );
        pComponentType->setY( mouseY - perspectiveY );
		
        // Drop component type
//        bool isEnoughSpace = MDropGraphObj( NULL, pPerspective, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom )°pComponentType();
        bool isEnoughSpace = MDropGraphObj::handling( pComponentType, nullptr, pPerspective, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );

        // Check if there was enought space to drop component type
        if( isEnoughSpace )
        {
            // Add new component type to list
            pPerspective->componentTypes.push_back( pComponentType );

            // Set new component type as selected
            pComponentType->setIsSelected( true );

            // Add new component type to selected objects
            selectedObjects.push_back( pComponentType );
		
            return( emsgNone );
        }
        else
        {
            delete pComponentType;
            return( emsgNotEnoughSpaceForComponentType );
        }
    }
    else if(    (curSelectedTool == tlPriority)
             || (curSelectedTool == tlFrequency)
             || (curSelectedTool == tlNecessity)
             || (curSelectedTool == tlTime)
             || (curSelectedTool == tlCycle)
             || (curSelectedTool == tlItem)
             || (curSelectedTool == tlSpace)
             || (curSelectedTool == tlCapacity)
             || (curSelectedTool == tlSize)
             || (curSelectedTool == tlSpeed) )
    {
        // Unselect all selected objects.
        releaseSelectedObjects();
		
        const bool isCheckWithFrame = false;
        const void* pObjOwner = nullptr;
        CComponentType* pHitComponentType = getComponentTypeAtScreenPos( curMouseX, curMouseY, isCheckWithFrame, pObjOwner );

        if( pHitComponentType != nullptr )
        {
            // Add hit component type to currently selected objects
            pHitComponentType->setIsSelected( true );
            selectedObjects.push_back( pHitComponentType );

            switch( curSelectedTool )
            {
                case tlPriority:
                    pHitComponentType->setHasPriority( true );
                    break;
                case tlFrequency:
                    pHitComponentType->setHasFrequency( true );
                    break;
                case tlNecessity:
                    pHitComponentType->setHasNecessity( true );
                    break;
                case tlTime:
                    pHitComponentType->setHasTime( true );
                    break;
                case tlCycle:
                    pHitComponentType->setHasCycle( true );
                    break;
                case tlItem:
//                    if( MGetEditorObjType()°pHitComponentType() == eotStationType )
                    if( MGetEditorObjType::handling( pHitComponentType ) == eotStationType )
                        static_cast<CStationType*>( pHitComponentType )->setHasItem( true );
                    break;
                case tlSpace:
//                    if( MGetEditorObjType()°pHitComponentType() == eotStationType )
                    if( MGetEditorObjType::handling( pHitComponentType ) == eotStationType )
                        static_cast<CStationType*>( pHitComponentType )->setHasSpace( true );
                    break;
                case tlCapacity:
//                    if( MGetEditorObjType()°pHitComponentType() == eotAgentType )
                    if( MGetEditorObjType::handling( pHitComponentType ) == eotAgentType )
                        static_cast<CAgentType*>( pHitComponentType )->setHasCapacity( true );
                    break;
                case tlSize:
//                    if( MGetEditorObjType()°pHitComponentType() == eotAgentType )
                    if( MGetEditorObjType::handling( pHitComponentType ) == eotAgentType )
                        static_cast<CAgentType*>( pHitComponentType )->setHasSize( true );
                    break;
                case tlSpeed:
//                    if( MGetEditorObjType()°pHitComponentType() == eotAgentType )
                    if( MGetEditorObjType::handling( pHitComponentType ) == eotAgentType )
                        static_cast<CAgentType*>( pHitComponentType )->setHasSpeed( true );
                    break;
            }
		
            // Redrop hit component type, to correct position
//            bool isEnoughSpace = MDrop( NULL, const_cast<void*>( pObjOwner ), isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom )°pHitComponentType();
            bool isEnoughSpace = MDrop::handling( pHitComponentType, nullptr, const_cast<void*>( pObjOwner ), isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );

            if( isEnoughSpace )
            {
                return( emsgNone );
            }
            else
            {
                switch( curSelectedTool )
                {
                    case tlPriority:
                        pHitComponentType->setHasPriority( false );
                        break;
                    case tlFrequency:
                        pHitComponentType->setHasFrequency( false );
                        break;
                    case tlNecessity:
                        pHitComponentType->setHasNecessity( false );
                        break;
                    case tlTime:
                        pHitComponentType->setHasTime( false );
                        break;
                    case tlCycle:
                        pHitComponentType->setHasCycle( false );
                        break;
                    case tlItem:
//                        if( MGetEditorObjType()°pHitComponentType() == eotStationType )
                        if( MGetEditorObjType::handling( pHitComponentType ) == eotStationType )
                            static_cast<CStationType*>( pHitComponentType )->setHasItem( false );
                        break;
                    case tlSpace:
//                        if( MGetEditorObjType()°pHitComponentType() == eotStationType )
                        if( MGetEditorObjType::handling( pHitComponentType ) == eotStationType )
                            static_cast<CStationType*>( pHitComponentType )->setHasSpace( false );
                        break;
                    case tlCapacity:
//                        if( MGetEditorObjType()°pHitComponentType() == eotAgentType )
                        if( MGetEditorObjType::handling( pHitComponentType ) == eotAgentType )
                            static_cast<CAgentType*>( pHitComponentType )->setHasCapacity( false );
                        break;
                    case tlSize:
//                        if( MGetEditorObjType()°pHitComponentType() == eotAgentType )
                        if( MGetEditorObjType::handling( pHitComponentType ) == eotAgentType )
                            static_cast<CAgentType*>( pHitComponentType )->setHasSize( false );
                        break;
                    case tlSpeed:
//                        if( MGetEditorObjType()°pHitComponentType() == eotAgentType )
                        if( MGetEditorObjType::handling( pHitComponentType ) == eotAgentType )
                            static_cast<CAgentType*>( pHitComponentType )->setHasSpeed( false );
                        break;
                }
		
                return( emsgNotEnoughSpaceForAttribute );
            }
        }
    }

    // If arrived here, then nothing happend, so no message required to send back to UI
    return( emsgNone );
}


void CEditor::doubleClick()
{
}


void CEditor::mouseDown( ENMouseButton mouseButton )
{
    // Unselect all selected objects.
    // If once it would be possible to select more than one object, clear only if shift key isn't down
    releaseSelectedObjects();

    // If no tool is selected
    if( curSelectedTool == tlNone )
    {
        CComponentType* pHitComponentType = nullptr;
        CPerspective* pHitPerspective = nullptr;
        CEdge* pHitEdge = nullptr;
			
        // First, try if edge is hit, if not try if component type is hit
        bool isHitEnd1 = false;
        bool isHitEnd2 = false;
        pHitEdge = getEdgeAtScreenPos( curMouseX, curMouseY, isHitEnd1, isHitEnd2 );
        if( pHitEdge == nullptr )
        {
            // Try if component type is hit, if not try at least if perspective is hit
            const bool isCheckWithFrame = false;
            pHitComponentType = getComponentTypeAtScreenPos( curMouseX, curMouseY, isCheckWithFrame, pDraggedObjectsOwner );
            if( pHitComponentType == nullptr )
            {
                pHitPerspective = getPerspectiveAtScreenPos( curMouseX, curMouseY, isCheckWithFrame, pDraggedObjectsOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
            }
        }

        if( pHitComponentType != nullptr )
        {
            // Add hit component type to currently selected objects
            pHitComponentType->setIsSelected( true );
            selectedObjects.push_back( pHitComponentType );

            // Add hit component type to currently dragged objects
            draggedObjects.push_back( pHitComponentType );
        }
        else if( pHitPerspective != nullptr )
        {
            // Add hit perspective to currently selected objects
            pHitPerspective->setIsSelected( true );
            selectedObjects.push_back( pHitPerspective );

            // Add hit perspective to currently dragged objects
            draggedObjects.push_back( pHitPerspective );
        }
        else if( pHitEdge != nullptr )
        {
            // Add hit edge to currently selected objects
            pHitEdge->setIsSelected( true );
            selectedObjects.push_back( pHitEdge );

            if( isHitEnd1 )
            {
                const CComponentType& componentType1 = pHitEdge->getConnector1().getComponentType();
                CEdgeConnector& connector1 = pHitEdge->getConnector1();
                const_cast<CComponentType&>( componentType1 ).removeEdgeConnector( connector1 );
				
                pHitEdge->setTempX( static_cast<int>( screenXToX( curMouseX ) ) );
                pHitEdge->setTempY( static_cast<int>( screenYToY( curMouseY ) ) );
				
                // Add hit edge to currently dragged objects
                draggedObjects.push_back( pHitEdge );
            }
            else if( isHitEnd2 )
            {
                const CComponentType& componentType2 = pHitEdge->getConnector2().getComponentType();
                CEdgeConnector& connector2 = pHitEdge->getConnector2();
                const_cast<CComponentType&>( componentType2 ).removeEdgeConnector( connector2 );

                pHitEdge->setTempX( static_cast<int>( screenXToX( curMouseX ) ) );
                pHitEdge->setTempY( static_cast<int>( screenYToY( curMouseY ) ) );

                // Add hit edge to currently dragged objects
                draggedObjects.push_back( pHitEdge );
            }
        }
    }
	
    // Else if perspective tool is selected...
    else if( curSelectedTool == tlPerspective )
    {
        // Add new Perspective to list
        CPerspective* pPerspective = new CPerspective;
        perspectives.push_back( pPerspective );
		
        // Set the perspective's anchor point
        int mouseX = static_cast<int>( screenXToX( curMouseX ) );
        int mouseY = static_cast<int>( screenYToY( curMouseY ) );
        pPerspective->setX( mouseX );
        pPerspective->setY( mouseY );
		
        // Set the perspective as selected
        pPerspective->setIsSelected( true );

        // Add perspective to selected objects
        selectedObjects.push_back( pPerspective );
		
        // Add perspective to currently dragged objects
        draggedObjects.push_back( pPerspective );

        // Set the object where the dragged objects belong to
        pDraggedObjectsOwner = this;
		
        // Specify which sides are currently dragged
        isDraggedRight = true;
        isDraggedBottom = true;
    }

    // Else if visit edge tool is selected...
    else if( (curSelectedTool == tlBoldVisitEdge) || (curSelectedTool == tlVisitEdge) )
    {
        const bool isCheckWithFrame = false;
        CComponentType* pHitComponentType = getComponentTypeAtScreenPos( curMouseX, curMouseY, isCheckWithFrame, pDraggedObjectsOwner );
		
        if( pHitComponentType != nullptr )
        {
            // Create new edge
            CVisitEdge* pVisitEdge = new CVisitEdge;

            // Set temporary position
            int mouseX = static_cast<int>( screenXToX( curMouseX ) );
            int mouseY = static_cast<int>( screenYToY( curMouseY ) );
            pVisitEdge->setTempX( mouseX );
            pVisitEdge->setTempY( mouseY );

            // Configure edge
            if( curSelectedTool == tlBoldVisitEdge )
                pVisitEdge->setIsBold( true );
				
            // Add edge to editor's edges
            edges.push_back( pVisitEdge );

            // Add edge connector to component type
            pHitComponentType->addEdgeConnector( pVisitEdge->getConnector1() );

            // Set the visit edge as selected
            pVisitEdge->setIsSelected( true );
			
            // Add visit edge to selected objects
            selectedObjects.push_back( pVisitEdge );
			
            // Add edge to currently dragged objects
            draggedObjects.push_back( pVisitEdge );

            // Set the object where the dragged objects belong to
            pDraggedObjectsOwner = pHitComponentType;
        }
    }

    // Else if time edge tool is selected...
    else if( curSelectedTool == tlTimeEdge )
    {
        const bool isCheckWithFrame = false;
        CComponentType* pHitComponentType = getComponentTypeAtScreenPos( curMouseX, curMouseY, isCheckWithFrame, pDraggedObjectsOwner );
		
        if( pHitComponentType != nullptr )
        {
            // Create new edge
            CTimeEdge* pTimeEdge = new CTimeEdge;

            // Set temporary position
            int mouseX = static_cast<int>( screenXToX( curMouseX ) );
            int mouseY = static_cast<int>( screenYToY( curMouseY ) );
            pTimeEdge->setTempX( mouseX );
            pTimeEdge->setTempY( mouseY );

            // Add edge to editor's edges
            edges.push_back( pTimeEdge );

            // Add edge connector to component type
            pHitComponentType->addEdgeConnector( pTimeEdge->getConnector1() );

            // Set the time edge as selected
            pTimeEdge->setIsSelected( true );
			
            // Add time edge to selected objects
            selectedObjects.push_back( pTimeEdge );

            // Add edge to currently dragged objects
            draggedObjects.push_back( pTimeEdge );

            // Set the object where the dragged objects belong to
            pDraggedObjectsOwner = pHitComponentType;
        }
    }

    // Else if place edge tool is selected...
    else if( curSelectedTool == tlPlaceEdge )
    {
        const bool isCheckWithFrame = false;
        CComponentType* pHitComponentType = getComponentTypeAtScreenPos( curMouseX, curMouseY, isCheckWithFrame, pDraggedObjectsOwner );
		
        if( pHitComponentType != nullptr )
        {
            // Create new edge
            CPlaceEdge* pPlaceEdge = new CPlaceEdge;

            // Set temporary position
            int mouseX = static_cast<int>( screenXToX( curMouseX ) );
            int mouseY = static_cast<int>( screenYToY( curMouseY ) );
            pPlaceEdge->setTempX( mouseX );
            pPlaceEdge->setTempY( mouseY );

            // Add edge to editor's edges
            edges.push_back( pPlaceEdge );

            // Add edge connector to component type
            pHitComponentType->addEdgeConnector( pPlaceEdge->getConnector1() );

            // Set the place edge as selected
            pPlaceEdge->setIsSelected( true );
			
            // Add place edge to selected objects
            selectedObjects.push_back( pPlaceEdge );

            // Add edge to currently dragged objects
            draggedObjects.push_back( pPlaceEdge );

            // Set the object where the dragged objects belong to
            pDraggedObjectsOwner = pHitComponentType;
        }
    }

    // Add mouse button to "down" list
    mouseButtonsDown.push_back( mouseButton );
}


void CEditor::mouseMove( int x, int y )
{
    // If there is something to drag: do it!
    list<CEditorObj*>::iterator itDraggedObj;
    for( itDraggedObj = draggedObjects.begin(); itDraggedObj != draggedObjects.end(); itDraggedObj++ )
    {
        // Calculate drag amount
        int oldX = static_cast<int>( screenXToX( curMouseX ) );
        int newX = static_cast<int>( screenXToX( x ) );
        int dragAmountX = newX-oldX;
        int oldY = static_cast<int>( screenYToY( curMouseY ) );
        int newY = static_cast<int>( screenYToY( y ) );
        int dragAmountY = newY-oldY;
		
        // Do drag
//        MDrag( dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom )°(*itDraggedObj)();
        MDrag::handling( *itDraggedObj, dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
    }

    // Set the incoming mouse position as current mouse position
    curMouseX = x;
    curMouseY = y;
}


ENEditorMessage CEditor::mouseUp( ENMouseButton mouseButton )
{
    bool isEnoughSpace = true;

    // Drop all dragged objects
    list<CEditorObj*>::iterator itDraggedObj;
    for( itDraggedObj = draggedObjects.begin(); itDraggedObj != draggedObjects.end(); itDraggedObj++ )
    {
        //
        // Do drop
        //
		
        const void* pObjOwner = nullptr;
        const bool isCheckWithFrame = false;
        CEditorObj* pDragTarget = getComponentTypeAtScreenPos( curMouseX, curMouseY, isCheckWithFrame, pObjOwner );

        bool isValidDrop = false;
//		isValidDrop = MDrop( pDragTarget, pDraggedObjectsOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom )°(*itDraggedObj)();
        isValidDrop = MDrop::handling( *itDraggedObj, pDragTarget, pDraggedObjectsOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );

		
        // If drop wasn't valid...
        if( !isValidDrop )
        {
            // If dropped object was a perspective and was not a valid drop, then there wasn't enough
            // space, so delete it!
            if( curSelectedTool == tlPerspective )
            {
                isEnoughSpace = false;
				
                // Delete perspective
                delete (*itDraggedObj);

                // Remove perspective from perspectives list
                perspectives.remove( static_cast<CPerspective*>( *itDraggedObj ) );

                // Remove perspective from selected objects list
                selectedObjects.remove( static_cast<CPerspective*>( *itDraggedObj ) );
            }
			
            // Check if there are any incomplete connected edges...
            list<CEdge*>::iterator itEdge;
            itEdge = edges.begin();
            while( itEdge != edges.end() )
            {
                // If there is an incomplete edge...
                if( ((*itEdge)->getConnector1().getIsConnected() == false) || ((*itEdge)->getConnector2().getIsConnected() == false) )
                {
                    // Remove edge from selected object list
                    selectedObjects.remove( *itEdge );
					
                    // Delete the incomplete connected edge and set iterator to next element
                    delete (*itEdge);
                    (*itEdge) = nullptr;
                    itEdge = edges.erase( itEdge );
                }
                else
                    itEdge++;
            }
        }
    }

    // No more dragging if mouse button is released
    isDraggedLeft = false;
    isDraggedTop = false;
    isDraggedRight = false;
    isDraggedBottom = false;

    // Release all dragged objects
    draggedObjects.clear();

    // Reset dragged objects owner
    pDraggedObjectsOwner = nullptr;

    // Delete released mouse button from list
    list<ENMouseButton>::iterator itMouseButton;
    for( itMouseButton = mouseButtonsDown.begin(); itMouseButton != mouseButtonsDown.end(); itMouseButton++ )
    {
        if( *itMouseButton == mouseButton )
        {
            mouseButtonsDown.erase( itMouseButton );
            break;
        }
    }

    // If selected tool is perspective and there wasn't enough space, send this knowledge to UI
    if( curSelectedTool == tlPerspective && (!isEnoughSpace) )
        return( emsgNotEnoughSpaceForPerspective );
	
    // In all ohter cases nothing important for UI can happen
    else
        return( emsgNone );
}


void CEditor::keyPress( char key )
{
}


void CEditor::keyDown( ENKey key )
{
    if( key == kDelete )
    {
        // If there are selected objects get the last one that was clicked
        CEditorObj* pSelectedObj = nullptr;
        list<CEditorObj*>::const_iterator itSelectedObj;
        for( itSelectedObj = selectedObjects.begin(); itSelectedObj != selectedObjects.end(); itSelectedObj++ )
            pSelectedObj = *itSelectedObj;
		
        // Delete the selected edge
        list<CEdge*>::iterator itEdge;
        for( itEdge = edges.begin(); itEdge != edges.end(); itEdge++ )
        {
            // If edge is selected, delete it
            if( (*itEdge) == pSelectedObj )
            {
                // Remove edge from selected object list
                selectedObjects.remove( *itEdge );

                delete *itEdge;
                *itEdge = nullptr;
                edges.erase( itEdge );
                break;
            }
        }
		
        // Delete the selected graph object
        list<CPerspective*>::iterator itPerspective;
        for( itPerspective = perspectives.begin(); itPerspective != perspectives.end(); itPerspective++ )
        {
            // If perspective is selected, delete it
            if( (*itPerspective) == pSelectedObj )
            {
                // Remove perspective from selected object list
                selectedObjects.remove( *itPerspective );
				
                delete *itPerspective;
                *itPerspective = nullptr;
                perspectives.erase( itPerspective );
                break;
            }

            // If component type inside perspective is selected
            bool isComponentTypeDeleted = false;
            list<CComponentType*>::iterator itComponentType;
            for( itComponentType = (*itPerspective)->componentTypes.begin(); itComponentType != (*itPerspective)->componentTypes.end(); itComponentType++ )
            {
                // If component type is selected, delete it
                if( (*itComponentType) == pSelectedObj )
                {
                    // Remove component type from selected object list
                    selectedObjects.remove( *itComponentType );

                    delete *itComponentType;
                    *itComponentType = nullptr;
                    (*itPerspective)->componentTypes.erase( itComponentType );
					
                    isComponentTypeDeleted = true;
                    break;
                }
						
            }

            if( isComponentTypeDeleted )
                break;
        }

        // Clear no more connected edges
        itEdge = edges.begin();
        while( itEdge != edges.end() )
        {
            bool isConnected1 = (*itEdge)->getConnector1().getIsConnected();
            bool isConnected2 = (*itEdge)->getConnector2().getIsConnected();
            if( (!isConnected1) || (!isConnected2) )
            {
                delete (*itEdge);
                (*itEdge) = nullptr;
                itEdge = edges.erase( itEdge );
            }
            else
            {
                itEdge++;
            }
        }
    }
	
    // Add key to "down" list
    keysDown.push_back( key );
}


void CEditor::keyUp( ENKey key )
{
    // Delete released key from list
    list<ENKey>::iterator itKey;
    for( itKey = keysDown.begin(); itKey != keysDown.end(); itKey++ )
    {
        if( *itKey == key )
        {
            keysDown.erase( itKey );
            break;
        }
    }
}





//------------------------------------------------------------
// Layout independant functionality and state setters
//------------------------------------------------------------


void CEditor::clear()
{
    //
    // Clean up current environment...
    //

    // Clean up selected objects
    selectedObjects.clear();
	
    // Delete all Perspectives
    list<CPerspective*>::iterator itPerspective;
    for( itPerspective = perspectives.begin(); itPerspective != perspectives.end(); itPerspective++ )
    {
        delete (*itPerspective);
    }
    perspectives.clear();

    // Delete all Edges
    list<CEdge*>::iterator itEdge;
    for( itEdge = edges.begin(); itEdge != edges.end(); itEdge++ )
    {
        delete (*itEdge);
    }
    edges.clear();

	
    // Reset scroll and zoom
    curScreenX = 0;
    curScreenY = 0;
    curZoomFactor = 1;
}


bool CEditor::load( char* pFileName )
{
    //
    // Test cheapest case (NULL or path is empty), do nothing and return false...
    //

    if( (pFileName == nullptr) || (string( pFileName ) == "") )
        return( false );

//    bool isLoadSuccessful = MLoadXmlFile( pFileName )°(this)( fpAddAgentToSimulation, fpRemoveAgentFromSimulation, fpAddStationToSimulation, fpRemoveStationFromSimulation );
    bool isLoadSuccessful = MLoadXmlFile::handling( this, pFileName, fpAddAgentToSimulation, fpRemoveAgentFromSimulation, fpAddStationToSimulation, fpRemoveStationFromSimulation );
    return( isLoadSuccessful );
}


void CEditor::save( char* pFileName )
{
//	MWriteXmlFile( pFileName )°(this)();
    MWriteXmlFile::handling( this, pFileName );
}


void CEditor::setSelectedTool( ENTool tool )
{
    curSelectedTool = tool;
}


void CEditor::setScrollX( int percentScrollX )
{
    curScreenX = static_cast<int>( EDITOR_WIDTH*GRID_XSIZE*curZoomFactor*(percentScrollX/100.0) );
}


void CEditor::setScrollY( int percentScrollY )
{
    curScreenY = static_cast<int>( EDITOR_HEIGHT*GRID_YSIZE*curZoomFactor*(percentScrollY/100.0) );
}


double CEditor::setZoom( double zoomFactor, int& percentScrollX, int& percentScrollY )
{
    // Check zoom factor validity and make it valid if not
    if( zoomFactor < MIN_ZOOM_FACTOR )
        zoomFactor = MIN_ZOOM_FACTOR;
    else if( zoomFactor > MAX_ZOOM_FACTOR )
        zoomFactor = MAX_ZOOM_FACTOR;
	
    // Get old real mouse position
    double mouseOldX = screenXToX( curMouseX );
    double mouseOldY = screenYToY( curMouseY );
	
    // Set the new zoom factor
    curZoomFactor = zoomFactor;

    // Now, get the new real mouse position
    double mouseNewX = screenXToX( curMouseX );
    double mouseNewY = screenYToY( curMouseY );

    // Correct scroll position due to offset between old and new real mouse positions
    curScreenX = curScreenX-(xToScreenX( mouseNewX )-xToScreenX( mouseOldX ));
    curScreenY = curScreenY-(yToScreenY( mouseNewY )-yToScreenY( mouseOldY ));

    // Update the given percent scroll variables for UI
    percentScrollX = static_cast<int>( (static_cast<double>( curScreenX )/(EDITOR_WIDTH*GRID_XSIZE*curZoomFactor))*100.0 );
    percentScrollY = static_cast<int>( (static_cast<double>( curScreenY )/(EDITOR_HEIGHT*GRID_YSIZE*curZoomFactor))*100.0 );

    // Return the new zoom factor
    return( curZoomFactor );
}


void CEditor::setVisibleFrameSize( int width, int height )
{
    visibleFrameWidth = width;
    visibleFrameHeight = height;
}


void CEditor::scrollToSceneMiddle()
{
    // Go to middle of scene...
    // First calculate the outer borders of all perspectives
    int smallestX = EDITOR_WIDTH;
    int smallestY = EDITOR_HEIGHT;
    int largestX = 0;
    int largestY = 0;
    list<CPerspective*>::iterator itPerspective;
    for( itPerspective = perspectives.begin(); itPerspective != perspectives.end(); itPerspective++ )
    {
        int perspectiveX = (*itPerspective)->getX();
        if( perspectiveX < smallestX )
            smallestX = perspectiveX;
        int perspectiveY = (*itPerspective)->getY();
        if( perspectiveY < smallestY )
            smallestY = perspectiveY;
        int perspectiveX2 = (*itPerspective)->getX()+(*itPerspective)->getWidth()-1;
        if( perspectiveX2 > largestX )
            largestX = perspectiveX2;
        int perspectiveY2 = (*itPerspective)->getY()+(*itPerspective)->getHeight()-1;
        if( perspectiveY2 > largestY )
            largestY = perspectiveY2;
    }

    // Calculate the middle point (depending on visible frame size)
    int middleX = smallestX+((largestX-smallestX)/2)-(visibleFrameWidth/GRID_XSIZE/2);
    int middleY = smallestY+((largestY-smallestY)/2)-(visibleFrameHeight/GRID_YSIZE/2);
    int percentMiddleX = static_cast<int>( static_cast<double>( middleX*100 )/EDITOR_WIDTH );
    int percentMiddleY = static_cast<int>( static_cast<double>( middleY*100 )/EDITOR_HEIGHT );
	
    // Scroll to middle point
    setScrollX( percentMiddleX );
    setScrollY( percentMiddleY );
}

		
////------------------------------------------------------------
//// UI data transfer and advanced communication
////------------------------------------------------------------


ENEditorObjType CEditor::getSelectedObjData( UEditorObjData selectedObjData[] ) const
{
    ENEditorObjType selectedObjType = eotNone;

    // If there are marked objects get the last one that was clicked and return it
    list<CEditorObj*>::const_iterator itSelectedObj;
    for( itSelectedObj = selectedObjects.begin(); itSelectedObj != selectedObjects.end(); itSelectedObj++ )
    {
//		selectedObjType = MGetEditorObjType()<-itSelectedObj();
        selectedObjType = MGetEditorObjType::handling( *itSelectedObj );

        if( selectedObjType != eotNone )
//            MGetEditorObjData( selectedObjData )<-itSelectedObj();
            MGetEditorObjData::handling( *itSelectedObj, selectedObjData );
    }
	
    return( selectedObjType );
}



void CEditor::setSelectedObjResults( UEditorObjData selectedObjData[] )
{
    // Get the current selected object
    CEditorObj* pSelectedObj = nullptr;
	
    list<CEditorObj*>::const_iterator itSelectedObj;
    for( itSelectedObj = selectedObjects.begin(); itSelectedObj != selectedObjects.end(); itSelectedObj++ )
        pSelectedObj = (*itSelectedObj);

    // Set the received data to selected object
//    MSetEditorObjData( selectedObjData )°pSelectedObj();
    MSetEditorObjData::handling( pSelectedObj, selectedObjData );
}


void CEditor::connectToSimulation( TfpAddStationToSimulation fpAddStationToSimulation, TfpRemoveStationFromSimulation fpRemoveStationFromSimulation, TfpAddAgentToSimulation fpAddAgentToSimulation, TfpRemoveAgentFromSimulation fpRemoveAgentFromSimulation )
{
    // Assign function pointers received from simulation
    this->fpAddStationToSimulation = fpAddStationToSimulation;
    this->fpRemoveStationFromSimulation = fpRemoveStationFromSimulation;
    this->fpAddAgentToSimulation = fpAddAgentToSimulation;
    this->fpRemoveAgentFromSimulation = fpRemoveAgentFromSimulation;
}


//------------------------------------------------------------
// Platform dependent(!) interface for visualization
//------------------------------------------------------------


void CEditor::draw( HDC graphicContext )
{
	// Init double-buffering
	HDC hBackbuffer;
	hBackbuffer = CreateCompatibleDC( graphicContext );
	HBITMAP theBitmap;
	theBitmap = CreateCompatibleBitmap( graphicContext, visibleFrameWidth, visibleFrameHeight );
	SelectObject( hBackbuffer, theBitmap );
    
	// Draw background
	HBRUSH hbrush, hbrushold;
    hbrush = static_cast<HBRUSH>( CreateSolidBrush( RGB( 150, 150, 150 ) ) );
    hbrushold = static_cast<HBRUSH>( SelectObject( hBackbuffer, hbrush ) );
	Rectangle( hBackbuffer, 0, 0, visibleFrameWidth, visibleFrameHeight );
	SelectObject( hBackbuffer, hbrushold );
	DeleteObject( hbrush );
	
	// Draw editable surface
	int red = EDIT_SURFACE_RED;
	int green = EDIT_SURFACE_GREEN; 
	int blue = EDIT_SURFACE_BLUE; 
    hbrush = static_cast<HBRUSH>( CreateSolidBrush( RGB( red, green, blue ) ) );
    hbrushold = static_cast<HBRUSH>( SelectObject( hBackbuffer, hbrush ) );
	Rectangle( hBackbuffer, xToScreenX( 0 ), yToScreenY( 0 ), xToScreenX( EDITOR_WIDTH ), yToScreenY( EDITOR_HEIGHT ) );
	SelectObject( hBackbuffer, hbrushold );
	DeleteObject( hbrush );

	// Draw grid
	int screenX = xToScreenX( 0 );
	int screenY = yToScreenY( 0 );
	CColor penColor( GRID_RED, GRID_GREEN, GRID_BLUE );
	CColor brushColor( 0, 0, 0 );
	bool isSelected = false;

//	MDrawWithStyles( hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected )°grid( EDITOR_WIDTH, EDITOR_HEIGHT );
    MDrawWithStyles::handling( grid, hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected, EDITOR_WIDTH, EDITOR_HEIGHT );

	// Draw perspectives
	list<CPerspective*>::iterator itPerspective;
	for( itPerspective = perspectives.begin(); itPerspective != perspectives.end(); itPerspective++ )
	{
		int screenX = xToScreenX( (*itPerspective)->getX() );
		int screenY = yToScreenY( (*itPerspective)->getY() );
		CColor penColor( PERSPECTIVE_FRAME_RED, PERSPECTIVE_FRAME_GREEN, PERSPECTIVE_FRAME_BLUE );
		CColor brushColor( PERSPECTIVE_RED, PERSPECTIVE_GREEN, PERSPECTIVE_BLUE );
		bool isSelected = (*itPerspective)->getIsSelected();

//		MDrawWithStyles( hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected )<-(*itPerspective)();
        MDrawWithStyles::handling( **itPerspective, hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected );
    }

	// Draw edges
	list<CEdge*>::iterator itEdge;
	for( itEdge = edges.begin(); itEdge != edges.end(); itEdge++ )
	{
		int screenX = xToScreenX( (*itEdge)->getX1() );
		int screenY = yToScreenY( (*itEdge)->getY1() );
		CColor penColor( EDGE_RED, EDGE_GREEN, EDGE_BLUE );
		CColor brushColor( 255, 255, 255 );
		bool isSelected = (*itEdge)->getIsSelected();

		// The edge label should be visible here
		(*itEdge)->setIsLabelVisible( true );

//		MDrawWithStyles( hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected )<-itEdge();
        MDrawWithStyles::handling( *itEdge, hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected );
    }

	// Draw component types
	for( itPerspective = perspectives.begin(); itPerspective != perspectives.end(); itPerspective++ )
	{
		list<CComponentType*>::iterator itComponentType;
		for( itComponentType = (*itPerspective)->componentTypes.begin(); itComponentType != (*itPerspective)->componentTypes.end(); itComponentType++ )
		{
			int screenX = xToScreenX( (*itPerspective)->getX() + (*itComponentType)->getX() );
			int screenY = yToScreenY( (*itPerspective)->getY() + (*itComponentType)->getY() );
			CColor penColor( COMPONENTTYPE_FRAME_RED, COMPONENTTYPE_FRAME_GREEN, COMPONENTTYPE_FRAME_BLUE );
			CColor brushColor( (*itComponentType)->color.getRed(), (*itComponentType)->color.getGreen(), (*itComponentType)->color.getBlue() );
			bool isSelected = (*itComponentType)->getIsSelected();
			
			// Reset the draw factor (no scaling when drawn in editor)
			(*itComponentType)->setDrawFactor( 1.0 );

//			MDrawWithStyles( hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected )<-itComponentType();
            MDrawWithStyles::handling( *itComponentType, hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected );
        }
	}

	// Show back-buffer
	BitBlt( graphicContext, 0, 0, EDITOR_WIDTH*GRID_XSIZE, EDITOR_HEIGHT*GRID_YSIZE, hBackbuffer, 0, 0, SRCCOPY );

	// Delete back-buffer device context and bitmap
	DeleteDC( hBackbuffer );
	DeleteObject( theBitmap );
}





//------------------------------------------------------------
// Helper
//------------------------------------------------------------


int CEditor::xToScreenX( double x ) const
{
	int scrollOffset = curScreenX;
	double gridSize = static_cast<double>( GRID_XSIZE );

    return( static_cast<int>( (x*gridSize*curZoomFactor) - scrollOffset ) );
}


int CEditor::yToScreenY( double y ) const
{
	int scrollOffset = curScreenY;
	double gridSize = static_cast<double>( GRID_YSIZE );

    return( static_cast<int>( (y*gridSize*curZoomFactor) - scrollOffset ) );
}


double CEditor::screenXToX( int x ) const
{
	int scrollOffset = curScreenX;
	double gridSize = static_cast<double>( GRID_XSIZE );
	
	return( (x + scrollOffset)/gridSize/curZoomFactor );
}


double CEditor::screenYToY( int y ) const
{
	int scrollOffset = curScreenY;
	double gridSize = static_cast<double>( GRID_YSIZE );

	return( (y + scrollOffset)/gridSize/curZoomFactor );
}


CPerspective* CEditor::getPerspectiveAtScreenPos( int screenX, int screenY, bool isCheckWithFrame, const void*& pObjOwner, bool& isHitLeft, bool& isHitTop, bool& isHitRight, bool& isHitBottom ) const
{
    pObjOwner = nullptr;
	isHitLeft = false;
	isHitTop = false;
	isHitRight = false;
	isHitBottom = false;

	int x = static_cast<int>( screenXToX( screenX ) );
	int y = static_cast<int>( screenYToY( screenY ) );

	list<CPerspective*>::const_iterator itPerspective;
	for( itPerspective = perspectives.begin(); itPerspective != perspectives.end(); itPerspective++ )
	{
		int persX = 0;
		int persY = 0;
		int persX2 = 0;
		int persY2 = 0;

		if( isCheckWithFrame )
		{
			persX = (*itPerspective)->getX()-GRAPHOBJ_MIN_DISTANCE;
			persY = (*itPerspective)->getY()-GRAPHOBJ_MIN_DISTANCE;
			persX2 = (*itPerspective)->getX()+(*itPerspective)->getWidth()-1+GRAPHOBJ_MIN_DISTANCE;
			persY2 = (*itPerspective)->getY()+(*itPerspective)->getHeight()-1+GRAPHOBJ_MIN_DISTANCE;
		}
		else
		{
			persX = (*itPerspective)->getX();
			persY = (*itPerspective)->getY();
			persX2 = (*itPerspective)->getX()+(*itPerspective)->getWidth()-1;
			persY2 = (*itPerspective)->getY()+(*itPerspective)->getHeight()-1;
		}
		
		if( x == persX )
			isHitLeft = true;
		if( y == persY )
			isHitTop = true;
		if( x == persX2 )
			isHitRight = true;
		if( y == persY2 )
			isHitBottom = true;
		
		if( (x >= persX) && (x <= persX2) && (y >= persY) && (y <= persY2) )
		{
			pObjOwner = this;
			return( *itPerspective );
		}
	}

    return( nullptr );
}


CComponentType* CEditor::getComponentTypeAtScreenPos( int screenX, int screenY, bool isCheckWithFrame, const void*& pObjOwner ) const
{
    pObjOwner = nullptr;
	bool isHitLeft = false;
	bool isHitTop = false;
	bool isHitRight = false;
	bool isHitBottom = false;

	bool isCheckWithFramePerspective = false;

	CPerspective* pPerspective = getPerspectiveAtScreenPos( screenX, screenY, isCheckWithFramePerspective, pObjOwner, isHitLeft, isHitTop, isHitRight, isHitBottom );
	
    if( pPerspective != nullptr )
	{
		int x = static_cast<int>( screenXToX( screenX ) );
		int y = static_cast<int>( screenYToY( screenY ) );

		int persX = pPerspective->getX();
		int persY = pPerspective->getY();

		list<CComponentType*>::const_iterator itComponentType;
		for( itComponentType = pPerspective->componentTypes.begin(); itComponentType != pPerspective->componentTypes.end(); itComponentType++ )
		{
			int compTypeX = 0;
			int compTypeY = 0;
			int compTypeX2 = 0;
			int compTypeY2 = 0;

			if( isCheckWithFrame )
			{
				compTypeX = persX+(*itComponentType)->getX()-GRAPHOBJ_MIN_DISTANCE;
				compTypeY = persY+(*itComponentType)->getY()-GRAPHOBJ_MIN_DISTANCE;
				compTypeX2 = persX+(*itComponentType)->getX()+(*itComponentType)->getWidth()-1+GRAPHOBJ_MIN_DISTANCE;
				compTypeY2 = persY+(*itComponentType)->getY()+(*itComponentType)->getHeight()-1+GRAPHOBJ_MIN_DISTANCE;
			}
			else
			{
				bool hasAttribs = (*itComponentType)->getHasAttribs();
				
				compTypeX = persX+(*itComponentType)->getX();
				compTypeY = persY+(*itComponentType)->getY()+(*itComponentType)->getHeight()/*-1*/-COMPONENTTYPE_HEIGHT;
				compTypeX2 = persX+(*itComponentType)->getX()+(*itComponentType)->getWidth()-1-(hasAttribs ? 1 : 0);
				compTypeY2 = persY+(*itComponentType)->getY()+(*itComponentType)->getHeight()-1;
			}
			
			if( (x >= compTypeX) && (x <= compTypeX2) && (y >= compTypeY) && (y <= compTypeY2) )
			{
				pObjOwner = pPerspective;
				return( *itComponentType );
			}
		}
	}

    return( nullptr );
}


CEdge* CEditor::getEdgeAtScreenPos( int screenX, int screenY, bool& isHitEnd1, bool& isHitEnd2 ) const
{
	const double edgeWidth = 1;  // Define edge has width of one grid element

	double x = screenXToX( screenX );
	double y = screenYToY( screenY );

	list<CEdge*>::const_iterator itEdge;
	for( itEdge = edges.begin(); itEdge != edges.end(); itEdge++ )
	{
		// Get the edge location
		int edgeX1 = (*itEdge)->getX1();
		int edgeY1 = (*itEdge)->getY1();
		int edgeX2 = (*itEdge)->getX2();
		int edgeY2 = (*itEdge)->getY2();

		// Calculate the edge vector
		double edgeVectorX = edgeX2-edgeX1;
		double edgeVectorY = edgeY2-edgeY1;

		// Create orthogonal vector to edge (for measuring distance to edge)
		double xOrth = edgeVectorY;
		double yOrth = -edgeVectorX;

		
		// Apply Cramer's rule to solve linear system of equations, needed to calculate intersection point
		// of edge's straight line with orthogonal straight line

		// Caclulate determinants
		double detA = edgeVectorX*(-yOrth)-edgeVectorY*(-xOrth);
		double detAs = (x-edgeX1)*(-yOrth)-(y-edgeY1)*(-xOrth);

		// Calculate scalar needed to scale direction vector of edge's straight line to get intersection
		// point's absolute coordinates
		double s = detAs/detA;
		
		// Calculate absolute coordinates of intersection point of edge's straight line and orthogonal 
		// straight line
		double interX = edgeX1 + s*edgeVectorX;
		double interY = edgeY1 + s*edgeVectorY;

		// Calculate distance to edge by calculating norm of vector from the given position to intersection
		// point
		double distanceToEdge = sqrt( (interX-x)*(interX-x) + (interY-y)*(interY-y) );

		
		// Determine if given position is within edge ends, depending on how the edge is located
		// (position (0|0) is left top position of coordinate system)
		
		bool isWithinEdgeEnds = false;

        /*
                 2
               /
             /
           1
        */
		if( (edgeVectorX >= 0) && (edgeVectorY <= 0) )
		{	
			isWithinEdgeEnds =    (interX >= edgeX1) 
			                   && (interY >= edgeY2) 
							   && (interX <= edgeX2) 
							   && (interY <= edgeY1);
		}
		
        /*
           2
             \
               \
                 1
        */
        else if( (edgeVectorX <= 0) && (edgeVectorY <= 0) )
		{
			isWithinEdgeEnds =    (interX >= edgeX2) 
			                   && (interY >= edgeY2) 
							   && (interX <= edgeX1) 
							   && (interY <= edgeY1);
		}

        /*
                 1
               /
             /
           2
        */
        else if( (edgeVectorX <= 0) && (edgeVectorY >= 0) )
		{	
			isWithinEdgeEnds =    (interX >= edgeX2) 
			                   && (interY >= edgeY1) 
							   && (interX <= edgeX1) 
							   && (interY <= edgeY2);
		}

        /*
           1
             \
               \
                 2
        */
        else if( (edgeVectorX >= 0) && (edgeVectorY >= 0) )
		{
			isWithinEdgeEnds =    (interX >= edgeX1) 
			                   && (interY >= edgeY1) 
							   && (interX <= edgeX2) 
							   && (interY <= edgeY2);
		}


		//
		// Check if edge could be hit at left/right end
		//
		
		bool isMayBeHitEnd1 = false;
		bool isMayBeHitEnd2 = false;

//		double edgeLength = sqrt( edgeVectorX*edgeVectorX + edgeVectorY*edgeVectorY );
		double distanceToEnd1 = sqrt( (interX-edgeX1)*(interX-edgeX1) + (interY-edgeY1)*(interY-edgeY1) );
		double distanceToEnd2 = sqrt( (interX-edgeX2)*(interX-edgeX2) + (interY-edgeY2)*(interY-edgeY2) );

		// Check if click is in between edge ends...
		double componentTypeRadius = ((COMPONENTTYPE_WIDTH/2.0)+(COMPONENTTYPE_HEIGHT/2.0))/2.0;
									 // Arithmetic middle of component types half width and half height 

		if( (distanceToEnd1 > componentTypeRadius) && (distanceToEnd1 <= /*edgeLength*0.25*/componentTypeRadius+1) )
			isMayBeHitEnd1 = true;

		if( (distanceToEnd2 > componentTypeRadius) && (distanceToEnd2 <= /*edgeLength*0.25*/componentTypeRadius+1) && (!isMayBeHitEnd1) )
			isMayBeHitEnd2 = true;

		// If distance of given position to edge is half of edge width and within edge ends (but not within
		// component type) edge is hit!
		if( (distanceToEdge <= (edgeWidth/2.0)) && isWithinEdgeEnds && (distanceToEnd1 > componentTypeRadius) && (distanceToEnd2 > componentTypeRadius) )
		{
			isHitEnd1 = isMayBeHitEnd1;
			isHitEnd2 = isMayBeHitEnd2;
			return( *itEdge );
		}
	}

	// Nothing is hit!
    return( nullptr );
}


CEditorObj* CEditor::getEditorObjFromId( int id )
{
    list<CPerspective*>::iterator itPerspective;
    for( itPerspective = perspectives.begin(); itPerspective != perspectives.end(); itPerspective++ )
    {
        if( (*itPerspective)->getId() == id )
            return( *itPerspective );
		
        list<CComponentType*>::iterator itComponentType;
        for( itComponentType = (*itPerspective)->componentTypes.begin(); itComponentType != (*itPerspective)->componentTypes.end(); itComponentType++ )
        {
            if( (*itComponentType)->getId() == id )
                return( *itComponentType );
        }
    }

    list<CEdge*>::iterator itEdge;
    for( itEdge = edges.begin(); itEdge != edges.end(); itEdge++ )
    {
        if( (*itEdge)->getId() == id )
            return( *itEdge );
    }

    return( nullptr );
}


void CEditor::releaseSelectedObjects()
{
    list<CEditorObj*>::iterator itSelectedObj;
    for( itSelectedObj = selectedObjects.begin(); itSelectedObj != selectedObjects.end(); itSelectedObj++ )
        (*itSelectedObj)->setIsSelected( false );

    selectedObjects.clear();
}
