//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Constants.h"
#include "Perspective.h"
#include "Editor.h"
#include <list>
#include <typeinfo>
//#pragma c-mol_hdrstop

//#include "GetDraggedObjOwnerData.hmol"
#include "GetDraggedObjOwnerData.hpp"





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MGetDraggedObjOwnerData::<CEditor*>()
void MGetDraggedObjOwnerData::handling( CEditor* that, METHOD_PARAMS_GETDRAGGEDOBJOWNERDATA )
{
	// Create list with all other graph objects relevant for correction of position when dropping
	objectsToCheck.clear();
	list<CPerspective*>::iterator itPerspective;
	for( itPerspective = that->perspectives.begin(); itPerspective != that->perspectives.end(); itPerspective++ )
		
		// The currently dragged object is not relevant for checking of its own position!
		if( *itPerspective != &draggedObj )
			objectsToCheck.push_back( *itPerspective );

	maxWidth = EDITOR_WIDTH;
	maxHeight = EDITOR_HEIGHT;
}


//void MGetDraggedObjOwnerData::<CPerspective*>()
void MGetDraggedObjOwnerData::handling( CPerspective* that, METHOD_PARAMS_GETDRAGGEDOBJOWNERDATA )
{
	// Create list with all other graph objects relevant for correction of position when dropping
	objectsToCheck.clear();
	list<CComponentType*>::iterator itComponentType;
	for( itComponentType = that->componentTypes.begin(); itComponentType != that->componentTypes.end(); itComponentType++ )

		// The currently dragged object is not relevant for checking of its own position!
		if( *itComponentType != &draggedObj )
			objectsToCheck.push_back( *itComponentType );

	maxWidth = that->getWidth();
	maxHeight = that->getHeight();
}
