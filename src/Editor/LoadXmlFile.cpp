//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Constants.h"
#include "Editor.h"
#include "StationType.h"
#include "AgentType.h"
#include "VisitEdge.h"
#include "PlaceEdge.h"
#include "TimeEdge.h"

// Xerces API
#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/dom/DOMNodeList.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//#pragma c-mol_hdrstop

#include "LoadXmlFile.hpp"





using namespace xercesc;





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//bool MLoadXmlFile::<CEditor*>()
bool MLoadXmlFile::handling( CEditor* that, METHOD_PARAMS_LOADXMLFILE )
{
//	bool result = MLoadXmlFile( sFileName )°that( NULL, NULL, NULL, NULL );
    bool result = MLoadXmlFile::handling( that, sFileName, nullptr, nullptr, nullptr, nullptr );

	return( result );
}


//bool MLoadXmlFile::<CEditor*>( TfpAddAgentToSimulation fpAddAgentToSimulation, TfpRemoveAgentFromSimulation fpRemoveAgentFromSimulation, TfpAddStationToSimulation fpAddStationToSimulation, TfpRemoveStationFromSimulation fpRemoveStationFromSimulation )
bool MLoadXmlFile::handling( CEditor* that, METHOD_PARAMS_LOADXMLFILE, TfpAddAgentToSimulation fpAddAgentToSimulation, TfpRemoveAgentFromSimulation fpRemoveAgentFromSimulation, TfpAddStationToSimulation fpAddStationToSimulation, TfpRemoveStationFromSimulation fpRemoveStationFromSimulation )
{
	// Clean up current environment
	that->clear();

	
	//
	// Init Xerces...
	//

	try
	{
		XMLPlatformUtils::Initialize();
	}
	catch( const XMLException& e )
	{
		// Create new message...
		char* pMessage = XMLString::transcode( e.getMessage() );
		string sMessageComplete( string( "Xerces initialization error: " )+pMessage );
		XMLString::release( &pMessage );

/*		
		// And pass it on as char* exception for access from host application
		char* pMessageComplete = new char[ sMessageComplete.length()+1 ];
		strcpy( pMessageComplete, sMessageComplete.c_str() );
		throw( pMessageComplete );
*/
		return( false );
	}

	XercesDOMParser* pXercesDOMParser = new XercesDOMParser;


/*
	// Test if the file is ok.

	struct stat fileStatus;

	int iretStat = stat( configFile.c_str(), &fileStatus );
	if( iretStat == ENOENT )
		throw ( std::runtime_error( "Path file_name does not exist, or path is an empty string." ) );
	else if( iretStat == ENOTDIR )
		throw ( std::runtime_error( "A component of the path is not a directory." ) );
//  else if( iretStat == ELOOP )
//		throw ( std::runtime_error("Too many symbolic links encountered while traversing the path."));
	else if( iretStat == EACCES )
	  throw ( std::runtime_error( "Permission denied." ));
	else if( iretStat == ENAMETOOLONG )
	  throw ( std::runtime_error( "File can not be read\n" ) );
*/


	//
	// Define the tags and attributes used in the XML file...
	//

	// Tags
	XMLCh* pTagRoot			= XMLString::transcode( XMLTAG_GRAPH );
	XMLCh* pTagPerspective	= XMLString::transcode( XMLTAG_PERSPECTIVE );
	XMLCh* pTagStationType	= XMLString::transcode( XMLTAG_STATIONTYPE );
	XMLCh* pTagAgentType	= XMLString::transcode( XMLTAG_AGENTTYPE );
	XMLCh* pTagVisitEdge	= XMLString::transcode( XMLTAG_VISITEDGE );
	XMLCh* pTagPlaceEdge	= XMLString::transcode( XMLTAG_PLACEEDGE );
	XMLCh* pTagTimeEdge		= XMLString::transcode( XMLTAG_TIMEEDGE );
	XMLCh* pTagAttribPriority	= XMLString::transcode( XMLTAG_PRIORITY );
	XMLCh* pTagAttribFrequency	= XMLString::transcode( XMLTAG_FREQUENCY );
	XMLCh* pTagAttribNecessity	= XMLString::transcode( XMLTAG_NECESSITY );
	XMLCh* pTagAttribTime		= XMLString::transcode( XMLTAG_TIME );
	XMLCh* pTagAttribCycle		= XMLString::transcode( XMLTAG_CYCLE );
	XMLCh* pTagAttribItem		= XMLString::transcode( XMLTAG_ITEM );
	XMLCh* pTagAttribSpace		= XMLString::transcode( XMLTAG_SPACE );
	XMLCh* pTagAttribCapacity	= XMLString::transcode( XMLTAG_CAPACITY );
	XMLCh* pTagAttribSize		= XMLString::transcode( XMLTAG_SIZE );
	XMLCh* pTagAttribSpeed		= XMLString::transcode( XMLTAG_SPEED );

	// Attributes
	XMLCh* pAttribId				= XMLString::transcode( XMLATTRIB_ID );
	XMLCh* pAttribName				= XMLString::transcode( XMLATTRIB_NAME );
	XMLCh* pAttribX					= XMLString::transcode( XMLATTRIB_X );
	XMLCh* pAttribY					= XMLString::transcode( XMLATTRIB_Y );
	XMLCh* pAttribWidth				= XMLString::transcode( XMLATTRIB_WIDTH );
	XMLCh* pAttribHeight			= XMLString::transcode( XMLATTRIB_HEIGHT );
	XMLCh* pAttribCount				= XMLString::transcode( XMLATTRIB_COUNT );
	XMLCh* pAttribColorR			= XMLString::transcode( XMLATTRIB_COLORRED );
	XMLCh* pAttribColorG			= XMLString::transcode( XMLATTRIB_COLORGREEN );
	XMLCh* pAttribColorB			= XMLString::transcode( XMLATTRIB_COLORBLUE );
	XMLCh* pAttribInterface			= XMLString::transcode( XMLATTRIB_INTERFACE );
	XMLCh* pAttribConnectedIdRef1	= XMLString::transcode( XMLATTRIB_CONNECTEDIDREF1 );
	XMLCh* pAttribConnectedIdRef2	= XMLString::transcode( XMLATTRIB_CONNECTEDIDREF2 );
	XMLCh* pAttribBold				= XMLString::transcode( XMLATTRIB_BOLD );
	XMLCh* pAttribValue				= XMLString::transcode( XMLATTRIB_VALUE );
	XMLCh* pAttribDirected			= XMLString::transcode( XMLATTRIB_DIRECTED );
	XMLCh* pAttribAndConnected1		= XMLString::transcode( XMLATTRIB_ANDCONNECTED1 );
	XMLCh* pAttribAndConnected2		= XMLString::transcode( XMLATTRIB_ANDCONNECTED2 );

	
	//
	// Configure DOM parser...
	//

	pXercesDOMParser->setValidationScheme( XercesDOMParser::Val_Never );
	pXercesDOMParser->setDoNamespaces( false );
	pXercesDOMParser->setDoSchema( false );
	pXercesDOMParser->setLoadExternalDTD( false );


	//
	// Load the file...
	//

	try
	{
		pXercesDOMParser->parse( sFileName.c_str() );

		// Get the parsed document
		xercesc::DOMDocument* pXmlDoc = pXercesDOMParser->getDocument();
        if( pXmlDoc == nullptr )
		{
/*
			throw( "File does not exist." );
*/
			return( false );
		}

		// Get the root element (project)
		DOMElement* pRootElement = pXmlDoc->getDocumentElement();
        if( pRootElement == nullptr )
		{
/*
			throw( "Empty XML file." );
*/
			return( false );
		}

		// Get pChildren of the root element (of the whole project)
		DOMNodeList* pChildren = pRootElement->getChildNodes();
		const  XMLSize_t nodeCount = pChildren->getLength();

		// Now look at all children of the whole project (perspectives, edges)
		for( XMLSize_t i = 0; i < nodeCount; ++i )
		{
			// Get a new child node (perspective/edge)
			DOMNode* pCurrentNode = pChildren->item( i );

			// Check if it is an element node
            if(    (pCurrentNode/*->getNodeType()*/ != nullptr)
				&& (pCurrentNode->getNodeType() == DOMNode::ELEMENT_NODE) )
			{
				// Make this node an element now
				DOMElement* currentElement = static_cast<DOMElement*>( pCurrentNode );

				// Check if the found element is a perspective ... (else cases if see below!!)
				if( XMLString::equals( currentElement->getTagName(), pTagPerspective ) )
				{
					//####################################################################################
					// Create new Perspective with all its attributes here and add it to perspective list
					//####################################################################################
					const XMLCh* xmlchId = currentElement->getAttribute( pAttribId );
					const XMLCh* xmlchName = currentElement->getAttribute( pAttribName );
					const XMLCh* xmlchX = currentElement->getAttribute( pAttribX );
					const XMLCh* xmlchY = currentElement->getAttribute( pAttribY );
					const XMLCh* xmlchWidth = currentElement->getAttribute( pAttribWidth );
					const XMLCh* xmlchHeight = currentElement->getAttribute( pAttribHeight );
					char* pIdString = XMLString::transcode( xmlchId );
					char* pNameString = XMLString::transcode( xmlchName );
					char* pXString = XMLString::transcode( xmlchX );
					char* pYString = XMLString::transcode( xmlchY );
					char* pWidthString = XMLString::transcode( xmlchWidth );
					char* pHeightString = XMLString::transcode( xmlchHeight );
					
					// cout << "perspective: " << endl;
					CPerspective* pPerspective = new CPerspective;
					// cout << "id: " << pIdString << endl;
					pPerspective->setId( atoi( pIdString ) );
					// cout << "name: " << pNameString << endl;
					pPerspective->setName( pNameString );
					// cout << "x: " << pXString << endl;
					pPerspective->setX( atoi( pXString ) );
					// cout << "y: " << pYString << endl;
					pPerspective->setY( atoi( pYString ) );
					// cout << "width: " << pWidthString << endl;
					pPerspective->setWidth( atoi( pWidthString ) );
					// cout << "height: " << pHeightString << endl;
					pPerspective->setHeight( atoi( pHeightString ) );
					// cout << endl;
					that->perspectives.push_back( pPerspective );
					
					XMLString::release( &pIdString );
					XMLString::release( &pNameString );
					XMLString::release( &pXString );
					XMLString::release( &pYString );
					XMLString::release( &pWidthString );
					XMLString::release( &pHeightString );


					// Get children of the perspective (component types)
					DOMNodeList* pComponentTypeNodes = pCurrentNode->getChildNodes();
					const XMLSize_t componentTypeCount = pComponentTypeNodes->getLength();

					// Now look at all component types of the perspective (station types. agent types)
					for( XMLSize_t i = 0; i < componentTypeCount; ++i )
					{
						// Get a new child node (component type)
						DOMNode* pCurComponentTypeNode = pComponentTypeNodes->item( i );

						// Check if it is an element node
                        if(    (pCurComponentTypeNode/*->getNodeType()*/ != nullptr)
							&& (pCurComponentTypeNode->getNodeType() == DOMNode::ELEMENT_NODE) )
						{
							// Make this component type node an element now
							DOMElement* pCurComponentTypeElement = static_cast<DOMElement*>( pCurComponentTypeNode );

							// Check if the found element is a station type... 
							if( XMLString::equals( pCurComponentTypeElement->getTagName(), pTagStationType ) )
							{
								//###############################################################################
								// Create new station type with all its attributes an add it to perspective here
								//###############################################################################
								const XMLCh* xmlchId = pCurComponentTypeElement->getAttribute( pAttribId );
								const XMLCh* xmlchName = pCurComponentTypeElement->getAttribute( pAttribName );
								const XMLCh* xmlchX = pCurComponentTypeElement->getAttribute( pAttribX );
								const XMLCh* xmlchY = pCurComponentTypeElement->getAttribute( pAttribY );
								const XMLCh* xmlchCount = pCurComponentTypeElement->getAttribute( pAttribCount );
								const XMLCh* xmlchColorR = pCurComponentTypeElement->getAttribute( pAttribColorR );
								const XMLCh* xmlchColorG = pCurComponentTypeElement->getAttribute( pAttribColorG );
								const XMLCh* xmlchColorB = pCurComponentTypeElement->getAttribute( pAttribColorB );
								const XMLCh* xmlchInterface = pCurComponentTypeElement->getAttribute( pAttribInterface );
								char* pIdString = XMLString::transcode( xmlchId );
								char* pNameString = XMLString::transcode( xmlchName );
								char* pXString = XMLString::transcode( xmlchX );
								char* pYString = XMLString::transcode( xmlchY );
								char* pCountString = XMLString::transcode( xmlchCount );
								char* pColorRString = XMLString::transcode( xmlchColorR );
								char* pColorGString = XMLString::transcode( xmlchColorG );
								char* pColorBString = XMLString::transcode( xmlchColorB );
								char* pInterfaceString = XMLString::transcode( xmlchInterface );
								
								// cout << "    " << "stationType: " << endl;
								CStationType* pStationType = new CStationType( *pPerspective, fpAddStationToSimulation, fpRemoveStationFromSimulation );
								// cout << "    " << "id: " << pIdString << endl;
								pStationType->setId( atoi( pIdString ) );
								// cout << "    " << "name: " << pNameString << endl;
								pStationType->setName( pNameString );
								
								// Have to set position after attributes, because adding them will change 
								// position
								// cout << "    " << "x: " << pXString << endl;
								// pStationType->setX( atoi( pXString ) );
								// cout << "    " << "y: " << pYString << endl;
								// pStationType->setY( atoi( pYString ) );
								// cout << "    " << "count: " << pCountString << endl;
								
								pStationType->setNoOfComponents( atoi( pCountString ) );
								// cout << "    " << "colorR: " << pColorRString << endl;
								pStationType->color.setRed( atoi( pColorRString ) );
								// cout << "    " << "colorG: " << pColorGString << endl;
								pStationType->color.setGreen( atoi( pColorGString ) );
								// cout << "    " << "colorB: " << pColorBString << endl;
								pStationType->color.setBlue( atoi( pColorBString ) );
								// cout << "    " << "interface: " << pInterfaceString << endl;
								// TODO: SET THE INTERFACE ATTRIBUTE HERE!
								// cout << "    " << endl;
								pPerspective->componentTypes.push_back( pStationType );

								XMLString::release( &pIdString );
								XMLString::release( &pNameString );
								// XMLString::release( &pXString );
								// XMLString::release( &pYString );
								XMLString::release( &pCountString );
								XMLString::release( &pColorRString );
								XMLString::release( &pColorGString );
								XMLString::release( &pColorBString );
								XMLString::release( &pInterfaceString );

								// Get children of the station type (attributes)
								DOMNodeList* pAttributeNodes = pCurComponentTypeNode->getChildNodes();
								const XMLSize_t attributeCount = pAttributeNodes->getLength();

								// Now look at all attributes of the station type
								for( XMLSize_t i = 0; i < attributeCount; ++i )
								{
									// Get a new child node (attribute)
									DOMNode* pCurAttribute = pAttributeNodes->item( i );

									// Check if it is an element node
                                    if(    (pCurAttribute/*->getNodeType()*/ != nullptr)
										&& (pCurAttribute->getNodeType() == DOMNode::ELEMENT_NODE) )
									{
										// Make this attribute node an element now
										DOMElement* pCurAttributeElement = static_cast<DOMElement*>( pCurAttribute );

										// Check if the found element is a priority attribute... 
										if( XMLString::equals( pCurAttributeElement->getTagName(), pTagAttribPriority ) )
										{
											//###############################################################################
											// Set priority attribute to station type here
											//###############################################################################
											const XMLCh* xmlchValue = pCurAttributeElement->getAttribute( pAttribValue );
											char* pValueString = XMLString::transcode( xmlchValue );
											
											// cout << "    "  << "    " << "priority: " << endl;
											pStationType->setHasPriority( true );
											// cout << "    "  << "    " << "value: " << pValueString << endl;
											pStationType->setPriority( atoi( pValueString ) );
											// cout << "    "  << "    " << endl;

											XMLString::release( &pValueString );
										}
										
										// ...or a frequency attribute
										else if( XMLString::equals( pCurAttributeElement->getTagName(), pTagAttribFrequency ) )
										{
											//###############################################################################
											// Set frequency attribute to station type here
											//###############################################################################
											const XMLCh* xmlchValue = pCurAttributeElement->getAttribute( pAttribValue );
											char* pValueString = XMLString::transcode( xmlchValue );
											
											// cout << "    "  << "    " << "frequency: " << endl;
											pStationType->setHasFrequency( true );
											// cout << "    "  << "    " << "value: " << pValueString << endl;
											pStationType->setFrequency( atoi( pValueString ) );
											// cout << "    "  << "    " << endl;
											
											XMLString::release( &pValueString );
										}
										
										// ...or a necessity attribute
										else if( XMLString::equals( pCurAttributeElement->getTagName(), pTagAttribNecessity ) )
										{
											//###############################################################################
											// Set necessity attribute to station type here
											//###############################################################################
											const XMLCh* xmlchValue = pCurAttributeElement->getAttribute( pAttribValue );
											char* pValueString = XMLString::transcode( xmlchValue );
											
											// cout << "    "  << "    " << "necessity: " << endl;
											pStationType->setHasNecessity( true );
											// cout << "    "  << "    " << "value: " << pValueString << endl;
											pStationType->setNecessity( atoi( pValueString ) );
											// cout << "    "  << "    " << endl;
											
											XMLString::release( &pValueString );
										}

										// ...or a time attribute
										else if( XMLString::equals( pCurAttributeElement->getTagName(), pTagAttribTime ) )
										{
											//###############################################################################
											// Set time attribute to station type here
											//###############################################################################
											const XMLCh* xmlchValue = pCurAttributeElement->getAttribute( pAttribValue );
											char* pValueString = XMLString::transcode( xmlchValue );
											
											// cout << "    "  << "    " << "time: " << endl;
											pStationType->setHasTime( true );
											// cout << "    "  << "    " << "value: " << pValueString << endl;
											pStationType->setTime( atoi( pValueString ) );
											// cout << "    "  << "    " << endl;
											
											XMLString::release( &pValueString );
										}
										
										// ...or a cycle attribute
										else if( XMLString::equals( pCurAttributeElement->getTagName(), pTagAttribCycle ) )
										{
											//###############################################################################
											// Set cycle attribute to station type here
											//###############################################################################
											const XMLCh* xmlchValue = pCurAttributeElement->getAttribute( pAttribValue );
											char* pValueString = XMLString::transcode( xmlchValue );
											
											// cout << "    "  << "    " << "cycle: " << endl;
											pStationType->setHasCycle( true );
											// cout << "    "  << "    " << "value: " << pValueString << endl;
											pStationType->setCycle( atoi( pValueString ) );
											// cout << "    "  << "    " << endl;
											
											XMLString::release( &pValueString );
										}

										// ...or an item attribute
										else if( XMLString::equals( pCurAttributeElement->getTagName(), pTagAttribItem ) )
										{
											//###############################################################################
											// Set item attribute to station type here
											//###############################################################################
											const XMLCh* xmlchValue = pCurAttributeElement->getAttribute( pAttribValue );
											char* pValueString = XMLString::transcode( xmlchValue );
											
											// cout << "    "  << "    " << "item: " << endl;
											pStationType->setHasItem( true );
											// cout << "    "  << "    " << "value: " << pValueString << endl;
											pStationType->setItem( atoi( pValueString ) );
											// cout << "    "  << "    " << endl;
											
											XMLString::release( &pValueString );
										}

										// ...or a space attribute
										else if( XMLString::equals( pCurAttributeElement->getTagName(), pTagAttribSpace ) )
										{
											//###############################################################################
											// Set space attribute to station type here
											//###############################################################################
											const XMLCh* xmlchValue = pCurAttributeElement->getAttribute( pAttribValue );
											char* pValueString = XMLString::transcode( xmlchValue );
											
											// cout << "    "  << "    " << "space: " << endl;
											pStationType->setHasSpace( true );
											// cout << "    "  << "    " << "value: " << pValueString << endl;
											pStationType->setSpace( atoi( pValueString ) );
											// cout << "    "  << "    " << endl;
											
											XMLString::release( &pValueString );
										}
									}
								}

								// Have to set position after attributes, because adding them will change 
								// position
								pStationType->setX( atoi( pXString ) );
								pStationType->setY( atoi( pYString ) );

								XMLString::release( &pXString );
								XMLString::release( &pYString );
							}
							// ... or an agent type
							else if( XMLString::equals( pCurComponentTypeElement->getTagName(), pTagAgentType ) )
							{
								//###############################################################################
								// Create new agent type with all its attributes an add it to perspective here
								//###############################################################################
								const XMLCh* xmlchId = pCurComponentTypeElement->getAttribute( pAttribId );
								const XMLCh* xmlchName = pCurComponentTypeElement->getAttribute( pAttribName );
								const XMLCh* xmlchX = pCurComponentTypeElement->getAttribute( pAttribX );
								const XMLCh* xmlchY = pCurComponentTypeElement->getAttribute( pAttribY );
								const XMLCh* xmlchCount = pCurComponentTypeElement->getAttribute( pAttribCount );
								const XMLCh* xmlchColorR = pCurComponentTypeElement->getAttribute( pAttribColorR );
								const XMLCh* xmlchColorG = pCurComponentTypeElement->getAttribute( pAttribColorG );
								const XMLCh* xmlchColorB = pCurComponentTypeElement->getAttribute( pAttribColorB );
								const XMLCh* xmlchInterface = pCurComponentTypeElement->getAttribute( pAttribInterface );
								char* pIdString = XMLString::transcode( xmlchId );
								char* pNameString = XMLString::transcode( xmlchName );
								char* pXString = XMLString::transcode( xmlchX );
								char* pYString = XMLString::transcode( xmlchY );
								char* pCountString = XMLString::transcode( xmlchCount );
								char* pColorRString = XMLString::transcode( xmlchColorR );
								char* pColorGString = XMLString::transcode( xmlchColorG );
								char* pColorBString = XMLString::transcode( xmlchColorB );
								char* pInterfaceString = XMLString::transcode( xmlchInterface );
								
								// cout << "    " << "agentType: " << endl;
								CAgentType* pAgentType = new CAgentType( *pPerspective, fpAddAgentToSimulation, fpRemoveAgentFromSimulation );
								// cout << "    " << "id: " << pIdString << endl;
								pAgentType->setId( atoi( pIdString ) );
								// cout << "    " << "name: " << pNameString << endl;
								pAgentType->setName( pNameString );
								
								// Have to set position after attributes, because adding them will change
								// position
								// cout << "    " << "x: " << pXString << endl;
								// pAgentType->setX( atoi( pXString ) );
								// cout << "    " << "y: " << pYString << endl;
								// pAgentType->setY( atoi( pYString ) );
								// cout << "    " << "count: " << pCountString << endl;
								
								pAgentType->setNoOfComponents( atoi( pCountString ) );
								// cout << "    " << "colorR: " << pColorRString << endl;
								pAgentType->color.setRed( atoi( pColorRString ) );
								// cout << "    " << "colorG: " << pColorGString << endl;
								pAgentType->color.setGreen( atoi( pColorGString ) );
								// cout << "    " << "colorB: " << pColorBString << endl;
								pAgentType->color.setBlue( atoi( pColorBString ) );
								// cout << "    " << "interface: " << pInterfaceString << endl;
								// TODO: SET THE INTERFACE ATTRIBUTE HERE!
								// cout << "    " << endl;
								pPerspective->componentTypes.push_back( pAgentType );
								
								XMLString::release( &pIdString );
								XMLString::release( &pNameString );
								// XMLString::release( &pXString );
								// XMLString::release( &pYString );
								XMLString::release( &pCountString );
								XMLString::release( &pColorRString );
								XMLString::release( &pColorGString );
								XMLString::release( &pColorBString );
								XMLString::release( &pInterfaceString );

								// Get pChildren of the agent type (attributes)
								DOMNodeList* pAttributeNodes = pCurComponentTypeNode->getChildNodes();
								const XMLSize_t attributeCount = pAttributeNodes->getLength();

								// Now look at all attributes of the agent type
								for( XMLSize_t i = 0; i < attributeCount; ++i )
								{
									// Get a new child node (attribute)
									DOMNode* pCurAttribute = pAttributeNodes->item( i );

									// Check if it is an element node
                                    if(    (pCurAttribute/*->getNodeType()*/ != nullptr)
										&& (pCurAttribute->getNodeType() == DOMNode::ELEMENT_NODE) )
									{
										// Make this attribute node an element now
										DOMElement* pCurAttributeElement = static_cast<DOMElement*>( pCurAttribute );

										// Check if the found element is a priority attribute... 
										if( XMLString::equals( pCurAttributeElement->getTagName(), pTagAttribPriority ) )
										{
											//###############################################################################
											// Set priority attribute to agent type here
											//###############################################################################
											const XMLCh* xmlchValue = pCurAttributeElement->getAttribute( pAttribValue );
											char* pValueString = XMLString::transcode( xmlchValue );
											
											// cout << "    "  << "    " << "priority: " << endl;
											pAgentType->setHasPriority( true );
											// cout << "    "  << "    " << "value: " << pValueString << endl;
											pAgentType->setPriority( atoi( pValueString ) );
											// cout << "    "  << "    " << endl;

											XMLString::release( &pValueString );
										}
										
										// ...or a frequency attribute
										else if( XMLString::equals( pCurAttributeElement->getTagName(), pTagAttribFrequency ) )
										{
											//###############################################################################
											// Set frequency attribute to agent type here
											//###############################################################################
											const XMLCh* xmlchValue = pCurAttributeElement->getAttribute( pAttribValue );
											char* pValueString = XMLString::transcode( xmlchValue );
											
											// cout << "    "  << "    " << "frequency: " << endl;
											pAgentType->setHasFrequency( true );
											// cout << "    "  << "    " << "value: " << pValueString << endl;
											pAgentType->setFrequency( atoi( pValueString ) );
											// cout << "    "  << "    " << endl;
											
											XMLString::release( &pValueString );
										}
										
										// ...or a necessity attribute
										else if( XMLString::equals( pCurAttributeElement->getTagName(), pTagAttribNecessity ) )
										{
											//###############################################################################
											// Set necessity attribute to agent type here
											//###############################################################################
											const XMLCh* xmlchValue = pCurAttributeElement->getAttribute( pAttribValue );
											char* pValueString = XMLString::transcode( xmlchValue );
											
											// cout << "    "  << "    " << "necessity: " << endl;
											pAgentType->setHasNecessity( true );
											// cout << "    "  << "    " << "value: " << pValueString << endl;
											pAgentType->setNecessity( atoi( pValueString ) );
											// cout << "    "  << "    " << endl;
											
											XMLString::release( &pValueString );
										}

										// ...or a time attribute
										else if( XMLString::equals( pCurAttributeElement->getTagName(), pTagAttribTime ) )
										{
											//###############################################################################
											// Set time attribute to agent type here
											//###############################################################################
											const XMLCh* xmlchValue = pCurAttributeElement->getAttribute( pAttribValue );
											char* pValueString = XMLString::transcode( xmlchValue );
											
											// cout << "    "  << "    " << "time: " << endl;
											pAgentType->setHasTime( true );
											// cout << "    "  << "    " << "value: " << pValueString << endl;
											pAgentType->setTime( atoi( pValueString ) );
											// cout << "    "  << "    " << endl;
											
											XMLString::release( &pValueString );
										}
										
										// ...or a cycle attribute
										else if( XMLString::equals( pCurAttributeElement->getTagName(), pTagAttribCycle ) )
										{
											//###############################################################################
											// Set cycle attribute to agent type here
											//###############################################################################
											const XMLCh* xmlchValue = pCurAttributeElement->getAttribute( pAttribValue );
											char* pValueString = XMLString::transcode( xmlchValue );
											
											// cout << "    "  << "    " << "cycle: " << endl;
											pAgentType->setHasCycle( true );
											// cout << "    "  << "    " << "value: " << pValueString << endl;
											pAgentType->setCycle( atoi( pValueString ) );
											// cout << "    "  << "    " << endl;
											
											XMLString::release( &pValueString );
										}

										// ...or an capacity attribute
										else if( XMLString::equals( pCurAttributeElement->getTagName(), pTagAttribCapacity ) )
										{
											//###############################################################################
											// Set capacity attribute to agent type here
											//###############################################################################
											const XMLCh* xmlchValue = pCurAttributeElement->getAttribute( pAttribValue );
											char* pValueString = XMLString::transcode( xmlchValue );
											
											// cout << "    "  << "    " << "capacity: " << endl;
											pAgentType->setHasCapacity( true );
											// cout << "    "  << "    " << "value: " << pValueString << endl;
											pAgentType->setCapacity( atoi( pValueString ) );
											// cout << "    "  << "    " << endl;
											
											XMLString::release( &pValueString );
										}

										// ...or a size attribute
										else if( XMLString::equals( pCurAttributeElement->getTagName(), pTagAttribSize ) )
										{
											//###############################################################################
											// Set size attribute to agent type here
											//###############################################################################
											const XMLCh* xmlchValue = pCurAttributeElement->getAttribute( pAttribValue );
											char* pValueString = XMLString::transcode( xmlchValue );
											
											// cout << "    "  << "    " << "size: " << endl;
											pAgentType->setHasSize( true );
											// cout << "    "  << "    " << "value: " << pValueString << endl;
											pAgentType->setSize( atoi( pValueString ) );
											// cout << "    "  << "    " << endl;
											
											XMLString::release( &pValueString );
										}

										// ...or a speed attribute
										else if( XMLString::equals( pCurAttributeElement->getTagName(), pTagAttribSpeed ) )
										{
											//###############################################################################
											// Set speed attribute to agent type here
											//###############################################################################
											const XMLCh* xmlchValue = pCurAttributeElement->getAttribute( pAttribValue );
											char* pValueString = XMLString::transcode( xmlchValue );
											
											// cout << "    "  << "    " << "speed: " << endl;
											pAgentType->setHasSpeed( true );
											// cout << "    "  << "    " << "value: " << pValueString << endl;
											pAgentType->setSpeed( atoi( pValueString ) );
											// cout << "    "  << "    " << endl;
											
											XMLString::release( &pValueString );
										}
									}
								}

								// Have to set position after attributes, because adding them will change
								// position
								pAgentType->setX( atoi( pXString ) );
								pAgentType->setY( atoi( pYString ) );

								XMLString::release( &pXString );
								XMLString::release( &pYString );
							}
						}
					}
				}
				
				// ... or if it is a visit edge
				else if( XMLString::equals( currentElement->getTagName(), pTagVisitEdge ) )
				{
					//########################################################
					// Create new visit egde and connect it as descriped here
					//########################################################
					const XMLCh* xmlchId = currentElement->getAttribute( pAttribId );
					const XMLCh* xmlchConnectedIdRef1 = currentElement->getAttribute( pAttribConnectedIdRef1 );
					const XMLCh* xmlchConnectedIdRef2 = currentElement->getAttribute( pAttribConnectedIdRef2 );
					const XMLCh* xmlchBold = currentElement->getAttribute( pAttribBold );
					char* pIdString = XMLString::transcode( xmlchId );
					char* pConnectedIdRef1String = XMLString::transcode( xmlchConnectedIdRef1 );
					char* pConnectedIdRef2String = XMLString::transcode( xmlchConnectedIdRef2 );
					char* pBoldString = XMLString::transcode( xmlchBold );
					
					// cout << "visit edge: " << endl;
					CVisitEdge* pVisitEdge = new CVisitEdge;
					// cout << "id: " << pIdString << endl;
					pVisitEdge->setId( atoi( pIdString ) );
					// cout << "connectedIdRef1: " << pConnectedIdRef1String << endl;
					CComponentType* pConnectedType1;
					pConnectedType1 = static_cast<CComponentType*>( that->getEditorObjFromId( atoi( pConnectedIdRef1String ) ) );
					pConnectedType1->addEdgeConnector( pVisitEdge->getConnector1() );
					// cout << "connectedIdRef2: " << pConnectedIdRef2String << endl;
					CComponentType* pConnectedType2;
					pConnectedType2 = static_cast<CComponentType*>( that->getEditorObjFromId( atoi( pConnectedIdRef2String ) ) );
					pConnectedType2->addEdgeConnector( pVisitEdge->getConnector2() );
					// cout << "bold: " << pBoldString << endl;
					if( string( pBoldString ) == XMLVALUE_YES )
						pVisitEdge->setIsBold( true );
					// cout << endl;
					that->edges.push_back( pVisitEdge );
					
					XMLString::release( &pIdString );
					XMLString::release( &pConnectedIdRef1String );
					XMLString::release( &pConnectedIdRef2String );
					XMLString::release( &pBoldString );
				}

				// ... or if it is a place edge
				else if( XMLString::equals( currentElement->getTagName(), pTagPlaceEdge ) )
				{
					//#######################################################################
					// Create new place egde, set its value and connect it as descriped here
					//#######################################################################
					const XMLCh* xmlchId = currentElement->getAttribute( pAttribId );
					const XMLCh* xmlchConnectedIdRef1 = currentElement->getAttribute( pAttribConnectedIdRef1 );
					const XMLCh* xmlchConnectedIdRef2 = currentElement->getAttribute( pAttribConnectedIdRef2 );
					const XMLCh* xmlchValue = currentElement->getAttribute( pAttribValue );
					const XMLCh* xmlchDirected = currentElement->getAttribute( pAttribDirected );
					char* pIdString = XMLString::transcode( xmlchId );
					char* pConnectedIdRef1String = XMLString::transcode( xmlchConnectedIdRef1 );
					char* pConnectedIdRef2String = XMLString::transcode( xmlchConnectedIdRef2 );
					char* pValueString = XMLString::transcode( xmlchValue );
					char* pDirectedString = XMLString::transcode( xmlchDirected );
					
					// cout << "place edge: " << endl;
					CPlaceEdge* pPlaceEdge = new CPlaceEdge;
					// cout << "id: " << pIdString << endl;
					pPlaceEdge->setId( atoi( pIdString ) );
					// cout << "connectedIdRef1: " << pConnectedIdRef1String << endl;
					CComponentType* pConnectedType1;
					pConnectedType1 = static_cast<CComponentType*>( that->getEditorObjFromId( atoi( pConnectedIdRef1String ) ) );
					pConnectedType1->addEdgeConnector( pPlaceEdge->getConnector1() );
					// cout << "connectedIdRef2: " << pConnectedIdRef2String << endl;
					CComponentType* pConnectedType2;
					pConnectedType2 = static_cast<CComponentType*>( that->getEditorObjFromId( atoi( pConnectedIdRef2String ) ) );
					pConnectedType2->addEdgeConnector( pPlaceEdge->getConnector2() );
					// cout << "value: " << pValueString << endl;
					pPlaceEdge->setValue( atoi( pValueString ) );
					// cout << "directed: " << pDirectedString << endl;
					if( string( pDirectedString ) == XMLVALUE_YES )
						pPlaceEdge->getConnector2().setIsDirected( true );
					// cout << endl;
					that->edges.push_back( pPlaceEdge );
					
					XMLString::release( &pIdString );
					XMLString::release( &pConnectedIdRef1String );
					XMLString::release( &pConnectedIdRef2String );
					XMLString::release( &pValueString );
					XMLString::release( &pDirectedString );
				}

				// ... or if it is a time edge
				else if( XMLString::equals( currentElement->getTagName(), pTagTimeEdge ) )
				{
					//######################################################################
					// Create new time egde, set its value and connect it as descriped here
					//######################################################################
					const XMLCh* xmlchId = currentElement->getAttribute( pAttribId );
					char* pIdString = XMLString::transcode( xmlchId );
					const XMLCh* xmlchConnectedIdRef1 = currentElement->getAttribute( pAttribConnectedIdRef1 );
					char* pConnectedIdRef1String = XMLString::transcode( xmlchConnectedIdRef1 );
					const XMLCh* xmlchConnectedIdRef2 = currentElement->getAttribute( pAttribConnectedIdRef2 );
					char* pConnectedIdRef2String = XMLString::transcode( xmlchConnectedIdRef2 );
					const XMLCh* xmlchValue = currentElement->getAttribute( pAttribValue );
					char* pValueString = XMLString::transcode( xmlchValue );
					const XMLCh* xmlchDirected = currentElement->getAttribute( pAttribDirected );
					char* pDirectedString = XMLString::transcode( xmlchDirected );
					const XMLCh* xmlchAndConnected1 = currentElement->getAttribute( pAttribAndConnected1 );
					char* pAndConnected1String = XMLString::transcode( xmlchAndConnected1 );
					const XMLCh* xmlchAndConnected2 = currentElement->getAttribute( pAttribAndConnected2 );
					char* pAndConnected2String = XMLString::transcode( xmlchAndConnected2 );
					
					// cout << "time edge: " << endl;
					CTimeEdge* pTimeEdge = new CTimeEdge;
					// cout << "id: " << pIdString << endl;
					pTimeEdge->setId( atoi( pIdString ) );
					// cout << "connectedIdRef1: " << pConnectedIdRef1String << endl;
					CComponentType* pConnectedType1;
					pConnectedType1 = static_cast<CComponentType*>( that->getEditorObjFromId( atoi( pConnectedIdRef1String ) ) );
					pConnectedType1->addEdgeConnector( pTimeEdge->getConnector1() );
					// cout << "connectedIdRef2: " << pConnectedIdRef2String << endl;
					CComponentType* pConnectedType2;
					pConnectedType2 = static_cast<CComponentType*>( that->getEditorObjFromId( atoi( pConnectedIdRef2String ) ) );
					pConnectedType2->addEdgeConnector( pTimeEdge->getConnector2() );
					// cout << "value: " << pValueString << endl;
					pTimeEdge->setValue( atoi( pValueString ) );
					// cout << "directed: " << pDirectedString << endl;
					if( string( pDirectedString ) == XMLVALUE_YES )
						pTimeEdge->setIsDirected2( true );
					// cout << "andConnected1: " << pAndConnected1String << endl;
					if( string( pAndConnected1String ) == XMLVALUE_YES )
						pTimeEdge->setIsAndConnection1( true );
					// cout << "andConnected2: " << pAndConnected2String << endl;
					if( string( pAndConnected2String ) == XMLVALUE_YES )
						pTimeEdge->setIsAndConnection2( true );
					// cout << endl;
					that->edges.push_back( pTimeEdge );
					
					XMLString::release( &pIdString );
					XMLString::release( &pConnectedIdRef1String );
					XMLString::release( &pConnectedIdRef2String );
					XMLString::release( &pValueString );
					XMLString::release( &pDirectedString );
					XMLString::release( &pAndConnected1String );
					XMLString::release( &pAndConnected2String );
				}
			}
		}

		// Free tags
		XMLString::release( &pTagRoot );
		XMLString::release( &pTagPerspective );
		XMLString::release( &pTagStationType );
		XMLString::release( &pTagAgentType );
		XMLString::release( &pTagVisitEdge );
		XMLString::release( &pTagPlaceEdge );
		XMLString::release( &pTagTimeEdge );
		XMLString::release( &pTagAttribPriority );
		XMLString::release( &pTagAttribFrequency );
		XMLString::release( &pTagAttribNecessity );
		XMLString::release( &pTagAttribTime );
		XMLString::release( &pTagAttribCycle );
		XMLString::release( &pTagAttribItem );
		XMLString::release( &pTagAttribSpace );
		XMLString::release( &pTagAttribCapacity );
		XMLString::release( &pTagAttribSize );
		XMLString::release( &pTagAttribSpeed );

		// Free attributes
		XMLString::release( &pAttribId );
		XMLString::release( &pAttribName );
		XMLString::release( &pAttribX );
		XMLString::release( &pAttribY );
		XMLString::release( &pAttribWidth );
		XMLString::release( &pAttribHeight );
		XMLString::release( &pAttribCount );
		XMLString::release( &pAttribColorR );
		XMLString::release( &pAttribColorG );
		XMLString::release( &pAttribColorB );
		XMLString::release( &pAttribInterface );
		XMLString::release( &pAttribConnectedIdRef1 );
		XMLString::release( &pAttribConnectedIdRef2 );
		XMLString::release( &pAttribBold );
		XMLString::release( &pAttribValue );
		XMLString::release( &pAttribDirected );
		XMLString::release( &pAttribAndConnected1 );
		XMLString::release( &pAttribAndConnected2 );


		// Terminate Xerces
		try
		{
			delete pXercesDOMParser;
			
			XMLPlatformUtils::Terminate();  
		}
		catch( const XMLException& e )
		{
			// Create new message...
			char* pMessage = XMLString::transcode( e.getMessage() );
			string sMessageComplete( string( "Xerces termination error: " )+pMessage );
			XMLString::release( &pMessage );

/*
			// And pass it on as char* exception for access from host application
			char* pMessageComplete = new char[ sMessageComplete.length()+1 ];
			strcpy( pMessageComplete, sMessageComplete.c_str() );
			
			throw( pMessageComplete );
*/
			return( false );
		}
	}
	catch( const XMLException& e )
	{
		// Create new message...
		char* pMessage = XMLString::transcode( e.getMessage() );
		string sMessageComplete( string( "Xerces parse error: " )+pMessage );
		XMLString::release( &pMessage );
		
/*
		// And pass it on as char* exception for access from host application
		char* pMessageComplete = new char[ sMessageComplete.length()+1 ];
		strcpy( pMessageComplete, sMessageComplete.c_str() );
		throw( pMessageComplete );
*/
		return( false );
	}

	
	//
	// Loading was successful! 
	//

	that->scrollToSceneMiddle();

	return( true );
}
