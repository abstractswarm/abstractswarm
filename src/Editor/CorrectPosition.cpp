//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Constants.h"
#include "Perspective.h"
#include "StationType.h"
#include "AgentType.h"
//#pragma c-mol_hdrstop

//#include "CorrectPosition.hmol"
#include "CorrectPosition.hpp"
//#include "CheckPosition.hmol"
#include "CheckPosition.hpp"





//------------------------------------------------------------
// Static data
//------------------------------------------------------------


static bool isValidPositionFound = false;





//------------------------------------------------------------
// Prologue
//------------------------------------------------------------


//MCorrectPosition::MCorrectPosition()
void MCorrectPosition::prologue( CPerspective& that, METHOD_PARAMS_CORRECTPOSITION )
{
    isValidPositionFound = false;

    // Check if current position is valid
    int x = that.getX();
    int y = that.getY();
//    bool isValidPosition = MCheckPosition( objectsToCheck, maxWidth, maxHeight, x, y)°that();
    bool isValidPosition = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, x, y );
    if( isValidPosition )
        isValidPositionFound = true;

    // Do concentric squares clockwise until empty place is found or no more place available
    int walkAmount = 0;
    while( (!isValidPositionFound) && (((x-walkAmount) >= 0) || ((y-walkAmount) >= 0) || ((x+walkAmount) <= maxWidth) || ((y+walkAmount) <= maxHeight)) )
    {
        walkAmount++;

        // Init offsets
        int xOffset = -walkAmount;
        int yOffset = -walkAmount;

        // Init step size
        int stepSize = 1;

        // Walk to right
        if( !isValidPositionFound )
        {
            for( xOffset = -walkAmount; xOffset < walkAmount; xOffset+=stepSize )
            {
                // Check if position is valid
                int x = that.getX();
                int y = that.getY();
//                bool isValidPosition = MCheckPosition( objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset )°that();
                bool isValidPosition = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset );

                if( isValidPosition )
                {
                    isValidPositionFound = true;
                    that.setX( x+xOffset );
                    that.setY( y+yOffset );
                    break;
                }
            }
        }

        // Walk to bottom
        if( !isValidPositionFound )
        {
            for( yOffset = -walkAmount; yOffset < walkAmount; yOffset+=stepSize )
            {
                // Check if position is valid
                int x = that.getX();
                int y = that.getY();
//                bool isValidPosition = MCheckPosition( objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset )°that();
                bool isValidPosition = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset );

                if( isValidPosition )
                {
                    isValidPositionFound = true;
                    that.setX( x+xOffset );
                    that.setY( y+yOffset );
                    break;
                }
            }
        }

        // Walk to left
        if( !isValidPositionFound )
        {
            for( xOffset = walkAmount; xOffset > -walkAmount; xOffset=xOffset-stepSize )
            {
                // Check if position is valid
                int x = that.getX();
                int y = that.getY();
//                bool isValidPosition = MCheckPosition( objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset )°that();
                bool isValidPosition = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset );

                if( isValidPosition )
                {
                    isValidPositionFound = true;
                    that.setX( x+xOffset );
                    that.setY( y+yOffset );
                    break;
                }
            }
        }

        // Walk to up
        if( !isValidPositionFound )
        {
            for( yOffset = walkAmount; yOffset > -walkAmount; yOffset=yOffset-stepSize )
            {
                // Check if position is valid
                int x = that.getX();
                int y = that.getY();
//                bool isValidPosition = MCheckPosition( objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset )°that();
                bool isValidPosition = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset );

                if( isValidPosition )
                {
                    isValidPositionFound = true;
                    that.setX( x+xOffset );
                    that.setY( y+yOffset );
                    break;
                }
            }
        }
    }
}
void MCorrectPosition::prologue( CStationType& that, METHOD_PARAMS_CORRECTPOSITION )
{
    isValidPositionFound = false;

    // Check if current position is valid
    int x = that.getX();
    int y = that.getY();
//    bool isValidPosition = MCheckPosition( objectsToCheck, maxWidth, maxHeight, x, y)°that();
    bool isValidPosition = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, x, y );
    if( isValidPosition )
        isValidPositionFound = true;

    // Do concentric squares clockwise until empty place is found or no more place available
    int walkAmount = 0;
    while( (!isValidPositionFound) && (((x-walkAmount) >= 0) || ((y-walkAmount) >= 0) || ((x+walkAmount) <= maxWidth) || ((y+walkAmount) <= maxHeight)) )
    {
        walkAmount++;

        // Init offsets
        int xOffset = -walkAmount;
        int yOffset = -walkAmount;

        // Init step size
        int stepSize = 1;

        // Walk to right
        if( !isValidPositionFound )
        {
            for( xOffset = -walkAmount; xOffset < walkAmount; xOffset+=stepSize )
            {
                // Check if position is valid
                int x = that.getX();
                int y = that.getY();
//                bool isValidPosition = MCheckPosition( objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset )°that();
                bool isValidPosition = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset );

                if( isValidPosition )
                {
                    isValidPositionFound = true;
                    that.setX( x+xOffset );
                    that.setY( y+yOffset );
                    break;
                }
            }
        }

        // Walk to bottom
        if( !isValidPositionFound )
        {
            for( yOffset = -walkAmount; yOffset < walkAmount; yOffset+=stepSize )
            {
                // Check if position is valid
                int x = that.getX();
                int y = that.getY();
//                bool isValidPosition = MCheckPosition( objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset )°that();
                bool isValidPosition = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset );

                if( isValidPosition )
                {
                    isValidPositionFound = true;
                    that.setX( x+xOffset );
                    that.setY( y+yOffset );
                    break;
                }
            }
        }

        // Walk to left
        if( !isValidPositionFound )
        {
            for( xOffset = walkAmount; xOffset > -walkAmount; xOffset=xOffset-stepSize )
            {
                // Check if position is valid
                int x = that.getX();
                int y = that.getY();
//                bool isValidPosition = MCheckPosition( objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset )°that();
                bool isValidPosition = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset );

                if( isValidPosition )
                {
                    isValidPositionFound = true;
                    that.setX( x+xOffset );
                    that.setY( y+yOffset );
                    break;
                }
            }
        }

        // Walk to up
        if( !isValidPositionFound )
        {
            for( yOffset = walkAmount; yOffset > -walkAmount; yOffset=yOffset-stepSize )
            {
                // Check if position is valid
                int x = that.getX();
                int y = that.getY();
//                bool isValidPosition = MCheckPosition( objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset )°that();
                bool isValidPosition = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset );

                if( isValidPosition )
                {
                    isValidPositionFound = true;
                    that.setX( x+xOffset );
                    that.setY( y+yOffset );
                    break;
                }
            }
        }
    }
}
void MCorrectPosition::prologue( CAgentType& that, METHOD_PARAMS_CORRECTPOSITION )
{
    isValidPositionFound = false;

    // Check if current position is valid
    int x = that.getX();
    int y = that.getY();
//    bool isValidPosition = MCheckPosition( objectsToCheck, maxWidth, maxHeight, x, y)°that();
    bool isValidPosition = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, x, y );
    if( isValidPosition )
        isValidPositionFound = true;

    // Do concentric squares clockwise until empty place is found or no more place available
    int walkAmount = 0;
    while( (!isValidPositionFound) && (((x-walkAmount) >= 0) || ((y-walkAmount) >= 0) || ((x+walkAmount) <= maxWidth) || ((y+walkAmount) <= maxHeight)) )
    {
        walkAmount++;

        // Init offsets
        int xOffset = -walkAmount;
        int yOffset = -walkAmount;

        // Init step size
        int stepSize = 1;

        // Walk to right
        if( !isValidPositionFound )
        {
            for( xOffset = -walkAmount; xOffset < walkAmount; xOffset+=stepSize )
            {
                // Check if position is valid
                int x = that.getX();
                int y = that.getY();
//                bool isValidPosition = MCheckPosition( objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset )°that();
                bool isValidPosition = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset );

                if( isValidPosition )
                {
                    isValidPositionFound = true;
                    that.setX( x+xOffset );
                    that.setY( y+yOffset );
                    break;
                }
            }
        }

        // Walk to bottom
        if( !isValidPositionFound )
        {
            for( yOffset = -walkAmount; yOffset < walkAmount; yOffset+=stepSize )
            {
                // Check if position is valid
                int x = that.getX();
                int y = that.getY();
//                bool isValidPosition = MCheckPosition( objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset )°that();
                bool isValidPosition = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset );

                if( isValidPosition )
                {
                    isValidPositionFound = true;
                    that.setX( x+xOffset );
                    that.setY( y+yOffset );
                    break;
                }
            }
        }

        // Walk to left
        if( !isValidPositionFound )
        {
            for( xOffset = walkAmount; xOffset > -walkAmount; xOffset=xOffset-stepSize )
            {
                // Check if position is valid
                int x = that.getX();
                int y = that.getY();
//                bool isValidPosition = MCheckPosition( objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset )°that();
                bool isValidPosition = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset );

                if( isValidPosition )
                {
                    isValidPositionFound = true;
                    that.setX( x+xOffset );
                    that.setY( y+yOffset );
                    break;
                }
            }
        }

        // Walk to up
        if( !isValidPositionFound )
        {
            for( yOffset = walkAmount; yOffset > -walkAmount; yOffset=yOffset-stepSize )
            {
                // Check if position is valid
                int x = that.getX();
                int y = that.getY();
//                bool isValidPosition = MCheckPosition( objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset )°that();
                bool isValidPosition = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, x+xOffset, y+yOffset );

                if( isValidPosition )
                {
                    isValidPositionFound = true;
                    that.setX( x+xOffset );
                    that.setY( y+yOffset );
                    break;
                }
            }
        }
    }
}


//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//bool MCorrectPosition::<CPerspective>()
bool MCorrectPosition::handling( CPerspective& that, METHOD_PARAMS_CORRECTPOSITION )
{
    prologue( that, objectsToCheck, maxWidth, maxHeight );

    return( isValidPositionFound );
}


//bool MCorrectPosition::<CStationType, CAgentType>()
bool MCorrectPosition::handling( CStationType& that, METHOD_PARAMS_CORRECTPOSITION )
{
    prologue( that, objectsToCheck, maxWidth, maxHeight );

    return( isValidPositionFound );
}
bool MCorrectPosition::handling( CAgentType& that, METHOD_PARAMS_CORRECTPOSITION )
{
    prologue( that, objectsToCheck, maxWidth, maxHeight );

    return( isValidPositionFound );
}
