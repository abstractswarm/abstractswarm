#if !defined LoadXmlFile_hmol
	#define LoadXmlFile_hmol





#include "AgentType.h"
#include "StationType.h"
#include <string>





using namespace std;





class CEditor;





/**
 * Loads an XML-file using the Xerces-C++ library.
 *
 * @param sFileName the path and the name of the file to be loaded
 */
//method MLoadXmlFile( string sFileName )
class MLoadXmlFile
{
#define METHOD_PARAMS_LOADXMLFILE string sFileName

public:

	/**
	 * Loads an XML-file to the editor without being connected to simulation (simulation will be empty after loading).
	 */
//	bool <CEditor*>();
    static bool handling( CEditor* that, METHOD_PARAMS_LOADXMLFILE );

	/**
	 * Loads an XML-file to the editor and adds the components contained in the component types to simulation (simulation is ready to launch afterwards).
	 * 
	 * @param fpAddAgentToSimulation pointer to the function to add an agent to the simulation
	 * @param fpRemoveAgentFromSimulation pointer to the function to remove an agent from the simulation
	 * @param fpAddStationToSimulation pointer to the function to add a station to the simulation
	 * @param fpRemoveStationFromSimulation pointer to the function to remove a station from the simulation
	 */
//	bool <CEditor*>( TfpAddAgentToSimulation fpAddAgentToSimulation, TfpRemoveAgentFromSimulation fpRemoveAgentFromSimulation, TfpAddStationToSimulation fpAddStationToSimulation, TfpRemoveStationFromSimulation fpRemoveStationFromSimulation );
    static bool handling( CEditor* that, METHOD_PARAMS_LOADXMLFILE, TfpAddAgentToSimulation fpAddAgentToSimulation, TfpRemoveAgentFromSimulation fpRemoveAgentFromSimulation, TfpAddStationToSimulation fpAddStationToSimulation, TfpRemoveStationFromSimulation fpRemoveStationFromSimulation );
};





#endif
