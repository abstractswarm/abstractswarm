/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "ComponentType.h"
#include "Constants.h"
#include "Edge.h"





//------------------------------------------------------------
// Construction
//------------------------------------------------------------


CComponentType::CComponentType( const CPerspective& owner ) : color( COMPONENTTYPE_DEF_RED, COMPONENTTYPE_DEF_GREEN, COMPONENTTYPE_DEF_BLUE ),
                                                              owner( owner )
{
	setWidth( COMPONENTTYPE_WIDTH );
	setHeight( COMPONENTTYPE_HEIGHT );

	// Init optional component type attributes
	hasPriority = false;
	priority = 1;
	hasFrequency = false;
	frequency = 1;
	hasNecessity = false;
	necessity = 1;
	hasTime = false;
	time = 1;
	hasCycle = false;
	cycle = 1;

	// Initialize the draw factor (used for scaling of component types in simulation)
	drawFactor = 1.0;
}


//------------------------------------------------------------
// Destruction
//------------------------------------------------------------


CComponentType::~CComponentType()
{
	// Erase all connectors of this component type
	list<CEdgeConnector*>::iterator itEdgeConnector;
	for( itEdgeConnector = edgeConnectors.begin(); itEdgeConnector != edgeConnectors.end(); itEdgeConnector++ )
        (*itEdgeConnector)->pComponentType = nullptr;
}


//------------------------------------------------------------
// Getter
//------------------------------------------------------------


const CPerspective& CComponentType::getOwner() const
{
	return( owner );
}


const list<CEdgeConnector*>& CComponentType::getEdgeConnectors() const
{
	return( edgeConnectors );
}


double CComponentType::getDrawFactor() const
{
	return( drawFactor );
}


//------------------------------------------------------------
// Optional component type attribute getter
//------------------------------------------------------------


bool CComponentType::getHasAttribs() const
{
	return( (hasPriority || hasFrequency || hasNecessity || hasTime || hasCycle) );
}


int CComponentType::getNoOfAttribs() const
{
	int attribsCount = 0;

	if( hasPriority )
		attribsCount++;
	if( hasFrequency )
		attribsCount++;
	if( hasNecessity )
		attribsCount++;
	if( hasTime )
		attribsCount++;
	if( hasCycle )
		attribsCount++;
	
	return( attribsCount );
}


bool CComponentType::getHasPriority() const
{
	return( hasPriority );
}


int CComponentType::getPriority() const
{
	return( priority );
}


bool CComponentType::getHasFrequency() const
{
	return( hasFrequency );
}


int CComponentType::getFrequency() const
{
	return( frequency );
}


bool CComponentType::getHasNecessity() const
{
	return( hasNecessity );
}


int CComponentType::getNecessity() const
{
	return( necessity );
}


bool CComponentType::getHasTime() const
{
	return( hasTime );
}


int CComponentType::getTime() const
{
	return( time );
}


bool CComponentType::getHasCycle() const
{
	return( hasCycle );
}


int CComponentType::getCycle() const
{
	return( cycle );
}


//------------------------------------------------------------
// Setter
//------------------------------------------------------------


void CComponentType::setWidth( int width )
{
	CGraphObj::setWidth( width );
}


void CComponentType::setHeight( int height)
{
	CGraphObj::setHeight( height );
}


void CComponentType::setDrawFactor( double factor )
{
	drawFactor = factor;
}


//------------------------------------------------------------
// Optional component type attribute setter
//------------------------------------------------------------


void CComponentType::setHasPriority( bool hasPriority )
{
	this->hasPriority = hasPriority;
	priority = 1;

	// Resize component type
	resize();		
}


void CComponentType::setPriority( int priority )
{
	this->priority = priority;
}


void CComponentType::setHasFrequency( bool hasFrequency )
{
	this->hasFrequency = hasFrequency;
	setFrequency( 1 );

	// Resize component type
	resize();		
}


void CComponentType::setFrequency( int frequency )
{
	this->frequency = frequency;

	// Update the components of this component type
	updateComponents();
}


void CComponentType::setHasNecessity( bool hasNecessity )
{
	this->hasNecessity = hasNecessity;
	setNecessity( 1 );

	// Resize component type
	resize();		
}


void CComponentType::setNecessity( int necessity )
{
	this->necessity = necessity;

	// Update the components of this component type
	updateComponents();
}


void CComponentType::setHasTime( bool hasTime )
{
	this->hasTime = hasTime;
	setTime( 1 );

	// Resize component type
	resize();		
}


void CComponentType::setTime( int time )
{
	this->time = time;

	// Update the components of this component type
	updateComponents();
}


void CComponentType::setHasCycle( bool hasCycle )
{
	this->hasCycle = hasCycle;
	cycle = 1;

	// Resize component type
	resize();		
}


void CComponentType::setCycle( int cycle )
{
	this->cycle = cycle;
}


//------------------------------------------------------------
// Add an edge connector
//------------------------------------------------------------


void CComponentType::addEdgeConnector( CEdgeConnector& edgeConnector )
{
	// Set this component type as owner
	edgeConnector.pComponentType = this;
	
	// Add to edge connectors
	edgeConnectors.push_back( &edgeConnector );

	// If the connection is complete...
    if( &(edgeConnector.getLinkedConnector().getComponentType()) != nullptr )
	{
		// Update the components of this component type 
		updateComponents();

		// And update the connected component type's components too
		const_cast<CComponentType&>( edgeConnector.getLinkedConnector().getComponentType() ).updateComponents();
	}
}


//------------------------------------------------------------
// Remove an edge connector
//------------------------------------------------------------


void CComponentType::removeEdgeConnector( CEdgeConnector& edgeConnector )
{
	// Search for the given edge connector	
	list<CEdgeConnector*>::iterator itEdgeConnector;
	for( itEdgeConnector = edgeConnectors.begin(); itEdgeConnector != edgeConnectors.end(); itEdgeConnector++ )
	{
		// If found, remove it and disconnect it
		if( (*itEdgeConnector) == (&edgeConnector) )
		{
            edgeConnector.pComponentType = nullptr;
			edgeConnectors.erase( itEdgeConnector );
			break;
		}
	}

	// If the connection was complete...
    if( &(edgeConnector.getLinkedConnector().getComponentType()) != nullptr )
	{
		// Update the components of this component type 
		updateComponents();

		// And update the connected component type's components too
		const_cast<CComponentType&>( edgeConnector.getLinkedConnector().getComponentType() ).updateComponents();
	}
}


/**
 * Returns the component type's edge connnectors
 */
const list<CEdgeConnector*>& CComponentType::getEdgeConnectors()
{
	return( edgeConnectors );
}


void CComponentType::updateConnectedComponents()
{
	list<CEdgeConnector*>::const_iterator itEdgeConnector;
	for( itEdgeConnector = getEdgeConnectors().begin(); itEdgeConnector != getEdgeConnectors().end(); itEdgeConnector++ )
	{
        if( (&((*itEdgeConnector)->getLinkedConnector().getComponentType())) != nullptr )
			const_cast<CComponentType&>( (*itEdgeConnector)->getLinkedConnector().getComponentType() ).updateComponents();
	}
}


//------------------------------------------------------------
// Resize component type
//------------------------------------------------------------


void CComponentType::resize()
{
	int attribsCount = this->getNoOfAttribs();
	int newWidth = COMPONENTTYPE_WIDTH;
	int newHeight = COMPONENTTYPE_HEIGHT;
	
	if( attribsCount >= 1 )
	{	
		newWidth++;
	}
	newHeight += (attribsCount+1)/2; 
	
	if( getHeight() < newHeight )
		setY( getY()-1 );
	else if( getHeight() > newHeight )
		setY( getY()+1 );

	setWidth( newWidth );
	setHeight( newHeight ); 
}
