/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Editor.h"


namespace EditorDll
{
    //------------------------------------------------------------
    // Construction/Destruction
    //------------------------------------------------------------

    void createEditor();

    
    void deleteEditor();

    
    
    //------------------------------------------------------------
    // Events
    //------------------------------------------------------------

    ENEditorMessage click();
    
    
    void doubleClick();
    
    
    void mouseDown( ENMouseButton mouseButton );
    
    
    void mouseMove( int x, int y );
    
    
    ENEditorMessage mouseUp( ENMouseButton mouseButton );
    
    
    void keyPress( char key );
    
    
    void keyDown( ENKey key );
    
    
    void keyUp( ENKey key );


    
    //------------------------------------------------------------
    // Layout independant functionality and state setters
    //------------------------------------------------------------

    void setSelectedTool( ENTool tool );
    
    
    void setScrollX( int percentScrollX );
    
    
    void setScrollY( int percentScrollY );
    
    
    double setZoom( double zoomFactor, int& percentScrollX, int& percentScrollY );
    
    
    void clear();
    
    
    bool load( char* pFileName );
    
    
    void save( char* pFileName );
    
    
    void setVisibleFrameSize( int width, int height );

    
    
    //------------------------------------------------------------
    // UI and simulation communication
    //------------------------------------------------------------

    ENEditorObjType getSelectedObjData( UEditorObjData selectedObjData[] );
    
    
    void setSelectedObjResults( UEditorObjData selectedObjData[] );


    void connectToSimulation( TfpAddStationToSimulation fpAddStationToSimulation, TfpRemoveStationFromSimulation fpRemoveStationFromSimulation, TfpAddAgentToSimulation fpAddAgentToSimulation, TfpRemoveAgentFromSimulation fpRemoveAgentFromSimulation );


    
    //------------------------------------------------------------
    // Platform dependent(!) interface for visualization
    //------------------------------------------------------------

    void draw( HDC graphicContext );
}
