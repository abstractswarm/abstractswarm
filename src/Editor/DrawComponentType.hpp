/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined DrawComponentType_hmol
	#define DrawComponentType_hmol


#include <typeinfo>
#include "windows.h"
#include "AgentType.h"
#include "StationType.h"





//method MDrawComponentType( HDC graphicContext, int screenX, int screenY, double zoomFactor )
class MDrawComponentType
{
#define METHOD_PARAMS_DRAWCOMPONENTTYPE HDC graphicContext, int screenX, int screenY, double zoomFactor

public:

    // Prologue
//	MDrawComponentType();
    static void prologue( CComponentType* that, METHOD_PARAMS_DRAWCOMPONENTTYPE );

	// Epilogue
//	~MDrawComponentType();
    static void epilogue( CComponentType* that, METHOD_PARAMS_DRAWCOMPONENTTYPE );

	// Handlings
//	virtual void <CStationType*>();
    static void handling( CStationType* that, METHOD_PARAMS_DRAWCOMPONENTTYPE );

//	virtual void <CAgentType*>();
    static void handling( CAgentType* that, METHOD_PARAMS_DRAWCOMPONENTTYPE );

    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline void handling( void* vp_that, METHOD_PARAMS_DRAWCOMPONENTTYPE )
    {
        if( typeid( *(static_cast<CStationType*>( vp_that ))) == typeid( CStationType ) )
            handling( static_cast<CStationType*>( vp_that ), graphicContext, screenX, screenY, zoomFactor );
        else if( typeid( *(static_cast<CAgentType*>( vp_that ))) == typeid( CAgentType ) )
            handling( static_cast<CAgentType*>( vp_that ), graphicContext, screenX, screenY, zoomFactor );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }
};





#endif
