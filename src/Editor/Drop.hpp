#if !defined Drop_hmol
	#define Drop_hmol





//#include "DropGraphObj.hmol"
#include "DropGraphObj.hpp"
//#include "DropEdge.hmol"
#include "DropEdge.hpp"





class CPerspective;
class CStationType;
class CAgentType;
class CEditorObj;





//method MDrop( CEditorObj* pDragTarget,
//			  const void* pDraggedObjOwner,
//			  bool isDraggedLeft,
//			  bool isDraggedTop,
//			  bool isDraggedRight,
//			  bool isDraggedBottom ) : MDropGraphObj( CEditorObj* pDragTarget,
//			                                          const void* pDraggedObjOwner,
//													  bool isDraggedLeft,
//			                                          bool isDraggedTop,
//			                                          bool isDraggedRight,
//			                                          bool isDraggedBottom ),
//									   MDropEdge( CEditorObj* pNewConnectedType )
class MDrop : public MDropGraphObj, public MDropEdge
{
#define METHOD_PARAMS_DROP CEditorObj* pDragTarget, const void* pDraggedObjOwner, bool isDraggedLeft, bool isDraggedTop, bool isDraggedRight, bool isDraggedBottom

public:

	// Epilogue
//	~MDrop();
    static void epilogue( CEditorObj* that, METHOD_PARAMS_DROP );

    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline bool handling( void* vp_that, METHOD_PARAMS_DROP )
    {
        if( typeid( *(static_cast<CPerspective*>( vp_that ))) == typeid( CPerspective ) )
            return( handling( static_cast<CPerspective*>( vp_that ), pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom ) );
        else if( typeid( *(static_cast<CAgentType*>( vp_that ))) == typeid( CAgentType ) )
            return( handling( static_cast<CAgentType*>( vp_that ), pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom ) );
        else if( typeid( *(static_cast<CStationType*>( vp_that ))) == typeid( CStationType ) )
            return( handling( static_cast<CStationType*>( vp_that ), pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom ) );
        else if( typeid( *(static_cast<CVisitEdge*>( vp_that ))) == typeid( CVisitEdge ) )
            return( handling( static_cast<CVisitEdge*>( vp_that ), pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom ) );
        else if( typeid( *(static_cast<CPlaceEdge*>( vp_that ))) == typeid( CPlaceEdge ) )
            return( handling( static_cast<CPlaceEdge*>( vp_that ), pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom ) );
        else if( typeid( *(static_cast<CTimeEdge*>( vp_that ))) == typeid( CTimeEdge ) )
            return( handling( static_cast<CTimeEdge*>( vp_that ), pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom ) );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }


    //
    // C-MOL INHERITANCE MECHANISM
    // (DO NOT FORGET TO OVERWRITE ALL INHERITED HANDLINGS HERE FOR SUPER-PROLOGUE/-EPILOGUE CALLS!)
    //

    static inline bool handling( CPerspective* that, METHOD_PARAMS_DROP )
    {
        bool tmp = MDropGraphObj::handling( that, pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
        epilogue( that, pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
        return( tmp );
    }

    static inline bool handling( CStationType* that, METHOD_PARAMS_DROP )
    {
        bool tmp = MDropGraphObj::handling( that, pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
        epilogue( that, pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
        return( tmp );
    }

    static inline bool handling( CAgentType* that, METHOD_PARAMS_DROP )
    {
        bool tmp = MDropGraphObj::handling( that, pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
        epilogue( that, pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
        return( tmp );
    }

    static inline bool handling( CVisitEdge* that, METHOD_PARAMS_DROP )
    {
        bool tmp = MDropEdge::handling( that, pDragTarget );
        epilogue( that, pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
        return( tmp );
    }

    static inline bool handling( CPlaceEdge* that, METHOD_PARAMS_DROP )
    {
        bool tmp = MDropEdge::handling( that, pDragTarget );
        epilogue( that, pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
        return( tmp );
    }

    static inline bool handling( CTimeEdge* that, METHOD_PARAMS_DROP )
    {
        bool tmp = MDropEdge::handling( that, pDragTarget );
        epilogue( that, pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
        return( tmp );
    }
};





#endif
