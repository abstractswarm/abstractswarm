/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Perspective.h"
#include "Constants.h"

#include <sstream>





using namespace std;





//------------------------------------------------------------
// Static Init
//------------------------------------------------------------


int CPerspective::noOfInstances = 0;


//------------------------------------------------------------
// Construction
//------------------------------------------------------------


CPerspective::CPerspective() : grid( GRID_XSIZE, GRID_YSIZE )
{
	// Count instances
	noOfInstances++;

	// Set the name
	ostringstream ossNoOfInstances; 
	ossNoOfInstances << noOfInstances;
	setName( string( "Perspective" )+ossNoOfInstances.str() );
}


//------------------------------------------------------------
// Destruction
//------------------------------------------------------------


CPerspective::~CPerspective()
{
	// Delete all component types
	list<CComponentType*>::iterator itComponentType;
	for( itComponentType = componentTypes.begin(); itComponentType != componentTypes.end(); itComponentType++ )
	{
		delete (*itComponentType);
	}
}


//------------------------------------------------------------
// Getter
//------------------------------------------------------------


int CPerspective::getNoOfInstances() const
{
	return( noOfInstances );
}


int CPerspective::getContentWidth() const
{
	// Calculate smallest and biggest x coordinates in the perspective frame
	int smallestContentX = getWidth();  // Init with sure bigger value
	int biggestContentX2 = 0;           // Init with sure smaller value
	list<CComponentType*>::const_iterator itComponentType;
	for( itComponentType = componentTypes.begin(); itComponentType != componentTypes.end(); itComponentType++ )
	{
		// Calculate current content pos left 
		int contentX = (*itComponentType)->getX(); 

		// Calculate current content pos right
		int contentX2 = (*itComponentType)->getX()+(*itComponentType)->getWidth()-1; 
		
		// Compare with smallest content pos left
		if( contentX < smallestContentX )
			smallestContentX = contentX;

		// Compare with biggest content pos right
		if( contentX2 > biggestContentX2 )
			biggestContentX2 = contentX2;
	}

	// Calculate content width
	int contentWidth = biggestContentX2 - smallestContentX + 1 + (2*MIN_DISTANCE_TO_FRAME);

	return( contentWidth );
}


int CPerspective::getContentHeight() const
{
	// Calculate smallest and biggest y coordinates in the perspective frame
	int smallestContentY = getHeight();  // Init with sure bigger value
	int biggestContentY2 = 0;           // Init with sure smaller value
	list<CComponentType*>::const_iterator itComponentType;
	for( itComponentType = componentTypes.begin(); itComponentType != componentTypes.end(); itComponentType++ )
	{
		// Calculate current content pos left 
		int ContentY = (*itComponentType)->getY(); 

		// Calculate current content pos right
		int ContentY2 = (*itComponentType)->getY()+(*itComponentType)->getHeight()-1; 
		
		// Compare with smallest content pos left
		if( ContentY < smallestContentY )
			smallestContentY = ContentY;

		// Compare with biggest content pos right
		if( ContentY2 > biggestContentY2 )
			biggestContentY2 = ContentY2;
	}

	// Calculate content Height
	int contentHeight = biggestContentY2 - smallestContentY + 1 + (2*MIN_DISTANCE_TO_FRAME);

	return( contentHeight );
}


//------------------------------------------------------------
// Setter
//------------------------------------------------------------


void CPerspective::setWidth( int width )
{
	// If size is set smaller than neccesary for inner component types => return without any changes
	list<CComponentType*>::iterator itComponentType;
	for( itComponentType = componentTypes.begin(); itComponentType != componentTypes.end(); itComponentType++ )
	{
		int componentTypeX2 = (*itComponentType)->getX()+(*itComponentType)->getWidth()-1;

		if( componentTypeX2 > (width-1-MIN_DISTANCE_TO_FRAME) )
			return;
	}

	// Set the new width
	CGraphObj::setWidth( width );
}


void CPerspective::setHeight( int height )
{
	// If size is set smaller than neccesary for inner component types => return without any changes
	list<CComponentType*>::iterator itComponentType;
	for( itComponentType = componentTypes.begin(); itComponentType != componentTypes.end(); itComponentType++ )
	{
		int componentTypeY2 = (*itComponentType)->getY()+(*itComponentType)->getHeight()-1;

		if( componentTypeY2 > (height-1-MIN_DISTANCE_TO_FRAME) )
			return;
	}

	// Set the new height
	CGraphObj::setHeight( height );
}


void CPerspective::setLeft( int x )
{
	// Calculate set amount
	int oldX = getX();
	int setAmount = x - oldX;
	
	// If size would be smaller than neccesary for inner component types => return without any changes
	list<CComponentType*>::iterator itComponentType;
	for( itComponentType = componentTypes.begin(); itComponentType != componentTypes.end(); itComponentType++ )
	{
		int componentTypeX = (*itComponentType)->getX();

		if( (componentTypeX - setAmount) < (0+MIN_DISTANCE_TO_FRAME) )
			return;
	}

	// Shift component types inside perspective to compensate left rezize of perspective
	for( itComponentType = componentTypes.begin(); itComponentType != componentTypes.end(); itComponentType++ )
	{
		int componentTypeX = (*itComponentType)->getX();
		(*itComponentType)->setX( componentTypeX - setAmount );
	}

	// Set the new x value
	setX( x );

	// Set the new width
	int oldWidth = getWidth();
	/*CGraphObj::*/setWidth( oldWidth - setAmount );

	return;
}


void CPerspective::setTop( int y )
{
	// Calculate set amount
	int oldY = getY();
	int setAmount = y - oldY;
	
	// If size is set smaller than neccesary for inner component types => return without any changes
	list<CComponentType*>::iterator itComponentType;
	for( itComponentType = componentTypes.begin(); itComponentType != componentTypes.end(); itComponentType++ )
	{
		int componentTypeY = (*itComponentType)->getY();

		if( (componentTypeY - setAmount) < (0+MIN_DISTANCE_TO_FRAME) )
			return;
	}

	// Shift component types inside perspective to compensate top rezize of perspective
	for( itComponentType = componentTypes.begin(); itComponentType != componentTypes.end(); itComponentType++ )
	{
		int componentTypeY = (*itComponentType)->getY();
		(*itComponentType)->setY( componentTypeY - setAmount );
	}

	// Set the new y value
	setY( y );

	// Set the new height
	int oldHeight = getHeight();
	/*CGraphObj::*/setHeight( oldHeight - setAmount );

	return;
}

