/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Constants.h"
#include "VisitEdge.h"
#include "PlaceEdge.h"
#include "TimeEdge.h"
#include "ComponentType.h"  // To get ids to which edge is connected
#include <fstream>
#include <typeinfo>
//#pragma c-mol_hdrstop

//#include "WriteXmlSpecialEdgeAttribs.hmol"
#include "WriteXmlSpecialEdgeAttribs.hpp"





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MWriteXmlSpecialEdgeAttribs::<CVisitEdge*>()
void MWriteXmlSpecialEdgeAttribs::handling( CVisitEdge *that, METHOD_PARAMS_MWRITEXMLSPECIALEDGEATTRIBS )
{
	// Write connectedIdRef1
	int id1 = that->getConnector1().getComponentType().getId();
	ofsOutputFile << " " << XMLATTRIB_CONNECTEDIDREF1 << " = " << '"' << id1 << '"'; 
		
	// Write connectedIdRef2
	int id2 = that->getConnector2().getComponentType().getId();
	ofsOutputFile << " " << XMLATTRIB_CONNECTEDIDREF2 << " = " << '"' << id2 << '"'; 

	// Write bold
	bool isBold = that->getIsBold();
	if( isBold )
		ofsOutputFile << " " << XMLATTRIB_BOLD << " = " << '"' << XMLVALUE_YES << '"'; 
	else
		ofsOutputFile << " " << XMLATTRIB_BOLD << " = " << '"' << XMLVALUE_NO << '"'; 
}
