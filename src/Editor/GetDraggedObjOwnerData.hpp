/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined GetDraggedObjectOwnerData_h
	#define GetDraggedObjectOwnerData_h





#include <typeinfo>
#include "Editor.h"
#include "Perspective.h"
#include "GraphObj.h"
#include <list>





using namespace std;





//method MGetDraggedObjOwnerData( const CGraphObj& draggedObj, list<CGraphObj*>& objectsToCheck, int& maxWidth, int& maxHeight )
class MGetDraggedObjOwnerData
{
#define METHOD_PARAMS_GETDRAGGEDOBJOWNERDATA const CGraphObj& draggedObj, list<CGraphObj*>& objectsToCheck, int& maxWidth, int& maxHeight

public:

//	virtual void <CEditor*>();
    static void handling( CEditor* that, METHOD_PARAMS_GETDRAGGEDOBJOWNERDATA );

//	virtual void <CPerspective*>();
    static void handling( CPerspective* that, METHOD_PARAMS_GETDRAGGEDOBJOWNERDATA );


    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline void handling( void* vp_that, METHOD_PARAMS_GETDRAGGEDOBJOWNERDATA )
    {
        if( typeid( *(static_cast<CEditor*>( vp_that ))) == typeid( CEditor ) )
            handling( static_cast<CEditor*>( vp_that ), draggedObj, objectsToCheck, maxWidth, maxHeight );
        else if( typeid( *(static_cast<CPerspective*>( vp_that ))) == typeid( CPerspective ) )
            handling( static_cast<CPerspective*>( vp_that ), draggedObj, objectsToCheck, maxWidth, maxHeight );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }
};





#endif
