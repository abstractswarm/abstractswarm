/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined DragEdge_hmol
	#define DragEdge_hmol


#include <typeinfo>
#include "VisitEdge.h"
#include "PlaceEdge.h"
#include "TimeEdge.h"





//method MDragEdge( int dragAmountX,
//				  int dragAmountY
//				)
class MDragEdge
{
#define METHOD_PARAMS_DRAGEDGE int dragAmountX, int dragAmountY

public:

	// Handlings
//	virtual void <CVisitEdge*, CPlaceEdge*, CTimeEdge*>();
    static void handling( CVisitEdge* that, METHOD_PARAMS_DRAGEDGE );
    static void handling( CPlaceEdge* that, METHOD_PARAMS_DRAGEDGE );
    static void handling( CTimeEdge* that, METHOD_PARAMS_DRAGEDGE );


    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline void handling( void* vp_that, METHOD_PARAMS_DRAGEDGE )
    {
        if( typeid( *(static_cast<CVisitEdge*>( vp_that ))) == typeid( CVisitEdge ) )
            handling( static_cast<CVisitEdge*>( vp_that ), dragAmountX, dragAmountY );
        else if( typeid( *(static_cast<CPlaceEdge*>( vp_that ))) == typeid( CPlaceEdge ) )
            handling( static_cast<CPlaceEdge*>( vp_that ), dragAmountX, dragAmountY );
        else if( typeid( *(static_cast<CTimeEdge*>( vp_that ))) == typeid( CTimeEdge ) )
            handling( static_cast<CTimeEdge*>( vp_that ), dragAmountX, dragAmountY );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }
};





#endif
