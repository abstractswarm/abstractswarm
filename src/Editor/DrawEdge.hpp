/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined DrawEdge_hmol
	#define DrawEdge_hmol


#include <typeinfo>
#include "DrawWeightEdge.hpp"
#include "VisitEdge.h"





extern int zoomedX2;
extern int zoomedY2;

extern int xOffset1;
extern int yOffset1;

extern int xOffset2;
extern int yOffset2;

extern double edgeAngle;





//method MDrawEdge( HDC graphicContext,
//				  int screenX,
//				  int screenY,
//				  double zoomFactor ) : MDrawWeightEdge( HDC graphicContext,
//				                                        int screenX,
//				                                        int screenY,
//				                                        double zoomFactor )
class MDrawEdge : public MDrawWeightEdge
{
#define METHOD_PARAMS_DRAWEDGE HDC graphicContext, int screenX, int screenY, double zoomFactor

public:

	// Prologue
//	MDrawEdge();
    static void prologue( CEdge* that, METHOD_PARAMS_DRAWEDGE );

	// Epilogue
//	~MDrawEdge();
    static void epilogue( CEdge* that, METHOD_PARAMS_DRAWEDGE );

	// Handlings
//	virtual void <CVisitEdge*>();
    static void handling( CVisitEdge* that, METHOD_PARAMS_DRAWEDGE );


    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline void handling( void* vp_that, METHOD_PARAMS_DRAWEDGE )
    {
        if( typeid( *(static_cast<CVisitEdge*>( vp_that ))) == typeid( CVisitEdge ) )
            handling( static_cast<CVisitEdge*>( vp_that ), graphicContext, screenX, screenY, zoomFactor );
        else if( typeid( *(static_cast<CPlaceEdge*>( vp_that ))) == typeid( CPlaceEdge ) )
            handling( static_cast<CPlaceEdge*>( vp_that ), graphicContext, screenX, screenY, zoomFactor );
        else if( typeid( *(static_cast<CTimeEdge*>( vp_that ))) == typeid( CTimeEdge ) )
            handling( static_cast<CTimeEdge*>( vp_that ), graphicContext, screenX, screenY, zoomFactor );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }


    //
    // C-MOL INHERITANCE MECHANISM
    // (DO NOT FORGET TO OVERWRITE ALL INHERITED HANDLINGS HERE FOR SUPER-PROLOGUE/-EPILOGUE CALLS!)
    //

    static inline void handling( CPlaceEdge* that, METHOD_PARAMS_DRAWEDGE, const CAgent* pAgentOnEdge = nullptr, const CEdgeConnector* pTargetEdgeConnector = nullptr, int coveredDistanceOnEdge = 0 )
    {
        prologue( that, graphicContext, screenX, screenY, zoomFactor );
        MDrawWeightEdge::handling( that, graphicContext, screenX, screenY, zoomFactor, pAgentOnEdge, pTargetEdgeConnector, coveredDistanceOnEdge );
        epilogue( that, graphicContext, screenX, screenY, zoomFactor );
    }

    static inline void handling( CTimeEdge* that, METHOD_PARAMS_DRAWEDGE )
    {
        prologue( that, graphicContext, screenX, screenY, zoomFactor );
        MDrawWeightEdge::handling( that, graphicContext, screenX, screenY, zoomFactor );
        epilogue( that, graphicContext, screenX, screenY, zoomFactor );
    }
};





#endif
