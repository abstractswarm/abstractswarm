/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Constants.h"
#include "StationType.h"
#include "AgentType.h"
#include <fstream>
#include <typeinfo>
//#pragma c-mol_hdrstop

//#include "WriteXmlComponentTypeTagContent.hmol"
#include "WriteXmlComponentTypeTagContent.hpp"
//#include "WriteXmlOpenTag.hmol"
#include "WriteXmlOpenTag.hpp"





//------------------------------------------------------------
// Prologue
//------------------------------------------------------------


//MWriteXmlComponentTypeTagContent::MWriteXmlComponentTypeTagContent()
void MWriteXmlComponentTypeTagContent::prologue( CComponentType* that, METHOD_PARAMS_WRITEXMLCOMPONENTTYPETAGCONTENT )
{
	// priority
	if( that->getHasPriority() )
	{
		int value = that->getPriority();
//        MWriteXmlOpenTag( ofsOutputFile )°value( XMLTAG_PRIORITY );
        MWriteXmlOpenTag::handling( value, ofsOutputFile, XMLTAG_PRIORITY );
    }
	
	// frequency
	if( that->getHasFrequency() )
	{
		int value = that->getFrequency();
//		MWriteXmlOpenTag( ofsOutputFile )°value( XMLTAG_FREQUENCY );
        MWriteXmlOpenTag::handling( value, ofsOutputFile, XMLTAG_FREQUENCY );
    }

	// necessity
	if( that->getHasNecessity() )
	{
		int value = that->getNecessity();
//		MWriteXmlOpenTag( ofsOutputFile )°value( XMLTAG_NECESSITY );
        MWriteXmlOpenTag::handling( value, ofsOutputFile, XMLTAG_NECESSITY );
    }
	
	// time
	if( that->getHasTime() )
	{
		int value = that->getTime();
//		MWriteXmlOpenTag( ofsOutputFile )°value( XMLTAG_TIME );
        MWriteXmlOpenTag::handling( value, ofsOutputFile, XMLTAG_TIME );
    }
	
	// cycle
	if( that->getHasCycle() )
	{
		int value = that->getCycle();
//		MWriteXmlOpenTag( ofsOutputFile )°value( XMLTAG_CYCLE );
        MWriteXmlOpenTag::handling( value, ofsOutputFile, XMLTAG_CYCLE );
    }
}





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------

	
//void MWriteXmlComponentTypeTagContent::<CStationType*>()
void MWriteXmlComponentTypeTagContent::handling( CStationType* that, METHOD_PARAMS_WRITEXMLCOMPONENTTYPETAGCONTENT )
{
    prologue( that, ofsOutputFile );

	// item
	if( that->getHasItem() )
	{
		int value = that->getItem();
//		MWriteXmlOpenTag( ofsOutputFile )°value( XMLTAG_ITEM );
        MWriteXmlOpenTag::handling( value, ofsOutputFile, XMLTAG_ITEM );
    }

	// space
	if( that->getHasSpace() )
	{
		int value = that->getSpace();
//		MWriteXmlOpenTag( ofsOutputFile )°value( XMLTAG_SPACE );
        MWriteXmlOpenTag::handling( value, ofsOutputFile, XMLTAG_SPACE );
    }
}

	
//void MWriteXmlComponentTypeTagContent::<CAgentType*>()
void MWriteXmlComponentTypeTagContent::handling( CAgentType* that, METHOD_PARAMS_WRITEXMLCOMPONENTTYPETAGCONTENT )
{
    prologue( that, ofsOutputFile );

    // capacity
	if( that->getHasCapacity() )
	{
		int value = that->getCapacity();
//		MWriteXmlOpenTag( ofsOutputFile )°value( XMLTAG_CAPACITY );
        MWriteXmlOpenTag::handling( value, ofsOutputFile, XMLTAG_CAPACITY );
    }

	// size
	if( that->getHasSize() )
	{
		int value = that->getSize();
//		MWriteXmlOpenTag( ofsOutputFile )°value( XMLTAG_SIZE );
        MWriteXmlOpenTag::handling( value, ofsOutputFile, XMLTAG_SIZE );
    }

	// speed
	if( that->getHasSpeed() )
	{
		int value = that->getSpeed();
//		MWriteXmlOpenTag( ofsOutputFile )°value( XMLTAG_SPEED );
        MWriteXmlOpenTag::handling( value, ofsOutputFile, XMLTAG_SPEED );
    }
}
