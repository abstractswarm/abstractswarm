//------------------------------------------------------------
// Includes
//------------------------------------------------------------

#include "Constants.h"
#include "Perspective.h"
#include "StationType.h"
#include "AgentType.h"
#include <list>
#include <typeinfo>
//#pragma c-mol_hdrstop

//#include "DropGraphObj.hmol"
#include "DropGraphObj.hpp"
//#include "GetDraggedObjOwnerData.hmol"
#include "GetDraggedObjOwnerData.hpp"
//#include "CorrectPosition.hmol"
#include "CorrectPosition.hpp"
//#include "CorrectSize.hmol"
#include "CorrectSize.hpp"





using namespace std; 





//------------------------------------------------------------
// Static data
//------------------------------------------------------------


static bool isEnoughSpace = false;
static list<CGraphObj*> objectsToCheck;
static int maxWidth = 0;
static int maxHeight = 0;





//------------------------------------------------------------
// Prologue
//------------------------------------------------------------


//MDropGraphObj::MDropGraphObj()
void MDropGraphObj::prologue( CGraphObj* that, METHOD_PARAMS_DROPGRAPHOBJ )
{
//	MGetDraggedObjOwnerData( *that, objectsToCheck, maxWidth, maxHeight )°(const_cast<void*>( pDraggedObjOwner ))();
    MGetDraggedObjOwnerData::handling( const_cast<void*>( pDraggedObjOwner ), *that, objectsToCheck, maxWidth, maxHeight );
}


//------------------------------------------------------------
// Epilogue
//------------------------------------------------------------


//MDropGraphObj::~MDropGraphObj()
void MDropGraphObj::epilogue( CPerspective* that, METHOD_PARAMS_DROPGRAPHOBJ )
{
//	isEnoughSpace = MCorrectPosition( objectsToCheck, maxWidth, maxHeight )<-that();
    isEnoughSpace = MCorrectPosition::handling( *that, objectsToCheck, maxWidth, maxHeight );
}
void MDropGraphObj::epilogue( CStationType* that, METHOD_PARAMS_DROPGRAPHOBJ )
{
//	isEnoughSpace = MCorrectPosition( objectsToCheck, maxWidth, maxHeight )<-that();
    isEnoughSpace = MCorrectPosition::handling( *that, objectsToCheck, maxWidth, maxHeight );
}
void MDropGraphObj::epilogue( CAgentType* that, METHOD_PARAMS_DROPGRAPHOBJ )
{
//	isEnoughSpace = MCorrectPosition( objectsToCheck, maxWidth, maxHeight )<-that();
    isEnoughSpace = MCorrectPosition::handling( *that, objectsToCheck, maxWidth, maxHeight );
}


//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//bool MDropGraphObj::<CPerspective*>()
bool MDropGraphObj::handling( CPerspective* that, METHOD_PARAMS_DROPGRAPHOBJ )
{
    prologue( that, pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );

//	MCorrectSize( objectsToCheck, maxWidth, maxHeight, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom )<-that();
    MCorrectSize::handling( *that, objectsToCheck, maxWidth, maxHeight, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );

    epilogue( that, pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );

    return( isEnoughSpace );
}


//bool MDropGraphObj::<CStationType*, CAgentType*>()
bool MDropGraphObj::handling( CStationType* that, METHOD_PARAMS_DROPGRAPHOBJ )
{
    prologue( that, pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );

    epilogue( that, pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );

    return( isEnoughSpace );
}
bool MDropGraphObj::handling( CAgentType* that, METHOD_PARAMS_DROPGRAPHOBJ )
{
    prologue( that, pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );

    epilogue( that, pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );

    return( isEnoughSpace );
}

