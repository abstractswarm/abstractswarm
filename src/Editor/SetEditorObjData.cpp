//------------------------------------------------------------
// Includes
//------------------------------------------------------------

#include "Perspective.h"
#include "StationType.h"
#include "AgentType.h"
#include "VisitEdge.h"
#include "PlaceEdge.h"
#include "TimeEdge.h"
#include "EditorTypes.h"
#include <typeinfo>
//#pragma c-mol_hdrstop

//#include "SetEditorObjData.hmol"
#include "SetEditorObjData.hpp"





using namespace std;





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MSetEditorObjData::<CPerspective*>()
void MSetEditorObjData::handling( CPerspective* that, METHOD_PARAMS_SETEDITOROBJDATA )
{
	that->setName( string( editorObjData[ eodiName ].pStringData ) );
}


//void MSetEditorObjData::<CStationType*>()
void MSetEditorObjData::handling( CStationType* that, METHOD_PARAMS_SETEDITOROBJDATA )
{
	that->setName( string( editorObjData[ eodiName ].pStringData ) );
	that->setNoOfComponents( editorObjData[ eodiCount ].intData );

	int red = editorObjData[ eodiColor ].rgbColorData[ 0 ];
	int green = editorObjData[ eodiColor ].rgbColorData[ 1 ];
	int blue = editorObjData[ eodiColor ].rgbColorData[ 2 ];
	that->color.setRed( red );
	that->color.setGreen( green );
	that->color.setBlue( blue );

	that->setHasPriority( editorObjData[ eodiHasPriority ].boolData  );
	that->setHasFrequency( editorObjData[ eodiHasFrequency ].boolData );
	that->setHasNecessity( editorObjData[ eodiHasNecessity ].boolData );
	that->setHasTime( editorObjData[ eodiHasTime ].boolData );
	that->setHasCycle( editorObjData[ eodiHasCycle ].boolData );
	that->setHasItem( editorObjData[ eodiHasItem ].boolData );
	that->setHasSpace( editorObjData[ eodiHasSpace ].boolData );

	if( editorObjData[ eodiHasPriority ].boolData )
		that->setPriority( editorObjData[ eodiPriority ].intData );
	if( editorObjData[ eodiHasFrequency ].boolData )
		that->setFrequency( editorObjData[ eodiFrequency ].intData );
	if( editorObjData[ eodiHasNecessity ].boolData )
		that->setNecessity( editorObjData[ eodiNecessity ].intData );
	if( editorObjData[ eodiHasTime ].boolData )
		that->setTime( editorObjData[ eodiTime ].intData );
	if( editorObjData[ eodiHasCycle ].boolData )
		that->setCycle( editorObjData[ eodiCycle ].intData );
	if( editorObjData[ eodiHasItem ].boolData )
		that->setItem( editorObjData[ eodiItem ].intData );
	if( editorObjData[ eodiHasSpace ].boolData )
		that->setSpace( editorObjData[ eodiSpace ].intData );
}


//void MSetEditorObjData::<CAgentType*>()
void MSetEditorObjData::handling( CAgentType* that, METHOD_PARAMS_SETEDITOROBJDATA )
{
	that->setName( string( editorObjData[ eodiName ].pStringData ) );
	that->setNoOfComponents( editorObjData[ eodiCount ].intData );

	int red = editorObjData[ eodiColor ].rgbColorData[ 0 ];
	int green = editorObjData[ eodiColor ].rgbColorData[ 1 ];
	int blue = editorObjData[ eodiColor ].rgbColorData[ 2 ];
	that->color.setRed( red );
	that->color.setGreen( green );
	that->color.setBlue( blue );

	that->setHasPriority( editorObjData[ eodiHasPriority ].boolData  );
	that->setHasFrequency( editorObjData[ eodiHasFrequency ].boolData );
	that->setHasNecessity( editorObjData[ eodiHasNecessity ].boolData );
	that->setHasTime( editorObjData[ eodiHasTime ].boolData );
	that->setHasCycle( editorObjData[ eodiHasCycle ].boolData );
	that->setHasCapacity( editorObjData[ eodiHasCapacity ].boolData );
	that->setHasSize( editorObjData[ eodiHasSize ].boolData );
	that->setHasSpeed( editorObjData[ eodiHasSpeed ].boolData );

	if( editorObjData[ eodiHasPriority ].boolData )
		that->setPriority( editorObjData[ eodiPriority ].intData );
	if( editorObjData[ eodiHasFrequency ].boolData )
		that->setFrequency( editorObjData[ eodiFrequency ].intData );
	if( editorObjData[ eodiHasNecessity ].boolData )
		that->setNecessity( editorObjData[ eodiNecessity ].intData );
	if( editorObjData[ eodiHasTime ].boolData )
		that->setTime( editorObjData[ eodiTime ].intData );
	if( editorObjData[ eodiHasCycle ].boolData )
		that->setCycle( editorObjData[ eodiCycle ].intData );
	if( editorObjData[ eodiHasCapacity ].boolData )
		that->setCapacity( editorObjData[ eodiCapacity ].intData );
	if( editorObjData[ eodiHasSize ].boolData )
		that->setSize( editorObjData[ eodiSize ].intData );
	if( editorObjData[ eodiHasSpeed ].boolData )
		that->setSpeed( editorObjData[ eodiSpeed ].intData );
}


//void MSetEditorObjData::<CVisitEdge*>()
void MSetEditorObjData::handling( CVisitEdge* that, METHOD_PARAMS_SETEDITOROBJDATA )
{
	that->setIsBold( editorObjData[ eodiBold ].boolData );
}


//void MSetEditorObjData::<CPlaceEdge*>()
void MSetEditorObjData::handling( CPlaceEdge* that, METHOD_PARAMS_SETEDITOROBJDATA )
{
	that->setValue( editorObjData[ eodiValue ].intData );
	that->setIsDirected1( editorObjData[ eodiDirected1 ].boolData );
	that->setIsDirected2( editorObjData[ eodiDirected2 ].boolData );
}


//void MSetEditorObjData::<CTimeEdge*>()
void MSetEditorObjData::handling( CTimeEdge* that, METHOD_PARAMS_SETEDITOROBJDATA )
{
	that->setValue( editorObjData[ eodiValue ].intData );
	that->setIsDirected1( editorObjData[ eodiDirected1 ].boolData );
	that->setIsDirected2( editorObjData[ eodiDirected2 ].boolData );
	that->setIsAndConnection1( editorObjData[ eodiAndConnection1 ].boolData );
	that->setIsAndConnection2( editorObjData[ eodiAndConnection2 ].boolData );
}
