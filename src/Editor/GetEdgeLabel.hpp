#if !defined GetEdgeLabel_hmol
	#define GetEdgeLabel_hmol


#include <typeinfo>
#include <string>
#include "VisitEdge.h"
#include "PlaceEdge.h"
#include "TimeEdge.h"





using namespace std;





//method MGetEdgeLabel()
class MGetEdgeLabel
{

public:

	// Handlings
//	virtual string <CVisitEdge*>();
    static string handling( CVisitEdge* );

//	virtual string <CPlaceEdge*>();
    static string handling( CPlaceEdge* );

//	virtual string <CTimeEdge*>();
    static string handling( CTimeEdge* );


    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline string handling( void* vp_that )
    {
        if( typeid( *(static_cast<CVisitEdge*>( vp_that ))) == typeid( CVisitEdge ) )
            return handling( static_cast<CVisitEdge*>( vp_that ) );
        else if( typeid( *(static_cast<CPlaceEdge*>( vp_that ))) == typeid( CPlaceEdge ) )
            return handling( static_cast<CPlaceEdge*>( vp_that ) );
        else if( typeid( *(static_cast<CTimeEdge*>( vp_that ))) == typeid( CTimeEdge ) )
            return handling( static_cast<CTimeEdge*>( vp_that ) );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }
};





#endif
