//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Constants.h"
#include "Perspective.h"
//#pragma c-mol_hdrstop

//#include "CorrectSize.hmol"
#include "CorrectSize.hpp"
//#include "CheckPosition.hmol"
#include "CheckPosition.hpp"





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MCorrectSize::<CPerspective>()
void MCorrectSize::handling( CPerspective& that, METHOD_PARAMS_CORRECTSIZE )
{
	// If side is dragged, return directly!
	if( (!isDraggedLeft) && (!isDraggedTop) && (!isDraggedRight) && (!isDraggedBottom) )
		return;
	
	// Set the perspective size to minimum size, if not sufficient
	if( that.getWidth() < PERSPECTIVE_MIN_WIDTH )
		that.setWidth( PERSPECTIVE_MIN_WIDTH );
	if( that.getHeight() < PERSPECTIVE_MIN_HEIGHT )
		that.setHeight( PERSPECTIVE_MIN_HEIGHT );

	// Caclulate perspective's minimum width
	int contentWidth = that.getContentWidth();
	int minWidth = 0;
	if( contentWidth > PERSPECTIVE_MIN_WIDTH )
		minWidth = contentWidth;
	else
		minWidth = PERSPECTIVE_MIN_WIDTH;

	// Caclulate perspective's minimum height
	int contentHeight = that.getContentHeight();
	int minHeight = 0;
	if( contentHeight > PERSPECTIVE_MIN_HEIGHT )
		minHeight = contentHeight;
	else
		minHeight = PERSPECTIVE_MIN_HEIGHT;
	
	// Check position validity
//	bool isPositionValid = MCheckPosition( objectsToCheck, maxWidth, maxHeight, that.getX(), that.getY() )°that();
    bool isPositionValid = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, that.getX(), that.getY() );

	// While position is invalid and minimum width and height is not yet reached and when there was a side 
	// was dragged, which allows size correction to minimize width and heigt
	int width = that.getWidth();
	int height = that.getHeight();
	while(    /*invalid position*/  (!isPositionValid) 
		   && /*not yet too small*/ (    /*width big enough and correction resizes width*/   ((width > minWidth) && (isDraggedLeft || isDraggedRight)) 
		                              || /*height big enough and correction resizes height*/ ((height > minHeight) && (isDraggedTop || isDraggedBottom))
			                        ) 
		 )
	{
		
//		// Extract due to which side is invalid
//		
//		bool isLeftInvalid = false;		
//		bool isTopInvalid = false;		
//		bool isRightInvalid = false;		
//		bool isBottomInvalid = false;		
//
//		int x = that.getX();
//		int y = that.getY();
//		int x2 = that.getX()+that.getWidth()-1;
//		int y2 = that.getY()+that.getHeight()-1;
//		
//		list<CGraphObj*>::iterator itObjToCheck;
//		for( itObjToCheck = objectsToCheck.begin(); itObjToCheck != objectsToCheck.end(); itObjToCheck++ )
//		{
//			// Do not compare position with its self
//			if( &that == *itObjToCheck )
//				continue;
//			
//			int objToCheckX = (*itObjToCheck)->getX()-GRAPHOBJ_MIN_DISTANCE; 
//			int objToCheckY = (*itObjToCheck)->getY()-GRAPHOBJ_MIN_DISTANCE; 
//			int objToCheckX2 = (*itObjToCheck)->getX()+(*itObjToCheck)->getWidth()-1+GRAPHOBJ_MIN_DISTANCE; 
//			int objToCheckY2 = (*itObjToCheck)->getY()+(*itObjToCheck)->getHeight()-1+GRAPHOBJ_MIN_DISTANCE; 
//
//			// Create list with current object to check
//			list<CGraphObj*> curObjectToCheck;
//			curObjectToCheck.push_back( *itObjToCheck );
//			bool isPositionValid = MCheckPosition( objectsToCheck, maxWidth, maxHeight, that.getX(), that.getY() )配hat();
//			curObjectToCheck.clear();
//			
//			if(    /*invalid?*/         (!isPositionValid)
//				&& /*due to left?*/     (x <= objToCheckX2) && (x2 >= objToCheckX2)
//				&& /*dragged left*/     isDraggedLeft
//			  )
//				isLeftInvalid = true;
//			if(    /*invalid?*/    (!isPositionValid)
//				&& /*due to top?*/ (y <= objToCheckY2) && (y2 >= objToCheckY2)
//				&& /*dragged top*/ isDraggedTop
//            )
//			    isTopInvalid = true;		
//			if(    /*invalid?*/      (!isPositionValid)
//				&& /*due to right?*/ (x <= objToCheckX) && (x2 >= objToCheckX)
//				&& /*dragged right*/ isDraggedRight
//            )
// 				isRightInvalid = true;		
//			if(    /*invalid?*/       (!isPositionValid)
//				&& /*due to bottom?*/ (y <= objToCheckY) && (y2 >= objToCheckY) 
//				&& /*dragged bottom*/ isDraggedBottom
//            )
//			    isBottomInvalid = true;		
//		}				
		
    	width = that.getWidth();
		if( (!isPositionValid) && isDraggedLeft && (width > minWidth) /*&& isLeftInvalid*/ )
		{
			// Set new left
			int x = that.getX();
			that.setLeft( x+1 );

//			isPositionValid = MCheckPosition( objectsToCheck, maxWidth, maxHeight, that.getX(), that.getY() )°that();
            isPositionValid = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, that.getX(), that.getY() );
        }
	    height = that.getHeight();
		if( (!isPositionValid) && isDraggedTop && (height > minHeight) /*&& isTopInvalid*/ )
		{
			// Set new top
			int y = that.getY();
			that.setTop( y+1 );
		
//			isPositionValid = MCheckPosition( objectsToCheck, maxWidth, maxHeight, that.getX(), that.getY() )配hat();
            isPositionValid = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, that.getX(), that.getY() );
        }
    	width = that.getWidth();
		if( (!isPositionValid) && isDraggedRight && (width > minWidth) /*&& isRightInvalid*/ )
		{
			// Set new width
			int width = that.getWidth();
			that.setWidth( width-1 );

//			isPositionValid = MCheckPosition( objectsToCheck, maxWidth, maxHeight, that.getX(), that.getY() )°that();
            isPositionValid = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, that.getX(), that.getY() );
        }
	    height = that.getHeight();
		if( (!isPositionValid) && isDraggedBottom && (height > minHeight) /*&& isBottomInvalid*/ )
		{
			// Set new height
			int height = that.getHeight();
			that.setHeight( height-1 );

//			isPositionValid = MCheckPosition( objectsToCheck, maxWidth, maxHeight, that.getX(), that.getY() )°that();
            isPositionValid = MCheckPosition::handling( that, objectsToCheck, maxWidth, maxHeight, that.getX(), that.getY() );
        }
	}
}
