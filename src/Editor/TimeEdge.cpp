/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "TimeEdge.h"





//------------------------------------------------------------
// Getter
//------------------------------------------------------------


bool CTimeEdge::getIsAndConnection1() const
{
	return( connector1.getIsAndConnection() );
}


bool CTimeEdge::getIsAndConnection2() const
{
	return( connector2.getIsAndConnection() );
}


//------------------------------------------------------------
// Setter
//------------------------------------------------------------


void CTimeEdge::setIsAndConnection1( bool isAndConnection )
{
	connector1.setIsAndConnection( isAndConnection );
}


void CTimeEdge::setIsAndConnection2( bool isAndConnection )
{
	connector2.setIsAndConnection( isAndConnection );
}
