/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined AgentType_h
	#define AgentType_h

#include "ComponentType.h"
#include "EditorTypes.h"





using namespace std;





class CAgentType : public CComponentType
{
	public:

		// Construction
        CAgentType( const CPerspective& owner, TfpAddAgentToSimulation fpAddAgentToSimulation = NULL, TfpRemoveAgentFromSimulation fpRemoveAgentFromSimulation = nullptr );

		/**
		 * Does the clean up work for agent types.
		 */
		~CAgentType();

		// Getter
		int getNoOfInstances() const;
		int getNoOfComponents() const;
		
		/**
		 * Returns the pointer to a function to add an agent to simulation.
		 *
		 * @return the pointer to a function to add an agent to simulation
		 */
		TfpAddAgentToSimulation getAddAgentToSimulation() const;

		/**
		 * Returns the pointer to a function to remove an agent from simulation.
		 *
		 * @return the pointer to a function to remove an agent from simulation
		 */
		TfpRemoveAgentFromSimulation getRemoveAgentFromSimulation() const;

		/** 
		 * Returns all agents belonging to this agent type.
		 *
		 * @return the agents belonging to this agent type
		 */
		const vector<CAgent*>& getAgents() const;

		// Optional agent type attribute getter
		bool getHasAttribs() const;
		int getNoOfAttribs() const;
		bool getHasCapacity() const;
		int getCapacity() const;
		bool getHasSize() const;
		int getSize() const;
		bool getHasSpeed() const;
		int getSpeed() const;

		// Setter
		void setNoOfComponents( int noOfComponents );

		// Optional agent type attribute setter
		void setHasCapacity( bool hasCapacity );
		void setCapacity( int capacity );
		void setHasSize( bool hasSize );
		void setSize( int size );
		void setHasSpeed( bool hasSpeed );
		void setSpeed( int speed );

		/**
		 * Updates all agents of the agent type (by recreating its stations; 
		 * must be changed later to allow dynamic changes during simulation). 
		 * This method should be called everytime some of these changes are made to a 
		 * agent type: An attribute is added or changed, an edge is added or changed.
		 */
		void updateComponents();

	protected:

		/**
		 * Deletes the agents belonging to this agent type 
		 * and clears the agents list.
		 */
		void deleteAgents();


	private:

		static int noOfInstances;

		/** Pointer to a function to add an agent to the simulation. */
		TfpAddAgentToSimulation fpAddAgentToSimulation;

		/** Pointer to a function to remove an agent from the simulation. */
		TfpRemoveAgentFromSimulation fpRemoveAgentFromSimulation;

		// Containee
		vector<CAgent*> agents;

		// Optional agent type attributes
		bool hasCapacity;
		int capacity;
		bool hasSize;
		int size;
		bool hasSpeed;
		int speed;


		/** 
		 * Returns all agents belonging to this agent type as components.
		 * <p>
		 * This method should not be used here. Use the method getAgents() instead, 
		 * which returns the agents belonging to this agent type as agents!
		 * This method only provides the possibility to access the components 
		 * from a component type when not knowing the specific type of it.
		 *
		 * @return the components belonging to this agent type as components
		 */
		const vector<CComponent*>& getComponents() const;
};	





#endif
