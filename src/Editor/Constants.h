/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined EditorConstants_h
	#define EditorConstants_h





//------------------------------------------------------------
// General
//------------------------------------------------------------


#define PI 3.1416


//------------------------------------------------------------
// Editor
//------------------------------------------------------------


// Size of editable surface (EDITOR_WIDTH*GRID_XSIZE*EDITOR_HEIGHT*GRID_YSIZE may not exceed 14546596)
#define EDITOR_WIDTH  100 
#define EDITOR_HEIGHT 100

// Zoom
#define MIN_ZOOM_FACTOR 0.2
#define MAX_ZOOM_FACTOR 5


//------------------------------------------------------------
// Visualization
//------------------------------------------------------------


// Editable surface color
#define EDIT_SURFACE_RED   200
#define EDIT_SURFACE_GREEN 200
#define EDIT_SURFACE_BLUE  200

// Grid size
#define GRID_XSIZE 25  // vertical cell size in grid
#define GRID_YSIZE 25  // horizontal cell size in grid	

// Grid color
#define GRID_RED   220	// RGB values of grid
#define GRID_GREEN 220			  
#define GRID_BLUE  220			  

// Graph objects
#define GRAPHOBJ_MIN_DISTANCE 1  // Minimal distance between objects in the graph
#define MIN_DISTANCE_TO_FRAME 1  // Minimal distance of graph objects to the frame in which they are


//------------------------------------------------------------
// Perspective
//------------------------------------------------------------


// Perspective size
#define PERSPECTIVE_MIN_WIDTH  5
#define PERSPECTIVE_MIN_HEIGHT 5

// Perspective color
#define PERSPECTIVE_RED   255 // RGB values of perspective
#define PERSPECTIVE_GREEN 255 
#define PERSPECTIVE_BLUE  255 

// Perspective frame color
#define PERSPECTIVE_FRAME_RED   0 // RGB values of perspective's frame
#define PERSPECTIVE_FRAME_GREEN 0 
#define PERSPECTIVE_FRAME_BLUE  0 


//------------------------------------------------------------
// Component type
//------------------------------------------------------------


// Text
#define ATTRIBTEXT_PRIORITY "Prio.:"
#define ATTRIBTEXT_FREQUENCY "Freq.:"
#define ATTRIBTEXT_NECESSITY "Nec.:"
#define ATTRIBTEXT_TIME "Time:"
#define ATTRIBTEXT_CYCLE "Cycle:"
#define ATTRIBTEXT_ITEM "Item:"
#define ATTRIBTEXT_SPACE "Space:"
#define ATTRIBTEXT_CAPACITY "Cap.:"
#define ATTRIBTEXT_SIZE "Size:"
#define ATTRIBTEXT_SPEED "Speed:"

// Component type size
#define COMPONENTTYPE_WIDTH  2
#define COMPONENTTYPE_HEIGHT 2

// Component type color
#define COMPONENTTYPE_DEF_RED   255 // RGB values of component type's default color
#define COMPONENTTYPE_DEF_GREEN 255 
#define COMPONENTTYPE_DEF_BLUE  255 

// Component type frame color
#define COMPONENTTYPE_FRAME_RED   0 // RGB values of component type's default color
#define COMPONENTTYPE_FRAME_GREEN 0 
#define COMPONENTTYPE_FRAME_BLUE  255 


//------------------------------------------------------------
// Edges
//------------------------------------------------------------


#define EDGE_ARROWLENGTH 10  // Length of the edge's arrow
#define EDGE_ARROWWIDTH 20   // Diameter of the edge's arrow

#define EDGE_DIAMONDSIZE 30  // Diameter of the value edge's diamond

#define VISITEDGE_LABEL "«visit»"  // Shown label for the visit edge
#define PLACEEDGE_LABEL "«place»"  // Shown label for the place edge
#define TIMEEDGE_LABEL "«time»"    // Shown label for the time edge

// Color 
#define EDGE_RED   0 // RGB values for edge color
#define EDGE_GREEN 0 
#define EDGE_BLUE  255 


//------------------------------------------------------------
// XML writing and loading
//------------------------------------------------------------

// Header information
#define XMLHEADER "<?xml version=\x022\x031.0\x022 encoding=\x022ISO-8859-1\x022 ?>"

// Indentation
#define XMLINDENT "    "

// Tags
#define XMLTAG_GRAPH		"logisticsGraph"
#define XMLTAG_PERSPECTIVE	"perspective"
#define XMLTAG_STATIONTYPE	"stationType"
#define XMLTAG_AGENTTYPE	"agentType"
#define XMLTAG_VISITEDGE	"visitEdge"
#define XMLTAG_PLACEEDGE	"placeEdge"
#define XMLTAG_TIMEEDGE		"timeEdge"
#define XMLTAG_PRIORITY		"priority"
#define XMLTAG_FREQUENCY	"frequency"
#define XMLTAG_NECESSITY	"necessity"
#define XMLTAG_TIME			"time"
#define XMLTAG_CYCLE		"cycle"
#define XMLTAG_ITEM			"item"
#define XMLTAG_SPACE		"space"
#define XMLTAG_CAPACITY		"capacity"
#define XMLTAG_SIZE			"size"
#define XMLTAG_SPEED		"speed"

// Attributes
#define XMLATTRIB_ID				"id"
#define XMLATTRIB_NAME				"name"
#define XMLATTRIB_X					"x"
#define XMLATTRIB_Y					"y"
#define XMLATTRIB_WIDTH				"width"
#define XMLATTRIB_HEIGHT			"height"
#define XMLATTRIB_COUNT				"count"
#define XMLATTRIB_COLORRED			"colorRed"
#define XMLATTRIB_COLORGREEN		"colorGreen"
#define XMLATTRIB_COLORBLUE			"colorBlue"
#define XMLATTRIB_INTERFACE			"interface"
#define XMLATTRIB_CONNECTEDIDREF1	"connectedIdRef1"
#define XMLATTRIB_CONNECTEDIDREF2	"connectedIdRef2"
#define XMLATTRIB_BOLD				"bold"
#define XMLATTRIB_VALUE				"value"
#define XMLATTRIB_DIRECTED			"directed"
#define XMLATTRIB_ANDCONNECTED1		"andConnected1"
#define XMLATTRIB_ANDCONNECTED2		"andConnected2"

// Constant values
#define XMLVALUE_YES	"yes"
#define XMLVALUE_NO		"no"





#endif
