/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "VisitEdge.h"
#include "PlaceEdge.h"
#include "TimeEdge.h"
#include <typeinfo>
//#pragma c-mol_hdrstop

//#include "DragEdge.hmol"
#include "DragEdge.hpp"





//void MDragEdge::<CVisitEdge*, CPlaceEdge*, CTimeEdge*>()
void MDragEdge::handling( CVisitEdge* that, METHOD_PARAMS_DRAGEDGE )
{
	bool isConnected1 = that->getConnector1().getIsConnected();
	bool isConnected2 = that->getConnector2().getIsConnected();
	
	int tempX = 0;
	int tempY = 0;
	
	if( !isConnected1 )
	{
		tempX = that->getX1();
		tempY = that->getY1();
	}
	else if( !isConnected2 )
	{
		tempX = that->getX2();
		tempY = that->getY2();
	}

	that->setTempX( tempX + dragAmountX );
	that->setTempY( tempY + dragAmountY );
}
void MDragEdge::handling( CPlaceEdge* that, METHOD_PARAMS_DRAGEDGE )
{
    bool isConnected1 = that->getConnector1().getIsConnected();
    bool isConnected2 = that->getConnector2().getIsConnected();

    int tempX = 0;
    int tempY = 0;

    if( !isConnected1 )
    {
        tempX = that->getX1();
        tempY = that->getY1();
    }
    else if( !isConnected2 )
    {
        tempX = that->getX2();
        tempY = that->getY2();
    }

    that->setTempX( tempX + dragAmountX );
    that->setTempY( tempY + dragAmountY );
}
void MDragEdge::handling( CTimeEdge* that, METHOD_PARAMS_DRAGEDGE )
{
    bool isConnected1 = that->getConnector1().getIsConnected();
    bool isConnected2 = that->getConnector2().getIsConnected();

    int tempX = 0;
    int tempY = 0;

    if( !isConnected1 )
    {
        tempX = that->getX1();
        tempY = that->getY1();
    }
    else if( !isConnected2 )
    {
        tempX = that->getX2();
        tempY = that->getY2();
    }

    that->setTempX( tempX + dragAmountX );
    that->setTempY( tempY + dragAmountY );
}
