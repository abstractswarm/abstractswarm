/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined DropGraphObj_hmol
	#define DropGraphObj_hmol


#include <typeinfo>
#include "Perspective.h"
#include "StationType.h"
#include "AgentType.h"





//method MDropGraphObj( CEditorObj* pDragTarget,
//					  const void* pDraggedObjOwner,
//			          bool isDraggedLeft,
//			          bool isDraggedTop,
//			          bool isDraggedRight,
//			          bool isDraggedBottom )
class MDropGraphObj
{
#define METHOD_PARAMS_DROPGRAPHOBJ CEditorObj* pDragTarget, const void* pDraggedObjOwner, bool isDraggedLeft,  bool isDraggedTop, bool isDraggedRight, bool isDraggedBottom

public:

	// Prologue
//	MDropGraphObj();
    static void prologue( CGraphObj* that, METHOD_PARAMS_DROPGRAPHOBJ );

	// Epilogue
//	~MDropGraphObj();
    // NOTE THAT THESE PROLOGUE OVERLOADS ARE NEEDED DUE TO CALLS TO ANOTHER METHOD IN THE EPILOGUE (WHICH NEEDS THE EXACT TYPE SINCE IT HAS NO VIRTUAL HANDLINGS)
    static void epilogue( CPerspective* that, METHOD_PARAMS_DROPGRAPHOBJ );
    static void epilogue( CStationType* that, METHOD_PARAMS_DROPGRAPHOBJ );
    static void epilogue( CAgentType* that, METHOD_PARAMS_DROPGRAPHOBJ );

	// Handlings
//	virtual bool <CPerspective*>();
    static bool handling( CPerspective* that, METHOD_PARAMS_DROPGRAPHOBJ );

//	virtual bool <CStationType*, CAgentType*>();
    static bool handling( CStationType* that, METHOD_PARAMS_DROPGRAPHOBJ );
    static bool handling( CAgentType* that, METHOD_PARAMS_DROPGRAPHOBJ );

    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline bool handling( void* vp_that, METHOD_PARAMS_DROPGRAPHOBJ )
    {
        if( typeid( *(static_cast<CPerspective*>( vp_that ))) == typeid( CPerspective ) )
            return( handling( static_cast<CPerspective*>( vp_that ), pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom ) );
        else if( typeid( *(static_cast<CStationType*>( vp_that ))) == typeid( CStationType ) )
            return( handling( static_cast<CStationType*>( vp_that ), pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom ) );
        else if( typeid( *(static_cast<CAgentType*>( vp_that ))) == typeid( CAgentType ) )
            return( handling( static_cast<CAgentType*>( vp_that ), pDragTarget, pDraggedObjOwner, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom ) );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }
};





#endif
