//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Constants.h"
#include "Perspective.h"
#include "StationType.h"
#include "AgentType.h"
#include "VisitEdge.h"
#include "PlaceEdge.h"
#include "TimeEdge.h"
#include <typeinfo>
//#pragma c-mol_hdrstop

//#include "Drop.hmol"
#include "Drop.hpp"





//------------------------------------------------------------
// Epilogue
//------------------------------------------------------------


//MDrop::~MDrop()
void MDrop::epilogue( CEditorObj* that, METHOD_PARAMS_DROP )
{
}
