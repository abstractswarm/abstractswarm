/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined GraphObj_h
	#define GraphObj_h





#include "EditorObj.h"
#include <string>





using namespace std;





class CGraphObj : public CEditorObj
{

	public:
		
		// Construction
		CGraphObj();

		// Destruction
		virtual ~CGraphObj();

		// Getter
		const string& getName() const;
		int getX() const;
		int getY() const;
		int getWidth() const;
		int getHeight() const;
		virtual int getNoOfInstances() const = 0;

		// Setter
		void setName( string name );
		void setX( int x );
		void setY( int y );
		virtual void setWidth( int width );
		virtual void setHeight( int height );

	private:	
		
		// Attributes
		string name;
		int x;
		int y;
		int width;
		int height;
};





#endif
