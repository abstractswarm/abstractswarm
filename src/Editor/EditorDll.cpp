#include "EditorDll.h"


//------------------------------------------------------------
// Construction/Destruction
//------------------------------------------------------------

static CEditor* pEditor;


void EditorDll::createEditor()
{
    pEditor = new CEditor();
}


void EditorDll::deleteEditor()
{
    delete pEditor;
}



//------------------------------------------------------------
// Events
//------------------------------------------------------------

ENEditorMessage EditorDll::click()
{
    return( pEditor->click() );
}


void EditorDll::doubleClick()
{
    pEditor->doubleClick();
}


void EditorDll::mouseDown( ENMouseButton mouseButton )
{
    pEditor->mouseDown( mouseButton );
}


void EditorDll::mouseMove( int x, int y )
{
    return( pEditor->mouseMove( x, y ) );
}


ENEditorMessage EditorDll::mouseUp( ENMouseButton mouseButton )
{
    return( pEditor->mouseUp( mouseButton ) );
}


void EditorDll::keyPress( char key )
{
    pEditor->keyPress( key );
}


void EditorDll::keyDown( ENKey key )
{
    pEditor->keyDown( key );
}


void EditorDll::keyUp( ENKey key )
{
    pEditor->keyUp( key );
}



//------------------------------------------------------------
// Layout independant functionality and state setters
//------------------------------------------------------------

void EditorDll::setSelectedTool( ENTool tool )
{
    pEditor->setSelectedTool( tool );
}


void EditorDll::setScrollX( int percentScrollX )
{
    pEditor->setScrollX( percentScrollX );
}


void EditorDll::setScrollY( int percentScrollY )
{
    pEditor->setScrollY( percentScrollY );
}


double EditorDll::setZoom( double zoomFactor, int& percentScrollX, int& percentScrollY )
{
    return( pEditor->setZoom( zoomFactor, percentScrollX, percentScrollY ) );
}


void EditorDll::clear()
{
    pEditor->clear();
}


bool EditorDll::load( char* pFileName )
{
    return( pEditor->load( pFileName ) );
}


void EditorDll::save( char* pFileName )
{
    pEditor->save( pFileName );
}


void EditorDll::setVisibleFrameSize( int width, int height )
{
    pEditor->setVisibleFrameSize( width, height );
}



//------------------------------------------------------------
// UI and simulation communication
//------------------------------------------------------------

ENEditorObjType EditorDll::getSelectedObjData( UEditorObjData selectedObjData[] )
{
    return( pEditor->getSelectedObjData( selectedObjData ) );
}


void EditorDll::setSelectedObjResults( UEditorObjData selectedObjData[] )
{
    pEditor->setSelectedObjResults( selectedObjData );
}


void EditorDll::connectToSimulation( TfpAddStationToSimulation fpAddStationToSimulation, TfpRemoveStationFromSimulation fpRemoveStationFromSimulation, TfpAddAgentToSimulation fpAddAgentToSimulation, TfpRemoveAgentFromSimulation fpRemoveAgentFromSimulation )
{
    pEditor->connectToSimulation( fpAddStationToSimulation, fpRemoveStationFromSimulation, fpAddAgentToSimulation, fpRemoveAgentFromSimulation );
}



//------------------------------------------------------------
// Platform dependent(!) interface for visualization
//------------------------------------------------------------

void EditorDll::draw( HDC graphicContext )
{
    pEditor->draw( graphicContext );
}
