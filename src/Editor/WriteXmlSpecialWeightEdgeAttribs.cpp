/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Constants.h"
#include "PlaceEdge.h"
#include "TimeEdge.h"
#include "ComponentType.h"  // To get ids to which edge is connected
#include <fstream>
#include <typeinfo>
//#pragma c-mol_hdrstop

//#include "WriteXmlSpecialWeightEdgeAttribs.hmol"
#include "WriteXmlSpecialWeightEdgeAttribs.hpp"





//------------------------------------------------------------
// Data
//------------------------------------------------------------


static bool isDirected1 = false; 
static bool isDirected2 = false; 





//------------------------------------------------------------
// Prologue
//------------------------------------------------------------

	
//MWriteXmlSpecialWeightEdgeAttribs::MWriteXmlSpecialWeightEdgeAttribs()
void MWriteXmlSpecialWeightEdgeAttribs::prologue( CWeightEdge* that, METHOD_PARAMS_WRITEXMLSPECIALWEIGHTEDGEATTRIBS )
{
	isDirected1 = that->getIsDirected1();
	isDirected2 = that->getIsDirected2();
	

	//
	// Write connectedIdRefs
	//
	
	// If edge is directed to connector1...
	if( isDirected1 )
	{
		// ...then write connector2 as origin (connectedIdRef1)...
		int id2 = that->getConnector2().getComponentType().getId();
		ofsOutputFile << " " << XMLATTRIB_CONNECTEDIDREF1 << " = " << '"' << id2 << '"'; 
		
		// ...and connector1 as destination (connectedIdRef2)
		int id1 = that->getConnector1().getComponentType().getId();
		ofsOutputFile << " " << XMLATTRIB_CONNECTEDIDREF2 << " = " << '"' << id1 << '"'; 
	}
	
	// ...else (if edge is directed to connector2 or if edge isn't directed)...
	else
	{
		// ...then write connector1 as origin (connectedIdRef1)...
		int id1 = that->getConnector1().getComponentType().getId();
		ofsOutputFile << " " << XMLATTRIB_CONNECTEDIDREF1 << " = " << '"' << id1 << '"'; 
		
		// ...and connector2 as destination (connectedIdRef2)
		int id2 = that->getConnector2().getComponentType().getId();
		ofsOutputFile << " " << XMLATTRIB_CONNECTEDIDREF2 << " = " << '"' << id2 << '"'; 
	}


	//
	// Write value
	//
	
	ofsOutputFile << " " << XMLATTRIB_VALUE << " = " << '"' << that->getValue() << '"'; 


	//
	// Write directed
	//
	
	if( isDirected1 || isDirected2 )
		ofsOutputFile << " " << XMLATTRIB_DIRECTED << " = " << '"' << XMLVALUE_YES << '"'; 
	else
		ofsOutputFile << " " << XMLATTRIB_DIRECTED << " = " << '"' << XMLVALUE_NO << '"'; 
}


//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MWriteXmlSpecialWeightEdgeAttribs::<CPlaceEdge*>()
void MWriteXmlSpecialWeightEdgeAttribs::handling( CPlaceEdge* that, METHOD_PARAMS_WRITEXMLSPECIALWEIGHTEDGEATTRIBS )
{
    prologue( that, ofsOutputFile );
}


//void MWriteXmlSpecialWeightEdgeAttribs::<CTimeEdge*>()
void MWriteXmlSpecialWeightEdgeAttribs::handling( CTimeEdge* that, METHOD_PARAMS_WRITEXMLSPECIALWEIGHTEDGEATTRIBS )
{
    prologue( that, ofsOutputFile );

    //
	// Write and-connected
	//

	bool isAndConnection1 = that->getIsAndConnection1();
	bool isAndConnection2 = that->getIsAndConnection2();
	
	// If edge is directed at first connector, then connectors will be swaped in xml file 
	// (because direction is implied by order of connection)
	if( isDirected1 )
	{
		if( isAndConnection2 )
			ofsOutputFile << " " << XMLATTRIB_ANDCONNECTED1 << " = " << '"' << XMLVALUE_YES << '"';
		else
			ofsOutputFile << " " << XMLATTRIB_ANDCONNECTED1 << " = " << '"' << XMLVALUE_NO << '"';
		
		if( isAndConnection1 )
			ofsOutputFile << " " << XMLATTRIB_ANDCONNECTED2 << " = " << '"' << XMLVALUE_YES << '"'; 
		else
			ofsOutputFile << " " << XMLATTRIB_ANDCONNECTED2 << " = " << '"' << XMLVALUE_NO << '"';
	}
	
	// Normal case: nothing is swapped...
	else
	{
		if( isAndConnection1 )
			ofsOutputFile << " " << XMLATTRIB_ANDCONNECTED1 << " = " << '"' << XMLVALUE_YES << '"';
		else
			ofsOutputFile << " " << XMLATTRIB_ANDCONNECTED1 << " = " << '"' << XMLVALUE_NO << '"';
		
		if( isAndConnection2 )
			ofsOutputFile << " " << XMLATTRIB_ANDCONNECTED2 << " = " << '"' << XMLVALUE_YES << '"'; 
		else
			ofsOutputFile << " " << XMLATTRIB_ANDCONNECTED2 << " = " << '"' << XMLVALUE_NO << '"';
	}
}
