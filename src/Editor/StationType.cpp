/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "StationType.h"
#include "./../Simulation/Components/Station.hpp"  // Included from simulation!

#include <sstream>





using namespace std;





//------------------------------------------------------------
// Static Init
//------------------------------------------------------------

int CStationType::noOfInstances = 0;


//------------------------------------------------------------
// Construction
//------------------------------------------------------------


CStationType::CStationType( const CPerspective& owner, TfpAddStationToSimulation fpAddStationToSimulation, TfpRemoveStationFromSimulation fpRemoveStationFromSimulation ) : CComponentType( owner )
{
	// Count instances
	noOfInstances++;

	// Store pointer to function to add station to simulation
	this->fpAddStationToSimulation = fpAddStationToSimulation;

	// Store pointer to function to remove station from simulation
	this->fpRemoveStationFromSimulation = fpRemoveStationFromSimulation;

	// Set the name
	ostringstream ossNoOfInstances; 
	ossNoOfInstances << noOfInstances;
	setName( string( "StationType" )+ossNoOfInstances.str() );

	// Add at least one station to station type
	stations.push_back( new CStation( *this ) );

	// Init optional component type attributes
	hasItem = false;
	item = 1;
	hasSpace = false;
	space = 1;
}


//------------------------------------------------------------
// Destruction
//------------------------------------------------------------


CStationType::~CStationType()
{
	deleteStations();

    // Update the components of all connected component types (cannot be done in super class since
	// this would cause a pure virtual function call because "updateConnectedComponents()" uses 
	// late binding calls which are not possible during destruction)
	updateConnectedComponents();
}


//------------------------------------------------------------
// Getter
//------------------------------------------------------------


int CStationType::getNoOfInstances() const
{
	return( noOfInstances );
}


int CStationType::getNoOfComponents() const
{
	int count = stations.size();
	return( count );
}


TfpAddStationToSimulation CStationType::getAddStationToSimulation() const
{
	return( fpAddStationToSimulation );
}


TfpRemoveStationFromSimulation CStationType::getRemoveStationFromSimulation() const
{
	return( fpRemoveStationFromSimulation );
}


const vector<CComponent*>& CStationType::getComponents() const
{
	return( reinterpret_cast<const vector<CComponent*>&>( stations ) );
}


const vector<CStation*>& CStationType::getStations() const
{
	return( stations );
}



//------------------------------------------------------------
// Optional station type attribute getter
//------------------------------------------------------------


bool CStationType::getHasAttribs() const
{
	return( (CComponentType::getHasAttribs() || hasItem || hasSpace) );
}


int CStationType::getNoOfAttribs() const
{
	int attribsCount = CComponentType::getNoOfAttribs();

	if( hasItem )
		attribsCount++;
	if( hasSpace )
		attribsCount++;

	return( attribsCount );
}


bool CStationType::getHasItem() const
{
	return( hasItem );
}


int CStationType::getItem() const
{
	return( item );
}


bool CStationType::getHasSpace() const
{
	return( hasSpace );
}


int CStationType::getSpace() const
{
	return( space );
}


//------------------------------------------------------------
// Setter
//------------------------------------------------------------


void CStationType::setNoOfComponents( int noOfComponents )
{
	// Free the old stations from memory first and clear the stations list
	deleteStations();

	// And finally create the new stations
	int i = 0;
	for( i = 0; i < noOfComponents; i++ )
		stations.push_back( new CStation( *this ) );

	// Update all attributes, etc. of the components of all connected component types
	updateConnectedComponents();
}


//------------------------------------------------------------
// Optional station type attribute setter
//------------------------------------------------------------


void CStationType::setHasItem( bool hasItem )
{
	this->hasItem = hasItem;
	item = 1;

	// Resize component type
	resize();		
}


void CStationType::setItem( int item )
{
	this->item = item;
}


void CStationType::setHasSpace( bool hasSpace )
{
	this->hasSpace = hasSpace;
	space = 1;

	// Resize component type
	resize();		
}


void CStationType::setSpace( int space )
{
	this->space = space;
}


//------------------------------------------------------------
// Clean up
//------------------------------------------------------------

void CStationType::deleteStations()
{
	vector<CStation*>::iterator itStation;
	for( itStation = stations.begin(); itStation != stations.end(); itStation++ )
		delete( *itStation );

	// Clear the station list for this station type
	stations.clear();
}


//------------------------------------------------------------
// Helper
//------------------------------------------------------------


void CStationType::updateComponents()
{
	// THIS IS SOME KIND OF DUMMY IMPLEMENTATION! 
	// NORMALLY IT SHOULD BE CHECKED HERE IF SOMETHING AND WHAT EXAKTLY HAS CHANGED
	// INSTEAD OF REINITIALIZING ALL COMPONENTS!
	// ALTERNATIVELY ALL CALLS TO THIS METHOD SHOULD BE REPLACED BY THE CORRESPONDING
	// BEHAVIOR THAT HAS TO BE EXECUTED TO UPDATE THE COMPONENTS THROUGH THE SPECIFIC CHANGE
	// (IN THE LATTER CASE MAY BE THIS METHOD COULD BE PARAMETERIZED WITH THE SPECIFIC CHANGE 
	// AND THE METHOD SHOULD BE CALLED THEN EXPLICITLY EVEN IN THE HAS-ATTRIBUTE-METHODS)

	vector<CStation*>::iterator itStation;
	for( itStation = stations.begin(); itStation != stations.end(); itStation++ )
		(*itStation)->initialize();
}
