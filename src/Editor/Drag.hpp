/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined Drag_hmol
	#define Drag_hmol





//#include "DragGraphObj.hmol"
#include "DragGraphObj.hpp"
//#include "DragEdge.hmol"
#include "DragEdge.hpp"





class CStationType;
class CAgentType;
class CPerspective;





//method MDrag( int dragAmountX,
//			  int dragAmountY,
//			  bool& isDraggedLeft,
//			  bool& isDraggedTop,
//			  bool& isDraggedRight,
//			  bool& isDraggedBottom ) : MDragGraphObj( int dragAmountX,
//			                                           int dragAmountY,
//			                                           bool& isDraggedLeft,
//			                                           bool& isDraggedTop,
//			                                           bool& isDraggedRight,
//			                                           bool& isDraggedBottom ),
//				                        MDragEdge( int dragAmountX,
//				                                   int dragAmountY )
class MDrag : public MDragGraphObj, public MDragEdge
{
#define METHOD_PARAMS_DRAG int dragAmountX, int dragAmountY, bool& isDraggedLeft, bool& isDraggedTop, bool& isDraggedRight, bool& isDraggedBottom

public:

    // Epilogue
//	~MDrag();
    static void epilogue( CEditorObj* that, METHOD_PARAMS_DRAG );

    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline void handling( void* vp_that, METHOD_PARAMS_DRAG )
    {
        if( typeid( *(static_cast<CPerspective*>( vp_that ))) == typeid( CPerspective ) )
            handling( static_cast<CPerspective*>( vp_that ), dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
        else if( typeid( *(static_cast<CAgentType*>( vp_that ))) == typeid( CAgentType ) )
            handling( static_cast<CAgentType*>( vp_that ), dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
        else if( typeid( *(static_cast<CStationType*>( vp_that ))) == typeid( CStationType ) )
            handling( static_cast<CStationType*>( vp_that ), dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
        else if( typeid( *(static_cast<CVisitEdge*>( vp_that ))) == typeid( CVisitEdge ) )
            handling( static_cast<CVisitEdge*>( vp_that ), dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
        else if( typeid( *(static_cast<CPlaceEdge*>( vp_that ))) == typeid( CPlaceEdge ) )
            handling( static_cast<CPlaceEdge*>( vp_that ), dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
        else if( typeid( *(static_cast<CTimeEdge*>( vp_that ))) == typeid( CTimeEdge ) )
            handling( static_cast<CTimeEdge*>( vp_that ), dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }


    //
    // C-MOL INHERITANCE MECHANISM
    // (DO NOT FORGET TO OVERWRITE ALL INHERITED HANDLINGS HERE FOR SUPER-PROLOGUE/-EPILOGUE CALLS!)
    //

    static inline void handling( CPerspective* that, METHOD_PARAMS_DRAG )
    {
        MDragGraphObj::handling( that, dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
        epilogue( that, dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
    }

    static inline void handling( CStationType* that, METHOD_PARAMS_DRAG )
    {
        MDragGraphObj::handling( that, dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
        epilogue( that, dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
    }

    static inline void handling( CAgentType* that, METHOD_PARAMS_DRAG )
    {
        MDragGraphObj::handling( that, dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
        epilogue( that, dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
    }

    static inline void handling( CVisitEdge* that, METHOD_PARAMS_DRAG )
    {
        MDragEdge::handling( that, dragAmountX, dragAmountY );
        epilogue( that, dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
    }

    static inline void handling( CPlaceEdge* that, METHOD_PARAMS_DRAG )
    {
        MDragEdge::handling( that, dragAmountX, dragAmountY );
        epilogue( that, dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
    }

    static inline void handling( CTimeEdge* that, METHOD_PARAMS_DRAG )
    {
        MDragEdge::handling( that, dragAmountX, dragAmountY );
        epilogue( that, dragAmountX, dragAmountY, isDraggedLeft, isDraggedTop, isDraggedRight, isDraggedBottom );
    }
};





#endif
