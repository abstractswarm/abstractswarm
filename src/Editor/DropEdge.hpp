#if !defined DropEdge_hmol
	#define DropEdge_hmol





#include <typeinfo>
#include "VisitEdge.h"
#include "PlaceEdge.h"
#include "TimeEdge.h"
class CEditorObj;





//method MDropEdge( CEditorObj* pNewConnectedType )
class MDropEdge
{
#define METHOD_PARAMS_DROPEDGE CEditorObj* pNewConnectedType

public:

	// Prologue
//	MDropEdge();
    static void prologue( CEdge* that, METHOD_PARAMS_DROPEDGE );

	// Handlings
//	virtual bool <CVisitEdge*>();
    static bool handling( CVisitEdge* that, METHOD_PARAMS_DROPEDGE);
//	virtual bool <CPlaceEdge*>();
    static bool handling( CPlaceEdge* that, METHOD_PARAMS_DROPEDGE);
//	virtual bool <CTimeEdge*>();
    static bool handling( CTimeEdge* that, METHOD_PARAMS_DROPEDGE);

    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline bool handling( void* vp_that, METHOD_PARAMS_DROPEDGE )
    {
        if( typeid( *(static_cast<CVisitEdge*>( vp_that ))) == typeid( CVisitEdge ) )
            return( handling( static_cast<CVisitEdge*>( vp_that ), pNewConnectedType ) );
        else if( typeid( *(static_cast<CPlaceEdge*>( vp_that ))) == typeid( CPlaceEdge ) )
            return( handling( static_cast<CPlaceEdge*>( vp_that ), pNewConnectedType ) );
        else if( typeid( *(static_cast<CTimeEdge*>( vp_that ))) == typeid( CTimeEdge ) )
            return( handling( static_cast<CTimeEdge*>( vp_that ), pNewConnectedType ) );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }
};





#endif
