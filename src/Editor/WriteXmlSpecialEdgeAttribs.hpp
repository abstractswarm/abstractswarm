/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined WriteXmlSpecialEdgeAttribs_hmol
	#define WriteXmlSpecialEdgeAttribs_hmol





#include <typeinfo>
//#include "WriteXmlSpecialWeightEdgeAttribs.hmol"
#include "WriteXmlSpecialWeightEdgeAttribs.hpp"
#include "VisitEdge.h"
#include <iosfwd>





using namespace std;





//method MWriteXmlSpecialEdgeAttribs( ofstream& ofsOutputFile ) : MWriteXmlSpecialWeightEdgeAttribs( ofstream& ofsOutputFile )
class MWriteXmlSpecialEdgeAttribs : public MWriteXmlSpecialWeightEdgeAttribs
{
#define METHOD_PARAMS_MWRITEXMLSPECIALEDGEATTRIBS ofstream& ofsOutputFile

public:

    // Handlings
//	virtual void <CVisitEdge*>();
    static void handling( CVisitEdge* that, METHOD_PARAMS_MWRITEXMLSPECIALEDGEATTRIBS );


    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline void handling( void* vp_that, METHOD_PARAMS_MWRITEXMLSPECIALEDGEATTRIBS )
    {
        if( typeid( *(static_cast<CVisitEdge*>( vp_that ))) == typeid( CVisitEdge ) )
            handling( static_cast<CVisitEdge*>( vp_that ), ofsOutputFile );
        else if( typeid( *(static_cast<CPlaceEdge*>( vp_that ))) == typeid( CPlaceEdge ) )
            handling( static_cast<CPlaceEdge*>( vp_that ), ofsOutputFile );
        else if( typeid( *(static_cast<CTimeEdge*>( vp_that ))) == typeid( CTimeEdge ) )
            handling( static_cast<CTimeEdge*>( vp_that ), ofsOutputFile );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }


    //
    // C-MOL INHERITANCE MECHANISM
    // (DO NOT FORGET TO OVERWRITE ALL INHERITED HANDLINGS HERE FOR SUPER-PROLOGUE/-EPILOGUE CALLS!)
    //

    static inline void handling( CPlaceEdge* that, METHOD_PARAMS_MWRITEXMLSPECIALEDGEATTRIBS )
    {
        // prologue( that, ofsOutputFile );
        MWriteXmlSpecialWeightEdgeAttribs::handling( that, ofsOutputFile );
        // epilogue( that, ofsOutputFile );
    }

    static inline void handling( CTimeEdge* that, METHOD_PARAMS_MWRITEXMLSPECIALEDGEATTRIBS )
    {
        // prologue( that, ofsOutputFile );
        MWriteXmlSpecialWeightEdgeAttribs::handling( that, ofsOutputFile );
        // epilogue( that, ofsOutputFile );
    }
};





#endif
