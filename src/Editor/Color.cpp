/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Color.h"





//------------------------------------------------------------
// Construction
//------------------------------------------------------------


CColor::CColor( int red, int green, int blue )
{
	this->red = red;
	this->green = green;
	this->blue = blue;
}

		
//------------------------------------------------------------
// Getter
//------------------------------------------------------------


int CColor::getRed() const
{
	return( red );
}


int CColor::getGreen() const
{
	return( green );
}


int CColor::getBlue() const
{
	return( blue );
}


//------------------------------------------------------------
// Setter
//------------------------------------------------------------


void CColor::setRed( int red )
{
	this->red = red;
}


void CColor::setGreen( int green )
{
	this->green = green;
}


void CColor::setBlue( int blue )
{
	this->blue = blue;
}
