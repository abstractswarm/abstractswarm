/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined CorrectPosition_h
	#define CorrectPosition_h

#include <list>





using namespace std;





class CGraphObj;
class CPerspective;
class CStationType;
class CAgentType;





//------------------------------------------------------------
// Method
//------------------------------------------------------------


//method MCorrectPosition( list<CGraphObj*>& objectsToCheck, int maxWidth, int maxHeight )
class MCorrectPosition
{
#define METHOD_PARAMS_CORRECTPOSITION list<CGraphObj*>& objectsToCheck, int maxWidth, int maxHeight

public:

    // Prologue
//	MCorrectPosition();
    // NOTE THAT THESE PROLOGUE OVERLOADS ARE NEEDED DUE TO CALLS TO ANOTHER METHOD IN THE PROLOGUE (WHICH NEEDS THE EXACT TYPE SINCE IT HAS NO VIRTUAL HANDLINGS)
    static void prologue( CPerspective& that, METHOD_PARAMS_CORRECTPOSITION );
    static void prologue( CStationType& that, METHOD_PARAMS_CORRECTPOSITION );
    static void prologue( CAgentType& that, METHOD_PARAMS_CORRECTPOSITION );

	// Handlings
//	bool <CPerspective>();
    static bool handling( CPerspective& that, METHOD_PARAMS_CORRECTPOSITION );
//	bool <CStationType, CAgentType>();
    static bool handling( CStationType& that, METHOD_PARAMS_CORRECTPOSITION );
    static bool handling( CAgentType& that, METHOD_PARAMS_CORRECTPOSITION );
};





#endif
