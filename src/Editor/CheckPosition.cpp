//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Constants.h"
#include "Perspective.h"
#include "StationType.h"
#include "AgentType.h"
//#pragma c-mol_hdrstop

//#include "CheckPosition.hmol"
#include "CheckPosition.hpp"





//------------------------------------------------------------
// Static data
//------------------------------------------------------------


static bool isValidPosition = false;





//------------------------------------------------------------
// Prologue
//------------------------------------------------------------


//MCheckPosition::MCheckPosition()
void MCheckPosition::prologue( CGraphObj& that, METHOD_PARAMS_CHECKPOSITION )
{
	isValidPosition = true;
	
	// New proposed coordinates
	int droppedX = newX;
	int droppedY = newY;
	int droppedX2 = newX+that.getWidth()-1;
	int droppedY2 = newY+that.getHeight()-1;
	
	// If current object is out of bounds => no valid position!
	if(	   /* bounds check for left */   (droppedX < (0+MIN_DISTANCE_TO_FRAME)) 
		|| /* bounds check for up */     (droppedY < (0+MIN_DISTANCE_TO_FRAME)) 
		|| /* bounds check for right */  (droppedX2 > (maxWidth-1-MIN_DISTANCE_TO_FRAME)) 
		|| /* bounds check for bottom */ (droppedY2 > (maxHeight-1-MIN_DISTANCE_TO_FRAME))
	  )
	{
		isValidPosition = false;
		return;
	}

	// Check positions for all other objects if there is any overlapping
	list<CGraphObj*>::iterator itGraphObj;
	for( itGraphObj = objectsToCheck.begin(); itGraphObj != objectsToCheck.end(); itGraphObj++ )
	{
		// Do not compare position with its self
		if( &that == *itGraphObj )
			continue;

		// Coordinates of corners from the current object to compare with minimum frame
		int itGraphObjX = (*itGraphObj)->getX()-GRAPHOBJ_MIN_DISTANCE;
		int itGraphObjY = (*itGraphObj)->getY()-GRAPHOBJ_MIN_DISTANCE;
		int itGraphObjX2 = (*itGraphObj)->getX()+(*itGraphObj)->getWidth()-1+GRAPHOBJ_MIN_DISTANCE;
		int itGraphObjY2 = (*itGraphObj)->getY()+(*itGraphObj)->getHeight()-1+GRAPHOBJ_MIN_DISTANCE;
		
		// If current object lapps over another one => no valid position!
		if(    /* left top corner */         ((droppedX >= itGraphObjX) && (droppedX <= itGraphObjX2) && (droppedY >= itGraphObjY) && (droppedY <= itGraphObjY2))
		    || /* right top corner */        ((droppedX2 >= itGraphObjX) && (droppedX2 <= itGraphObjX2) && (droppedY >= itGraphObjY) && (droppedY <= itGraphObjY2))
			|| /* right bottom corner */     ((droppedX2 >= itGraphObjX) && (droppedX2 <= itGraphObjX2) && (droppedY2 >= itGraphObjY) && (droppedY2 <= itGraphObjY2))
			|| /* left bottom corner */      ((droppedX >= itGraphObjX) && (droppedX <= itGraphObjX2) && (droppedY2 >= itGraphObjY) && (droppedY2 <= itGraphObjY2))
			|| /* another left line*/        ((droppedX <= itGraphObjX) && (droppedX2 >= itGraphObjX) && (droppedY <= itGraphObjY) && (droppedY2 >= itGraphObjY2))
			|| /* another top line*/         ((droppedX <= itGraphObjX) && (droppedX2 >= itGraphObjX2) && (droppedY <= itGraphObjY) && (droppedY2 >= itGraphObjY))
			|| /* another right line*/       ((droppedX <= itGraphObjX2) && (droppedX2 >= itGraphObjX2) && (droppedY <= itGraphObjY) && (droppedY2 >= itGraphObjY2))
			|| /* another bottom line*/      ((droppedX <= itGraphObjX) && (droppedX2 >= itGraphObjX2) && (droppedY <= itGraphObjY2) && (droppedY2 >= itGraphObjY2)) 
			|| /* without corners/lines 1 */ ((droppedX <= itGraphObjX) && (droppedX2 >= itGraphObjX2) && (droppedY >= itGraphObjY) && (droppedY2 <= itGraphObjY2))
			|| /* without corners/lines 2 */ ((droppedX >= itGraphObjX) && (droppedX2 <= itGraphObjX2) && (droppedY <= itGraphObjY) && (droppedY2 >= itGraphObjY2))
		  )
		{
			isValidPosition = false;
			return;
		}
	}
}


//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//bool MCheckPosition::<CPerspective>()
bool MCheckPosition::handling( CPerspective& that, METHOD_PARAMS_CHECKPOSITION )
{
    prologue( that, objectsToCheck, maxWidth, maxHeight, newX, newY );

    return( isValidPosition );
}


//bool MCheckPosition::<CStationType, CAgentType>()
bool MCheckPosition::handling( CStationType& that, METHOD_PARAMS_CHECKPOSITION )
{
    prologue( that, objectsToCheck, maxWidth, maxHeight, newX, newY );

    return( isValidPosition );
}
bool MCheckPosition::handling( CAgentType& that, METHOD_PARAMS_CHECKPOSITION )
{
    prologue( that, objectsToCheck, maxWidth, maxHeight, newX, newY );

    return( isValidPosition );
}
