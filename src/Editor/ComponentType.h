/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined ComponentType_h
	#define ComponentType_h





#include "GraphObj.h"
#include "Color.h"
#include "EdgeConnector.h"
#include <string>
#include <list>
#include <vector>





using namespace std;





class CPerspective;
class CComponent;





class CComponentType : public CGraphObj
{

	public:

		CColor color;

		// Construction
		CComponentType( const CPerspective& owner );

		// Destruction
		virtual ~CComponentType();

		// Getter
		const list<CEdgeConnector*>& getEdgeConnectors() const;
		const CPerspective& getOwner() const;
		virtual int getNoOfInstances() const = 0;
		virtual int getNoOfComponents() const = 0;

		/**
		 * Sets the number of components belonging to the component type.
		 * 
		 * @param noOfComponents the new number of components of the component type
		 */
		virtual void setNoOfComponents( int noOfComponents ) = 0;

		/** 
		 * Returns all components belonging to this component type.
		 *
		 * @return the components belonging to this component type
		 */
		virtual const vector<CComponent*>& getComponents() const = 0;

		/**
		 * Sets the factor that should be used to draw the component type (as used for simulation visualization).
		 *
		 * @param factor   the factor that should be used to draw the component type
		 */
		void setDrawFactor( double factor );

		/**
		 * Returns the factor that should be used to draw the component type (as used for simulation visualization).
		 *
		 * @return the factor that should be used to draw the component type
		 */
		double getDrawFactor() const;

		// Optional component type attribute getter
		virtual bool getHasAttribs() const;
		virtual int getNoOfAttribs() const;
		bool getHasPriority() const;
		int getPriority() const;
		bool getHasFrequency() const;
		int getFrequency() const;
		bool getHasNecessity() const;
		int getNecessity() const;
		bool getHasTime() const;
		int getTime() const;
		bool getHasCycle() const;
		int getCycle() const;
		
		// Optional component type attribute setter
		void setHasPriority( bool hasPriority );
		void setPriority( int priority );
		void setHasFrequency( bool hasFrequency );
		void setFrequency( int frequency );
		void setHasNecessity( bool hasNecessity );
		void setNecessity( int necessity );
		void setHasTime( bool hasTime );
		void setTime( int time );
		void setHasCycle( bool hasCycle );
		void setCycle( int cycle );

		// Add an edge connector
		void addEdgeConnector( CEdgeConnector& edgeConnector );
		
		// Remove an edge connector
		void removeEdgeConnector( CEdgeConnector& edgeConnector );

		/** Returns the component type's edge connectors. */
		const list<CEdgeConnector*>& getEdgeConnectors();
	
		/**
		 * Updates all components of the component type (by recreating its components; 
		 * must be changed later to allow dynamic changes during simulation). 
		 * This method should be called everytime some of these changes are made to a 
		 * component type: An attribute is added or changed, an edge is added or changed.
		 */
		virtual void updateComponents() = 0;

		/**
		 * Updates all components of all connected component types.
		 * This method should be called everytime some of these changes are made to a
		 * component type: The component type is deleted, the components count is changed.
		 */
		void updateConnectedComponents();

	protected:

		// Helper
		void resize();

	
	private:

		/** The factor that should be used to draw the component type (as used for simulation visualization). */
		double drawFactor;

		// Attributes
		const CPerspective& owner;	
		list<CEdgeConnector*> edgeConnectors;

		// Optional component type attributes
		bool hasPriority;
		int priority;
		bool hasFrequency;
		int frequency;
		bool hasNecessity;
		int necessity;
		bool hasTime;
		int time;
		bool hasCycle;
		int cycle;
		
		// Setter
		void setWidth( int width );
		void setHeight( int height );
};





#endif
