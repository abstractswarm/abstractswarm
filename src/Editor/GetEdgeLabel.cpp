//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Constants.h"
#include "VisitEdge.h"
#include "PlaceEdge.h"
#include "TimeEdge.h"
#include <string>
#include <typeinfo>
//#pragma c-mol_hdrstop

#include "GetEdgeLabel.hpp"





using namespace std;





//string MGetEdgeLabel::<CVisitEdge*>()
string MGetEdgeLabel::handling( CVisitEdge* that )
{
	return( VISITEDGE_LABEL );
}


//string MGetEdgeLabel::<CPlaceEdge*>()
string MGetEdgeLabel::handling( CPlaceEdge* that )
{
	return( PLACEEDGE_LABEL );
}


//string MGetEdgeLabel::<CTimeEdge*>()
string MGetEdgeLabel::handling( CTimeEdge* that )
{
	return( TIMEEDGE_LABEL );
}
