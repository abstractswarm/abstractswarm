/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined CalcEdgeDrawOffset_hmol
	#define CalcEdgeDrawOffset_hmol


#include "AgentType.h"
#include "StationType.h"





//method MCalcEdgeDrawOffset( int edgeRelX, int edgeRelY, double zoomFactor, int& xOffset, int& yOffset )
class MCalcEdgeDrawOffset
{
#define METHOD_PARAM_CALCEDGEDRAWOFFSET int edgeRelX, int edgeRelY, double zoomFactor, int& xOffset, int& yOffset

public:

	// Prologue
//	MCalcEdgeDrawOffset();
    static void prologue( CComponentType* that, METHOD_PARAM_CALCEDGEDRAWOFFSET );

	// Handlings
//	virtual void <CStationType*>();
    static void handling( CStationType* that, METHOD_PARAM_CALCEDGEDRAWOFFSET );
//	virtual void <CAgentType*>();
    static void handling( CAgentType* that, METHOD_PARAM_CALCEDGEDRAWOFFSET );

    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline void handling( void* vp_that, METHOD_PARAM_CALCEDGEDRAWOFFSET )
    {
        if( typeid( *(static_cast<CStationType*>( vp_that ))) == typeid( CStationType ) )
            handling( static_cast<CStationType*>( vp_that ), edgeRelX, edgeRelY, zoomFactor, xOffset, yOffset );
        else if( typeid( *(static_cast<CAgentType*>( vp_that ))) == typeid( CAgentType ) )
            handling( static_cast<CAgentType*>( vp_that ), edgeRelX, edgeRelY, zoomFactor, xOffset, yOffset );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }
};





#endif
