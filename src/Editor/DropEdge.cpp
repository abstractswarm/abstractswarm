//------------------------------------------------------------
// Includes
//------------------------------------------------------------

#include "EditorTypes.h"
#include "VisitEdge.h"
#include "PlaceEdge.h"
#include "TimeEdge.h"
#include "ComponentType.h"
#include <typeinfo>
//#pragma c-mol_hdrstop

//#include "DropEdge.hmol"
#include "DropEdge.hpp"
//#include "GetEditorObjType.hmol"
#include "GetEditorObjType.hpp"





//------------------------------------------------------------
// Data
//------------------------------------------------------------


static bool isValidDrop = false;
static ENEditorObjType alreadyConnectedType;
static ENEditorObjType newConnectedType;


//------------------------------------------------------------
// Prologue
//------------------------------------------------------------


//MDropEdge::MDropEdge()
void MDropEdge::prologue( CEdge* that, METHOD_PARAMS_DROPEDGE )
{
    if( pNewConnectedType != nullptr )
	{
//		newConnectedType = MGetEditorObjType()°pNewConnectedType();
        newConnectedType = MGetEditorObjType::handling( pNewConnectedType );
        isValidDrop = true;
	}
	else
		isValidDrop = false;

	CComponentType* pAlreadyConnectedType = &const_cast<CComponentType&>( that->getConnectedConnector().getComponentType() );
//	alreadyConnectedType = MGetEditorObjType()°pAlreadyConnectedType();
    alreadyConnectedType = MGetEditorObjType::handling( pAlreadyConnectedType );

	// Can't connect an edge with same source and destination component type
	if( pNewConnectedType == pAlreadyConnectedType )
		isValidDrop = false;


	//
	// Can't connect an edge if there is already one between these two component types
	//

	// Check for all connectors of the already connected component type, if there is any who connects to 
	// the new component type
	list<CEdgeConnector*>::const_iterator itEdgeConnector;
	for( itEdgeConnector = pAlreadyConnectedType->getEdgeConnectors().begin(); itEdgeConnector != pAlreadyConnectedType->getEdgeConnectors().end(); itEdgeConnector++ )
	{
		if( &((*itEdgeConnector)->getLinkedConnector().getComponentType()) == static_cast<CComponentType*>( pNewConnectedType ) )
		{
			isValidDrop = false;
			break;
		}
	}
}


//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//bool MDropEdge::<CVisitEdge*>()
bool MDropEdge::handling( CVisitEdge* that, METHOD_PARAMS_DROPEDGE )
{
    prologue( that, pNewConnectedType );

	if( isValidDrop && (alreadyConnectedType != newConnectedType) )
		static_cast<CComponentType*>( pNewConnectedType )->addEdgeConnector( that->getFreeConnector() );
	else 
		isValidDrop = false;

	return( isValidDrop );
}


//bool MDropEdge::<CPlaceEdge*>()
bool MDropEdge::handling( CPlaceEdge* that, METHOD_PARAMS_DROPEDGE )
{
    prologue( that, pNewConnectedType );

    if( isValidDrop && (alreadyConnectedType == eotStationType) && (newConnectedType == eotStationType) )
		static_cast<CComponentType*>( pNewConnectedType )->addEdgeConnector( that->getFreeConnector() );
	else 
		isValidDrop = false;

	return( isValidDrop );
}


//bool MDropEdge::<CTimeEdge*>()
bool MDropEdge::handling( CTimeEdge* that, METHOD_PARAMS_DROPEDGE )
{
    prologue( that, pNewConnectedType );

    if( isValidDrop )
		static_cast<CComponentType*>( pNewConnectedType )->addEdgeConnector( that->getFreeConnector() );
	else 
		isValidDrop = false;

	return( isValidDrop );
}
