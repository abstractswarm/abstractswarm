/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Constants.h"
#include "Editor.h"
#include "Perspective.h"
#include "StationType.h"
#include "AgentType.h"
#include "VisitEdge.h"
#include "PlaceEdge.h"
#include "TimeEdge.h"
#include <fstream>
#include <typeinfo>
//#pragma c-mol_hdrstop

//#include "WriteXmlAttribs.hmol"
#include "WriteXmlAttribs.hpp"





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MWriteXmlAttribs::<CEditor*>()
void MWriteXmlAttribs::handling( CEditor* that, METHOD_PARAMS_WRITEXMLATTRIBS )
{
	// The graph root element has no attributes
}


//void MWriteXmlAttribs::<int>()
void MWriteXmlAttribs::handling( int that, METHOD_PARAMS_WRITEXMLATTRIBS )
{
	// Write value
	ofsOutputFile << " " << XMLATTRIB_VALUE << " = " << '"' << that << '"'; 
}
