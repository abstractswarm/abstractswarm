#if !defined CorrectSize_hmol
	#define CorrectSize_hmol

#include <list>





using namespace std;





class CGraphObj;
class CPerspective;





//------------------------------------------------------------
// Method
//------------------------------------------------------------


//method MCorrectSize( list<CGraphObj*>& objectsToCheck,
//					 int maxWidth,
//					 int maxHeight,
//					 bool isDraggedLeft,
//					 bool isDraggedTop,
//					 bool isDraggedRight,
//					 bool isDraggedBottom
//				   )
class MCorrectSize
{
#define METHOD_PARAMS_CORRECTSIZE list<CGraphObj*>& objectsToCheck, int maxWidth, int maxHeight, bool isDraggedLeft, bool isDraggedTop, bool isDraggedRight, bool isDraggedBottom

public:

	// Handlings
//	void <CPerspective>();
    static void handling( CPerspective& that, METHOD_PARAMS_CORRECTSIZE );
};





#endif
