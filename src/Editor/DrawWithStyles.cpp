/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Constants.h"

// Needed for platform dependent(!) visualization
#include "windows.h"

#include "Perspective.h"
#include "Grid.h"
#include "Color.h"

// Needed for inherited polymorphic handlings
#include "StationType.h"
#include "AgentType.h"
#include "VisitEdge.h"
#include "PlaceEdge.h"
#include "TimeEdge.h"
//#pragma c-mol_hdrstop

#include "DrawWithStyles.hpp"





//------------------------------------------------------------
// Static data
//------------------------------------------------------------


static HPEN hPen, hPenOld; 
static HBRUSH hBrush, hBrushOld;
static HFONT hFont, hFontOld;





//------------------------------------------------------------
// Prologue
//------------------------------------------------------------


//MDrawWithStyles::MDrawWithStyles()
void MDrawWithStyles::prologue( CEditorObj* that, METHOD_PARAMS_DRAWWITHSTYLES )
{

	// Create the pen style

	int penRed = penColor.getRed();
	int penGreen = penColor.getGreen();
	int penBlue = penColor.getBlue();
	
	int penWidth = 1;
	
	// If the object is selected, draw it with red frame lines
	if( isSelected )
	{
		penRed = 255;
		penGreen = 0;
		penBlue = 0;

		penWidth = 2;
	}

	LOGPEN logPen;
	logPen.lopnColor = RGB( penRed, penGreen, penBlue );
	logPen.lopnStyle = PS_SOLID;
	logPen.lopnWidth.x = penWidth;
	
	hPen = CreatePenIndirect( &logPen );
    hPenOld = static_cast<HPEN>( SelectObject( graphicContext, hPen ) );
	
	
	// Create the brush style
	
	int brushRed = brushColor.getRed();
	int brushGreen = brushColor.getGreen();
	int brushBlue = brushColor.getBlue();
    hBrush = static_cast<HBRUSH>( CreateSolidBrush( RGB( brushRed, brushGreen, brushBlue ) ) );
    hBrushOld = static_cast<HBRUSH>( SelectObject( graphicContext, hBrush ) );


	// Create the font style

	LOGFONT logFont;
    
    logFont.lfHeight = static_cast<long>( 14*zoomFactor );      // height of font
    logFont.lfWidth = 0;										// average character width
    logFont.lfEscapement = 0;									// angle of escapement
    logFont.lfOrientation = 0;									// base-line orientation angle
    logFont.lfWeight = FW_NORMAL;  /* (400) */					// font weight
    logFont.lfItalic = FALSE;									// italic attribute option
    logFont.lfUnderline = FALSE;								// underline attribute option
    logFont.lfStrikeOut = FALSE;								// strikeout attribute option
    logFont.lfCharSet = ANSI_CHARSET;							// character set identifier
    logFont.lfOutPrecision = OUT_DEFAULT_PRECIS;				// output precision
    logFont.lfClipPrecision = CLIP_DEFAULT_PRECIS;				// clipping precision
    logFont.lfQuality = ANTIALIASED_QUALITY;					// output quality
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_DONTCARE;	// pitch and family
//    strcpy( logFont.lfFaceName, "Arial" );						// typeface name
    mbstowcs( logFont.lfFaceName, "Arial", LF_FACESIZE );

	hFont = CreateFontIndirect( &logFont );
    hFontOld = static_cast<HFONT>( SelectObject( graphicContext, hFont ) );

	
	// Set the text color

	SetTextColor( graphicContext, RGB( 0, 0, 0 ) );
	

	// Make the background transparent

	SetBkMode( graphicContext, TRANSPARENT );
}
void MDrawWithStyles::prologue( CGrid& that, METHOD_PARAMS_DRAWWITHSTYLES )
{

    // Create the pen style

    int penRed = penColor.getRed();
    int penGreen = penColor.getGreen();
    int penBlue = penColor.getBlue();

    int penWidth = 1;

    // If the object is selected, draw it with red frame lines
    if( isSelected )
    {
        penRed = 255;
        penGreen = 0;
        penBlue = 0;

        penWidth = 2;
    }

    LOGPEN logPen;
    logPen.lopnColor = RGB( penRed, penGreen, penBlue );
    logPen.lopnStyle = PS_SOLID;
    logPen.lopnWidth.x = penWidth;

    hPen = CreatePenIndirect( &logPen );
    hPenOld = static_cast<HPEN>( SelectObject( graphicContext, hPen ) );


    // Create the brush style

    int brushRed = brushColor.getRed();
    int brushGreen = brushColor.getGreen();
    int brushBlue = brushColor.getBlue();
    hBrush = static_cast<HBRUSH>( CreateSolidBrush( RGB( brushRed, brushGreen, brushBlue ) ) );
    hBrushOld = static_cast<HBRUSH>( SelectObject( graphicContext, hBrush ) );


    // Create the font style

    LOGFONT logFont;

    logFont.lfHeight = static_cast<long>( 14*zoomFactor );      // height of font
    logFont.lfWidth = 0;										// average character width
    logFont.lfEscapement = 0;									// angle of escapement
    logFont.lfOrientation = 0;									// base-line orientation angle
    logFont.lfWeight = FW_NORMAL;  /* (400) */					// font weight
    logFont.lfItalic = FALSE;									// italic attribute option
    logFont.lfUnderline = FALSE;								// underline attribute option
    logFont.lfStrikeOut = FALSE;								// strikeout attribute option
    logFont.lfCharSet = ANSI_CHARSET;							// character set identifier
    logFont.lfOutPrecision = OUT_DEFAULT_PRECIS;				// output precision
    logFont.lfClipPrecision = CLIP_DEFAULT_PRECIS;				// clipping precision
    logFont.lfQuality = ANTIALIASED_QUALITY;					// output quality
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_DONTCARE;	// pitch and family
//    strcpy( logFont.lfFaceName, "Arial" );						// typeface name
    mbstowcs( logFont.lfFaceName, "Arial", LF_FACESIZE );

    hFont = CreateFontIndirect( &logFont );
    hFontOld = static_cast<HFONT>( SelectObject( graphicContext, hFont ) );


    // Set the text color

    SetTextColor( graphicContext, RGB( 0, 0, 0 ) );


    // Make the background transparent

    SetBkMode( graphicContext, TRANSPARENT );
}


//------------------------------------------------------------
// Epilogue
//------------------------------------------------------------


//MDrawWithStyles::~MDrawWithStyles()
void MDrawWithStyles::epilogue( CEditorObj* that, METHOD_PARAMS_DRAWWITHSTYLES )
{
	// Release pen
	SelectObject( graphicContext, hPenOld );
	DeleteObject( hPen );

	// Release brush
	SelectObject( graphicContext, hBrushOld );
	DeleteObject( hBrush );

	// Release font
	SelectObject( graphicContext, hFontOld );
	DeleteObject( hFont );
}
void MDrawWithStyles::epilogue( CGrid& that, METHOD_PARAMS_DRAWWITHSTYLES )
{
    // Release pen
    SelectObject( graphicContext, hPenOld );
    DeleteObject( hPen );

    // Release brush
    SelectObject( graphicContext, hBrushOld );
    DeleteObject( hBrush );

    // Release font
    SelectObject( graphicContext, hFontOld );
    DeleteObject( hFont );
}


//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MDrawWithStyles::<CPerspective>()
void MDrawWithStyles::handling( CPerspective& that, METHOD_PARAMS_DRAWWITHSTYLES )
{
    prologue( &that, graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );

	// Calculate zoomed width and height
    int zoomedWidth = static_cast<int>( that.getWidth()*GRID_XSIZE*zoomFactor );
    int zoomedHeight = static_cast<int>( that.getHeight()*GRID_YSIZE*zoomFactor );

	// Draw perspective frame
	Rectangle( graphicContext, screenX, screenY, screenX+zoomedWidth, screenY+zoomedHeight ); 

	// Save the graphic handles to restore them after station types and agent types are drawn
	HPEN hPerspectivePen = hPen;
	HPEN hPerspectivePenOld	= hPenOld;
	HBRUSH hPerspectiveBrush = hBrush;
	HBRUSH hPerspectiveBrushOld	= hBrushOld;
    HFONT  hPerspectiveFont = hFont;
	HFONT  hPerspectiveFontOld = hFontOld;

	// Draw inner grid
    CColor penColorGrid( GRID_RED, GRID_GREEN, GRID_BLUE );
    CColor brushColorGrid( 0, 0, 0 );
	bool isBoldPen = false;
	int gridWidth = that.getWidth();
	int gridHeight = that.getHeight();
//	MDrawWithStyles( graphicContext, screenX, screenY, zoomFactor, penColorGrid, brushColorGrid, false )°(that.grid)( gridWidth, gridHeight );
    MDrawWithStyles::handling( that.grid, graphicContext, screenX, screenY, zoomFactor, penColorGrid, brushColorGrid, false, gridWidth, gridHeight );

/*	
	// Draw inner component types
	list<CComponentType*>::iterator itComponentType;
	for( itComponentType = that.componentTypes.begin(); itComponentType != that.componentTypes.end(); itComponentType++ )
	{
		// Calculate relative component type's position
		int zoomedCompTypeX = (*itComponentType)->getX()*GRID_XSIZE*zoomFactor;
		int zoomedCompTypeY = (*itComponentType)->getY()*GRID_YSIZE*zoomFactor;

		// Style settings for component type
		CColor penColor( COMPONENTTYPE_FRAME_RED, COMPONENTTYPE_FRAME_GREEN, COMPONENTTYPE_FRAME_BLUE );
		CColor brushColor( (*itComponentType)->color.getRed(), (*itComponentType)->color.getGreen(), (*itComponentType)->color.getBlue() );
		bool isMarked = (*itComponentType)->getIsSelected();
		
		MDrawWithStyles( graphicContext, screenX+zoomedCompTypeX, screenY+zoomedCompTypeY, zoomFactor, penColor, brushColor, isMarked )<-itComponentType();
	}
*/

	// Restore graphic handles for perspective
	hPen = hPerspectivePen;
	hPenOld = hPerspectivePenOld;
	hBrush = hPerspectiveBrush;
	hBrushOld = hPerspectiveBrushOld;
    hFont = hPerspectiveFont;
	hFontOld = hPerspectiveFontOld; 

	// Calculate text on screen in pixels
	SIZE textSize;
    wchar_t wcName[ 512 ];  // NOTE THAT THE NAME IS NOT ALLLOWED TO BE LONGER THAN 512
    mbstowcs( wcName, that.getName().c_str(), 512 );
    GetTextExtentPoint32( graphicContext, wcName, static_cast<int>( that.getName().length() ), &textSize );
	
	// Draw title only if there is enought place 
    const int textOffset = static_cast<int>( 5*zoomFactor );  // text offset in pixels to left border of perspective frame
	if( zoomedWidth-textOffset > textSize.cx )
	{
		// Draw text highlighting
		LOGPEN logPen;
		logPen.lopnStyle = PS_NULL;
		
		HPEN hPen = CreatePenIndirect( &logPen );
        HPEN hPenOld = static_cast<HPEN>( SelectObject( graphicContext, hPen ) );

        Rectangle( graphicContext, screenX+textOffset, static_cast<int>( screenY-(textSize.cy/2.0) ), screenX+textOffset+textSize.cx, static_cast<int>( screenY-(textSize.cy/2.0)+textSize.cy ) );

		// Release pen
		SelectObject( graphicContext, hPenOld );
		DeleteObject( hPen );
		
		// Text
        TextOut( graphicContext, screenX+textOffset, static_cast<int>( screenY-(textSize.cy/2.0) ), wcName, static_cast<int>( that.getName().length() ) );
	}

    epilogue( &that, graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );
}


//void MDrawWithStyles::<CGrid>( int width, int height )
void MDrawWithStyles::handling( CGrid& that, METHOD_PARAMS_DRAWWITHSTYLES, int width, int height )
{
    prologue( that, graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );

    // Get the grid's size
	int gridXSize = that.getXSize();
	int gridYSize = that.getYSize();

	// Draw vertical grid lines
	int x = 0;
	for( x = 1; x < width; x++ )
	{
		// Calculate current grid position relative to the whole grid's position on screen
        int relGridXPos = static_cast<int>( x*gridXSize*zoomFactor );
        int relGridYPos = static_cast<int>( height*gridYSize*zoomFactor );

		// Draw line
        MoveToEx( graphicContext, screenX+relGridXPos, screenY+1, nullptr );
		LineTo( graphicContext, screenX+relGridXPos, screenY+relGridYPos-1 );
	}

	// Draw horizontal grid lines
	int y = 0;
	for( y = 1; y < height; y++ )
	{
		// Calculate current grid position relative to the whole grid's position on screen
        int relGridXPos = static_cast<int>( width*gridXSize*zoomFactor );
        int relGridYPos = static_cast<int>( y*gridYSize*zoomFactor );

		// Draw line
        MoveToEx( graphicContext, screenX+1, screenY+relGridYPos, nullptr );
		LineTo( graphicContext, screenX+relGridXPos-1, screenY+relGridYPos );
	}

    epilogue( that, graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, isSelected );
}
