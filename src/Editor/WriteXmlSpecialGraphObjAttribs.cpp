/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Constants.h"
#include "Perspective.h"
#include "StationType.h"
#include "AgentType.h"
#include <fstream>
#include <typeinfo>
//#pragma c-mol_hdrstop

//#include "WriteXmlSpecialGraphObjAttribs.hmol"
#include "WriteXmlSpecialGraphObjAttribs.hpp"





//------------------------------------------------------------
// Prologue
//------------------------------------------------------------


//MWriteXmlSpecialGraphObjAttribs::MWriteXmlSpecialGraphObjAttribs()
void MWriteXmlSpecialGraphObjAttribs::prologue( CGraphObj* that, METHOD_PARAMS_WRITEXMLSPECIALGRAPHOBJATTRIBS )
{
	// Write name
	ofsOutputFile << " " << XMLATTRIB_NAME << " = " << '"' << that->getName() << '"' ; 

	// Write x
	ofsOutputFile << " " << XMLATTRIB_X << " = " << '"' << that->getX() << '"'; 

	// Write y
	ofsOutputFile << " " << XMLATTRIB_Y << " = " << '"' << that->getY() << '"'; 
}





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MWriteXmlSpecialGraphObjAttribs::<CPerspective*>()
void MWriteXmlSpecialGraphObjAttribs::handling( CPerspective* that, METHOD_PARAMS_WRITEXMLSPECIALGRAPHOBJATTRIBS )
{
    prologue( that, ofsOutputFile );

	// Write width
	ofsOutputFile << " " << XMLATTRIB_WIDTH << " = " << '"' << that->getWidth() << '"'; 

	// Write height
	ofsOutputFile << " " << XMLATTRIB_HEIGHT << " = " << '"' << that->getHeight() << '"'; 
}

	
//void MWriteXmlSpecialGraphObjAttribs::<CStationType*, CAgentType*>()
void MWriteXmlSpecialGraphObjAttribs::handling( CComponentType* that, METHOD_PARAMS_WRITEXMLSPECIALGRAPHOBJATTRIBS )
{
    prologue( that, ofsOutputFile );

    // Write count
	ofsOutputFile << " " << XMLATTRIB_COUNT << " = " << '"' << that->getNoOfComponents() << '"'; 

	// Write colorRed
	ofsOutputFile << " " << XMLATTRIB_COLORRED << " = " << '"' << that->color.getRed() << '"'; 

	// Write colorGreen
	ofsOutputFile << " " << XMLATTRIB_COLORGREEN << " = " << '"' << that->color.getGreen() << '"'; 

	// Write colorBlue
	ofsOutputFile << " " << XMLATTRIB_COLORBLUE << " = " << '"' << that->color.getBlue() << '"'; 
	
	// Write interface
	// TODO: Implement interface support with line below
	ofsOutputFile << " " << XMLATTRIB_INTERFACE << " = " << '"' << XMLVALUE_NO << '"'; 
}
