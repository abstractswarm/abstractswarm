/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined EditorTypes_h
	#define EditorTypes_h

class CAgent;
class CStation;





// Mouse button type (needed to map mouse button types from UI)
enum ENMouseButton
{
	mbtLeft, mbtMiddle, mbtRight
};


// Key type (needed to map key types from UI; incomplete - only used keys are listed!)
enum ENKey
{
	kShift, kAlt, kStrg,  
	kDelete  // add further keys here...
};


// Tool type (needed to map selected tools independantly from UI layout)
enum ENTool
{
	tlNone,
	tlPerspective,
	tlStationType, tlAgentType, 
	tlPriority, tlFrequency, tlNecessity, tlTime, tlCycle,
	tlItem, tlSpace,
	tlCapacity, tlSize, tlSpeed,
	tlBoldVisitEdge, tlVisitEdge, tlPlaceEdge, tlTimeEdge
};


// Editor object type (needed for data transfer with host application and for type checking)
enum ENEditorObjType
{
	eotNone,
	eotPerspective,
	eotStationType, eotAgentType, 
	eotVisitEdge, eotPlaceEdge, eotTimeEdge
};


// Editor object data index (needed for data transfer with host application)
enum ENEditorObjDataIndex
{
	eodiName = 0,
	eodiCount = 1,
	eodiColor = 2,
	eodiHasPriority = 3, eodiHasFrequency = 4, eodiHasNecessity = 5, eodiHasTime = 6, eodiHasCycle = 7,
	eodiHasItem = 8, eodiHasSpace = 9,
	eodiHasCapacity = 10, eodiHasSize = 11, eodiHasSpeed = 12,
	eodiPriority = 13, eodiFrequency = 14, eodiNecessity = 15, eodiTime = 16, eodiCycle = 17,
	eodiItem = 18, eodiSpace = 19,
	eodiCapacity = 20, eodiSize = 21, eodiSpeed = 22,
	eodiBold = 23,
	eodiDirected1 = 24,
	eodiDirected2 = 25,
	eodiValue = 26,
	eodiAndConnection1 = 27,
	eodiAndConnection2 = 28,
	
	eodiMAXINDEX = 28
};

static const char * const  ATTRIBUTE_NAMES[] =
{
    "Name",
    "Count",
    "Color",
    "",    "",    "",
    "",    "",    "",
    "",    "",    "",
    "",
    "Priority",
    "Frequency",
    "Necessity",
    "Time",
    "Cycle",
    "Item",
    "Space",
    "Capacity",
    "Size",
    "Speed",
    "Bold"
};

// Editor object data (needed for data transfer with host application)
union UEditorObjData
{
	int intData;
	const char* pStringData;
	bool boolData;
	int rgbColorData[ 3 ];
};


// Editor message type (needed for giving a message to host application)
enum ENEditorMessage
{
	emsgNone,
	emsgNotEnoughSpaceForPerspective,
	emsgNotEnoughSpaceForComponentType,
	emsgNotEnoughSpaceForAttribute
};


/** Type for pointer to function to add an agent to simulation. */
typedef void (*TfpAddAgentToSimulation)( CAgent* );

/** Type for pointer to function to remove an agent from simulation. */
typedef void (*TfpRemoveAgentFromSimulation)( CAgent* );

/** Type for pointer to function to add a station to simulation. */
typedef void (*TfpAddStationToSimulation)( CStation* );

/** Type for pointer to function to remove a station from simulation. */
typedef void (*TfpRemoveStationFromSimulation)( CStation* );





#endif
