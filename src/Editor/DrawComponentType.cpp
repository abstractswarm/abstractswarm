/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Constants.h"

// Needed for platform dependent(!) visualization
#include "windows.h"

#include "StationType.h"
#include "AgentType.h"
#include <sstream>
#include <typeinfo>
//#pragma c-mol_hdrstop

#include "DrawComponentType.hpp"





using namespace std;





//------------------------------------------------------------
// Static data
//------------------------------------------------------------


static double zoomedWidth = 0;
static double zoomedHeight = 0;
static double frameOffset = 0;
static double frameX = 0;
static double frameY = 0;
static double frameX2 = 0;
static double frameY2 = 0;
static double frameWidth = 0;
static double frameHeight = 0;
static double frameAttribPadding = 0;
static int noOfComponents = 0;

static double attribYOffset = 0;




//------------------------------------------------------------
// Prologue
//------------------------------------------------------------


//MDrawComponentType::MDrawComponentType()
void MDrawComponentType::prologue( CComponentType* that, METHOD_PARAMS_DRAWCOMPONENTTYPE )
{
	// Get the draw factor
	double simulationDrawFactor = that->getDrawFactor();

	// Calculate zoomed width and height
	zoomedWidth = that->getWidth()*GRID_XSIZE*simulationDrawFactor*zoomFactor;
	zoomedHeight = that->getHeight()*GRID_YSIZE*simulationDrawFactor*zoomFactor;
	double zoomedWidthNoSimDrawFactor = that->getWidth()*GRID_XSIZE*zoomFactor;
	double zoomedHeightNoSimDrawFactor = that->getHeight()*GRID_YSIZE*zoomFactor;
	

	//
	// Calculate positions of the component type frame(s)
	//

	// Calculate frame(s) size
	frameWidth = COMPONENTTYPE_WIDTH*GRID_XSIZE*simulationDrawFactor*zoomFactor;
	frameHeight = COMPONENTTYPE_HEIGHT*GRID_YSIZE*simulationDrawFactor*zoomFactor;
	double frameWidthNoSimDrawFactor = COMPONENTTYPE_WIDTH*GRID_XSIZE*zoomFactor;
	double frameHeightNoSimDrawFactor = COMPONENTTYPE_HEIGHT*GRID_YSIZE*zoomFactor;

	// Calculate padding for attributes
	frameAttribPadding = (that->getHasAttribs() ? 1 : 0)*GRID_XSIZE*zoomFactor;

	frameX = screenX+zoomedWidth-frameWidth-frameAttribPadding;
	frameY = screenY+zoomedHeight-frameHeight;
	frameX2 = screenX+zoomedWidth-frameAttribPadding;
	frameY2 = screenY+zoomedHeight;
	double frameX2NoSimDrawFactor = screenX+zoomedWidthNoSimDrawFactor-frameAttribPadding;
	double frameY2NoSimDrawFactor = screenY+zoomedHeightNoSimDrawFactor;
	double simDrawFactorCenterXOffset = (frameX2 - (frameWidth / 2)) - (frameX2NoSimDrawFactor - (frameWidthNoSimDrawFactor / 2));
	double simDrawFactorCenterYOffset = (frameY2 - (frameHeight / 2)) - (frameY2NoSimDrawFactor - (frameHeightNoSimDrawFactor / 2));
	frameX = frameX - simDrawFactorCenterXOffset;
	frameY = frameY - simDrawFactorCenterYOffset;
	frameX2 = frameX2 - simDrawFactorCenterXOffset;
	frameY2 = frameY2 - simDrawFactorCenterYOffset;

	
	// Set the frame offset for component types (to have space for up to three frames)
	frameOffset = 2*zoomFactor;
	
	// Count components
	noOfComponents = that->getNoOfComponents();

	// Init the attribute offset
	attribYOffset = 0;
}


//------------------------------------------------------------
// Epilogue
//------------------------------------------------------------


//MDrawComponentType::~MDrawComponentType()
void MDrawComponentType::epilogue( CComponentType* that, METHOD_PARAMS_DRAWCOMPONENTTYPE )
{
	//
	// Draw component type title
	//
	
	// Calculate title size on screen in pixels
	string sTitle = that->getName();
    wchar_t wcTitle[ 512 ];  // NOTE THAT THE TITLE IS NOT ALLLOWED TO BE LONGER THAN 512
    mbstowcs( wcTitle, sTitle.c_str(), 512 );
    SIZE textSize;
    GetTextExtentPoint32( graphicContext, wcTitle, static_cast<int>( sTitle.length() ), &textSize );
	
	// Calculate title position 
	int textX = static_cast<int>( frameX+(frameWidth/2.0)-(textSize.cx/2.0)-(2*frameOffset/2.0) );
	int textY = static_cast<int>( frameY+(frameHeight/2.0)-(textSize.cy/2.0)+(2*frameOffset/2.0) );
	
	// If title doesn't fit in square +/- half a distance between objects
	// TODO: Change later to real (new current) component type width!!
    const int minTextXPos = static_cast<int>( frameX-((GRAPHOBJ_MIN_DISTANCE/2.0+0.5)*GRID_XSIZE*zoomFactor) );
	if( textX <= minTextXPos )
	{
		// Replace the end with '...' (horizontal ellipse) and make title string smaller until it fits 
        sTitle += "…";
        while( textX <= minTextXPos )
		{
			// Create the new title
            sTitle = sTitle.substr( 0, sTitle.length()-2 ) + "_";  // Should be "horizontal ellipse" ("…") instead of "underscore" ("_") here, but this leads to encoding problems which causes/may cause an infinite while loop here
            mbstowcs( wcTitle, sTitle.c_str(), 512 );

			// Calculate title size on screen in pixels
            GetTextExtentPoint32( graphicContext, wcTitle, static_cast<int>( sTitle.length() ), &textSize );

			// Calculate title position 
			textX = static_cast<int>( frameX+(frameWidth/2.0)-(textSize.cx/2.0)-(2*frameOffset/2.0) );
			textY = static_cast<int>( frameY+(frameHeight/2.0)-(textSize.cy/2.0)+(2*frameOffset/2.0) );
		}
	}
/*
	// Draw text highlighting
	LOGPEN logPen;
	logPen.lopnStyle = PS_NULL;
	
	HPEN hPen = CreatePenIndirect( &logPen );
	HPEN hPenOld = (HPEN) SelectObject( graphicContext, hPen );

	Rectangle( graphicContext, textX, textY, textX+textSize.cx, textY+textSize.cy ); 

	// Release pen
	SelectObject( graphicContext, hPenOld );
	DeleteObject( hPen );
*/
	// Draw title
    TextOut( graphicContext, textX, textY, wcTitle, static_cast<int>( sTitle.length() ) );


	//
	// Draw number of components
	//

	// Convert number of components to string
	ostringstream ossNoOfComponents; 
	ossNoOfComponents << noOfComponents;
	string sNoOfComponents = ossNoOfComponents.str();
    wchar_t wcNoOfComponents[ 512 ];  // NOTE THAT THE NUMBER OF COMPONENTS IS NOT ALLLOWED TO BE LONGER THAN 512
    mbstowcs( wcNoOfComponents, sNoOfComponents.c_str(), 512 );

	// Calculate "number of components" size on screen in pixels
	SIZE noOfComponentsSize;
    GetTextExtentPoint32( graphicContext, wcNoOfComponents, static_cast<int>( sNoOfComponents.length() ), &noOfComponentsSize );

	// Draw number of components
	int noOfComponentsX = static_cast<int>( frameX-noOfComponentsSize.cx );
	int noOfComponentsY = static_cast<int>( frameY-noOfComponentsSize.cy );
    TextOut( graphicContext, noOfComponentsX, noOfComponentsY, wcNoOfComponents, static_cast<int>( sNoOfComponents.length() ) );


	// Draw component type attributes, if defined
	
	// Cycle
	if( that->getHasCycle() )
	{
		// Convert cycle value to string
		ostringstream ossValue; 
		ossValue << that->getCycle();
		string sValue = ossValue.str();
		
		// Build whole string
		string sCycle( ATTRIBTEXT_CYCLE );
		sCycle = sCycle+" "+sValue;
        wchar_t wcCycle[ 512 ];  // NOTE THAT THE CYCLE IS NOT ALLLOWED TO BE LONGER THAN 512
        mbstowcs( wcCycle, sCycle.c_str(), 512 );

		// Get size on screen
		SIZE cycleSize;
        GetTextExtentPoint32( graphicContext, wcCycle, static_cast<int>( sCycle.length() ), &cycleSize );
		
		// Remember attribute y offset for next attribute
		attribYOffset = attribYOffset-cycleSize.cy;
		
		// Draw Text on screen
		int attribX = static_cast<int>( frameX2+frameAttribPadding-cycleSize.cx );
		int attribY = static_cast<int>( frameY+attribYOffset );
        TextOut( graphicContext, attribX, attribY, wcCycle, static_cast<int>( sCycle.length() ) );
	}

	// Time
	if( that->getHasTime() )
	{
		// Convert time value to string
		ostringstream ossValue; 
		ossValue << that->getTime();
		string sValue = ossValue.str();
		
		// Build whole string
		string sTime( ATTRIBTEXT_TIME );
		sTime = sTime+" "+sValue;
        wchar_t wcTime[ 512 ];  // NOTE THAT THE TIME IS NOT ALLLOWED TO BE LONGER THAN 512
        mbstowcs( wcTime, sTime.c_str(), 512 );

		// Get size on screen
		SIZE timeSize;
        GetTextExtentPoint32( graphicContext, wcTime, static_cast<int>( sTime.length() ), &timeSize );
		
		// Remember attribute y offset for next attribute
		attribYOffset = attribYOffset-timeSize.cy;
		
		// Draw Text on screen
		int attribX = static_cast<int>( frameX2+frameAttribPadding-timeSize.cx );
		int attribY = static_cast<int>( frameY+attribYOffset );
        TextOut( graphicContext, attribX, attribY, wcTime, static_cast<int>( sTime.length() ) );
	}


	// Necessity
	if( that->getHasNecessity() )
	{
		// Convert necessity value to string
		ostringstream ossValue; 
		ossValue << that->getNecessity();
		string sValue = ossValue.str();
		
		// Build whole string
		string sNecessity( ATTRIBTEXT_NECESSITY );
		sNecessity = sNecessity+" "+sValue;
        wchar_t wcNecessity[ 512 ];  // NOTE THAT THE NECESSITY IS NOT ALLLOWED TO BE LONGER THAN 512
        mbstowcs( wcNecessity, sNecessity.c_str(), 512 );

		// Get size on screen
		SIZE necessitySize;
        GetTextExtentPoint32( graphicContext, wcNecessity, static_cast<int>( sNecessity.length() ), &necessitySize );
		
		// Remember attribute y offset for next attribute
		attribYOffset = attribYOffset-necessitySize.cy;
		
		// Draw Text on screen
		int attribX = static_cast<int>( frameX2+frameAttribPadding-necessitySize.cx );
		int attribY = static_cast<int>( frameY+attribYOffset );
        TextOut( graphicContext, attribX, attribY, wcNecessity, static_cast<int>( sNecessity.length() ) );
	}


	// Frequency
	if( that->getHasFrequency() )
	{
		// Convert frequency value to string
		ostringstream ossValue; 
		ossValue << that->getFrequency();
		string sValue = ossValue.str();
		
		// Build whole string
		string sFrequency( ATTRIBTEXT_FREQUENCY );
		sFrequency = sFrequency+" "+sValue;
        wchar_t wcFrequency[ 512 ];  // NOTE THAT THE FREQUENCY IS NOT ALLLOWED TO BE LONGER THAN 512
        mbstowcs( wcFrequency, sFrequency.c_str(), 512 );

		// Get size on screen
		SIZE frequencySize;
        GetTextExtentPoint32( graphicContext, wcFrequency, static_cast<int>( sFrequency.length() ), &frequencySize );
		
		// Remember attribute y offset for next attribute
		attribYOffset = attribYOffset-frequencySize.cy;
		
		// Draw Text on screen
		int attribX = static_cast<int>( frameX2+frameAttribPadding-frequencySize.cx );
		int attribY = static_cast<int>( frameY+attribYOffset );
        TextOut( graphicContext, attribX, attribY, wcFrequency, static_cast<int>( sFrequency.length() ) );
	}

	// Priority
	if( that->getHasPriority() )
	{
		// Convert priority value to string
		ostringstream ossValue; 
		ossValue << that->getPriority();
		string sValue = ossValue.str();
		
		// Build whole string
		string sPriority( ATTRIBTEXT_PRIORITY );
		sPriority = sPriority+" "+sValue;
        wchar_t wcPriority[ 512 ];  // NOTE THAT THE PRIORITY IS NOT ALLLOWED TO BE LONGER THAN 512
        mbstowcs( wcPriority, sPriority.c_str(), 512 );

		// Get size on screen
		SIZE prioritySize;
        GetTextExtentPoint32( graphicContext, wcPriority, static_cast<int>( sPriority.length() ), &prioritySize );

		// Remember attribute y offset for next attribute
		attribYOffset = attribYOffset-prioritySize.cy;
		
		// Draw Text on screen
		int attribX = static_cast<int>( frameX2+frameAttribPadding-prioritySize.cx );
		int attribY = static_cast<int>( frameY+attribYOffset );
        TextOut( graphicContext, attribX, attribY, wcPriority, static_cast<int>( sPriority.length() ) );
	}
}


//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MDrawComponentType::<CStationType*>()
void MDrawComponentType::handling( CStationType* that, METHOD_PARAMS_DRAWCOMPONENTTYPE )
{
    prologue( that, graphicContext, screenX, screenY, zoomFactor );

	// Draw station type with up to three frames, depending on how many stations are represented
	if( noOfComponents <= 1 )
	{
		Rectangle( graphicContext, static_cast<int>( frameX ), static_cast<int>( frameY+2*frameOffset ), static_cast<int>( frameX2-2*frameOffset ), static_cast<int>( frameY2 ) ); 
	}
	else if( noOfComponents == 2 )
	{
		Rectangle( graphicContext, static_cast<int>( frameX+frameOffset ), static_cast<int>( frameY+frameOffset ), static_cast<int>( frameX2-frameOffset ), static_cast<int>( frameY2-frameOffset ) ); 
		Rectangle( graphicContext, static_cast<int>( frameX ), static_cast<int>( frameY+2*frameOffset ), static_cast<int>( frameX2-2*frameOffset ), static_cast<int>( frameY2 ) ); 
	}
	else if( noOfComponents >= 3 )
	{
		Rectangle( graphicContext, static_cast<int>( frameX+2*frameOffset ), static_cast<int>( frameY ), static_cast<int>( frameX2 ), static_cast<int>( frameY2-2*frameOffset ) ); 
		Rectangle( graphicContext, static_cast<int>( frameX+frameOffset ), static_cast<int>( frameY+frameOffset ), static_cast<int>( frameX2-frameOffset ), static_cast<int>( frameY2-frameOffset ) ); 
		Rectangle( graphicContext, static_cast<int>( frameX ), static_cast<int>( frameY+2*frameOffset ), static_cast<int>( frameX2-2*frameOffset ), static_cast<int>( frameY2 ) ); 
	}

	
	// Draw station type attributes, if defined

	// Space
	if( that->getHasSpace() )
	{
		// Convert space value to string
		ostringstream ossValue; 
		ossValue << that->getSpace();
		string sValue = ossValue.str();
		
		// Build whole string
		string sSpace( ATTRIBTEXT_SPACE );
		sSpace = sSpace+" "+sValue;
        wchar_t wcSpace[ 512 ];  // NOTE THAT THE SPACE IS NOT ALLLOWED TO BE LONGER THAN 512
        mbstowcs( wcSpace, sSpace.c_str(), 512 );

		// Get size on screen
		SIZE spaceSize;
        GetTextExtentPoint32( graphicContext, wcSpace, static_cast<int>( sSpace.length() ), &spaceSize );
		
		// Remember attribute y offset for next attribute
		attribYOffset = attribYOffset-spaceSize.cy;
		
		// Draw Text on screen
		int attribX = static_cast<int>( frameX2+frameAttribPadding-spaceSize.cx );
		int attribY = static_cast<int>( frameY+attribYOffset );
        TextOut( graphicContext, attribX, attribY, wcSpace, static_cast<int>( sSpace.length() ) );
	}

	// Item
	if( that->getHasItem() )
	{
		// Convert item value to string
		ostringstream ossValue; 
		ossValue << that->getItem();
		string sValue = ossValue.str();
		
		// Build whole string
		string sItem( ATTRIBTEXT_ITEM );
		sItem = sItem+" "+sValue;
        wchar_t wcItem[ 512 ];  // NOTE THAT THE ITEM IS NOT ALLLOWED TO BE LONGER THAN 512
        mbstowcs( wcItem, sItem.c_str(), 512 );

		// Get size on screen
		SIZE itemSize;
        GetTextExtentPoint32( graphicContext, wcItem, static_cast<int>( sItem.length() ), &itemSize );

		// Remember attribute y offset for next attribute
		attribYOffset = attribYOffset-itemSize.cy;
		
		// Draw Text on screen
		int attribX = static_cast<int>( frameX2+frameAttribPadding-itemSize.cx );
		int attribY = static_cast<int>( frameY+attribYOffset );
        TextOut( graphicContext, attribX, attribY, wcItem, static_cast<int>( sItem.length() ) );
	}

    epilogue( that, graphicContext, screenX, screenY, zoomFactor );
}


//void MDrawComponentType::<CAgentType*>()
void MDrawComponentType::handling( CAgentType* that, METHOD_PARAMS_DRAWCOMPONENTTYPE )
{
    prologue( that, graphicContext, screenX, screenY, zoomFactor );

    // Draw agent type with up to three frames, depending on how many agents are represented
	if( noOfComponents <= 1 )
	{
		Ellipse( graphicContext, static_cast<int>( frameX ), static_cast<int>( frameY+2*frameOffset ), static_cast<int>( frameX2-2*frameOffset ), static_cast<int>( frameY2 ) ); 
	}
	else if( noOfComponents == 2 )
	{
		Ellipse( graphicContext, static_cast<int>( frameX+frameOffset ), static_cast<int>( frameY+frameOffset ), static_cast<int>( frameX2-frameOffset ), static_cast<int>( frameY2-frameOffset ) ); 
		Ellipse( graphicContext, static_cast<int>( frameX ), static_cast<int>( frameY+2*frameOffset ), static_cast<int>( frameX2-2*frameOffset ), static_cast<int>( frameY2 ) ); 
	}
	else if( noOfComponents >= 3 )
	{
		Ellipse( graphicContext, static_cast<int>( frameX+2*frameOffset ), static_cast<int>( frameY ), static_cast<int>( frameX2 ), static_cast<int>( frameY2-2*frameOffset ) ); 
		Ellipse( graphicContext, static_cast<int>( frameX+frameOffset ), static_cast<int>( frameY+frameOffset ), static_cast<int>( frameX2-frameOffset ), static_cast<int>( frameY2-frameOffset ) ); 
		Ellipse( graphicContext, static_cast<int>( frameX ), static_cast<int>( frameY+2*frameOffset ), static_cast<int>( frameX2-2*frameOffset ), static_cast<int>( frameY2 ) ); 
	}

	
	// Draw agent type attributes, if defined

	// Speed
	if( that->getHasSpeed() )
	{
		// Convert speed value to string
		ostringstream ossValue; 
		ossValue << that->getSpeed();
		string sValue = ossValue.str();
		
		// Build whole string
		string sSpeed( ATTRIBTEXT_SPEED );
		sSpeed = sSpeed+" "+sValue;
        wchar_t wcSpeed[ 512 ];  // NOTE THAT THE SPEED IS NOT ALLLOWED TO BE LONGER THAN 512
        mbstowcs( wcSpeed, sSpeed.c_str(), 512 );

		// Get size on screen
		SIZE speedSize;
        GetTextExtentPoint32( graphicContext, wcSpeed, static_cast<int>( sSpeed.length() ), &speedSize );
		
		// Remember attribute y offset for next attribute
		attribYOffset = attribYOffset-speedSize.cy;
		
		// Draw Text on screen
		int attribX = static_cast<int>( frameX2+frameAttribPadding-speedSize.cx );
		int attribY = static_cast<int>( frameY+attribYOffset );
        TextOut( graphicContext, attribX, attribY, wcSpeed, static_cast<int>( sSpeed.length() ) );
	}

	// Size
	if( that->getHasSize() )
	{
		// Convert size value to string
		ostringstream ossValue; 
		ossValue << that->getSize();
		string sValue = ossValue.str();
		
		// Build whole string
		string sSize( ATTRIBTEXT_SIZE );
		sSize = sSize+" "+sValue;
        wchar_t wcSize[ 512 ];  // NOTE THAT THE SIZE IS NOT ALLLOWED TO BE LONGER THAN 512
        mbstowcs( wcSize, sSize.c_str(), 512 );

		// Get size on screen
		SIZE sizeSize;
        GetTextExtentPoint32( graphicContext, wcSize, static_cast<int>( sSize.length() ), &sizeSize );
		
		// Remember attribute y offset for next attribute
		attribYOffset = attribYOffset-sizeSize.cy;
		
		// Draw Text on screen
		int attribX = static_cast<int>( frameX2+frameAttribPadding-sizeSize.cx );
		int attribY = static_cast<int>( frameY+attribYOffset );
        TextOut( graphicContext, attribX, attribY, wcSize, static_cast<int>( sSize.length() ) );
	}

	// Capacity
	if( that->getHasCapacity() )
	{
		// Convert capacity value to string
		ostringstream ossValue; 
		ossValue << that->getCapacity();
		string sValue = ossValue.str();
		
		// Build whole string
		string sCapacity( ATTRIBTEXT_CAPACITY );
		sCapacity = sCapacity+" "+sValue;
        wchar_t wcCapacity[ 512 ];  // NOTE THAT THE CAPACITY IS NOT ALLLOWED TO BE LONGER THAN 512
        mbstowcs( wcCapacity, sCapacity.c_str(), 512 );

		// Get size on screen
		SIZE capacitySize;
        GetTextExtentPoint32( graphicContext, wcCapacity, static_cast<int>( sCapacity.length() ), &capacitySize );
		
		// Remember attribute y offset for next attribute
		attribYOffset = attribYOffset-capacitySize.cy;
		
		// Draw Text on screen
		int attribX = static_cast<int>( frameX2+frameAttribPadding-capacitySize.cx );
		int attribY = static_cast<int>( frameY+attribYOffset );
        TextOut( graphicContext, attribX, attribY, wcCapacity, static_cast<int>( sCapacity.length() ) );
	}

    epilogue( that, graphicContext, screenX, screenY, zoomFactor );
}
