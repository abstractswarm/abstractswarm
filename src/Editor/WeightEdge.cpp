/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "WeightEdge.h"





//------------------------------------------------------------
// Construction
//------------------------------------------------------------

CWeightEdge::CWeightEdge()
{
	value = 0;
}


//------------------------------------------------------------
// Destruction
//------------------------------------------------------------

CWeightEdge::~CWeightEdge()
{
}


//------------------------------------------------------------
// Getter
//------------------------------------------------------------


int CWeightEdge::getValue() const
{
	return( value );
}


bool CWeightEdge::getIsDirected1() const
{
	if( connector1.getIsDirected() == true )
		return( true );
	else
		return( false );
}


bool CWeightEdge::getIsDirected2() const
{
	if( connector2.getIsDirected() == true )
		return( true );
	else
		return( false );
}


//------------------------------------------------------------
// Setter
//------------------------------------------------------------


void CWeightEdge::setValue( int value )
{
	this->value = value;
}


void CWeightEdge::setIsDirected1( bool isDirected )
{
	// Only one end can be directed 
	if( isDirected )
	{
		connector2.setIsDirected( false );
		connector1.setIsDirected( true );
	}
	else
		connector1.setIsDirected( false );
}


void CWeightEdge::setIsDirected2( bool isDirected )
{
	// Only one end can be directed 
	if( isDirected )
	{
		connector1.setIsDirected( false );
		connector2.setIsDirected( true );
	}
	else
		connector2.setIsDirected( false );
}
