#if !defined SetEditorObjType_hmol
	#define SetEditorObjType_hmol





union UEditorObjData;
#include <typeinfo>
#include "EditorTypes.h"
#include "Perspective.h"
#include "StationType.h"
#include "AgentType.h"
#include "VisitEdge.h"
#include "PlaceEdge.h"
#include "TimeEdge.h"





//method MSetEditorObjData( UEditorObjData editorObjData[] )
class MSetEditorObjData
{
#define METHOD_PARAMS_SETEDITOROBJDATA UEditorObjData editorObjData[]

public:

	// Handlings
//	virtual void <CPerspective*>();
    static void handling( CPerspective* that, METHOD_PARAMS_SETEDITOROBJDATA );
//	virtual void <CStationType*>();
    static void handling( CStationType* that, METHOD_PARAMS_SETEDITOROBJDATA );
//	virtual void <CAgentType*>();
    static void handling( CAgentType* that, METHOD_PARAMS_SETEDITOROBJDATA );
//	virtual void <CVisitEdge*>();
    static void handling( CVisitEdge* that, METHOD_PARAMS_SETEDITOROBJDATA );
//	virtual void <CPlaceEdge*>();
    static void handling( CPlaceEdge* that, METHOD_PARAMS_SETEDITOROBJDATA );
//	virtual void <CTimeEdge*>();
    static void handling( CTimeEdge* that, METHOD_PARAMS_SETEDITOROBJDATA );

    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline void handling( void* vp_that, METHOD_PARAMS_SETEDITOROBJDATA )
    {
        if( typeid( *(static_cast<CPerspective*>( vp_that ))) == typeid( CPerspective ) )
            handling( static_cast<CPerspective*>( vp_that ), editorObjData );
        else if( typeid( *(static_cast<CStationType*>( vp_that ))) == typeid( CStationType ) )
            handling( static_cast<CStationType*>( vp_that ), editorObjData );
        else if( typeid( *(static_cast<CAgentType*>( vp_that ))) == typeid( CAgentType ) )
            handling( static_cast<CAgentType*>( vp_that ), editorObjData );
        else if( typeid( *(static_cast<CVisitEdge*>( vp_that ))) == typeid( CVisitEdge ) )
            handling( static_cast<CVisitEdge*>( vp_that ), editorObjData );
        else if( typeid( *(static_cast<CPlaceEdge*>( vp_that ))) == typeid( CPlaceEdge ) )
            handling( static_cast<CPlaceEdge*>( vp_that ), editorObjData );
        else if( typeid( *(static_cast<CTimeEdge*>( vp_that ))) == typeid( CTimeEdge ) )
            handling( static_cast<CTimeEdge*>( vp_that ), editorObjData );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }
};





#endif
