/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Constants.h"

// Needed for platform dependent(!) visualization
#include "windows.h"

#include "PlaceEdge.h"
#include "TimeEdge.h"
#include "VisitEdge.h"
#include "./../Simulation/Components/Agent.hpp"
#include "./../Simulation/Components/Station.hpp"
#include <sstream>
#include <math.h>
#include <typeinfo>
//#pragma c-mol_hdrstop

#include "DrawWeightEdge.hpp"
#include "CalcEdgeDrawOffset.hpp"





using namespace std;





//------------------------------------------------------------
// Data
//------------------------------------------------------------

extern int zoomedX2;
extern int zoomedY2;

extern int xOffset1;
extern int yOffset1;
extern int xOffset2;
extern int yOffset2;

extern double edgeAngle;

static bool isDrawWeightEdgeAdditionalParts;





//------------------------------------------------------------
// Prologue
//------------------------------------------------------------


//MDrawWeightEdge::~MDrawWeightEdge()
void MDrawWeightEdge::prologue( CWeightEdge* that, METHOD_PARAMS_DRAWWEIGHTEDGE )
{
    isDrawWeightEdgeAdditionalParts = true;
}


//------------------------------------------------------------
// Epilogue
//------------------------------------------------------------


//MDrawWeightEdge::~MDrawWeightEdge()
void MDrawWeightEdge::epilogue( CWeightEdge* that, METHOD_PARAMS_DRAWWEIGHTEDGE )
{
    if( isDrawWeightEdgeAdditionalParts )
    {
        // Draw diamond

        POINT diamondPoints[ 4 ];

        // Calculate the top and bottom points
        double aLeg = zoomedX2-screenX;
        double oLeg = zoomedY2-screenY;
        double alpha = atan2( oLeg, aLeg );
        double deltaX = sin(PI-alpha)*(EDGE_DIAMONDSIZE/2.0)*zoomFactor;
        double deltaY = cos(PI-alpha)*(EDGE_DIAMONDSIZE/2.0)*zoomFactor;
        diamondPoints[ 0 ].x = static_cast<long>( screenX+(aLeg/2.0)-deltaX );
        diamondPoints[ 0 ].y = static_cast<long>( screenY+(oLeg/2.0)-deltaY );
        diamondPoints[ 2 ].x = static_cast<long>( screenX+(aLeg/2.0)+deltaX );
        diamondPoints[ 2 ].y = static_cast<long>( screenY+(oLeg/2.0)+deltaY );

        // Calculate the left and right points
        alpha = atan2( oLeg, aLeg )+(PI/2.0);
        deltaX = sin(PI-alpha)*(EDGE_DIAMONDSIZE/2.0)*zoomFactor;
        deltaY = cos(PI-alpha)*(EDGE_DIAMONDSIZE/2.0)*zoomFactor;
        diamondPoints[ 1 ].x = static_cast<long>( screenX+(aLeg/2.0)-deltaX );
        diamondPoints[ 1 ].y = static_cast<long>( screenY+(oLeg/2.0)-deltaY );
        diamondPoints[ 3 ].x = static_cast<long>( screenX+(aLeg/2.0)+deltaX );
        diamondPoints[ 3 ].y = static_cast<long>( screenY+(oLeg/2.0)+deltaY );

        Polygon( graphicContext, diamondPoints, 4 );


        // Draw edge arrow (if it is directed)

        // If directed to first connected type
        if( that->getIsDirected1() )
        {
            // Calculate the points
            double aLeg = zoomedX2-screenX;
            double oLeg = zoomedY2-screenY;
            double alpha = atan2( oLeg, aLeg );
            double deltaWidthX = sin(PI-alpha)*(EDGE_ARROWWIDTH/2.0)*zoomFactor;
            double deltaWidthY = cos(PI-alpha)*(EDGE_ARROWWIDTH/2.0)*zoomFactor;
            double deltaLengthX = cos(PI-alpha)*EDGE_ARROWLENGTH*zoomFactor;
            double deltaLengthY = sin(PI-alpha)*EDGE_ARROWLENGTH*zoomFactor;

            double arrowHeadX = screenX+xOffset1;
            double arrowHeadY = screenY+yOffset1;

            double topPointX = screenX+xOffset1-deltaWidthX-deltaLengthX;
            double topPointY = screenY+yOffset1-deltaWidthY+deltaLengthY;

            double bottomPointX = screenX+xOffset1+deltaWidthX-deltaLengthX;
            double bottomPointY = screenY+yOffset1+deltaWidthY+deltaLengthY;

            // Draw top line of the arrow
            MoveToEx( graphicContext, static_cast<int>( arrowHeadX ), static_cast<int>( arrowHeadY ), nullptr );
            LineTo( graphicContext, static_cast<int>( topPointX ), static_cast<int>( topPointY ) );

            // Draw bottom line of the arrow
            MoveToEx( graphicContext, static_cast<int>( arrowHeadX ), static_cast<int>( arrowHeadY ), nullptr );
            LineTo( graphicContext, static_cast<int>( bottomPointX ), static_cast<int>( bottomPointY ) );
        }

        // If directed to second connected type
        if( that->getIsDirected2() )
        {
            // Calculate the points
            double aLeg = zoomedX2-screenX;
            double oLeg = zoomedY2-screenY;
            double alpha = atan2( oLeg, aLeg );
            double deltaWidthX = sin(PI-alpha)*(EDGE_ARROWWIDTH/2.0)*zoomFactor;
            double deltaWidthY = cos(PI-alpha)*(EDGE_ARROWWIDTH/2.0)*zoomFactor;
            double deltaLengthX = cos(PI-alpha)*EDGE_ARROWLENGTH*zoomFactor;
            double deltaLengthY = sin(PI-alpha)*EDGE_ARROWLENGTH*zoomFactor;

            double arrowHeadX = zoomedX2+xOffset2;
            double arrowHeadY = zoomedY2+yOffset2;

            double topPointX = zoomedX2+xOffset2+deltaWidthX+deltaLengthX;
            double topPointY = zoomedY2+yOffset2+deltaWidthY-deltaLengthY;

            double bottomPointX = zoomedX2+xOffset2-deltaWidthX+deltaLengthX;
            double bottomPointY = zoomedY2+yOffset2-deltaWidthY-deltaLengthY;

            // Draw top line of the arrow
            MoveToEx( graphicContext, static_cast<int>( arrowHeadX ), static_cast<int>( arrowHeadY ), nullptr );
            LineTo( graphicContext, static_cast<int>( topPointX ), static_cast<int>( topPointY ) );

            // Draw bottom line of the arrow
            MoveToEx( graphicContext, static_cast<int>( arrowHeadX ), static_cast<int>( arrowHeadY ), nullptr );
            LineTo( graphicContext, static_cast<int>( bottomPointX ), static_cast<int>( bottomPointY ) );
        }


        // Draw edge value text

        // Calculate the angle of edge due relative to horizontal base line
        alpha = atan2( oLeg, aLeg );
        if( (fabs( alpha ) > (PI/2.0)) )
            alpha += PI;

        // Modify current selected font by angle
        HFONT hCurFont;
        hCurFont = static_cast<HFONT>( GetCurrentObject( graphicContext, OBJ_FONT ) );
        LOGFONT logFont;
        GetObject( hCurFont, sizeof( LOGFONT ), &logFont );
        logFont.lfEscapement = static_cast<long>( -((alpha*180)/PI)*10 );  // angle of escapement

        // Create an select new modified font
        HFONT hFont = CreateFontIndirect( &logFont );
        HFONT hFontOld = static_cast<HFONT>( SelectObject( graphicContext, hFont ) );

        // Prepeare edge value text
        ostringstream ossEdgeValue;
        ossEdgeValue << that->getValue();
        string sEdgeValue( ossEdgeValue.str() );
        wchar_t wcEdgeValue[ 512 ];  // NOTE THAT THE EDGE VALUE IS NOT ALLLOWED TO BE LONGER THAN 512
        mbstowcs( wcEdgeValue, sEdgeValue.c_str(), 512 );

        // Calculate egde value size on screen
        SIZE valueSize;
        GetTextExtentPoint32( graphicContext, wcEdgeValue, static_cast<int>( sEdgeValue.length() ), &valueSize );

        // Calculate egde value text offset on screen
        double hyp1 = (valueSize.cy/2.0);
        double aLeg1 = cos( -alpha )*hyp1;
        double oLeg1 = sin( -alpha )*hyp1;

        double hyp2 = (valueSize.cx/2.0);
        double aLeg2 = cos( -alpha )*hyp2;
        double oLeg2 = sin( -alpha )*hyp2;

        double textDeltaX = aLeg2+oLeg1;
        double textDeltaY = aLeg1-oLeg2;

        // Draw the value text
        TextOut( graphicContext, static_cast<int>( screenX+(aLeg/2.0)-textDeltaX ), static_cast<int>( screenY+(oLeg/2.0)-textDeltaY ), wcEdgeValue, static_cast<int>( sEdgeValue.length() ) );

        // Release new modified font
        SelectObject( graphicContext, hFontOld );
        DeleteObject( hFont );
    }
}


//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MDrawWeightEdge::<CPlaceEdge*>()
void MDrawWeightEdge::handling( CPlaceEdge* that, METHOD_PARAMS_DRAWWEIGHTEDGE, const CAgent* pAgentOnEdge, const CEdgeConnector* pTargetEdgeConnector, int coveredDistanceOnEdge )
{
    prologue( that, graphicContext, screenX, screenY, zoomFactor );

    // If an agent to draw on the edge is provided
    if( pAgentOnEdge != nullptr )
    {
        // Calculate the percentage of the agent's progress from its starting point to its target
        double progressPercent = static_cast<double>( coveredDistanceOnEdge ) / that->getValue();

        // Calculate agent size with direction indicator
        const int agentSize = 5;
        int zoomedAgentSize = static_cast<int>( agentSize * zoomFactor );
        double unzoomedEdgeLength = sqrt( ((zoomedX2+xOffset2) - (screenX+xOffset1)) * ((zoomedX2+xOffset2) - (screenX+xOffset1)) + ((zoomedY2+yOffset2) - (screenY+yOffset1)) * ((zoomedY2+yOffset2) - (screenY+yOffset1)) ) / zoomFactor;
        double agentDirIndicatorPosOffset = 0;
        if( unzoomedEdgeLength > 0 )
        {
            agentDirIndicatorPosOffset = (agentSize * 0.85) / unzoomedEdgeLength;  // Depends on the (unzoomed) edge length to be invariant against the length of the edge
        }

        // Determine direction
        int agentX = 0;
        int agentY = 0;
        int agentDirIndicatorX = 0;
        int agentDirIndicatorY = 0;
        if( pTargetEdgeConnector == (&(that->getConnector2())) )
        {
            // Calculate the agent's x and y position
            agentX = screenX+xOffset1 + ((zoomedX2+xOffset2) - (screenX+xOffset1)) * progressPercent;
            agentY = screenY+yOffset1 + ((zoomedY2+yOffset2) - (screenY+yOffset1)) * progressPercent;

            // Calculate the agent's x and y direction indicator
            agentDirIndicatorX = screenX+xOffset1 + ((zoomedX2+xOffset2) - (screenX+xOffset1)) * (progressPercent + agentDirIndicatorPosOffset);
            agentDirIndicatorY = screenY+yOffset1 + ((zoomedY2+yOffset2) - (screenY+yOffset1)) * (progressPercent + agentDirIndicatorPosOffset);
        }
        else
        {
            // Calculate the agent's x and y position vice-versa
            agentX = screenX+xOffset1 + ((zoomedX2+xOffset2) - (screenX+xOffset1)) * (1.0 - progressPercent);
            agentY = screenY+yOffset1 + ((zoomedY2+yOffset2) - (screenY+yOffset1)) * (1.0 - progressPercent);

            // Calculate the agent's x and y direction indicator
            agentDirIndicatorX = screenX+xOffset1 + ((zoomedX2+xOffset2) - (screenX+xOffset1)) * (1.0 - (progressPercent + agentDirIndicatorPosOffset));
            agentDirIndicatorY = screenY+yOffset1 + ((zoomedY2+yOffset2) - (screenY+yOffset1)) * (1.0 - (progressPercent + agentDirIndicatorPosOffset));
        }


        //
        // Draw agent (according to its size?)
        //

        // Draw the agent's direction indicator

        HBRUSH hBrush = static_cast<HBRUSH>( CreateSolidBrush( RGB( COMPONENTTYPE_FRAME_RED, COMPONENTTYPE_FRAME_GREEN, COMPONENTTYPE_FRAME_BLUE ) ) );
        HBRUSH hBrushOld = static_cast<HBRUSH>( SelectObject( graphicContext, hBrush ) );

        Ellipse( graphicContext, agentDirIndicatorX - (zoomedAgentSize / 2.0), agentDirIndicatorY - (zoomedAgentSize / 2.0), agentDirIndicatorX + (zoomedAgentSize / 2.0), agentDirIndicatorY + (zoomedAgentSize / 2.0) );

        // Release brush
        SelectObject( graphicContext, hBrushOld );
        DeleteObject( hBrush );

        // Draw agent
        Ellipse( graphicContext, agentX - zoomedAgentSize, agentY - zoomedAgentSize, agentX + zoomedAgentSize, agentY + zoomedAgentSize );


        // No additional weight edge parts (diamond and arrow) if agent is drawn on edge here only
        // (diamond is expected to be drawn already and the agent will be drawn on top of it)
        isDrawWeightEdgeAdditionalParts = false;
    }
    else
    {
        // Draw line
        MoveToEx( graphicContext, screenX+xOffset1, screenY+yOffset1, nullptr );
        LineTo( graphicContext, zoomedX2+xOffset2, zoomedY2+yOffset2 );
    }

    epilogue( that, graphicContext, screenX, screenY, zoomFactor );
}


//void MDrawWeightEdge::<CTimeEdge*>()
void MDrawWeightEdge::handling( CTimeEdge* that, METHOD_PARAMS_DRAWWEIGHTEDGE )
{
    prologue( that, graphicContext, screenX, screenY, zoomFactor );

    const int edgeWidth = 5;

	// Calculate the little offset due to time edge width, which is little to big for its arrow
	double aLeg = zoomedX2-screenX;
	double oLeg = zoomedY2-screenY;
	double alpha = atan2( oLeg, aLeg );
	double deltaLengthX = cos(PI-alpha)*((edgeWidth+1)/2.0)*zoomFactor;
	double deltaLengthY = sin(PI-alpha)*((edgeWidth+1)/2.0)*zoomFactor;

	//
	// Draw outer lines
	//
	
	// Get and modify the current pen style with width
	HPEN hCurPen;
    hCurPen = static_cast<HPEN>( GetCurrentObject( graphicContext, OBJ_PEN ) );
	LOGPEN logPen;
	GetObject( hCurPen, sizeof( LOGPEN ), &logPen );
	
	// Safe the old set pen width (needed to calculate pen width of outer lines)
	int oldPenWidth = logPen.lopnWidth.x;

	// Modify pen width
    logPen.lopnWidth.x = static_cast<long>( edgeWidth*zoomFactor+((oldPenWidth-1)*2)/2 );
                                                                // Also consider the currently set pen width; this is
                                                                // neccessary due to trick with a thick line as background
                                                                // and a white thin line as foreground to draw double lined
                                                                // time edge

	// Create outer pen
	HPEN hOuterPen = CreatePenIndirect( &logPen );
    HPEN hOuterPenOld = static_cast<HPEN>( SelectObject( graphicContext, hOuterPen ) );
	
	// Draw line
    MoveToEx( graphicContext, static_cast<int>( screenX+xOffset1-deltaLengthX ), static_cast<int>( screenY+yOffset1+deltaLengthY ), nullptr );
    LineTo( graphicContext, static_cast<int>( zoomedX2+xOffset2+deltaLengthX ), static_cast<int>( zoomedY2+yOffset2-deltaLengthY ) );

	// Release pen
	SelectObject( graphicContext, hOuterPenOld );
	DeleteObject( hOuterPen );

	
	//
	// Draw inner line
	//

	// Change pen style
	logPen.lopnColor = RGB( 255, 255, 255 );
	logPen.lopnStyle = PS_SOLID;
    logPen.lopnWidth.x = static_cast<long>( edgeWidth*zoomFactor-2-((oldPenWidth-1)*2)/2 );
                                                                  // Also consider the currently set pen width; this is
                                                                  // neccessary due to trick with a thick line as background
                                                                  // and a white thin line as foreground to draw double lined
                                                                  // time edge
	
	// Create inner pen
	HPEN hInnerPen = CreatePenIndirect( &logPen );
    HPEN hInnerPenOld = static_cast<HPEN>( SelectObject( graphicContext, hInnerPen ) );
	
	// Draw line
    MoveToEx( graphicContext, static_cast<int>( screenX+xOffset1-deltaLengthX ), static_cast<int>( screenY+yOffset1+deltaLengthY ), nullptr );
    LineTo( graphicContext, static_cast<int>( zoomedX2+xOffset2+deltaLengthX ), static_cast<int>( zoomedY2+yOffset2-deltaLengthY ) );

	// Release pen
	SelectObject( graphicContext, hInnerPenOld );
	DeleteObject( hInnerPen );


	//
	// Draw '&' Symbols (for making some sort of white frame, due to anti-aliasing arround symbol)
	//

	// Modify current selected font with double size
	HFONT hCurFont;
    hCurFont = static_cast<HFONT>( GetCurrentObject( graphicContext, OBJ_FONT ) );
	LOGFONT logFont;
	GetObject( hCurFont, sizeof( LOGFONT ), &logFont );
    logFont.lfHeight = logFont.lfHeight*2;  // double size
    logFont.lfWeight = FW_BOLD;  // (700)

	// Create an select new modified font
	HFONT hFont = CreateFontIndirect( &logFont );
    HFONT hFontOld = static_cast<HFONT>( SelectObject( graphicContext, hFont ) );

	// Set a grey text color
	COLORREF crTextColorOld = GetTextColor( graphicContext );
	SetTextColor( graphicContext, RGB( 255, 255, 255 ) );
	
	// Calculate &-size on screen
	SIZE andSymSize;
    GetTextExtentPoint32( graphicContext, L"&", 1, &andSymSize );

	// Calculate distance of &-Symbol to component type
	// (the delta can be calculated for both connectors: either the edge is undirected or the edge can only have one &-symbol)
	double deltaAndSymbolX = 0;
	double deltaAndSymbolY = 0;
	if( that->getIsDirected1() || that->getIsDirected2() )
	{
		deltaAndSymbolX = cos(PI-alpha)*(((sqrt( static_cast<double>( andSymSize.cx*andSymSize.cy ) ))/1.0)/*+EDGE_ARROWLENGTH*zoomFactor*/);
		deltaAndSymbolY = sin(PI-alpha)*(((sqrt( static_cast<double>( andSymSize.cx*andSymSize.cy ) ))/1.0)/*+EDGE_ARROWLENGTH*zoomFactor*/);
	}
	else
	{
		deltaAndSymbolX = cos(PI-alpha)*(((sqrt( static_cast<double>( andSymSize.cx*andSymSize.cy ) ))/2.0)/*+EDGE_ARROWLENGTH*zoomFactor*/);
		deltaAndSymbolY = sin(PI-alpha)*(((sqrt( static_cast<double>( andSymSize.cx*andSymSize.cy ) ))/2.0)/*+EDGE_ARROWLENGTH*zoomFactor*/);
	}

	// if first side is &-Connection
	if( that->getIsAndConnection1() )
	{	
		// Draw first &-Symbol
        TextOut( graphicContext, static_cast<int>( screenX+xOffset1-deltaAndSymbolX-(andSymSize.cx/2.0) ), static_cast<int>( screenY+yOffset1+deltaAndSymbolY-(andSymSize.cy/2.0) ), L"&", 1 );
	}

	// if second side is &-Connection
	if( that->getIsAndConnection2() )
	{
		// Draw first &-Symbol
        TextOut( graphicContext, static_cast<int>( zoomedX2+xOffset2+deltaAndSymbolX-(andSymSize.cx/2.0) ), static_cast<int>( zoomedY2+yOffset2-deltaAndSymbolY-(andSymSize.cy/2.0) ), L"&", 1 );
	}

	// Release new modified font
	SelectObject( graphicContext, hFontOld );
	DeleteObject( hFont );

	// Reset to old text color
	SetTextColor( graphicContext, crTextColorOld );


	//
	// Draw '&' Symbols (grey)
	//

	// Modify current selected font with double size
    hCurFont = static_cast<HFONT>( GetCurrentObject( graphicContext, OBJ_FONT ) );
	GetObject( hCurFont, sizeof( LOGFONT ), &logFont );
    logFont.lfHeight = logFont.lfHeight*2;  // double size
    logFont.lfWeight = FW_BOLD;  // (700)

	// Create an select new modified font
	hFont = CreateFontIndirect( &logFont );
    hFontOld = static_cast<HFONT>( SelectObject( graphicContext, hFont ) );

	// Set a grey text color
	crTextColorOld = GetTextColor( graphicContext );
	SetTextColor( graphicContext, RGB( 160, 160, 160 ) );
	
	// Calculate &-size on screen
//	andSymSize;
    GetTextExtentPoint32( graphicContext, L"&", 1, &andSymSize );

	// Calculate distance of &-Symbol to component type
	// (the delta can be calculated for both connectors: either the edge is undirected or the edge can only have one &-symbol)
	if( that->getIsDirected1() || that->getIsDirected2() )
	{
		deltaAndSymbolX = cos(PI-alpha)*(((sqrt( static_cast<double>( andSymSize.cx*andSymSize.cy ) ))/1.0)/*+EDGE_ARROWLENGTH*zoomFactor*/);
		deltaAndSymbolY = sin(PI-alpha)*(((sqrt( static_cast<double>( andSymSize.cx*andSymSize.cy ) ))/1.0)/*+EDGE_ARROWLENGTH*zoomFactor*/);
	}
	else
	{
		deltaAndSymbolX = cos(PI-alpha)*(((sqrt( static_cast<double>( andSymSize.cx*andSymSize.cy ) ))/2.0)/*+EDGE_ARROWLENGTH*zoomFactor*/);
		deltaAndSymbolY = sin(PI-alpha)*(((sqrt( static_cast<double>( andSymSize.cx*andSymSize.cy ) ))/2.0)/*+EDGE_ARROWLENGTH*zoomFactor*/);
	}

	// if first side is &-Connection
	if( that->getIsAndConnection1() )
	{	
		// Draw first &-Symbol
        TextOut( graphicContext, static_cast<int>( screenX+xOffset1-deltaAndSymbolX-(andSymSize.cx/2.0) ), static_cast<int>( screenY+yOffset1+deltaAndSymbolY-(andSymSize.cy/2.0) ), L"&", 1 );
	}

	// if second side is &-Connection
	if( that->getIsAndConnection2() )
	{
		// Draw first &-Symbol
        TextOut( graphicContext, static_cast<int>( zoomedX2+xOffset2+deltaAndSymbolX-(andSymSize.cx/2.0) ), static_cast<int>( zoomedY2+yOffset2-deltaAndSymbolY-(andSymSize.cy/2.0) ), L"&", 1 );
	}

	// Release new modified font
	SelectObject( graphicContext, hFontOld );
	DeleteObject( hFont );

	// Reset to old text color
	SetTextColor( graphicContext, crTextColorOld );

    epilogue( that, graphicContext, screenX, screenY, zoomFactor );
}
