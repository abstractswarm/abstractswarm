/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined Edge_h
	#define Edge_h





#include "EditorObj.h"
#include "EdgeConnector.h"





class CEdge : public CEditorObj
{
	public:

		// Construction
		CEdge();

		// Destruction
		virtual ~CEdge();

		// Getter
		int getX1() const;
		int getY1() const;
		int getX2() const;
		int getY2() const;
		CEdgeConnector& getConnector1() const;
		CEdgeConnector& getConnector2() const;
		CEdgeConnector& getFreeConnector() const;
		CEdgeConnector& getConnectedConnector() const;

		/** 
		 * Returns whether the edge label is visible when edge is drawn. 
		 *
		 * @return  whether the edge label should be drawn
		 */
		bool getIsLabelVisible() const;

		// Setter
		void setTempX( int tempX );
		void setTempY( int tempY );

		/** Sets whether the edge label is visible when edge is drawn. */
		void setIsLabelVisible( bool isVisible );
		
	protected:
		
		// Attributes
		CEdgeConnector connector1;
		CEdgeConnector connector2;

	private:

		// Attributes
		int tempX;
		int tempY;

		/** Determines whether the edge label is visible when edge is drawn. */
		bool isLabelVisible;
};





#endif
