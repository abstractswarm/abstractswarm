/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined Perspective_h
	#define Perspective_h





#include "GraphObj.h"
#include "ComponentType.h"
#include "Grid.h"
#include <list>





using namespace std;





class CPerspective : public CGraphObj
{
	public:

		// Construction
		CPerspective();

		// Destruction
		~CPerspective();

		// Containees
		list<CComponentType*> componentTypes;
		CGrid grid;

		// Getter
		int getNoOfInstances() const;
		int getContentWidth() const;
		int getContentHeight() const;

		// Setter
		void setWidth( int width );
		void setHeight( int height );
		void setLeft( int x );
		void setTop( int y );

	private: 
		static int noOfInstances;
};





#endif
