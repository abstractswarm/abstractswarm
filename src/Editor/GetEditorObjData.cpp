/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)
                    (partly auto-generated by Qt Creator)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------

#include "Perspective.h"
#include "StationType.h"
#include "AgentType.h"
#include "VisitEdge.h"
#include "PlaceEdge.h"
#include "TimeEdge.h"
#include "EditorTypes.h"
#include <typeinfo>
//#pragma c-mol_hdrstop

//#include "GetEditorObjData.hmol"
#include "GetEditorObjData.hpp"





using namespace std;





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MGetEditorObjData::<CPerspective*>()
void MGetEditorObjData::handling( CPerspective* that, METHOD_PARAMS_GETEDITOROBJDATA )
{
	editorObjData[ eodiName ].pStringData = that->getName().c_str();
}


//void MGetEditorObjData::<CStationType*>()
void MGetEditorObjData::handling( CStationType* that, METHOD_PARAMS_GETEDITOROBJDATA )
{
	editorObjData[ eodiName ].pStringData = that->getName().c_str();
	editorObjData[ eodiCount ].intData = that->getNoOfComponents();

	editorObjData[ eodiColor ].rgbColorData[ 0 ] = that->color.getRed();
	editorObjData[ eodiColor ].rgbColorData[ 1 ] = that->color.getGreen();
	editorObjData[ eodiColor ].rgbColorData[ 2 ] = that->color.getBlue();

	editorObjData[ eodiHasPriority ].boolData = that->getHasPriority();
	editorObjData[ eodiHasFrequency ].boolData = that->getHasFrequency();
	editorObjData[ eodiHasNecessity ].boolData = that->getHasNecessity();
	editorObjData[ eodiHasTime ].boolData = that->getHasTime();
	editorObjData[ eodiHasCycle ].boolData = that->getHasCycle();
	editorObjData[ eodiHasItem ].boolData = that->getHasItem();
	editorObjData[ eodiHasSpace ].boolData = that->getHasSpace();

	if( that->getHasPriority() )
		editorObjData[ eodiPriority ].intData = that->getPriority();
	if( that->getHasFrequency() )
		editorObjData[ eodiFrequency ].intData = that->getFrequency();
	if( that->getHasNecessity() )
		editorObjData[ eodiNecessity ].intData = that->getNecessity();
	if( that->getHasTime() )
		editorObjData[ eodiTime ].intData = that->getTime();
	if( that->getHasCycle() )
		editorObjData[ eodiCycle ].intData = that->getCycle();
	if( that->getHasItem() )
		editorObjData[ eodiItem ].intData = that->getItem();
	if( that->getHasSpace() )
		editorObjData[ eodiSpace ].intData = that->getSpace();
}


//void MGetEditorObjData::<CAgentType*>()
void MGetEditorObjData::handling( CAgentType* that, METHOD_PARAMS_GETEDITOROBJDATA )
{
	editorObjData[ eodiName ].pStringData = that->getName().c_str();
	editorObjData[ eodiCount ].intData = that->getNoOfComponents();

	editorObjData[ eodiColor ].rgbColorData[ 0 ] = that->color.getRed();
	editorObjData[ eodiColor ].rgbColorData[ 1 ] = that->color.getGreen();
	editorObjData[ eodiColor ].rgbColorData[ 2 ] = that->color.getBlue();

	editorObjData[ eodiHasPriority ].boolData = that->getHasPriority();
	editorObjData[ eodiHasFrequency ].boolData = that->getHasFrequency();
	editorObjData[ eodiHasNecessity ].boolData = that->getHasNecessity();
	editorObjData[ eodiHasTime ].boolData = that->getHasTime();
	editorObjData[ eodiHasCycle ].boolData = that->getHasCycle();
	editorObjData[ eodiHasCapacity ].boolData = that->getHasCapacity();
	editorObjData[ eodiHasSize ].boolData = that->getHasSize();
	editorObjData[ eodiHasSpeed ].boolData = that->getHasSpeed();

	if( that->getHasPriority() )
		editorObjData[ eodiPriority ].intData = that->getPriority();
	if( that->getHasFrequency() )
		editorObjData[ eodiFrequency ].intData = that->getFrequency();
	if( that->getHasNecessity() )
		editorObjData[ eodiNecessity ].intData = that->getNecessity();
	if( that->getHasTime() )
		editorObjData[ eodiTime ].intData = that->getTime();
	if( that->getHasCycle() )
		editorObjData[ eodiCycle ].intData = that->getCycle();
	if( that->getHasCapacity() )
		editorObjData[ eodiCapacity ].intData = that->getCapacity();
	if( that->getHasSize() )
		editorObjData[ eodiSize ].intData = that->getSize();
	if( that->getHasSpeed() )
		editorObjData[ eodiSpeed ].intData = that->getSpeed();
}


//void MGetEditorObjData::<CVisitEdge*>()
void MGetEditorObjData::handling( CVisitEdge* that, METHOD_PARAMS_GETEDITOROBJDATA )
{
	editorObjData[ eodiBold ].boolData = that->getIsBold();
}


//void MGetEditorObjData::<CPlaceEdge*>()
void MGetEditorObjData::handling( CPlaceEdge* that, METHOD_PARAMS_GETEDITOROBJDATA )
{
	editorObjData[ eodiValue ].intData = that->getValue();
	editorObjData[ eodiDirected1 ].boolData = that->getIsDirected1();
	editorObjData[ eodiDirected2 ].boolData = that->getIsDirected2();
}


//void MGetEditorObjData::<CTimeEdge*>()
void MGetEditorObjData::handling( CTimeEdge* that, METHOD_PARAMS_GETEDITOROBJDATA )
{
	editorObjData[ eodiValue ].intData = that->getValue();
	editorObjData[ eodiDirected1 ].boolData = that->getIsDirected1();
	editorObjData[ eodiDirected2 ].boolData = that->getIsDirected2();
	editorObjData[ eodiAndConnection1 ].boolData = that->getIsAndConnection1();
	editorObjData[ eodiAndConnection2 ].boolData = that->getIsAndConnection2();
}
