/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "GraphObj.h"





using namespace std;





//------------------------------------------------------------
// Construction
//------------------------------------------------------------


CGraphObj::CGraphObj()
{
	// Init with default values
	name = "";
	x = 0; 
	y = 0;
	width = 0;
	height = 0;
}


//------------------------------------------------------------
// Destruction
//------------------------------------------------------------


CGraphObj::~CGraphObj()
{
}


//------------------------------------------------------------
// Getter
//------------------------------------------------------------


const string& CGraphObj::getName() const
{
	return( name ); 
}


int CGraphObj::getX() const
{
	return( x );
}


int CGraphObj::getY() const
{
	return( y );
}


int CGraphObj::getWidth() const
{
	return( width );
}


int CGraphObj::getHeight() const
{
	return( height );
}


//------------------------------------------------------------
// Setter
//------------------------------------------------------------


void CGraphObj::setName( string name )
{
	this->name = name;
}


void CGraphObj::setX( int x )
{
	this->x = x;
}


void CGraphObj::setY( int y )
{
	this->y = y;
}


void CGraphObj::setWidth( int width )
{
	this->width = width;
}


void CGraphObj::setHeight( int height )
{
	this->height = height;
}
