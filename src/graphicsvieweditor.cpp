/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "graphicsvieweditor.h"
#include "mainwindow.h"
#include <QMouseEvent>



GraphicsViewEditor::GraphicsViewEditor( QWidget* parent, QWidget* mainWindow, int width, int height ) : GraphicsViewGDI( parent )
{
    // Set the size of this widget
    setGeometry( 0, 0, width, height );

    // Enable mouse move events without a mouse button being pressed
    setMouseTracking( true );

    // Init previous coordinates
    previousPressedX = 0;
    previousPressedY = 0;

    // Init the widget to which the callbacks will be delegated
    this->mainWindow = mainWindow;
}


GraphicsViewEditor::~GraphicsViewEditor()
{
}


void GraphicsViewEditor::keyPressEvent( QKeyEvent *event )
{
     // TODO This delegation should be removed in the context of the refactoring!
    static_cast<MainWindow*>( mainWindow )->graphPaperKeyPressEvent( event );
}

void GraphicsViewEditor::keyReleaseEvent(QKeyEvent *event)
{
    // TODO This delegation should be removed in the context of the refactoring!
   static_cast<MainWindow*>( mainWindow )->graphPaperKeyReleaseEvent( event );
}

void GraphicsViewEditor::mouseClickEvent( QMouseEvent* event )
{
    // TODO This delegation should be removed in the context of the refactoring!
    static_cast<MainWindow*>( mainWindow )->graphPaperMouseClickEvent( event );
}


void GraphicsViewEditor::mouseDoubleClickEvent( QMouseEvent* event )
{
    // TODO This delegation should be removed in the context of the refactoring!
    static_cast<MainWindow*>( mainWindow )->graphPaperMouseDoubleClickEvent( event );
}


void GraphicsViewEditor::mousePressEvent( QMouseEvent* event )
{
    // TODO This delegation should be removed in the context of the refactoring!
    static_cast<MainWindow*>( mainWindow )->graphPaperMousePressEvent( event );

    // Store previous coordinates
    previousPressedX = event->x();
    previousPressedY = event->y();
}


void GraphicsViewEditor::mouseMoveEvent( QMouseEvent* event )
{
    // TODO This delegation should be removed in the context of the refactoring!
    static_cast<MainWindow*>( mainWindow )->graphPaperMouseMoveEvent( event );
}


void GraphicsViewEditor::mouseReleaseEvent( QMouseEvent* event )
{
    // Simulate click event
    if( event->x() == previousPressedX && event->y() == previousPressedY )
    {
        mouseClickEvent( event );
    }

    // TODO This delegation should be removed in the context of the refactoring!
    static_cast<MainWindow*>( mainWindow )->graphPaperMouseReleaseEvent( event );
}


void GraphicsViewEditor::drawGDI( HDC hdc )
{
    // TODO This delegation should be removed in the context of the refactoring!
    static_cast<MainWindow*>( mainWindow )->graphPaperDrawGDI( hdc );
}
