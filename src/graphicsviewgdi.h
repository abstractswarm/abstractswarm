/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GRAPHICSVIEWGDI_H
#define GRAPHICSVIEWGDI_H

#include <QGraphicsView>


/**
 * @brief Objects of this class are graphics views capable of displaying natively drawn GDI content.
 */
class GraphicsViewGDI : public QGraphicsView
{
    public:

        /**
         * @brief Creates a QGraphicsView which is able to draw native GDI graphics.
         *
         * @param parent  the graphic view's parent widget
         */
        GraphicsViewGDI( QWidget *parent = nullptr );

        /**
         * @brief Releases the (native) graphics context.
         *
         * @param parent  the graphic view's parent widget
         */
        virtual ~GraphicsViewGDI() Q_DECL_OVERRIDE;

        /**
         * @brief Implements the event handling for the graphics view.
         *
         * @param event  the incoming event
         * @return       true if the incoming event was processed (cf. coresponding method of QObject)
         */
        virtual bool event( QEvent* event ) Q_DECL_OVERRIDE;

        /**
         * @brief Returns the handle to the native graphics device context.
         *
         * @return  the handle to the (native) graphics context of the graphics view
         */
        HDC getHDC() const;


    protected:

        /**
         * @brief Method to be implemented for native GDI drawing.
         *
         * @param hdc  the native GDI graphics device context to be drawn on
         */
        virtual void drawGDI( HDC hdc ) = 0;


    private:

        /**
         * @brief The native window handle.
         */
        HWND hwnd;

        /**
         * @brief The handle to the (native) graphics context of the graphics view.
         */
        HDC hdc;
};


#endif // GRAPHICSVIEWGDI_H
