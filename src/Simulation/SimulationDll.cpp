/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "SimulationDll.h"


//------------------------------------------------------------
// Construction
//------------------------------------------------------------

static CSimulation* pSimulation;


void SimulationDll::createSimulation( string sApplicationPath, string sAgentName, bool createConsole )
{
    pSimulation = new CSimulation( sApplicationPath, sAgentName, createConsole );
}


void SimulationDll::deleteSimulation()
{
    delete pSimulation;
}





//------------------------------------------------------------
// Adding/removing components
//------------------------------------------------------------


void SimulationDll::addStation( CStation* pStation )
{
    pSimulation->addStation( pStation );
}


void SimulationDll::addAgent( CAgent* pAgent )
{
    pSimulation->addAgent( pAgent );
}


void SimulationDll::removeStation( CStation* pStation )
{
    pSimulation->removeStation( pStation );
}


void SimulationDll::removeAgent( CAgent* pAgent )
{
    pSimulation->removeAgent( pAgent );
}





//------------------------------------------------------------
// Simulation
//------------------------------------------------------------

void SimulationDll::simulateStep( HDC graphicContext )
{
    pSimulation->simulateStep( graphicContext );
}

bool SimulationDll::getIsFinished()
{
    return( pSimulation->getIsFinished() );
}

bool SimulationDll::getIsValid()
{
    return( pSimulation->getIsValid() );
}

void SimulationDll::setAgentFile( const char* pFileName )
{
    pSimulation->setAgentFile( pFileName );
}

void SimulationDll::reloadAgentJavaFile()
{
    pSimulation->reloadAgentJavaFile();
}

void SimulationDll::reset()
{
     pSimulation->reset();
}



//------------------------------------------------------------
// UI and advanced communcation
//------------------------------------------------------------

void SimulationDll::connectToTimetable( TfpAddAgentEnterEvent fpAddAgentEnterEvent, TfpAddAgentLeaveEvent fpAddAgentLeaveEvent )
{
    pSimulation->connectToTimetable( fpAddAgentEnterEvent, fpAddAgentLeaveEvent );
}

void SimulationDll::flushConsole()
{
    pSimulation->flushConsole();
}



//------------------------------------------------------------
// Events
//------------------------------------------------------------

void SimulationDll::mouseMove( int x, int y )
{
    pSimulation->mouseMove( x, y );
}



//------------------------------------------------------------
// Layout independant functionality and state setters
//------------------------------------------------------------

void SimulationDll::setScrollX( int percentScrollX )
{
    pSimulation->setScrollX( percentScrollX );
}


void SimulationDll::setScrollY( int percentScrollY )
{
    pSimulation->setScrollY( percentScrollY );
}


double SimulationDll::setZoom( double zoomFactor, int& percentScrollX, int& percentScrollY )
{
    return( pSimulation->setZoom( zoomFactor, percentScrollX, percentScrollY ) );
}


void SimulationDll::setVisibleFrameSize( int width, int height )
{
    pSimulation->setVisibleFrameSize( width, height );
}


//------------------------------------------------------------
// Platform dependent(!) interface for visualization
//------------------------------------------------------------

void SimulationDll::draw( HDC graphicContext )
{
    pSimulation->draw( graphicContext );
}
