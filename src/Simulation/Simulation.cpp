/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Simulation.h"
#include "Constants.h"
#include "Utilities.h"
#include ".//Components//Agent.hpp"
#include ".//Components//Station.hpp"

#include <algorithm>
#include <fstream>   // TEST: Time table test code!
#include <sstream>
#include <iostream>
#include "windows.h"
#include <ctime>
//#pragma c-mol_hdrstop

#include ".//Calculation//Simulate.hpp"
#include ".//Calculation//ChooseAction.hpp"
#include ".//Calculation//Communicate.hpp"
#include ".//Calculation//CheckTimeEdgeConstraint.hpp"
#include ".//Calculation//CheckFinished.hpp"
//#include ".//Calculation//TransferToJava.hmol"
#include ".//Calculation//TransferToJava.hpp"
//#include ".//Calculation//TransferCycleToJava.hmol"
#include ".//Calculation//TransferCycleToJava.hpp"
//#include ".//Calculation//TransferNecessityToJava.hmol"
#include ".//Calculation//TransferNecessityToJava.hpp"
//#include ".//Calculation//GetSimulationDrawFactor.hmol"
#include ".//Calculation//GetSimulationDrawFactor.hpp"

// Platform dependant drawing interface
//#include "./../../EditorDll/Editor/DrawWithStyles.hmol"
#include "./../Editor/DrawWithStyles.hpp"
#include "./../Editor/Constants.h"  // Corresponding constants from Editor
//#include "./../../EditorDll/Editor/Perspective.h"
#include "./../Editor/Perspective.h"
//#include "./../../EditorDll/Editor/Edge.h"
#include "./../Editor/Edge.h"





//------------------------------------------------------------
// Construction
//------------------------------------------------------------


CSimulation::CSimulation( string sApplicationPath, string sAgentName, bool createConsole ) : grid( GRID_XSIZE, GRID_YSIZE )
{
	// Initialize the currently known width and height of the visible area
	// (maximum until set by the UI)
	visibleFrameWidth = EDITOR_WIDTH * GRID_XSIZE;
	visibleFrameHeight = EDITOR_HEIGHT * GRID_YSIZE;

	// Set default values for timetable
	curMouseX = 0;
	curMouseY = 0;
	curScreenX = 0;
	curScreenY = 0;
	curZoomFactor = 1;

	// Store application path
	this->sApplicationPath = sApplicationPath;

	// Set the current agent file
    if( sAgentName != "" )
    {
        sAgentFile = sAgentName;
    }
    else
    {
        sAgentFile = JAVA_AGENT_DEFAULT_NAME;
    }

	// Initialize global simulation time
    timeStep  = 0;

	// Initialize function pointers for connection to timetable
    fpAddAgentEnterEvent = nullptr;
    fpAddAgentLeaveEvent = nullptr;

	// Open a new console or attach one (the latter is used, e.g., when running as console application) 
    if( createConsole )
    {
        AllocConsole();
    }
    else
    {
        AttachConsole( ATTACH_PARENT_PROCESS );
    }
    freopen( "CONOUT$", "wb", stdout ); // Reopen stream (if not done, no output will be shown!)

    // Say hello if a console is newly created
    if( createConsole )
    {
        cout << "This is the AbstractSwarm console for agent output and debugging." << endl;
    }
    else
    {
        // A new line to do not let the following output start in the same line as the command prompt
        cout << endl;
    }


#if !defined USE_NATIVE_AGENT_INTERFACE

    //
    // Initialize and print welcome message for Java
    //

    // (Re)initialize the java interpreter
    initializeJava();

#endif


// NO NEED TO RELOAD THE AGENT JAVA FILE HERE SINCE THIS IS AUTOMATICALLY DONE WHEN INITIALIZING (SEE ABOVE)
//    // Reload the agent Java file
//    reloadAgentJavaFile();

	// Set a seed for random generator
    srand( static_cast<unsigned int>( time( nullptr ) ) );
}





//------------------------------------------------------------
// Destruction
//------------------------------------------------------------


CSimulation::~CSimulation()
{
    // Eventually finalize Java here
    // ...

	// Close console
	fclose( stdout );  // Close reopened stream
	FreeConsole();
}



//------------------------------------------------------------
// Events
//------------------------------------------------------------


void CSimulation::mouseMove( int x, int y )
{
	// Set the incoming mouse position as current mouse position
	curMouseX = x;
	curMouseY = y;
}


//------------------------------------------------------------
// Layout independant functionality and state setters
//------------------------------------------------------------


void CSimulation::setScrollX( int percentScrollX )
{
    curScreenX = static_cast<int>( EDITOR_WIDTH*GRID_XSIZE*curZoomFactor*(percentScrollX/100.0) );
}


void CSimulation::setScrollY( int percentScrollY )
{
    curScreenY = static_cast<int>( EDITOR_HEIGHT*GRID_YSIZE*curZoomFactor*(percentScrollY/100.0) );
}


double CSimulation::setZoom( double zoomFactor, int& percentScrollX, int& percentScrollY )
{
	// Check zoom factor validity and make it valid if not
	if( zoomFactor < MIN_ZOOM_FACTOR )
		zoomFactor = MIN_ZOOM_FACTOR;
	else if( zoomFactor > MAX_ZOOM_FACTOR )
		zoomFactor = MAX_ZOOM_FACTOR;
	
	// Get old real mouse position
	double mouseOldX = screenXToX( curMouseX );
	double mouseOldY = screenYToY( curMouseY );
	
	// Set the new zoom factor
	curZoomFactor = zoomFactor;

	// Now, get the new real mouse position
	double mouseNewX = screenXToX( curMouseX );
	double mouseNewY = screenYToY( curMouseY );

	// Correct scroll position due to offset between old and new real mouse positions
	curScreenX = curScreenX-(xToScreenX( mouseNewX )-xToScreenX( mouseOldX ));
	curScreenY = curScreenY-(yToScreenY( mouseNewY )-yToScreenY( mouseOldY ));

	// Update the given percent scroll variables for UI
    percentScrollX = static_cast<int>( (static_cast<double>( curScreenX )/(EDITOR_WIDTH*GRID_XSIZE*curZoomFactor))*100.0 );
    percentScrollY = static_cast<int>( (static_cast<double>( curScreenY )/(EDITOR_HEIGHT*GRID_YSIZE*curZoomFactor))*100.0 );

	// Return the new zoom factor
	return( curZoomFactor );
}


void CSimulation::setVisibleFrameSize( int width, int height )
{
	visibleFrameWidth = width;
	visibleFrameHeight = height;
}



//------------------------------------------------------------
// Getter
//------------------------------------------------------------

int CSimulation::getTimeStep() const
{
	return( timeStep );
}


const list<CAgent*>& CSimulation::getAgents() const
{
	return( agents );
}



//------------------------------------------------------------
// Setter
//------------------------------------------------------------

void CSimulation::setAgentFile( const char* pFileName )
{
	// Store the new file name
	sAgentFile = pFileName;

    // Reset the Java environment (automatically reloads the Java agent file)
    initializeJava();

	// Reset the whole simulation
	reset();
}



//------------------------------------------------------------
// Adding/removing components
//------------------------------------------------------------


void CSimulation::addStation( CStation* pStation )
{
	stations.push_back( pStation );
}


void CSimulation::addAgent( CAgent* pAgent )
{
	agents.push_back( pAgent );
}


void CSimulation::removeStation( CStation* pStation )
{
	// Visiting agents must be removed here and further clean up stuff must
	// be done since it seems to be that the elements of a container must be
	// removed in the same DLL that was addind them before
	pStation->cleanUp();

	stations.remove( pStation );
}


void CSimulation::removeAgent( CAgent* pAgent )
{
	// Clean up stuff must be done since it seems to be that the elements of
	// a container must be removed in the same DLL that was addind them before
	pAgent->cleanUp();

	agents.remove( pAgent );
}





//------------------------------------------------------------
// Simulation
//------------------------------------------------------------

void CSimulation::simulateStep( HDC graphicContext )
{
#if defined ALLOW_FAST_FORWARDING

    // Indicates whether the previous step was a fast-forwarded
    static bool isPreviousStepFastForward = false;


    //
    // Fast-forward time if there are no events and if there was no fast forward before and if it is not the initialization time step 0
    //

    if( !isPreviousStepFastForward && (timeStep > 0) )
    {
        // Get the time to fast-forward by searching for the time when the next potential event will occur (i.e., finish working or reaching a station)
        long fastForwardTime = -1;  // -1 for "not yet set"
        list<CAgent*>::iterator itAgent;
        for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
        {
            // Do not consider if agent was not active in the previous step (i.e., no entering, leaving, moving, working)
            if( !(*itAgent)->getIsActive() )
            {
                continue;
            }

            // The (shortest) time to the next event
            long timeToNextEvent = 0;

            // If agent is visiting and remaining time is > 0, get the remaining time
            int remainingTime = -1;
            if( (*itAgent)->getIsVisiting() && (*itAgent)->getAgentType().getHasTime()  )
            {
                remainingTime = (*itAgent)->getAgentType().getTime() - (*itAgent)->getTime();
            }
            else if( (*itAgent)->getIsVisiting() && (*itAgent)->getTarget()->getStationType().getHasTime()  )
            {
                remainingTime = (*itAgent)->getTarget()->getStationType().getTime() - (*itAgent)->getTime();
            }

            // If the remaining time is > 0, then the time to next event for this agent is one unit before the remaining time ends
            if( remainingTime > 0 )
            {
               timeToNextEvent = remainingTime - 1;
            }

            // ...else check whether the agent is on the way to a target station (MOVE)
            else if( (!(*itAgent)->getIsVisiting()) && ((*itAgent)->getTarget() != nullptr) && ((*itAgent)->getDistanceToTarget() != NO_PATH) && ((*itAgent)->getCoveredDistanceToTarget() < (*itAgent)->getDistanceToTarget()) )
            {
                // If the agent has a target and is on its way, get the remaining time (i.e., distance considering speed - one unit before reaching the station)
                int remainingDistanceTime = ((*itAgent)->getDistanceToTarget() - (*itAgent)->getCoveredDistanceToTarget()) / (*itAgent)->getAgentType().getSpeed();
                if( remainingDistanceTime > 0 )
                {
                    // If there is a residual time remaining from the agent's speed, take the remaining time as is
                    int residual = ((*itAgent)->getDistanceToTarget() - (*itAgent)->getCoveredDistanceToTarget()) % (*itAgent)->getAgentType().getSpeed();
                    if( residual > 0 )
                    {
                        timeToNextEvent = remainingDistanceTime;
                    }

                    // ...else fast-forward to one unit before the agent reaches its target
                    else
                    {
                        timeToNextEvent = remainingDistanceTime - 1;
                    }
                }
            }

            // ...else if the agent is on the way to a target station target station is not reachable, do not constrain fast-forward
            else if( (!(*itAgent)->getIsVisiting()) && ((*itAgent)->getTarget() != nullptr) && ((*itAgent)->getDistanceToTarget() == NO_PATH) )
            {
                continue;
            }

            // If the time to fast-forward is not yet set or if the agent's time to the next event is smaller than the time to fast-forward
            if( (fastForwardTime == -1) || (timeToNextEvent < fastForwardTime) )
            {
                // Decrease the additional time to fast-forward to the smaller time to the next event
                fastForwardTime = timeToNextEvent;
            }

            // If the time to fast-forward reached 0, break (do not fast-forward)
            if( fastForwardTime <= 0 )
            {
                break;
            }
        }

        // Fast-forward the time
        if( fastForwardTime > 0 )
        {
            // Mark that this step is fast-forwarded
            isPreviousStepFastForward = true;

            timeStep += fastForwardTime;
            for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
            {
                // MOVE
                // If agent is not visiting a station and has a target and its target station is not reached yet
                if( (!(*itAgent)->getIsVisiting()) && ((*itAgent)->getTarget() != nullptr) && ((*itAgent)->getDistanceToTarget() != NO_PATH) && ((*itAgent)->getCoveredDistanceToTarget() < (*itAgent)->getDistanceToTarget()) )
                {
                    // Move the agent to its target (according to its speed)
                    (*itAgent)->setCoveredDistanceToTarget( (*itAgent)->getCoveredDistanceToTarget() + (fastForwardTime * (*itAgent)->getAgentType().getSpeed()) );
                }

                // WORK
                // If agent currently visits a station
                else if( (*itAgent)->getIsVisiting() )
                {
                    // Progress its visit time
                    (*itAgent)->setTime( (*itAgent)->getTime() + fastForwardTime );
                }
            }
        }
    }
    else
    {
        isPreviousStepFastForward = false;
    }

#endif

    // Progress time
	timeStep++;

	// Randomly permute stations
	vector<CStation*> stationsVector( stations.begin(), stations.end() );
	random_shuffle( stationsVector.begin(), stationsVector.end() );
	stations.assign( stationsVector.begin(), stationsVector.end() );

	// Randomly permute agents
	vector<CAgent*> agentsVector( agents.begin(), agents.end() );
	random_shuffle( agentsVector.begin(), agentsVector.end() );
	agents.assign( agentsVector.begin(), agentsVector.end() );

// TODO Implement "-" operator for agents or implement another sorting, since this one seems to require "-" operator
//	// Sort agents by priority by keeping the relative order given by the random permutation
//	stable_sort( agents.begin(), agents.end(), comparePriorities );

    // JAVA INTERFACE
	// Do complete initialization only in the first time step (will be continued successivly 
	// after simulation step and reversing a simulation step)
	if( timeStep == 1 )
	{
        transferComponentsToJava();
	}
	else
	{
#if !defined USE_NATIVE_AGENT_INTERFACE

        //
        // Update Java environment
        //

        ostringstream ossJavaCode;
		list<CStation*>::iterator itStation;
		for( itStation = stations.begin(); itStation != stations.end(); itStation++ )
            transferAttributesToJava( **itStation, ossJavaCode );
		list<CAgent*>::iterator itAgent;
		for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
            transferAttributesToJava( **itAgent, ossJavaCode );

        // Update Java agent interface with data
        externalJavaAgentInterface.update( ossJavaCode.str() );

#endif
    }

	// Simulate all Stations
	list<CStation*>::iterator itStation;
	for( itStation = stations.begin(); itStation != stations.end(); itStation++ )
	{
//		MSimulate( timeStep )<-MCommunicate( timeStep )<-MChooseAction( STRATEGY_JAVA, timeStep )<-itStation();
        MChooseAction::handling( *itStation, STRATEGY_JAVA, timeStep, nullptr );
        MCommunicate::handling( *itStation, timeStep, &externalJavaAgentInterface );
        MSimulate::handling( *itStation, timeStep );
	}
	
	// Simulate all Agents, store agents that did not move (waiting) 
	// and agents that entered a station this simulation step
	list<CAgent*> waitingAgents;
	list<CAgent*> enteringAgents;
	bool isAgentAddedToStation = false;
    list<CAgent*>::iterator itAgent;
	for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
	{
		// Store the agent's position before the simulation step
		int agentPosition = (*itAgent)->getCoveredDistanceToTarget();

//		MSimulate( timeStep )<-MCommunicate( timeStep )<-MChooseAction( STRATEGY_JAVA, timeStep )<-itAgent();
#if !defined USE_NATIVE_AGENT_INTERFACE
        MChooseAction::handling( *itAgent, STRATEGY_JAVA, timeStep, &externalJavaAgentInterface );
#else
        MChooseAction::handling( *itAgent, STRATEGY_RANDOM, timeStep, &externalJavaAgentInterface );
#endif
        MCommunicate::handling( *itAgent, timeStep, &externalJavaAgentInterface );
        MSimulate::handling( *itAgent, timeStep );

		// If the agents position did not change and agent is not visiting a station => agent is waiting!
		if( (agentPosition == (*itAgent)->getCoveredDistanceToTarget()) && (!(*itAgent)->getIsVisiting()) )
			waitingAgents.push_back( *itAgent );

		// If the agent was entering a station during the simulation step, put it to the list of entering agents
		if( (*itAgent)->getHasEnterEvent() )
		{
			enteringAgents.push_back( *itAgent );
			isAgentAddedToStation = true;
		}
	}


	//
	// Calculate time edge constraints (the simulation step is performed without time 
	// egde constraints and the result of the relaxed problem will now be repaired here)
	//

	// While one or more agents are added to a station after time edge contraints were violated
	while( isAgentAddedToStation )
	{
		// At the beginning no agents are added additionally
		isAgentAddedToStation = false;

		// While agents are removed due to violated time-edge-contraints
		bool isAgentRemoved = true;
		while( isAgentRemoved )
		{
			// At the beginning no agents are removed due to time-edge-constraints
			isAgentRemoved = false;

			// For all entering agents (used while here since agents may be removed during loop)
			itAgent = enteringAgents.begin();
			while( itAgent != enteringAgents.end() )
			{
				//
				// Calculate time edge contraints
				//

				// For all connected time edges (with 0 weight --> MORE TO BE IMPLEMENTED!)
				bool hasTimeEdge = false;
				bool hasAndTimeEdge = false;
				bool isAndTimeEdgeConstraintOk = true;
				bool isOrTimeEdgeConstraintOk = false;
				list<CComponent*> andTimeEdgeConstraintFulfillers;
                CComponent* pOrTimeEdgeConstraintFulfiller = nullptr;
				list<CEdgeConnector*>::const_iterator itEdgeConnector;
				for( itEdgeConnector = (*itAgent)->getAgentType().getEdgeConnectors().begin(); itEdgeConnector != (*itAgent)->getAgentType().getEdgeConnectors().end(); itEdgeConnector++ )
				{
					// Store time edge contraint violation in flag
					bool isTimeEdge = false;
					bool isOrConstraint = false;
                    CComponent* pFulfiller = nullptr;
//					bool isTimeEdgeConstraintPartOk = MCheckTimeEdgeConstraint( **itAgent )°(const_cast<CEdge*>( &((*itEdgeConnector)->getEdge()) ))( isTimeEdge, isOrConstraint, pFulfiller );
                    bool isTimeEdgeConstraintPartOk = MCheckTimeEdgeConstraint::handling( const_cast<CEdge*>( &((*itEdgeConnector)->getEdge()) ), **itAgent, isTimeEdge, isOrConstraint, pFulfiller );

					// If this edge is a time edge, store that the agent type has a time edge
					if( isTimeEdge )
					{
						hasTimeEdge = true;

						// Store if the agent type has an and-time-edge as well
						if( !isOrConstraint )
						{
							hasAndTimeEdge = true;
						}
					}

					// If time constraint not violated and its an or-time-constraint
					if( isTimeEdge && isTimeEdgeConstraintPartOk && isOrConstraint )
					{
						isOrTimeEdgeConstraintOk = true;
						pOrTimeEdgeConstraintFulfiller = pFulfiller;
						break;
					}

					// If time edge contraint is violated and it is an and-time-constraint
					if( isTimeEdge && (!isTimeEdgeConstraintPartOk) && (!isOrConstraint) )
						isAndTimeEdgeConstraintOk = false;

					// ...else if time edge contraint is fulfilled and it is an and-time-constraint, 
					// store the fulfiller in list!
					else if( isTimeEdge && isTimeEdgeConstraintPartOk && (!isOrConstraint) )
						andTimeEdgeConstraintFulfillers.push_back( pFulfiller );
				}
				bool isTimeEdgeConstraintOk = (!hasTimeEdge) || isOrTimeEdgeConstraintOk || (hasAndTimeEdge && isAndTimeEdgeConstraintOk);

				// Store time edge constraint violation of target station
				bool hasTargetStationTimeEdge = false;
				bool hasTargetStationAndTimeEdge = false;
				bool isTargetStationAndTimeEdgeConstraintOk = true;
				bool isTargetStationOrTimeEdgeConstraintOk = false;
				list<CComponent*> targetStationAndTimeEdgeConstraintFulfillers;
                CComponent* pTargetStationOrTimeEdgeConstraintFulfiller = nullptr;
				if( isTimeEdgeConstraintOk )
				{
					list<CEdgeConnector*>::const_iterator itEdgeConnector;
					for( itEdgeConnector = (*itAgent)->getTarget()->getStationType().getEdgeConnectors().begin(); itEdgeConnector != (*itAgent)->getTarget()->getStationType().getEdgeConnectors().end(); itEdgeConnector++ )
					{
						// Store target station time edge constraint violation in flag
						bool isTimeEdge = false;
						bool isOrConstraint = false;
                        CComponent* pFulfiller = nullptr;
//						bool isTargetStationTimeEdgeConstraintPartOk = MCheckTimeEdgeConstraint( *((*itAgent)->getTarget()) )°(const_cast<CEdge*>( &((*itEdgeConnector)->getEdge()) ))( isTimeEdge, isOrConstraint, pFulfiller );
                        bool isTargetStationTimeEdgeConstraintPartOk = MCheckTimeEdgeConstraint::handling( const_cast<CEdge*>( &((*itEdgeConnector)->getEdge()) ), *((*itAgent)->getTarget()), isTimeEdge, isOrConstraint, pFulfiller );

						// If this edge is a time edge, store that the agent type has a time edge
						if( isTimeEdge )
						{
							hasTargetStationTimeEdge = true;

							// Store if the agent type has an and-time-edge as well
							if( !isOrConstraint )
							{
								hasTargetStationAndTimeEdge = true;
							}
						}

						// If time constraint not violated and its an or-time-constraint
						if( isTimeEdge && isTargetStationTimeEdgeConstraintPartOk && isOrConstraint )
						{
							isTargetStationOrTimeEdgeConstraintOk = true;
							pTargetStationOrTimeEdgeConstraintFulfiller = pFulfiller;
							break;
						}

						// If time edge contraint is violated and it is an and-time-constraint
						if( isTimeEdge && (!isTargetStationTimeEdgeConstraintPartOk) && (!isOrConstraint) )
							isTargetStationAndTimeEdgeConstraintOk = false;
						
						// ...else if time edge contraint is fulfilled and it is an and-time-constraint, 
						// store the fulfiller in list!
						else if( isTimeEdge && isTargetStationTimeEdgeConstraintPartOk && (!isOrConstraint) )
							targetStationAndTimeEdgeConstraintFulfillers.push_back( pFulfiller );
					}
				}
				bool isTargetStationTimeEdgeConstraintOk = (!hasTargetStationTimeEdge) || isTargetStationOrTimeEdgeConstraintOk || (hasTargetStationAndTimeEdge && isTargetStationAndTimeEdgeConstraintOk);

				// If there are any violated time edge contraints for the agent
				if( (!isTimeEdgeConstraintOk) || (!isTargetStationTimeEdgeConstraintOk) )
				{
					// Reverse the previous simulation step for agent
					(*itAgent)->setTime( (*itAgent)->getTime() - 1 );
					(*itAgent)->setEnterEventCount( (*itAgent)->getEnterEventCount() - 1 );
					if( (*itAgent)->getAgentType().getHasNecessity() )
					{
						(*itAgent)->setNecessity( *((*itAgent)->getTarget()), (*itAgent)->getNecessity( *((*itAgent)->getTarget()) ) - 1 );
					}
					if( (*itAgent)->getTarget()->getStationType().getHasNecessity() )
					{
						const_cast<CStation*>( (*itAgent)->getTarget() )->setNecessity( **itAgent, (*itAgent)->getTarget()->getNecessity( **itAgent ) - 1 );
					}
					(*itAgent)->setLastEnterEventTime( (*itAgent)->getPreviousEnterEventTime() );
					(*itAgent)->setPreviousEnterEventTime( INVALID_TIME );
					const_cast<CStation*>( (*itAgent)->getTarget() )->setLastEnterEventTime( (*itAgent)->getTarget()->getPreviousEnterEventTime() );
					const_cast<CStation*>( (*itAgent)->getTarget() )->setPreviousEnterEventTime( INVALID_TIME );

					// Reset time edge constraint fulfiller status of agent
					(*itAgent)->setIsTimeEdgeConstraintFulfiller( false );

					// Reset the time edge constraint fulfiller status of all fulfillers of the agent
					// and remove the agent from their fulfiller lists (if contained) in case the 
					// fulfiller status was set in the current time step (e.g. for directed edges
					// the status could have been set in the past and therefore may not be reset here!)
					list<CComponent*>::iterator itComponent;
					for( itComponent = (*itAgent)->getTimeEdgeConstraintFulfillers().begin(); itComponent != (*itAgent)->getTimeEdgeConstraintFulfillers().end(); itComponent++ )
					{
						if( (*itComponent)->getTimeEdgeConstraintFulfillerTime() == timeStep )
						{
							(*itComponent)->setIsTimeEdgeConstraintFulfiller( false );
							(*itComponent)->getTimeEdgeConstraintFulfillers().remove( *itAgent );
						}
					}
					(*itAgent)->getTimeEdgeConstraintFulfillers().clear();

					// Reset time edge constraint fulfiller status of target station
					const_cast<CStation*>( (*itAgent)->getTarget() )->setIsTimeEdgeConstraintFulfiller( false );

					// Reset the time edge constraint fulfiller status of all connected components of the target station
					// and remove the target station from their fulfiller lists (if contained) in case the 
					// fulfiller status was set in the current time step (e.g. for directed edges
					// the status could have been set in the past and therefore may not be reset here!)
					for( itComponent = const_cast<CStation*>( (*itAgent)->getTarget() )->getTimeEdgeConstraintFulfillers().begin(); itComponent != const_cast<CStation*>( (*itAgent)->getTarget() )->getTimeEdgeConstraintFulfillers().end(); itComponent++ )
					{
						if( (*itComponent)->getTimeEdgeConstraintFulfillerTime() == timeStep )
						{
							(*itComponent)->setIsTimeEdgeConstraintFulfiller( false );
							(*itComponent)->getTimeEdgeConstraintFulfillers().remove( const_cast<CStation*>( (*itAgent)->getTarget() ) );
						}
					}
					const_cast<CStation*>( (*itAgent)->getTarget() )->getTimeEdgeConstraintFulfillers().clear();

					// Remove agent from station
					const_cast<CStation*>( (*itAgent)->getTarget() )->removeAgent( **itAgent );
					const_cast<CStation*>( (*itAgent)->getTarget() )->setEnterEventCount( const_cast<CStation*>( (*itAgent)->getTarget() )->getEnterEventCount() - 1 );
					(*itAgent)->setIsVisiting( false );
					(*itAgent)->setHasEnterEvent( false );

					// Remove agent from list of entered agents
					itAgent = enteringAgents.erase( itAgent );

					// Store that there was an agent removed due to time-edge-constraints
					isAgentRemoved = true;
				}
				else
				{
					// Mark the opposite agent(s) as time edge contraint fulfiller(s)
                    if( pOrTimeEdgeConstraintFulfiller != nullptr )
					{
						pOrTimeEdgeConstraintFulfiller->setIsTimeEdgeConstraintFulfiller( true, timeStep );

						// Store time edge constraint fulfiller for agent
						list<CComponent*> fulfillers;
						fulfillers.clear();
						fulfillers.push_back( pOrTimeEdgeConstraintFulfiller );
						(*itAgent)->setTimeEdgeConstraintFulfillers( fulfillers );
					}
					else
					{
						list<CComponent*>::iterator itAndTimeEdgeConstraintFulfiller;
						for( itAndTimeEdgeConstraintFulfiller = andTimeEdgeConstraintFulfillers.begin(); itAndTimeEdgeConstraintFulfiller != andTimeEdgeConstraintFulfillers.end(); itAndTimeEdgeConstraintFulfiller++ )
							(*itAndTimeEdgeConstraintFulfiller)->setIsTimeEdgeConstraintFulfiller( true, timeStep );

						// Store time edge constraint fulfillers for agent
						(*itAgent)->setTimeEdgeConstraintFulfillers( andTimeEdgeConstraintFulfillers );
					}
                    pOrTimeEdgeConstraintFulfiller = nullptr;
					andTimeEdgeConstraintFulfillers.clear();

					// Mark the opposite station(s) as time edge contraint fulfiller(s)
                    if( pTargetStationOrTimeEdgeConstraintFulfiller != nullptr )
					{
						pTargetStationOrTimeEdgeConstraintFulfiller->setIsTimeEdgeConstraintFulfiller( true, timeStep );

						// Store time edge constraint fulfiller for target station
						list<CComponent*> fulfillers;
						fulfillers.clear();
						fulfillers.push_back( pTargetStationOrTimeEdgeConstraintFulfiller );
						const_cast<CStation*>( (*itAgent)->getTarget() )->setTimeEdgeConstraintFulfillers( fulfillers );
					}
					else
					{
						list<CComponent*>::iterator itTargetStationAndTimeEdgeConstraintFulfiller;
						for( itTargetStationAndTimeEdgeConstraintFulfiller = targetStationAndTimeEdgeConstraintFulfillers.begin(); itTargetStationAndTimeEdgeConstraintFulfiller != targetStationAndTimeEdgeConstraintFulfillers.end(); itTargetStationAndTimeEdgeConstraintFulfiller++ )
							(*itTargetStationAndTimeEdgeConstraintFulfiller)->setIsTimeEdgeConstraintFulfiller( true, timeStep );

						// Store time edge constraint fulfillers for target station
						const_cast<CStation*>( (*itAgent)->getTarget() )->setTimeEdgeConstraintFulfillers( targetStationAndTimeEdgeConstraintFulfillers );
					}
                    pTargetStationOrTimeEdgeConstraintFulfiller = nullptr;
					targetStationAndTimeEdgeConstraintFulfillers.clear();

					itAgent++;
				}
			}
		}

		// Clear the list of entering agents (will be filled by the calculation step for all waiting agents)
		enteringAgents.clear();

		// For all agents that have not moved during simulation, give them a new chance since they
		// could have been waiting due to agents that entered by violating a time edge constraint
		itAgent = waitingAgents.begin();
		while( itAgent != waitingAgents.end() )
		{
			// Recalculate simulation step for agent (to give those agents a chance to 
			// get to a station, that was missed due to unconsidered time constraints of other agents)
//			MSimulate( timeStep )<-itAgent();
            MSimulate::handling( *itAgent, timeStep );

			// If the agent was entering a station during calculation, put it to the list of entering agents
			if( (*itAgent)->getHasEnterEvent() )
			{
				isAgentAddedToStation = true;
				enteringAgents.push_back( *itAgent );
				itAgent = waitingAgents.erase( itAgent );
			}
			else
			{
				itAgent++;
			}
		}
	}


	//
	// Add real enter events to time table
	//

	// For all agents
	for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
	{
		// If agent finally entered...
		if( (*itAgent)->getHasEnterEvent() )
		{
			//
			// FIX ME - THERE ARE TWO ISSUES HERE:
			// 1.) THIS IS THE WRONG PLACE FOR RESETTING THE FULFILLER STATI. IT WILL NOT WORK
			//     IN CASE OF DIRECTED TIME EDGES WITH A WEIGHT <= 1, SINCE THE ENTERING EVENTS
			//     REALLY HAVE TO TAKE PLACE TO RESET THE FULFILLER STATUS. IT SHOULD BE DONE
			//     WHERE THE HYPOTHETICAL ENTER EVENTS ARE CALCULATED AND THEN REVERTED WHENEVER
			//     THE WHOLE ACTION OF AN AGENT IS REVERTED:
			// 2.) THE ALGORITHM IS VERY INEFFICIENT: IT SHOULD BE ITERATED OVER THE COMPONENTS
			//     THAT ARE CONNECTED BY VISIT EDGES, INSTEAD OF *ALL* COMPONENTS. THEREFORE A
			//     NEW C-MOL-METHOD HAS TO BE IMPLEMENTED.
			//

			// Reset agent's flag for being a time edge constraint fulfiller
			(*itAgent)->setIsTimeEdgeConstraintFulfiller( false );
			
			// Iterate over all connected components and remove the agent from their lists of 
			// time edge contraint fulfillers
			list<CAgent*>::iterator itFulfilledAgent;
			for( itFulfilledAgent = agents.begin(); itFulfilledAgent != agents.end(); itFulfilledAgent++ )
			{
				(*itFulfilledAgent)->getTimeEdgeConstraintFulfillers().remove( *itAgent );
			}
			list<CStation*>::iterator itFulfilledStation;
			for( itFulfilledStation = stations.begin(); itFulfilledStation != stations.end(); itFulfilledStation++ )
			{
				(*itFulfilledStation)->getTimeEdgeConstraintFulfillers().remove( *itAgent );
			}

			// Reset agent target's flag for being a time edge constraint fulfiller
			const_cast<CStation*>( (*itAgent)->getTarget() )->setIsTimeEdgeConstraintFulfiller( false );

			// Iterate over all connected components and remove the station from their lists of 
			// time edge contraint fulfillers
			for( itFulfilledAgent = agents.begin(); itFulfilledAgent != agents.end(); itFulfilledAgent++ )
			{
				(*itFulfilledAgent)->getTimeEdgeConstraintFulfillers().remove( const_cast<CStation*>( (*itAgent)->getTarget() ) );
			}
			for( itFulfilledStation = stations.begin(); itFulfilledStation != stations.end(); itFulfilledStation++ )
			{
				(*itFulfilledStation)->getTimeEdgeConstraintFulfillers().remove( const_cast<CStation*>( (*itAgent)->getTarget() ) );
			}


			// Set the agent as being active in this simulation step (important for break condition)
			(*itAgent)->setIsActive( true );

			// Add enter event
			fpAddAgentEnterEvent( *itAgent, (*itAgent)->getTarget(), timeStep );
		}
	}


	//
	// Cumulate rewards
	//

	// For all agents
	int visitingAgents = 0;
	int agentsAlreadyHavingVisitEvents = 0;
	for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
	{
		// Count visiting agents
		if( (*itAgent)->getIsVisiting() )
			visitingAgents++;
	}

	// Calculate reward
	double reward = 0;
	if( agents.size() > 0 )
		reward = static_cast<double>( visitingAgents ) / agents.size();

	// For each agent having a target, cumulate the reward
	for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
	{
        if( (*itAgent)->getTarget() != nullptr )
			(*itAgent)->setReward( (*itAgent)->getReward() + reward );
	}

	// Visualize the current state of simulation if a graphic context was given
    if( graphicContext != nullptr )
		draw( graphicContext );

	// Calculate leaving events for all agents 
	// (this is done seperately after a simulation step to avoid other agents from visiting full stations
	//  in the same simulation step an agent left a full station, in case other agents are calculated 
	// after the agent left the full station)
	for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
	{
		// LEAVE
		// If visit event is over
		// (if the agent currently visits a station and the corresponding agent type and/or station
		// type has a time attribute and the agent already remained the corresponding time on its station)
		if( (*itAgent)->getIsVisiting() && (((*itAgent)->getComponentType().getHasTime() && ((*itAgent)->getTime() >= (*itAgent)->getComponentType().getTime())) || ((*itAgent)->getTarget()->getComponentType().getHasTime() && ((*itAgent)->getTime() >= (*itAgent)->getTarget()->getComponentType().getTime()))) )
		{
            // Transfer agent's reward to Java
            transferRewardToJava( **itAgent );

			// Set the agent as being active in this simulation step (important for break condition)
			(*itAgent)->setIsActive( true );

			// Add leave event to time table
			fpAddAgentLeaveEvent( (*itAgent), (*itAgent)->getTarget(), timeStep );

			// Reset corresponding values of station
			const_cast<CStation*>( (*itAgent)->getTarget() )->removeAgent( *(*itAgent) );

			// Leave station
			(*itAgent)->setPreviousTarget( (*itAgent)->getTarget() );
            (*itAgent)->setTarget( nullptr, 0, INVALID_TIME );
			(*itAgent)->setCoveredDistanceToTarget( 0 );
			(*itAgent)->setIsVisiting( false );

			// Reset the agent's visit time
			(*itAgent)->setTime( 0 );
		}
	}
}


bool CSimulation::getIsFinished()
{
	// For all agents
	list<CAgent*>::const_iterator itAgent;
	for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
	{
		// If the agent is active or if the agent has no target but could still visit a connected 
		// station according to the frequency and necessity attributes
		// (There are mainly three important cases in which the agent can have a NULL target:
		//  1. The agent has chosen a target with fulfilled frequency/necessity => if the agent can
		//     still visit other stations, NULL target does not imply inactivity).
		//  2. The agent decided itself for a NULL target and simulation is not finished yet =>
		//     if the agent can still visit other stations, NULL target states nothing about 
		//     inactivity but won't take the simulation any further towards the end).
		//  3. The agent decided itself for a NULL target and simulation is finished =>
		//     THIS CASE CAN CAUSE PROBLEMS IF THE AGENT AND ITS POSSIBLE TARGETS HAVE NO 
		//     FREQUENCY/NECESSITY ATTRIBUTES. IN THIS CASE NOTHING CAN BE STATED ABOUT INACTIVITY
		//     OF THE AGENT (EVEN IF THE SIMULATION IS ALREADY FINISHED).
		//
		// IN GENERAL AGENTS SHOULD REFRAIN FROM CONSTANTLY RETURNING NULL TARGETS WHEN SIMULATION IS 
		// FINISHED SINCE THIS WOULD PREVENT THE SIMULATION END FROM BEING CORRECTLY DETECTED.
		// (BUT THIS CASE IS VERY UNLIKELY SINCE AN AGENT HAS NO ACCESS TO THE COMPLETE MODEL AND THUS
		// COULD ONLY DETERMINE ITS SIMULATION END FROM OBSERVING AND LEARNING. FURTHERMORE THE AGENT 
		// WOULD HAVE TO DETERMINE THE EXACT SIMULATION STEP (BEFORE THE SIMULATION ITSELF CAN 
		// DETERMINE THE END) AND THEN CONSTANTLY KEEP RETURNING NULL TARGETS).
		// 
        if( (*itAgent)->getIsActive() || (((*itAgent)->getTarget() == nullptr) && !getIsFinished( *itAgent )) )
			return( false );
	}

	return( true );
}


bool CSimulation::getIsFinished( CAgent* pAgent )
{
	bool isAgentFinished = true;

	// If the agent has no Frequency and Necessity OR one of the two is not fulfilled
	bool agentHasFrequency = pAgent->getAgentType().getHasFrequency();
	bool agentFrequencyFulfilled = pAgent->getEnterEventCount() >= pAgent->getAgentType().getFrequency();
	bool agentHasNecessity = pAgent->getAgentType().getHasNecessity();
	bool agentNecessityFulfilled = pAgent->getIsNecessityFulfilled();
	if(    (!agentHasFrequency && !agentHasNecessity) 
		|| (agentHasFrequency && !agentFrequencyFulfilled) 
		|| (agentHasNecessity && !agentNecessityFulfilled) )
	{
		// Store that the agent is not finished
		isAgentFinished = false;
	}

	bool isAgentDueToConnectedStationsFinished = true;
	
	// For all edges
	list<CEdgeConnector*>::const_iterator itEdgeConnector;
	for( itEdgeConnector = pAgent->getAgentType().getEdgeConnectors().begin(); itEdgeConnector != pAgent->getAgentType().getEdgeConnectors().end(); itEdgeConnector++ )
	{
//		isAgentDueToConnectedStationsFinished = MCheckFinished( *pAgent )°(const_cast<CEdge*>( &((*itEdgeConnector)->getEdge()) ))();
        isAgentDueToConnectedStationsFinished = MCheckFinished::handling( const_cast<CEdge*>( &((*itEdgeConnector)->getEdge()) ), *pAgent );
        if( isAgentDueToConnectedStationsFinished == false )
			break;
	}

	if( !isAgentFinished && !isAgentDueToConnectedStationsFinished )
		return( false );

	return( true );
}


bool CSimulation::getIsValid()
{
	// For all stations
	list<CStation*>::const_iterator itStation;
	for( itStation = stations.begin(); itStation != stations.end(); itStation++ )
	{
		// If the stations's frequency or necessity is not fulfilled
		bool stationHasFrequency = (*itStation)->getStationType().getHasFrequency();
		bool stationFrequencyFulfilled = (*itStation)->getEnterEventCount() >= (*itStation)->getStationType().getFrequency();
		bool stationHasNecessity = (*itStation)->getStationType().getHasNecessity();
		bool stationNecessityFulfilled = (*itStation)->getIsNecessityFulfilled();
		if( (stationHasFrequency && (!stationFrequencyFulfilled)) || (stationHasNecessity && (!stationNecessityFulfilled)) )
			return( false );
	}

	// For all agents
	list<CAgent*>::const_iterator itAgent;
	for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
	{
		// If the agent's frequency or necessity is not fulfilled
		bool agentHasFrequency = (*itAgent)->getAgentType().getHasFrequency();
		bool agentFrequencyFulfilled = (*itAgent)->getEnterEventCount() >= (*itAgent)->getAgentType().getFrequency();
		bool agentHasNecessity = (*itAgent)->getAgentType().getHasNecessity();
		bool agentNecessityFulfilled = (*itAgent)->getIsNecessityFulfilled();
		if( (agentHasFrequency && (!agentFrequencyFulfilled)) || (agentHasNecessity && (!agentNecessityFulfilled)) )
 			return( false );
	}

	return( true );
}


void CSimulation::reset()
{
	// Reset all stations
	list<CStation*>::const_iterator itStation;
	for( itStation = stations.begin(); itStation != stations.end(); itStation++ )
	{
		(*itStation)->reset();
	}

	// Reset all agents
	list<CAgent*>::const_iterator itAgent;
	for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
	{
		(*itAgent)->reset();
	}

	// Reset simulation time
	timeStep = 0;

// SHUT DOWN, REINITIALIZATION, ETC. CANNOT BE DONE HERE, SINCE THE JAVA OBJECTS MUST SURVIVE WHEN SIMULATION IS RESET
// FOR THE NEXT RUN! SHOULD BE DONE MAYBE ON NEW GRAPH/LOAD GRAPH!?
//  // Reinitialize Java
//  initializeJava();

// RELOADING THE AGENT FILE NEEDS TO RESTART THE AGENT INTERFACE SERVER; BY NOT DOING THIS HERE, IT WILL NOT BE POSSIBLE
// TO CHANGE AN AGENT DURING A RUNNING SIMULATION (BUT THIS SEEMS TO BE NEEDED RARELY)
//	// Reload the agent file
//    reloadAgentJavaFile();
}


//------------------------------------------------------------
// Simulation
//------------------------------------------------------------


void CSimulation::initializeJava()
{
    // Reinitialize the interface (does also perform proper shut down, if already initialized)
    externalJavaAgentInterface.initialize( string( sAgentFile ) );

    if( externalJavaAgentInterface.isInitialized() )
    {
        string message = externalJavaAgentInterface.hello();
        externalJavaAgentInterface.print( message );
    }
}


void CSimulation::reloadAgentJavaFile()
{
#if !defined USE_NATIVE_AGENT_INTERFACE

    // Reload/run the file containing the evaluation function of the agent
    // INITIALIZING JAVA AUTOMATICALLY RECOMPILES AND LOADS THE JAVA AGENT FILE
    initializeJava();

#endif
}


void CSimulation::transferComponentsToJava()
{
#if !defined USE_NATIVE_AGENT_INTERFACE

    // Create stream for the code to be transferred
    ostringstream ossJavaCode;

	
	//
	// Transfer component types
	//

	// Transfer component types for stations
	list<CStation*>::iterator itStation;
	for( itStation = stations.begin(); itStation != stations.end(); itStation++ )
	{
		// Create station type name
		ostringstream ossStationTypeName; 
        ossStationTypeName << "stationType_" << (*itStation)->getStationType().getName() << "_" << (*itStation)->getStationType().getId();
        const string& sStationTypeName = ossStationTypeName.str();

		// Create space
		int space = -1;
		if( (*itStation)->getStationType().getHasSpace() )
			space = (*itStation)->getStationType().getSpace();

		// Create frequency
		int frequency = -1;
		if( (*itStation)->getStationType().getHasFrequency() )
			frequency = (*itStation)->getStationType().getFrequency();

		// Create necessity
		int necessity = -1;
		if( (*itStation)->getStationType().getHasNecessity() )
			necessity = (*itStation)->getStationType().getNecessity();

		// Create time
		int time = -1;
		if( (*itStation)->getStationType().getHasTime() )
			time = (*itStation)->getStationType().getTime();

		// Create cycle
		int cycle = -1;
		if( (*itStation)->getStationType().getHasCycle() )
			cycle = (*itStation)->getStationType().getCycle();

        // Write the Java code
        ossJavaCode <<
            // HERE AN STATION TYPE COULD BE CREATED WITH CORRECT TYPE!
            sStationTypeName << "." << "name" << " = " << "\"" << (*itStation)->getStationType().getName() << "\"" << ";\n" <<
            sStationTypeName << "." << "type" << " = " << "\"STATION_TYPE\"" << ";\n" <<
            sStationTypeName << "." << "components" << " = " << "new ArrayList<Station>()" << ";\n" <<
            sStationTypeName << "." << "space" << " = " << space << ";\n" <<
            sStationTypeName << "." << "frequency" << " = " << frequency << ";\n" <<
            sStationTypeName << "." << "necessity" << " = " << necessity << ";\n" <<
            sStationTypeName << "." << "time" << " = " << time << ";\n" <<
            sStationTypeName << "." << "cycle" << " = " << cycle << ";\n";

		// Add empty line
        ossJavaCode << "\n";
	}

    // Add empty line
    ossJavaCode << "\n";

	// Transfer component types for agents
	list<CAgent*>::iterator itAgent;
	for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
	{
        // Create agent type name
		ostringstream ossAgentTypeName; 
        ossAgentTypeName << "agentType_" << (*itAgent)->getAgentType().getName() << "_" << (*itAgent)->getAgentType().getId();
        const string& sAgentTypeName = ossAgentTypeName.str();

		// Create frequency
		int size = -1;
		if( (*itAgent)->getAgentType().getHasSize() )
			size = (*itAgent)->getAgentType().getSize();

		// Create priority
		int priority = -1;
		if( (*itAgent)->getAgentType().getHasPriority() )
			priority = (*itAgent)->getAgentType().getPriority();

		// Create frequency
		int frequency = -1;
		if( (*itAgent)->getAgentType().getHasFrequency() )
			frequency = (*itAgent)->getAgentType().getFrequency();

		// Create necessity
		int necessity = -1;
		if( (*itAgent)->getAgentType().getHasNecessity() )
			necessity = (*itAgent)->getAgentType().getNecessity();

		// Create time
		int time = -1;
		if( (*itAgent)->getAgentType().getHasTime() )
			time = (*itAgent)->getAgentType().getTime();

		// Create cycle
		int cycle = -1;
		if( (*itAgent)->getAgentType().getHasCycle() )
			cycle = (*itAgent)->getAgentType().getCycle();

        // Write the Java code
        ossJavaCode <<
            // HERE AN AGENT TYPE COULD BE CREATED WITH CORRECT TYPE!
            sAgentTypeName << "." << "name" << " = " << "\"" << (*itAgent)->getAgentType().getName() << "\"" << ";\n" <<
            sAgentTypeName << "." << "type" << " = " << "\"AGENT_TYPE\"" << ";\n" <<
            sAgentTypeName << "." << "components" << " = " << "new ArrayList<Agent>()" << ";\n" <<
            sAgentTypeName << "." << "size" << " = " << size << ";\n" <<
            sAgentTypeName << "." << "priority" << " = " << priority << ";\n" <<
            sAgentTypeName << "." << "frequency" << " = " << frequency << ";\n" <<
            sAgentTypeName << "." << "necessity" << " = " << necessity << ";\n" <<
            sAgentTypeName << "." << "time" << " = " << time << ";\n" <<
            sAgentTypeName << "." << "cycle" << " = " << cycle << ";\n";

        // Add empty line
        ossJavaCode << "\n";
	}

    // Add empty line
    ossJavaCode << "\n";


	//
	// Transfer components and set component type edge relations
	//

	// Transfer stations
	for( itStation = stations.begin(); itStation != stations.end(); itStation++ )
	{
		// Create station type name
		ostringstream ossStationTypeName; 
        ossStationTypeName << "stationType_" << (*itStation)->getStationType().getName() << "_" << (*itStation)->getStationType().getId();
        const string& sStationTypeName = ossStationTypeName.str();

		// Create station name
		ostringstream ossStationName; 
        ossStationName << "station_" << sStationTypeName << "_" << (*itStation)->getNumber();
        const string& sStationName = ossStationName.str();

		// Create station object, set its type and set the component type --> components relation 
		// and the edge relations
        ossJavaCode <<
            // HERE AN STATION COULD BE CREATED WITH CORRECT TYPE!
            sStationName << "." << "name" << " = " << "\"" << (*itStation)->getStationType().getName() << "." << (*itStation)->getNumber() << "\"" << ";\n" <<
            sStationName << "." << "type" << " = " << sStationTypeName << ";\n" <<
            sStationName << "." << "type" << "." << "components" << ".add" << "( " << sStationName << " )" << ";\n" <<
            sStationName << "." << "type" << "." << "visitEdges = new ArrayList<VisitEdge>()" << ";\n" <<
            sStationName << "." << "type" << "." << "placeEdges = new ArrayList<PlaceEdge>()" << ";\n" <<
            sStationName << "." << "type" << "." << "timeEdges = new ArrayList<TimeEdge>()" << ";\n";
		list<CEdgeConnector*>::const_iterator itEdgeConnector;
		for( itEdgeConnector = (*itStation)->getStationType().getEdgeConnectors().begin(); itEdgeConnector != (*itStation)->getStationType().getEdgeConnectors().end(); itEdgeConnector++ )
		{
            // Create type name of opposite connector (SPECIFIC TYPE CANNOT BE INFERRED HERE EASILY - WILL BE DONE BY AGENT INTERFACE SERVER!)
			ostringstream ossOppositeTypeName; 
            ossOppositeTypeName << "componentType_" << (*itEdgeConnector)->getLinkedConnector().getComponentType().getName() << "_" << (*itEdgeConnector)->getLinkedConnector().getComponentType().getId();
            const string& sOppositeTypeName = ossOppositeTypeName.str();

//			MTransferToJava( ossJavaCode, sStationTypeName.c_str(), sOppositeTypeName.c_str(), (*itEdgeConnector)->getIsDirected(), (*itEdgeConnector)->getLinkedConnector().getIsDirected(), (*itEdgeConnector)->getIsAndConnection(), (*itEdgeConnector)->getLinkedConnector().getIsAndConnection() )°(const_cast<CEdge*>( &(*itEdgeConnector)->getEdge() ))();
            MTransferToJava::handling( const_cast<CEdge*>( &(*itEdgeConnector)->getEdge() ), ossJavaCode, sStationTypeName.c_str(), sOppositeTypeName.c_str(), (*itEdgeConnector)->getIsDirected(), (*itEdgeConnector)->getLinkedConnector().getIsDirected(), (*itEdgeConnector)->getIsAndConnection(), (*itEdgeConnector)->getLinkedConnector().getIsAndConnection() );
        }

        // ADD ALL RELEVANT STATION PROPERTIES HERE
        transferAttributesToJava( **itStation, ossJavaCode );

        // Add empty line
        ossJavaCode << "\n";
	}

	// Add empty line
    ossJavaCode << "\n";

	// Create and transfer a list containing all stations
    ossJavaCode << "stations = new ArrayList<Station>();\n";
	for( itStation = stations.begin(); itStation != stations.end(); itStation++ )
	{
		// Create station type name
		ostringstream ossStationTypeName; 
        ossStationTypeName << "stationType_" << (*itStation)->getStationType().getName() << "_" << (*itStation)->getStationType().getId();
        const string& sStationTypeName = ossStationTypeName.str();

		// Create station name
		ostringstream ossStationName; 
        ossStationName << "station_" << sStationTypeName << "_" << (*itStation)->getNumber();
        const string& sStationName = ossStationName.str();

        ossJavaCode << "stations.add( " << sStationName << " );\n";
	}

	// Transfer agents
	for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
	{
		// Create agent type name
		ostringstream ossAgentTypeName; 
        ossAgentTypeName << "agentType_" << (*itAgent)->getAgentType().getName() << "_" << (*itAgent)->getAgentType().getId();
        const string& sAgentTypeName = ossAgentTypeName.str();

		// Create agent name
		ostringstream ossAgentName; 
        ossAgentName << "agent_"  << sAgentTypeName << "_" << (*itAgent)->getNumber();
        const string& sAgentName = ossAgentName.str();

		// Create agent object, set its type and set the component type --> components relation 
		// and the edge relations
        ossJavaCode <<
            // HERE AN AGENT COULD BE CREATED WITH CORRECT TYPE!
            sAgentName << "." << "name" << " = " << "\"" << (*itAgent)->getAgentType().getName() << "." << (*itAgent)->getNumber() << "\"" << ";\n" <<
            sAgentName << "." << "type" << " = " << sAgentTypeName << ";\n" <<
            sAgentName << "." << "type" << "." << "components" << ".add" << "( " << sAgentName << " )" << ";\n" <<
            sAgentName << "." << "type" << "." << "visitEdges = new ArrayList<VisitEdge>()" << ";\n" <<
            sAgentName << "." << "type" << "." << "placeEdges = new ArrayList<PlaceEdge>()" << ";\n" <<
            sAgentName << "." << "type" << "." << "timeEdges = new ArrayList<TimeEdge>()" << ";\n";
        list<CEdgeConnector*>::const_iterator itEdgeConnector;
		for( itEdgeConnector = (*itAgent)->getAgentType().getEdgeConnectors().begin(); itEdgeConnector != (*itAgent)->getAgentType().getEdgeConnectors().end(); itEdgeConnector++ )
		{
            // Create type name of opposite connector (SPECIFIC TYPE CANNOT BE INFERRED HERE EASILY - WILL BE DONE BY AGENT INTERFACE SERVER!)
			ostringstream ossOppositeTypeName; 
            ossOppositeTypeName << "componentType_" << (*itEdgeConnector)->getLinkedConnector().getComponentType().getName() << "_" << (*itEdgeConnector)->getLinkedConnector().getComponentType().getId();
            const string& sOppositeTypeName = ossOppositeTypeName.str();

//			MTransferToJava( ossJavaCode, sAgentTypeName.c_str(), sOppositeTypeName.c_str(), (*itEdgeConnector)->getIsDirected(), (*itEdgeConnector)->getLinkedConnector().getIsDirected(), (*itEdgeConnector)->getIsAndConnection(), (*itEdgeConnector)->getLinkedConnector().getIsAndConnection() )°(const_cast<CEdge*>( &(*itEdgeConnector)->getEdge() ))();
            MTransferToJava::handling( const_cast<CEdge*>( &(*itEdgeConnector)->getEdge() ), ossJavaCode, sAgentTypeName.c_str(), sOppositeTypeName.c_str(), (*itEdgeConnector)->getIsDirected(), (*itEdgeConnector)->getLinkedConnector().getIsDirected(), (*itEdgeConnector)->getIsAndConnection(), (*itEdgeConnector)->getLinkedConnector().getIsAndConnection() );
        }

        // ADD ALL RELEVANT AGENT PROPERTIES HERE
        transferAttributesToJava( **itAgent, ossJavaCode );

        // Add empty line
        ossJavaCode << "\n";
	}

    // Add empty line
    ossJavaCode << "\n";

    // Create and transfer an (ordered) hash map containing all agents
    ossJavaCode << "agents = new LinkedHashMap<Agent, Object>();\n";
	for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
	{
        // Create agent type name
		ostringstream ossAgentTypeName; 
        ossAgentTypeName << "agentType_"  << (*itAgent)->getAgentType().getName() << "_" << (*itAgent)->getAgentType().getId();
        const string& sAgentTypeName = ossAgentTypeName.str();

		// Create agent name
		ostringstream ossAgentName; 
        ossAgentName << "agent_"  << sAgentTypeName << "_" << (*itAgent)->getNumber();
        const string& sAgentName = ossAgentName.str();

        ossJavaCode << "agents.put( " << sAgentName << ", null )" << ";\n";
    }

    // Update Java agent interface with data
    externalJavaAgentInterface.update( ossJavaCode.str() );

#endif
}


void CSimulation::transferAttributesToJava( const CStation& station, ostringstream& ossJavaCode )
{
#if !defined USE_NATIVE_AGENT_INTERFACE

    // Create station type name
	ostringstream ossStationTypeName; 
    ossStationTypeName << "stationType_" << station.getStationType().getName() << "_" << station.getStationType().getId();
    const string& sStationTypeName = ossStationTypeName.str();

	// Create station name
	ostringstream ossStationName; 
    ossStationName << "station_" << sStationTypeName << "_" << station.getNumber();
    const string& sStationName = ossStationName.str();

	// Create remaining space
	int remainingSpace = -1;
	if( station.getStationType().getHasSpace() )
		remainingSpace = station.getStationType().getSpace() - station.getSpace();

	// Create remaining frequency
	int remainingFrequency = -1;
	if( station.getStationType().getHasFrequency() )
		remainingFrequency = station.getStationType().getFrequency() - station.getEnterEventCount();

	// Remaining space
    ossJavaCode << sStationName << "." << "space" << " = " << remainingSpace << ";\n";

	// Remaining frequency
    ossJavaCode << sStationName << "." << "frequency" << " = " << remainingFrequency << ";\n";
	
    // Remaining necessities
//	MTransferNecessityToJava( ossJavaCode, sStationTypeName.c_str(), sStationName.c_str() )°station();
    MTransferNecessityToJava::handling( station, ossJavaCode, sStationTypeName.c_str(), sStationName.c_str() );

    // Remaining cycle(s)
//	MTransferCycleToJava( ossJavaCode, sStationTypeName.c_str(), sStationName.c_str() )°station();
    MTransferCycleToJava::handling( station, ossJavaCode, sStationTypeName.c_str(), sStationName.c_str() );

#endif
}


void CSimulation::transferAttributesToJava( const CAgent& agent, ostringstream& ossJavaCode )
{
#if !defined USE_NATIVE_AGENT_INTERFACE

	// Create agent type name
	ostringstream ossAgentTypeName; 
    ossAgentTypeName << "agentType_" << agent.getAgentType().getName() << "_" << agent.getAgentType().getId();
    const string& sAgentTypeName = ossAgentTypeName.str();

	// Create agent name
	ostringstream ossAgentName; 
    ossAgentName << "agent_" << sAgentTypeName << "_" << agent.getNumber();
    const string& sAgentName = ossAgentName.str();

	// Create remaining frequency
	int remainingFrequency = -1;
	if( agent.getAgentType().getHasFrequency() )
		remainingFrequency = agent.getAgentType().getFrequency() - agent.getEnterEventCount();

	// Create remaining visiting time
	int remainingTime = -1;
	if( agent.getIsVisiting() && agent.getAgentType().getHasTime()  )
		remainingTime = agent.getAgentType().getTime() - agent.getTime();
	else if( agent.getIsVisiting() && agent.getTarget()->getStationType().getHasTime()  )
		remainingTime = agent.getTarget()->getStationType().getTime() - agent.getTime();


	//
	// Create target
	//

    string sTarget = "null";
    if( agent.getTarget() != nullptr )
	{
		// Create station type name
		ostringstream ossStationTypeName; 
        ossStationTypeName << "stationType_" << agent.getTarget()->getStationType().getName() << "_" << agent.getTarget()->getStationType().getId();
        const string& sStationTypeName = ossStationTypeName.str();

		// Create station name
		ostringstream ossStationName; 
        ossStationName << "station_" << sStationTypeName << "_" << agent.getTarget()->getNumber();
        const string& sStationName = ossStationName.str();

		sTarget = sStationName;
	}


    //
    // Create previous target
    //

    string sPreviousTarget = "null";
    if( agent.getPreviousTarget() != nullptr )
    {
        // Create station type name
        ostringstream ossStationTypeName;
        ossStationTypeName << "stationType_" << agent.getPreviousTarget()->getStationType().getName() << "_" << agent.getPreviousTarget()->getStationType().getId();
        const string& sStationTypeName = ossStationTypeName.str();

        // Create station name
        ostringstream ossStationName;
        ossStationName << "station_" << sStationTypeName << "_" << agent.getPreviousTarget()->getNumber();
        const string& sStationName = ossStationName.str();

        sPreviousTarget = sStationName;
    }


	// Create visiting
    string sIsVisiting = "false";
	if( agent.getIsVisiting() )
        sIsVisiting = "true";

	// Remaining frequency
    ossJavaCode << sAgentName << "." << "frequency" << " = " << remainingFrequency << ";\n";

	// Remaining time
    ossJavaCode << sAgentName << "." << "time" << " = " << remainingTime << ";\n";

    // Remaining necessities
//	MTransferNecessityToJava( ossJavaCode, sAgentTypeName.c_str(), sAgentName.c_str() )°agent();
    MTransferNecessityToJava::handling( agent, ossJavaCode, sAgentTypeName.c_str(), sAgentName.c_str() );

    // Remaining cycle(s)
//	MTransferCycleToJava( ossJavaCode, sAgentTypeName.c_str(), sAgentName.c_str() )°agent();
    MTransferCycleToJava::handling( agent, ossJavaCode, sAgentTypeName.c_str(), sAgentName.c_str() );

	// Target station
    ossJavaCode << sAgentName << "." << "target" << " = " << sTarget << ";\n";

    // Previous target station
    ossJavaCode << sAgentName << "." << "previousTarget" << " = " << sPreviousTarget << ";\n";

    // Is visiting
    ossJavaCode << sAgentName << "." << "visiting" << " = " << sIsVisiting << ";\n";

	// Reset communication data
    // TODO: HERE A PROPER IF STATEMENT IS NEEDED! THIS LINE IS BUGGY
    // SINCE THE agents ARRAY MAY BE NULL HERE WHEN transferAttributesToJava( const CAgent& agent )
    // IS CALLED FROM transferComponentsToJava() (I. E., IN TIME STEP 1?). THE ARRAY IS CREATED
    // AT THE END OF transferComponentsToJava().
    // (THIS LINE WORKS ONLY DUE TO THE PERMISSIVENESS OF THE JAVA AGENT INTERFACE SERVER!)
    ossJavaCode << "agents.put( " << sAgentName << ", null );\n";

#endif
}


void CSimulation::transferRewardToJava( const CAgent& agent )
{
#if !defined USE_NATIVE_AGENT_INTERFACE

    // Calculate reward
	double reward = agent.getReward() / (timeStep - agent.getActionSelectionTime() + 1);


	//
	// Call reward method for agent
	//

	// Create agent type name
	ostringstream ossAgentTypeName; 
    ossAgentTypeName << "agentType_" << agent.getAgentType().getName() << "_" << agent.getAgentType().getId();
    const string& sAgentTypeName = ossAgentTypeName.str();

	// Create agent name
	ostringstream ossAgentName; 
    ossAgentName << "agent_" << sAgentTypeName << "_" << agent.getNumber();
    const string& sAgentName = ossAgentName.str();

	// Create the corresponding method call
    ostringstream ossRewardMethodCallParameters;
    ossRewardMethodCallParameters << sAgentName << " " << timeStep << " " << reward;

    // Run the reward method of the external java agent interface
    externalJavaAgentInterface.reward( ossRewardMethodCallParameters.str() );

#endif
}


//------------------------------------------------------------
// UI and advanced communcation
//------------------------------------------------------------

void CSimulation::connectToTimetable( TfpAddAgentEnterEvent fpAddAgentEnterEvent, TfpAddAgentLeaveEvent fpAddAgentLeaveEvent )
{
	this->fpAddAgentEnterEvent = fpAddAgentEnterEvent;
	this->fpAddAgentLeaveEvent = fpAddAgentLeaveEvent;
}


void CSimulation::flushConsole()
{
    externalJavaAgentInterface.flushConsole();
}


//------------------------------------------------------------
// Platform dependent(!) interface for visualization
//------------------------------------------------------------

void CSimulation::draw( HDC graphicContext )
{
    // Init double-buffering
    HDC hBackbuffer;
    hBackbuffer = CreateCompatibleDC( graphicContext );
    HBITMAP theBitmap;
    theBitmap = CreateCompatibleBitmap( graphicContext, visibleFrameWidth, visibleFrameHeight );
    SelectObject( hBackbuffer, theBitmap );
    
    // Draw background
    HBRUSH hbrush, hbrushold;
    hbrush = static_cast<HBRUSH>( CreateSolidBrush( RGB( 150, 150, 150 ) ) );
    hbrushold = static_cast<HBRUSH>( SelectObject( hBackbuffer, hbrush ) );
    Rectangle( hBackbuffer, 0, 0, visibleFrameWidth, visibleFrameHeight );
    SelectObject( hBackbuffer, hbrushold );
    DeleteObject( hbrush );
	
    // Draw editable surface
    int red = EDIT_SURFACE_RED;
    int green = EDIT_SURFACE_GREEN;
    int blue = EDIT_SURFACE_BLUE;
    hbrush = static_cast<HBRUSH>( CreateSolidBrush( RGB( red, green, blue ) ) );
    hbrushold = static_cast<HBRUSH>( SelectObject( hBackbuffer, hbrush ) );
    Rectangle( hBackbuffer, xToScreenX( 0 ), yToScreenY( 0 ), xToScreenX( EDITOR_WIDTH ), yToScreenY( EDITOR_HEIGHT ) );
    SelectObject( hBackbuffer, hbrushold );
    DeleteObject( hbrush );

    // Draw grid
    int screenX = xToScreenX( 0 );
    int screenY = yToScreenY( 0 );
    CColor penColor( GRID_RED, GRID_GREEN, GRID_BLUE );
    CColor brushColor( 0, 0, 0 );
    bool isSelected = false;

//    MDrawWithStyles( hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected )°grid( EDITOR_WIDTH, EDITOR_HEIGHT );
    MDrawWithStyles::handling( grid, hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected, EDITOR_WIDTH, EDITOR_HEIGHT );

    // Draw perspectives for agent types
    list<CPerspective*> drawnPerspectives;
    list<CAgent*>::iterator itAgent;
    for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
    {
        // Draw perspective if it was not drawn yet
        CPerspective* pPerspective = &const_cast<CPerspective&>( (*itAgent)->getComponentType().getOwner() );
        if( find( drawnPerspectives.begin(), drawnPerspectives.end(), pPerspective ) == drawnPerspectives.end() )
        {
            int screenX = xToScreenX( pPerspective->getX() );
            int screenY = yToScreenY( pPerspective->getY() );
            CColor penColor( PERSPECTIVE_FRAME_RED, PERSPECTIVE_FRAME_GREEN, PERSPECTIVE_FRAME_BLUE );
            CColor brushColor( PERSPECTIVE_RED, PERSPECTIVE_GREEN, PERSPECTIVE_BLUE );
            bool isSelected = false;

//            MDrawWithStyles( hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected )<-pPerspective();
            MDrawWithStyles::handling( *pPerspective, hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected );

            // Store that the perspective was already drawn
            drawnPerspectives.push_back( pPerspective );
        }
    }

    // Draw perspectives for station types
    list<CStation*>::iterator itStation;
    for( itStation = stations.begin(); itStation != stations.end(); itStation++ )
    {
        // Draw perspective if it was not drawn yet
        CPerspective* pPerspective = &const_cast<CPerspective&>( (*itStation)->getComponentType().getOwner() );
        if( find( drawnPerspectives.begin(), drawnPerspectives.end(), pPerspective ) == drawnPerspectives.end() )
        {
            int screenX = xToScreenX( pPerspective->getX() );
            int screenY = yToScreenY( pPerspective->getY() );
            CColor penColor( PERSPECTIVE_FRAME_RED, PERSPECTIVE_FRAME_GREEN, PERSPECTIVE_FRAME_BLUE );
            CColor brushColor( PERSPECTIVE_RED, PERSPECTIVE_GREEN, PERSPECTIVE_BLUE );
            bool isSelected = false;

//            MDrawWithStyles( hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected )<-pPerspective();
            MDrawWithStyles::handling( *pPerspective, hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected );

            // Store that the perspective was already drawn
            drawnPerspectives.push_back( pPerspective );
        }
    }

    // Draw edges for agents
    list<CEdge*> drawnEdges;
    for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
    {
        // Draw edges
        CComponentType* pComponentType = &const_cast<CComponentType&>( (*itAgent)->getComponentType() );
        list<CEdgeConnector*>::const_iterator itEdgeConnector;
        for( itEdgeConnector = pComponentType->getEdgeConnectors().begin(); itEdgeConnector != pComponentType->getEdgeConnectors().end(); itEdgeConnector++ )
        {
            CEdge* pEdge = &const_cast<CEdge&>( (*itEdgeConnector)->getEdge() );

            // Draw edge only if it was not drawn yet
            if( find( drawnEdges.begin(), drawnEdges.end(), pEdge ) == drawnEdges.end() )
            {
                int screenX = xToScreenX( pEdge->getX1() );
                int screenY = yToScreenY( pEdge->getY1() );
                CColor penColor( EDGE_RED, EDGE_GREEN, EDGE_BLUE );
                CColor brushColor( 255, 255, 255 );
                bool isSelected = false;

                // The edge label should be invisible here
                pEdge->setIsLabelVisible( false );

//                MDrawWithStyles( hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected )°pEdge();
                MDrawWithStyles::handling( pEdge, hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected );

                // Store that the edge was already drawn
                drawnEdges.push_back( pEdge );
            }
        }
    }

    // Draw edges for stations
    for( itStation = stations.begin(); itStation != stations.end(); itStation++ )
    {
        // Draw edges
        CComponentType* pComponentType = &const_cast<CComponentType&>( (*itStation)->getComponentType() );
        list<CEdgeConnector*>::const_iterator itEdgeConnector;
        for( itEdgeConnector = pComponentType->getEdgeConnectors().begin(); itEdgeConnector != pComponentType->getEdgeConnectors().end(); itEdgeConnector++ )
        {
            CEdge* pEdge = &const_cast<CEdge&>( (*itEdgeConnector)->getEdge() );

            // Draw edge only if it was not drawn yet
            if( find( drawnEdges.begin(), drawnEdges.end(), pEdge ) == drawnEdges.end() )
            {
                int screenX = xToScreenX( pEdge->getX1() );
                int screenY = yToScreenY( pEdge->getY1() );
                CColor penColor( EDGE_RED, EDGE_GREEN, EDGE_BLUE );
                CColor brushColor( 255, 255, 255 );
                bool isSelected = false;

                // The edge label should be invisible here
                pEdge->setIsLabelVisible( false );

//                MDrawWithStyles( hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected )°pEdge();
                MDrawWithStyles::handling( pEdge, hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected );

                // Store that the edge was already drawn
                drawnEdges.push_back( pEdge );
            }
        }
    }

    // Draw agents on their current place edges
    for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
    {
        if( (!(*itAgent)->getIsVisiting()) && ((*itAgent)->getTarget() != nullptr) && ((*itAgent)->getDistanceToTarget() != NO_PATH) && ((*itAgent)->getCoveredDistanceToTarget() < (*itAgent)->getDistanceToTarget()) )
        {
            // Get the agent's next target edge connector and its distance to the next station type
            const CEdgeConnector* pNextTargetEdgeConnector = nullptr;
            int distanceFromStartToNextStationType = 0;
            const list<const CEdgeConnector*>& path = (*itAgent)->getPathToTarget();
            list<const CEdgeConnector*>::const_iterator itEdgeConnector;
            for( itEdgeConnector = path.begin(); itEdgeConnector != path.end(); itEdgeConnector++ )
            {
                distanceFromStartToNextStationType += static_cast<const CPlaceEdge&>( (*itEdgeConnector)->getEdge() ).getValue();

                if( (*itAgent)->getCoveredDistanceToTarget() <= /*?*/ distanceFromStartToNextStationType )
                {
                    pNextTargetEdgeConnector = (*itEdgeConnector);
                    break;
                }
            }

            // Calculate covered distance on the agent's current edge
            int coveredDistanceToTarget = (*itAgent)->getCoveredDistanceToTarget();
            int coveredDistanceOnEdge = static_cast<const CPlaceEdge&>( pNextTargetEdgeConnector->getEdge() ).getValue() - (distanceFromStartToNextStationType - coveredDistanceToTarget);

            int screenX = xToScreenX( const_cast<CEdge*>( &(pNextTargetEdgeConnector->getEdge()) )->getX1() );
            int screenY = yToScreenY( const_cast<CEdge*>( &(pNextTargetEdgeConnector->getEdge()) )->getY1() );
            CColor penColor( COMPONENTTYPE_FRAME_RED, COMPONENTTYPE_FRAME_GREEN, COMPONENTTYPE_FRAME_BLUE );  // The frame of agents should have the same color than the agent type frame
            CColor brushColor( (*itAgent)->getComponentType().color.getRed(), (*itAgent)->getComponentType().color.getGreen(), (*itAgent)->getComponentType().color.getBlue() );
            bool isSelected = false;

            // The edge label should be invisible here
            const_cast<CEdge*>( &(pNextTargetEdgeConnector->getEdge()) )->setIsLabelVisible( false );

            // Draw the agent on the edge
            // MDrawWithStyles( hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected )°pEdge();
            MDrawWithStyles::handling( static_cast<CPlaceEdge*>( const_cast<CEdge*>( &(pNextTargetEdgeConnector->getEdge()) ) ), hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected, *itAgent, pNextTargetEdgeConnector, coveredDistanceOnEdge );
        }
    }

    // Draw component types for agents
    list<CComponentType*> drawnComponentTypes;
    for( itAgent = agents.begin(); itAgent != agents.end(); itAgent++ )
    {
        // Draw component type only if not drawn yet
        CComponentType* pComponentType = &const_cast<CComponentType&>( (*itAgent)->getComponentType() );
        CPerspective* pPerspective = &const_cast<CPerspective&>( (*itAgent)->getComponentType().getOwner() );
        if( find( drawnComponentTypes.begin(), drawnComponentTypes.end(), pComponentType ) == drawnComponentTypes.end() )
        {
            int screenX = xToScreenX( pPerspective->getX() + pComponentType->getX() );
            int screenY = yToScreenY( pPerspective->getY() + pComponentType->getY() );
            CColor penColor( COMPONENTTYPE_FRAME_RED, COMPONENTTYPE_FRAME_GREEN, COMPONENTTYPE_FRAME_BLUE );
            CColor brushColor( pComponentType->color.getRed(), pComponentType->color.getGreen(), pComponentType->color.getBlue() );
            bool isSelected = false;

//            MDrawWithStyles( hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected )°MGetSimulationDrawFactor()°pComponentType();
            MGetSimulationDrawFactor::handling( pComponentType );
            MDrawWithStyles::handling( pComponentType, hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected );

            // Store that the component type was already drawn
            drawnComponentTypes.push_back( pComponentType );
        }
    }

    // Draw component types and edges for stations
    for( itStation = stations.begin(); itStation != stations.end(); itStation++ )
    {
        // Draw component type only if not drawn yet
        CComponentType* pComponentType = &const_cast<CComponentType&>( (*itStation)->getComponentType() );
        CPerspective* pPerspective = &const_cast<CPerspective&>( (*itStation)->getComponentType().getOwner() );
        if( find( drawnComponentTypes.begin(), drawnComponentTypes.end(), pComponentType ) == drawnComponentTypes.end() )
        {
            int screenX = xToScreenX( pPerspective->getX() + pComponentType->getX() );
            int screenY = yToScreenY( pPerspective->getY() + pComponentType->getY() );
            CColor penColor( COMPONENTTYPE_FRAME_RED, COMPONENTTYPE_FRAME_GREEN, COMPONENTTYPE_FRAME_BLUE );
            CColor brushColor( pComponentType->color.getRed(), pComponentType->color.getGreen(), pComponentType->color.getBlue() );
            bool isSelected = false;

//            MDrawWithStyles( hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected )°MGetSimulationDrawFactor()°pComponentType();
            MGetSimulationDrawFactor::handling( pComponentType );
            MDrawWithStyles::handling( pComponentType, hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, isSelected );

            // Store that the component type was already drawn
            drawnComponentTypes.push_back( pComponentType );
        }
    }


    // Show back-buffer
    BitBlt( graphicContext, 0, 0, EDITOR_WIDTH*GRID_XSIZE, EDITOR_HEIGHT*GRID_YSIZE, hBackbuffer, 0, 0, SRCCOPY );

    // Delete back-buffer device context and bitmap
    DeleteDC( hBackbuffer );
    DeleteObject( theBitmap );
}



//------------------------------------------------------------
// Helper
//------------------------------------------------------------


int CSimulation::xToScreenX( double x ) const
{
	int scrollOffset = curScreenX;
	double gridSize = static_cast<double>( GRID_XSIZE );

    return( static_cast<int>( (x*gridSize*curZoomFactor) - scrollOffset ) );
}


int CSimulation::yToScreenY( double y ) const
{
	int scrollOffset = curScreenY;
	double gridSize = static_cast<double>( GRID_YSIZE );

    return( static_cast<int>( (y*gridSize*curZoomFactor) - scrollOffset ) );
}


double CSimulation::screenXToX( int x ) const
{
	int scrollOffset = curScreenX;
	double gridSize = static_cast<double>( GRID_XSIZE );
	
	return( (x + scrollOffset)/gridSize/curZoomFactor );
}


double CSimulation::screenYToY( int y ) const
{
	int scrollOffset = curScreenY;
	double gridSize = static_cast<double>( GRID_YSIZE );

	return( (y + scrollOffset)/gridSize/curZoomFactor );
}
