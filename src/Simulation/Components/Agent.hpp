/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined Agent_hpp
	#define Agent_hpp





#include "Component.hpp"
#include "./../../Editor/AgentType.h"  // Included from editor!
//class CAgentType;

// Forward declarations
class CStation;
class CStationType;





// using namespace std;





/**
 * Objects of this class represent single simulated agents that serve as data structures
 * for the simulation.
 */
class CAgent : public CComponent
{
	public: 

		/**
		 * Creates a new agent and initializes its (edge dependent) agent specific attribute values.
		 * 
		 * @param agentType   the agent's associated agent type
		 */
		CAgent( CAgentType& agentType );

		/**
		 * Destroys the station.
		 */ 
		virtual ~CAgent();

		/** 
		 * Initializes the agent's (edge dependent) agent specific attribute values.
		 */
		void initialize();

		/** 
		 * Resets the agent's values to start values at the beginning of a simulation.
		 * <p>
		 * ALL INITIALIZATION WORK OF VALUES HAS TO BE DONE HERE TO GUARANTEE THAT VALUES ARE 
		 * REINITIALIZED EXACLTY THE SAME WAY AS IN THE FIRST RUN WHEN SIMULATION RESTARTS.
		 */
		void reset();

		/** 
		 * Returns the agent's component type as agent type. 
		 *
		 * @return   the agent's associated agent type
		 */
		const CAgentType& getAgentType() const;

		/** 
		 * Returns the agent's number, identifying the agent in the context of its agent type. 
		 *
		 * @return   the number identifying the agent in the context of its agent type
		 */
		int getNumber() const;

		/** 
		 * Returns the agent's cumulated reward since his last action selection. 
		 *
		 * @return   the agent's cumulated reward since his last action selection
		 */
		double getReward() const;

		/** 
		 * Returns the time unit when the agent selected its current target station (time of action selection). 
		 *
		 * @return   the time unit when the agent selected its current target station
		 */
		int getActionSelectionTime() const;

		/** 
		 * Returns the agent's current target station; NULL if agent currently has no target. 
		 *
		 * @return   the agent's current target station
		 */
		const CStation* getTarget() const;

		/** 
		 * Returns the agent's current target evaluation value; 0 if agent currently has no target. 
		 *
		 * @return   the agent's current target evaluation value
		 */
		double getTargetEvaluation() const;

		/** 
		 * Returns the agent's previous target station; NULL if agent has no previous target. 
		 * (This should only happen if its previous target was deleted during simulation!) 
		 *
		 * @return   the agent's previous target station
		 */
        const CStation* getPreviousTarget() const;

		/** 
		 * Returns the agent's current covered distance to target station. 
		 *
         * @return   the agent's current covered distance to target station; getDistanceToTarget() if the agent type has no speed attribute; zero if the agent has not covered any distance yet or if the agent has no previous/current target
		 */
		int getCoveredDistanceToTarget();

		/** 
		 * Returns the agent's distance to its current target station. 
		 *
         * @return   the agent's distance to its current target station; NO_PATH if there is no path to the agent's current target station or if the agent has no previous or no current target station
		 */
		int getDistanceToTarget();

        /**
         * @brief Returns the path to the agent's current target in form of a list of edge connectors of the edges' (incoming) endpoints.
         *
         * @return   the path to the agent's current target; empty list if the agent currently has no target
         */
        const list<const CEdgeConnector*>& getPathToTarget();

		/** 
		 * Returns whether the agent is currently visiting a station. 
		 *
		 * @return   true when the agent is currently visiting a station, false otherwise
		 */
		bool getIsVisiting() const;

		/** 
		 * Returns whether the agent has entered a station (in the last calculated time step). 
		 *
		 * @return   true when the agent has entered a station, false otherwise
		 */
		bool getHasEnterEvent() const;

		/** 
		 * Returns the time the agent has already worked on its current station. 
		 *
		 * @return   the time the agent has already worked on its current station
		 */
		int getTime() const;

		/** 
		 * Returns the agent's size on the currently visited station. 
		 * (When visiting a station this should correspond to the agent type's size, but in case it has no size attribute 
		 * the size is dynamically set to the remaining space of the currently visited station.)
		 *
		 * @return   the agent's size on the currently visited station.
		 */
		int getSizeOnCurrentStation();

		/**
		 * Returns whether the agent was active in its previous simulation step.
		 *
		 * @return   whether the agent was active in its previous simulation step
		 */
		bool getIsActive();

		/** 
		 * Sets the agent's cumulated reward since his last action selection. 
		 *
		 * @param   the agent's new cumulated reward since his last action selection
		 */
		void setReward( double reward );

		/** 
		 * Sets the agent's current target station and evaluation value for this station. 
		 * If the given station is NULL, the evaluation value is always set to 0 and the 
		 * time is set to INVALID_TIME.In addition the cumulated reward is reset to 0 if 
		 * the target is reset to NULL.
		 * 
		 * @param target       the agent's new current target station
		 * @param evaluation   the agent's new current target evauation value
		 * @param time         the time when the target was set (time of action selection)
		 */
		void setTarget( const CStation* target, double evaluation, int time );

		/** 
		 * Sets the agent's previous target station. 
		 *
		 * @param previousTarget   the agent's new previous target station
		 */
		void setPreviousTarget( const CStation* previousTarget );

		/** 
		 * Sets the agent's current covered distance to target station. 
		 *
		 * @param coveredDistanceToTarget   the agent's new covered distance to its target station
		 */
		void setCoveredDistanceToTarget( int coveredDistanceToTarget );

		/** 
		 * Sets whether the agent is currently visiting its target station. 
		 *
		 * @param isVisiting   new value indicating whether the agent is currently visiting its target station
		 */
		void setIsVisiting( bool isVisiting );

		/** 
		 * Sets whether the agent has entered a station (in the last calculated time step). 
		 *
		 * @param isVisiting   new value indicating whether the agent has entered a station
		 */
		void setHasEnterEvent( bool isVisiting );

		/** 
		 * Sets the time the agent has already worked on its current station. 
		 *
		 * @param time   the new time value representing the time the agent has already worked on its station
		 */
		void setTime( int time );

		/** 
		 * Sets the agent's size on the currently visited station. 
		 * (When visiting a station this should correspond to the agent type's size, but in case it has no size attribute 
		 * the size should be dynamically set to the remaining space of the currently visited station.)
		 *
		 * @param time   the current agent's size on its target station
		 */
		void setSizeOnCurrentStation( int sizeOnCurrentStation );


		/**
		 * Marks the agent as active/inactive. If set inactive and the agent is active during a 
		 * simulation step, its state will be reset to active (in case of movement, visiting/leaving 
		 * and staying at a station).
		 *
		 * @param isActive   the flag to determine whether the agent was active or inactive
		 */
		void setIsActive( bool isActive );

private:

		/** The agent's number, identifying the agent in the context of its agent type. */
		int number;

		/** The agent's cumulated reward since his last action selection. */
		double reward;

		/** The time unit when the agent selected its current target station. */
		int actionSelectionTime;

		/** The agent's time already worked on its current station. */
		int time;

		/** The agent's current covered capacity. */
		int capacity;

		/** 
		 * The agent's size on the currently visited station. 
		 * (When visiting a station this should correspond to the agent type's size, but in case it has no size attribute 
		 * the size is dynamically set to the remaining space of the currently visited station.)
		 */ 
		int sizeOnCurrentStation;

		/** The agent's current target station. */
		const CStation* target;

		/** The agent's evaluation value for its current selected target. */
		double targetEvaluation;

		/** The agent's previous target station. (Belongs to the last enter event - not to the previous enter event!) */
		const CStation* previousTarget;

		/** The agent's currently covered distance to it's current target. */
		int coveredDistanceToTarget;

		/** The agent's distance to it's current target. */
		int distanceToTarget;

        /** The path to the agent's current target in form of a list of edge connectors of the edges' (incoming) endpoints */
        list<const CEdgeConnector*> pathToTarget;

		/** Determines whether the agent is currently visiting a station. */
		bool isVisiting;

		/** Determines whether the agent has entered a station (in the last calculated time step). */
		bool hasEntered;

		/** Determines whether the agent was active in its previous simulation step. */
		bool isActive;
};





#endif
