/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined InitEdgeDependentStationValues_hmol
	#define InitEdgeDependentStationValues_hmol


#include <typeinfo>
#include "./../../Editor/VisitEdge.h"  // Included from editor!
#include "./../../Editor/PlaceEdge.h"  // Included from editor!
#include "./../../Editor/TimeEdge.h"  // Included from editor!
#include "Station.hpp"





/**
 * The method initializes the attribute values for the given station that are dependent from an edge
 * (for example the station's time counters that are dependent from visit edges of connected agents).
 *
 * @param station the station whose attribute values will be initialized
 */
//method MInitEdgeDependentStationValues( CStation& station )
class MInitEdgeDependentStationValues
{
#define METHOD_PARAMS_INITEDGEDEPENDENTSTATIONVALUES CStation& station

public:

	/**
	 * Initializes the time counters of the given station regarding 
	 * the agents that are connected to it via the visit edge.
	 */
//	virtual void <CVisitEdge*>();
    static void handling( CVisitEdge* that, METHOD_PARAMS_INITEDGEDEPENDENTSTATIONVALUES );

//	virtual void <CPlaceEdge*>();
    static void handling( CPlaceEdge* that, METHOD_PARAMS_INITEDGEDEPENDENTSTATIONVALUES );
//	virtual void <CTimeEdge*>();
    static void handling( CTimeEdge* that, METHOD_PARAMS_INITEDGEDEPENDENTSTATIONVALUES );


    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline void handling( void* vp_that, METHOD_PARAMS_INITEDGEDEPENDENTSTATIONVALUES )
    {
        if( typeid( *(static_cast<CVisitEdge*>( vp_that ))) == typeid( CVisitEdge ) )
            handling( static_cast<CVisitEdge*>( vp_that ), station );
        else if( typeid( *(static_cast<CPlaceEdge*>( vp_that ))) == typeid( CPlaceEdge ) )
            handling( static_cast<CPlaceEdge*>( vp_that ), station );
        else if( typeid( *(static_cast<CTimeEdge*>( vp_that ))) == typeid( CTimeEdge ) )
            handling( static_cast<CTimeEdge*>( vp_that ), station );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }
};





#endif
