/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined Station_hpp
	#define Station_hpp





#include "Component.hpp"
#include "./../../Editor/StationType.h"  // Included from editor!
//class CStationType;





class CAgentType;
class CAgent;





// using namespace std;





/**
 * Objects of this class represent single simulated stations that serve as data structures
 * for the simulation.
 */
class CStation : public CComponent 
{
	public: 

		/**
		 * Creates a new station and initializes its (edge dependent) station specific attribute values.
		 * 
		 * @param stationType   the station's associated station type
		 */
		CStation( CStationType& stationType );

		/**
		 * Destroys the station.
		 */ 
		virtual ~CStation();

		/** 
		 * Initializes the station's (edge dependent) station specific attribute values.
		 */
		void initialize();

		/** 
		 * Resets the stations's values to start values at the beginning of a simulation.
		 * <p>
		 * ALL INITIALIZATION WORK OF VALUES HAS TO BE DONE HERE TO GUARANTEE THAT VALUES ARE 
		 * REINITIALIZED EXACLTY THE SAME WAY AS IN THE FIRST RUN WHEN SIMULATION RESTARTS.
		 */
		void reset();

		/** 
		 * Cleans up the station by removing all visiting agents from list (and maybe performing 
		 * more similar actions later).
		 * <p>
		 * This method is called by simulation before a station is removed from simulation
		 * since it seems to make problems when an element is added to a container by 
		 * simulation DLL and this element is later removed by the editor DLL through the 
		 * station's destructor.
		 */
		void cleanUp();

		/** 
		 * Returns the station's number, identifying the station in the context of its station type. 
		 *
		 * @return   the station's number, identifying the station in the context of its station type
		 */
		int getNumber() const;

		/** 
		 * Returns whether this station was entered by an agent (in the last calculated time step) 
		 * by checking if one of its currently visiting agents has entered a station.
		 *
		 * @return true if station was entered by an agent; false otherwise
		 */
		bool getHasEnterEvent() const;

		/**
		 * Initializes the time counters of the station for the agents that are 
		 * connected to it via a visit edge.
		 *
		 * @param agentType   the connected agent type for whose components the time counters should be initialized
		 */
		void initTimes( const CAgentType& agentType );

		/** 
		 * Returns the station's component type as station type.
		 *
		 * @return   the stations's associated station type
		 */
		const CStationType& getStationType() const;

		/** 
		 * Returns the station's currently covered space. 
		 *
		 * @return   the station's currently covered space
		 */
		int getSpace() const;

		/** 
		 * Adds the agent as current visitor to this station. 
		 * Adapts the agent's size on this station in case the agent type has no size attribute. 
		 */
		void addAgent( CAgent& agent );

		/** 
		 * Removes the visiting agent from this station. 
		 * Resets the agent's size on this station to 0. 
		 */
		void removeAgent( CAgent& agent );


	private:
		
		/** The station's number, identifying the station in the context of its station type. */
		int number;

		/** 
		 * The time counters for each connected agent, representing the time a connected
		 * agent is involved in the current visit event on this station.
	     */
		map<CAgent*, int> times;

		/** The station's currently covered space. */
		int space;

		/** The agents currently visiting this station. */
		list<CAgent*> visitingAgents;
};





#endif
