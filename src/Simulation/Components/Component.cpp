/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------

#include "Component.hpp"
#include "./../Constants.h"
//#include "./../../../EditorDll/Editor/ComponentType.h"  // Included from editor!
#include <typeinfo>
//#pragma c-mol_hdrstop

#include "./InitEdgeDependentComponentValues.hpp"





//------------------------------------------------------------
// Construction/destruction
//------------------------------------------------------------


CComponent::CComponent( CComponentType& componentType ) : componentType( componentType )
{
	initialize();
}


CComponent::~CComponent()
{
}


void CComponent::initialize()
{
	// Initialize the component's necessities
	necessities.clear();
	list<CEdgeConnector*>::const_iterator itEdgeConnector;
	for( itEdgeConnector = getComponentType().getEdgeConnectors().begin(); itEdgeConnector != getComponentType().getEdgeConnectors().end(); itEdgeConnector++ )
    {
		// Check if the linked connector is connected as well (this is important since when an edge is 
		// deleted, its connectors are deleted serialized while the edge remains in a semi-destructed 
		// state where polymorphism is no longer supported and the method would not recognize the edge type!)
        if( &((*itEdgeConnector)->getLinkedConnector().getComponentType()) != nullptr )
//			MInitEdgeDependentComponentValues( *this )°(const_cast<CEdge*>(&((*itEdgeConnector)->getEdge())))();
            MInitEdgeDependentComponentValues::handling( const_cast<CEdge*>(&((*itEdgeConnector)->getEdge())), *this );
	}

	// Reset all component's values to start values for simulation
	reset();
}


void CComponent::reset()
{
	// Do clean up first
	cleanUp();

	// Reset the necessity values
	map<const CComponent*, int>::iterator itNecessityValue;
	for( itNecessityValue = necessities.begin(); itNecessityValue != necessities.end(); itNecessityValue++ )
		itNecessityValue->second = 0;

	// Initialize the component's current enter event counter
	enterEventCount = 0;

	// Initialize the component's time stamp of its last and previous enter events
	lastEnterEventTime = INVALID_TIME;
	previousEnterEventTime = INVALID_TIME;

	// At the beginning a component does not serve as a fulfiller for a time edge contraint
	// of another component
	isTimeEdgeConstraintFulfiller = false;
	timeEdgeConstraintFulfillerTime = INVALID_TIME;
}


void CComponent::cleanUp()
{
	// Remove all time edge constraint fulfillers
	timeEdgeConstraintFulfillers.clear();
}


void CComponent::initNecessities( const CComponentType& componentType )
{
	vector<CComponent*>::const_iterator itComponent;
	for( itComponent = componentType.getComponents().begin(); itComponent != componentType.getComponents().end(); itComponent++ )
		necessities[ *itComponent ] = 0;
}



//------------------------------------------------------------
// Getter
//------------------------------------------------------------


const CComponentType& CComponent::getComponentType() const
{
	return( componentType );
}


bool CComponent::getHadEnterEventAlready() const
{
	return( getEnterEventCount() > 0 );
}


int CComponent::getLastEnterEventTime() const
{
	return( lastEnterEventTime );
}


int CComponent::getPreviousEnterEventTime() const
{
	return( previousEnterEventTime );
}


int CComponent::getEnterEventCount() const
{
	return( enterEventCount );
}


int CComponent::getNecessity( const CComponent& component ) const
{
	return( necessities.find( &component )->second );
}


bool CComponent::getIsNecessityFulfilled() const
{
	// If component type has necessity attribute set...
	if( getComponentType().getHasNecessity() )
	{
		// Iterate over all necessity counters
		map<const CComponent*, int>::const_iterator itNecessityCounter;
		for( itNecessityCounter = necessities.begin(); itNecessityCounter != necessities.end(); itNecessityCounter++ )
		{
			// If there is a not filfilled necessity constraint, return false!
			if( itNecessityCounter->second < getComponentType().getNecessity() )
			{
				return( false );
			}
		}

		// If reached to here: All necessities of connected components were fulfilled 
		// (or component type has no necessity) => Return true!
		return( true );
	}

	// ...else if component has no necessity attribute set => constraint can not be fulfilled!
	else
	{
		return( false );
	}
}


bool CComponent::getIsTimeEdgeConstraintFulfiller() const
{
	return( isTimeEdgeConstraintFulfiller );
}


int CComponent::getTimeEdgeConstraintFulfillerTime() const
{
	return( timeEdgeConstraintFulfillerTime );
}


list<CComponent*>& CComponent::getTimeEdgeConstraintFulfillers()
{
	return( timeEdgeConstraintFulfillers );
}



//------------------------------------------------------------
// Setter
//------------------------------------------------------------


void CComponent::setLastEnterEventTime( int time )
{
	lastEnterEventTime = time;
}


void CComponent::setPreviousEnterEventTime( int time )
{
	previousEnterEventTime = time;
}


void CComponent::setEnterEventCount( int count )
{
	enterEventCount = count;
}


void CComponent::setNecessity( const CComponent& component, int necessity )
{
	necessities[ &component ] = necessity;
}


void CComponent::setIsTimeEdgeConstraintFulfiller( bool isTimeEdgeConstraintFulfiller, int time )
{
	this->isTimeEdgeConstraintFulfiller = isTimeEdgeConstraintFulfiller;
	this->timeEdgeConstraintFulfillerTime = time;
}


void CComponent::setTimeEdgeConstraintFulfillers( list<CComponent*> timeEdgeConstraintFulfillers )
{
	this->timeEdgeConstraintFulfillers = timeEdgeConstraintFulfillers;
}
