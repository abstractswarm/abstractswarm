/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------

#include "Station.hpp"
#include "Agent.hpp"
//#include "./../../../EditorDll/Editor/StationType.h"  // Included from editor! 
#include "./../../Editor/AgentType.h"  // Included from editor!
#include <typeinfo>
//#pragma c-mol_hdrstop

#include "./InitEdgeDependentStationValues.hpp"





//------------------------------------------------------------
// Construction/destruction
//------------------------------------------------------------


CStation::CStation( CStationType& stationType ) : CComponent( stationType ) /*CComponent( reinterpret_cast<CComponentType&>( stationType ) )*/
{
	// Initialize the station's number (in the context of its station type)
	number = stationType.getNoOfComponents() + 1;

	// Initialize attributes
	initialize();

	// Add the station to simulation when created and connected to simulation
	TfpAddStationToSimulation fpAddStationToSimulation = getStationType().getAddStationToSimulation();
    if( fpAddStationToSimulation != nullptr )
		(*fpAddStationToSimulation)( this );
}


CStation::~CStation()
{
	// Remove the station from simulation when deleted and connected to simulation
	TfpRemoveStationFromSimulation fpRemoveStationFromSimulation = getStationType().getRemoveStationFromSimulation();
    if( fpRemoveStationFromSimulation != nullptr )
		(*fpRemoveStationFromSimulation)( this );
}

void CStation::initialize()
{
	// Add station anew to simulation, if connected
	// (This is needed since by removing a component, a clean up is performed by 
	// simulation DLL, which is also performed by the reset at the end of initialization. 
	// Thus, the reset function is used by both simulation and editor, but in general, 
	// a clean up can only be performed by simulation (or must not cause deletions). 
	// This is because a clean up deletes the values created during simulation. Because 
	// initialization uses the reset function to (re)set all values, it must be ensured 
	// that the component is in a state were the clean up performed by the reset causes 
	// no delete operateions of values created during simulation.)
	TfpAddStationToSimulation fpAddStationToSimulation = getStationType().getAddStationToSimulation();
	TfpRemoveStationFromSimulation fpRemoveStationFromSimulation = getStationType().getRemoveStationFromSimulation();
    if( (fpAddStationToSimulation != nullptr) && (fpRemoveStationFromSimulation != nullptr) )
	{
		(*fpRemoveStationFromSimulation)( this );
		(*fpAddStationToSimulation)( this );
	}

	// Initialize the component's (edge dependent) general attribute values
	CComponent::initialize();

	// Initialize the stations's time counters
	times.clear();
	list<CEdgeConnector*>::const_iterator itEdgeConnector;
	for( itEdgeConnector = getComponentType().getEdgeConnectors().begin(); itEdgeConnector != getComponentType().getEdgeConnectors().end(); itEdgeConnector++ )
    {
		// Check if the linked connector is connected as well (this is important since when an edge is 
		// deleted, its connectors are deleted serialized while the edge remains in a semi-destructed 
		// state where polymorphism is no longer supported and the method would not recognize the edge type!)
        if( &((*itEdgeConnector)->getLinkedConnector().getComponentType()) != nullptr )
//			MInitEdgeDependentStationValues( *this )°(const_cast<CEdge*>(&((*itEdgeConnector)->getEdge())))();
            MInitEdgeDependentStationValues::handling( const_cast<CEdge*>(&((*itEdgeConnector)->getEdge())), *this );
	}

	// Reset all station's values to start values for simulation
	reset();
}


void CStation::reset()
{
	// Reset component values
	CComponent::reset();

	// Do clean up first
	cleanUp();

	// Reset the times values
	map<CAgent*, int>::iterator itTimesValue;
	for( itTimesValue = times.begin(); itTimesValue != times.end(); itTimesValue++ )
		itTimesValue->second = 0;

	// Initialize the station's space counter
	space = 0;
}

void CStation::initTimes( const CAgentType& agentType )
{
	vector<CAgent*>::const_iterator itAgent;
	for( itAgent = agentType.getAgents().begin(); itAgent != agentType.getAgents().end(); itAgent++ )
		times[ *itAgent ] = 0;
}


void CStation::cleanUp()
{
	// Do base class' clean up stuff
	CComponent::cleanUp();

	// Remove all elements from visiting agents
	while( !visitingAgents.empty() )
	{
		visitingAgents.pop_back();
	}
}



//------------------------------------------------------------
// Getter
//------------------------------------------------------------


int CStation::getNumber() const
{
	return( number );
}


bool CStation::getHasEnterEvent() const
{
	// For all currently visiting agents
	list<CAgent*>::const_iterator itAgent;
	for( itAgent = visitingAgents.begin(); itAgent != visitingAgents.end(); itAgent++ )
	{
		// If agent entered this station (in the previous simulation step) => station was entered!
		if( (*itAgent)->getHasEnterEvent() )
			return( true );
	}

	// If reached to here, station was not entered by an agent (in the previous simulation step)!
	return( false );
}


const CStationType& CStation::getStationType() const
{
	return( static_cast<const CStationType&>( getComponentType() ) );
}


int CStation::getSpace() const
{
	// Iterate over all agent types connected by visit edges to this station's station type

		// Iterate over all agents of the agent type

			// If an agent is currently visiting this station

				// Sum up the agent's size as the station's covered space

	// Return the covered space
	return( space );
}



//------------------------------------------------------------
// Adding/removing agents
//------------------------------------------------------------

void CStation::addAgent( CAgent& agent )
{
	// Increase the enter event count
	setEnterEventCount( getEnterEventCount() + 1 );

	//  If has necessity attribute
	if( getStationType().getHasNecessity() )
	{
		// Increase the necessity count for its entering agent
		setNecessity( agent, getNecessity( agent ) + 1 );
	}

	// Add the agent to list of currently visiting agents
	visitingAgents.push_back( &agent );

	// If the agent has a size attribute, set its size on this station to its normal size
	if( agent.getAgentType().getHasSize() )
	{
		agent.setSizeOnCurrentStation( agent.getAgentType().getSize() );
	}

	// If the station has a space attribute and the agent type has a size attribute, add size
	if( getStationType().getHasSpace() && agent.getAgentType().getHasSize() )
	{
		space = space + agent.getSizeOnCurrentStation();
	}

	// else if station type has a space attribute
	else if( getStationType().getHasSpace() )
	{
		// Store the agent's size on this station
		agent.setSizeOnCurrentStation( getStationType().getSpace() - getSpace() );

		// Set space to maximum
		space = getStationType().getSpace();
	}
}


void CStation::removeAgent( CAgent& agent )
{
	// Remove the agent from list of currently visiting agents
	visitingAgents.remove( &agent );

	// If station type has a space attribute, free the space used by the agent on the station
	if( getStationType().getHasSpace() )
	{
		space = space - agent.getSizeOnCurrentStation();
	}

	// Reset the agent's size on its current station
	agent.setSizeOnCurrentStation( 0 );
}
