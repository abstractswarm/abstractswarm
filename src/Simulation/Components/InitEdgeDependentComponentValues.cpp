/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------

#include "./../..//Editor/VisitEdge.h"
#include "./../../Editor/PlaceEdge.h"
#include "./../../Editor/TimeEdge.h"
#include "Component.hpp"
#include <typeinfo>
//#pragma c-mol_hdrstop

#include "InitEdgeDependentComponentValues.hpp"





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MInitEdgeDependentComponentValues::<CVisitEdge*>()
void MInitEdgeDependentComponentValues::handling( CVisitEdge* that, METHOD_PARAMS_INITEDGEDEPENDENTCOMPONENTVALUES )
{
	// If the edge is completly connected
    if( (&(that->getConnector1().getComponentType()) != nullptr) && (&(that->getConnector2().getComponentType()) != nullptr) )
	{
		// If the component type of the given component is connected to the edge's first connector, add the necessity for the second one
		if( &(that->getConnector1().getComponentType()) == &(component.getComponentType()) )
		{
			// Add the necessity only if the component has a necessity attribute set
			if( component.getComponentType().getHasNecessity() )
				component.initNecessities( that->getConnector2().getComponentType() );
		}

		// ...else if the component type of the given component is connected to the edge's second connector, add the necessity for the first one
		else if( &(that->getConnector2().getComponentType()) == &(component.getComponentType()) )
		{
			// Add the necessity only if the component has a necessity attribute set
			if( component.getComponentType().getHasNecessity() )
				component.initNecessities( that->getConnector1().getComponentType() );
		}
	}
}


//void MInitEdgeDependentComponentValues::<CPlaceEdge*>()
void MInitEdgeDependentComponentValues::handling( CPlaceEdge* that, METHOD_PARAMS_INITEDGEDEPENDENTCOMPONENTVALUES )
{
}


//void MInitEdgeDependentComponentValues::<CTimeEdge*>()
void MInitEdgeDependentComponentValues::handling( CTimeEdge* that, METHOD_PARAMS_INITEDGEDEPENDENTCOMPONENTVALUES )
{
}
