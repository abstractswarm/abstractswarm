/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------

#include "./../../Editor/VisitEdge.h"
#include "./../../Editor/PlaceEdge.h"
#include "./../../Editor/TimeEdge.h"
#include "Station.hpp"
#include <typeinfo>
//#pragma c-mol_hdrstop

#include "InitEdgeDependentStationValues.hpp"





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MInitEdgeDependentStationValues::<CVisitEdge*>()
void MInitEdgeDependentStationValues::handling( CVisitEdge* that, METHOD_PARAMS_INITEDGEDEPENDENTSTATIONVALUES )
{
	// If the edge is completly connected
    if( (&(that->getConnector1().getComponentType()) != nullptr) && (&(that->getConnector2().getComponentType()) != nullptr) )
	{
		// If the station type of the given station is connected to the edge's first connector, add the time counters for the second one
		if( &(that->getConnector1().getComponentType()) == &(station.getComponentType()) )
		{
			// Add the time counters only if the station has a time attribute set
			if( station.getComponentType().getHasTime() )
				station.initTimes( reinterpret_cast<const CAgentType&>( that->getConnector2().getComponentType() ) );
		}

		// ...else if the station type of the given station is connected to the edge's second connector, add the time counters for the first one
		else if( &(that->getConnector2().getComponentType()) == &(station.getComponentType()) )
		{
			// Add the time counters only if the station has a time attribute set
			if( station.getComponentType().getHasTime() )
				station.initTimes( reinterpret_cast<const CAgentType&>( that->getConnector1().getComponentType() ) );
		}
	}
}


//void MInitEdgeDependentStationValues::<CPlaceEdge*>()
void MInitEdgeDependentStationValues::handling( CPlaceEdge* that, METHOD_PARAMS_INITEDGEDEPENDENTSTATIONVALUES )
{
}


//void MInitEdgeDependentStationValues::<CTimeEdge*>()
void MInitEdgeDependentStationValues::handling( CTimeEdge* that, METHOD_PARAMS_INITEDGEDEPENDENTSTATIONVALUES )
{
}
