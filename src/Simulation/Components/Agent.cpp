/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------

#include "Agent.hpp"
#include "Station.hpp"
#include "./../Utilities.h"
#include "./../../Editor/AgentType.h"  // Included from editor!
#include "./../../Editor/StationType.h"  // Included from editor!
#include "./../../Editor/PlaceEdge.h"  // Included from editor!
#include "./../../Editor/VisitEdge.h"  // Included from editor!
#include <algorithm>





//------------------------------------------------------------
// Construction/destruction
//------------------------------------------------------------


CAgent::CAgent( CAgentType& agentType ) : CComponent( agentType ) /*CComponent( reinterpret_cast<CComponentType&>( agentType ) )*/
{
	// Initialize the agent's number (in the context of its agent type)
	number = agentType.getNoOfComponents() + 1;

	// Initialize attributes
	initialize();

	// Add the agent to simulation when created and connected to simulation
	TfpAddAgentToSimulation fpAddAgentToSimulation = getAgentType().getAddAgentToSimulation();
    if( fpAddAgentToSimulation != nullptr )
		(*fpAddAgentToSimulation)( this );
}


CAgent::~CAgent()
{
	// Remove the agent from simulation when deleted and conneted to simulation
	TfpRemoveAgentFromSimulation fpRemoveAgentFromSimulation = getAgentType().getRemoveAgentFromSimulation();
    if( fpRemoveAgentFromSimulation != nullptr )
		(*fpRemoveAgentFromSimulation)( this );
}


void CAgent::initialize()
{
	// Add agent anew to simulation if connected
	// (This is needed since by removing a component, a clean up is performed by 
	// simulation DLL, which is also performed by the reset at the end of initialization. 
	// Thus, the reset function is used by both simulation and editor, but in general, 
	// a clean up can only be performed by simulation (or must not cause deletions). 
	// This is because a clean up deletes the values created during simulation. Because 
	// initialization uses the reset function to (re)set all values, it must be ensured 
	// that the component is in a state were the clean up performed by the reset causes 
	// no delete operateions of values created during simulation.)
	TfpAddAgentToSimulation fpAddAgentToSimulation = getAgentType().getAddAgentToSimulation();
	TfpRemoveAgentFromSimulation fpRemoveAgentFromSimulation = getAgentType().getRemoveAgentFromSimulation();
    if( (fpAddAgentToSimulation != nullptr) && (fpRemoveAgentFromSimulation != nullptr) )
	{
		(*fpRemoveAgentFromSimulation)( this );
		(*fpAddAgentToSimulation)( this );
	}

	// Initialize the component's (edge dependent) general attribute values
	CComponent::initialize();

	// Reset all agent's values to start values for simulation
	reset();
}


void CAgent::reset()
{
	// Reset component values
	CComponent::reset();

	// Do clean up first
	cleanUp();

	// Initialize the agent's cumulated reward since his last action selection and
	// the time of its action selection
	reward = 0;
	actionSelectionTime = INVALID_TIME;

	// Initialize the agent's time counter
	time = 0;

	// Initialize the agent's capacity counter
	capacity = 0;


    //
	// Initialize the agent's current and previous target and it's distance related values
    //

    target = nullptr;
	targetEvaluation = 0;

    // Determine where the agent will start
    previousTarget = nullptr;
    vector<const CStation*> potentialStartStations;
    vector<const CStation*> connectedStations;
    for( list<CEdgeConnector*>::const_iterator itEdgeConnector = getComponentType().getEdgeConnectors().begin(); itEdgeConnector != getComponentType().getEdgeConnectors().end(); itEdgeConnector++ )
    {
        // Get visit edge
        const CVisitEdge* pVisitEdge = dynamic_cast<const CVisitEdge*>( &(*itEdgeConnector)->getEdge() );
        if( pVisitEdge != nullptr )
        {
            const CStationType* pConnectedStationType = static_cast<const CStationType*>( &(*itEdgeConnector)->getLinkedConnector().getComponentType() );
            if( pConnectedStationType != nullptr )
            {
                for( vector<CStation*>::const_iterator itStation = pConnectedStationType->getStations().begin(); itStation != pConnectedStationType->getStations().end(); itStation++ )
                {
                    // Add to connected stations
                    connectedStations.push_back( *itStation );

                    // If visit edge is bold, add to potential start stations
                    if( pVisitEdge->getIsBold() )
                    {
                        potentialStartStations.push_back( *itStation );
                    }
                }
            }
        }
    }

    // If bold visit edges, select from those, else select from all
    if( !potentialStartStations.empty() )
    {
        previousTarget = potentialStartStations[ static_cast<unsigned int>( randomNumber( 0, static_cast<int>( potentialStartStations.size() - 1 ) ) ) ];
    }
    else if( !connectedStations.empty() )
    {
        previousTarget = connectedStations[ static_cast<unsigned int>( randomNumber( 0, static_cast<int>( connectedStations.size() - 1 ) ) ) ];
    }

    // If there is no initial target (unconnected agent), there is no path to an initial target station, thus distance is -1 (no path)
    if( previousTarget == nullptr )
    {
        distanceToTarget = NO_PATH;
    }

    // else if there is an initial target, distance is 0
    else
    {
        distanceToTarget = 0;
    }

    coveredDistanceToTarget = 0;


	// Initialize the agent's size on its current station
	sizeOnCurrentStation = 0;

	// Initialize if the agent is currently visiting a station#
	isVisiting = false;

	// Initialize the agents attribute for entering a station
	hasEntered = false;

	// At the beginning agent is not active
	isActive = false;
}


//------------------------------------------------------------
// Getter
//------------------------------------------------------------

const CAgentType& CAgent::getAgentType() const
{
	return( static_cast<const CAgentType&>( getComponentType() ) );
}


int CAgent::getNumber() const
{
	return( number );
}


double CAgent::getReward() const
{
	return( reward );
}


int CAgent::getActionSelectionTime() const
{
	return( actionSelectionTime );
}


const CStation* CAgent::getTarget() const
{
	return( target );
}


double CAgent::getTargetEvaluation() const
{
	return( targetEvaluation );
}


const CStation* CAgent::getPreviousTarget() const
{
	return( previousTarget );
}


int CAgent::getCoveredDistanceToTarget()
{
    // If the agent has a speed attribute, return the covered distance
    if( static_cast<const CAgentType&>( getComponentType() ).getHasSpeed() )
    {
        return( coveredDistanceToTarget );
    }

    // ...else (if agent has no speed attribute)
    else
    {
        // If there is a path to the current target, the distance is always 100% covered
        int distance = getDistanceToTarget();
        if( distance != NO_PATH )
        {
            return( distance );
        }

        // ...else (if there is no path), nothing from the distance can be covered
        else
        {
            return( 0 );
        }
    }
}


int CAgent::getDistanceToTarget()
{
	// If previousTarget and target exist
    if( (getPreviousTarget() != nullptr) && (getTarget() != nullptr) )
    {
		// DO SOME A*-SEARCH HERE TO CALCULATE THE SHORTEST PATH TO THE TARGET STATION
		// (FOR OPTIMIZATION DO THE A*-SEARCH SOMEWHERE ELSE AND ONLY IF SOMETHING HAS CHANGED!)

        // Create open and closed lists
        map<const CStationType*, int> openList;
        vector<const CStationType*> closedList;
        map<const CStationType*, int> distances;
        map<const CStationType*, list<const CEdgeConnector*>*> pathsToTarget;

        // Initialize open list with first node
        openList[ static_cast<const CStationType*>( &this->getPreviousTarget()->getComponentType() ) ] = 0;

        do
        {
            // Get and remove node with minimal value
            int minValue = 0;
            const CStationType* pMinStationType = nullptr;
            for( map<const CStationType*, int>::iterator itComponentType = openList.begin(); itComponentType != openList.end(); itComponentType++  )
            {
                if( (pMinStationType == nullptr) || itComponentType->second < minValue )
                {
                    minValue = itComponentType->second;
                    pMinStationType = itComponentType->first;
                }
            }
            openList.erase( pMinStationType );

            // If goal found, set current distance and path to target and stop search
            if( pMinStationType == static_cast<const CStationType*>( &getTarget()->getComponentType() ) )
            {
                if( distances.count( pMinStationType ) > 0 )
                {
                    distanceToTarget = distances[ pMinStationType ];

                    pathToTarget.clear();
                    list<const CEdgeConnector*>::const_iterator itEdgeConnector;
                    for( itEdgeConnector = pathsToTarget[ pMinStationType ]->begin(); itEdgeConnector != pathsToTarget[ pMinStationType ]->end(); itEdgeConnector++ )
                    {
                        pathToTarget.push_back( *itEdgeConnector );
                    }
                }
                else
                {
                    distanceToTarget = 0;
                }

                // Delete lists of paths
                map<const CStationType*, list<const CEdgeConnector*>*>::const_iterator itPathKeyValuePair;
                for( itPathKeyValuePair = pathsToTarget.begin(); itPathKeyValuePair != pathsToTarget.end(); itPathKeyValuePair++ )
                {
                    delete itPathKeyValuePair->second;
                }
                pathsToTarget.clear();

                return( distanceToTarget );
            }

            // If goal not found yet, put to closed list (will no longer be considered)
            closedList.push_back( pMinStationType );

            // Consider all neighbor nodes
            for( list<CEdgeConnector*>::const_iterator itEdgeConnector = pMinStationType->getEdgeConnectors().begin(); itEdgeConnector != pMinStationType->getEdgeConnectors().end(); itEdgeConnector++ )
            {
                // If connection is a place edge
                const CPlaceEdge* pPlaceEdge = dynamic_cast<const CPlaceEdge*>( &(*itEdgeConnector)->getEdge() );
                if( pPlaceEdge != nullptr )
                {
                    const CStationType* pNeighbor = static_cast<const CStationType*>( &(*itEdgeConnector)->getLinkedConnector().getComponentType() );

                    // If the current considered node with minimal value is not in the closed list
                    if( find( closedList.begin(), closedList.end(), pNeighbor ) == closedList.end() )
                    {
                        // Calculate actual distance and path to the neighbor
                        int distance = 0;
                        list<const CEdgeConnector*>* pPath = nullptr;
                        if( distances.count( pMinStationType ) > 0 )
                        {
                            distance = distances[ pMinStationType ];

                            // Copy the list here, since it is built from a previous one and otherwise
                            // all lists in pathsToTarget will point to the same list
                            pPath = new list<const CEdgeConnector*>( *pathsToTarget[ pMinStationType ] );
                        }
                        else
                        {
                            pPath = new list<const CEdgeConnector*>();
                        }
                        distance += pPlaceEdge->getValue();
                        if( (&(pPlaceEdge->getConnector1().getComponentType())) == pNeighbor )
                        {
                            pPath->push_back( &(pPlaceEdge->getConnector1()));
                        }
                        else
                        {
                            pPath->push_back( &(pPlaceEdge->getConnector2()));
                        }

                        // If node is not in open list or the new distance will be better than the old one
                        if( (openList.count( pNeighbor ) == 0) || (distance < distances[ pNeighbor ]) )
                        {
                            openList[ pNeighbor ] = distance;  // TODO: Add an estimated value here (without overestimation) for the A* heuristic (e.g. something like Manhattan or Euclidean distance in the graph?)
                            distances[ pNeighbor ] = distance;
                            pathsToTarget[ pNeighbor ] = pPath;
                        }
                    }
                }
            }
        }
        while( !openList.empty() );

        // Delete lists of paths
        map<const CStationType*, list<const CEdgeConnector*>*>::const_iterator itPathKeyValuePair;
        for( itPathKeyValuePair = pathsToTarget.begin(); itPathKeyValuePair != pathsToTarget.end(); itPathKeyValuePair++ )
        {
            delete itPathKeyValuePair->second;
        }
        pathsToTarget.clear();
    }

    // If reached to here no path could be found (or no target or previous target)
    distanceToTarget = NO_PATH;
    pathToTarget.clear();
    return( distanceToTarget );
}


const list<const CEdgeConnector*>& CAgent::getPathToTarget()
{
    // Call the distance calculation here, since it also calculates the path (same algorithm)
    getDistanceToTarget();

    return( pathToTarget );
}


bool CAgent::getIsVisiting() const
{
	return( isVisiting );
}


bool CAgent::getHasEnterEvent() const
{
	return( hasEntered );
}


int CAgent::getTime() const
{
	return( time );
}


int CAgent::getSizeOnCurrentStation()
{
	return( sizeOnCurrentStation );
}


bool CAgent::getIsActive()
{
	return( isActive );
}



//------------------------------------------------------------
// Setter
//------------------------------------------------------------

void CAgent::setReward( double reward )
{
	this->reward = reward;
}


void CAgent::setTarget( const CStation* target, double evaluation, int time )
{
	this->target = target;
    if( target == nullptr )
	{
		targetEvaluation = 0;
		reward = 0;
		actionSelectionTime = INVALID_TIME;
	}
	else
	{
		this->targetEvaluation = evaluation;
		this->actionSelectionTime = time;
	}
}


void CAgent::setPreviousTarget( const CStation* previousTarget )
{
	this->previousTarget = previousTarget;
}


void CAgent::setCoveredDistanceToTarget( int coveredDistanceToTarget )
{
	this->coveredDistanceToTarget = coveredDistanceToTarget;
}


void CAgent::setIsVisiting( bool isVisiting )
{
	this->isVisiting = isVisiting;
}


void CAgent::setHasEnterEvent( bool hasEntered )
{
	this->hasEntered = hasEntered;

	// If enter event happened 
	if( hasEntered )
	{
		// Increase the enter event count
		setEnterEventCount( getEnterEventCount() + 1 );

		//  If has necessity attribute
		if( getAgentType().getHasNecessity() )
		{
			// Increase the necessity count for its target station
			setNecessity( *getTarget(), getNecessity( *getTarget() ) + 1 );
		}
	}
}


void CAgent::setTime( int time )
{
	this->time = time;
}


void CAgent::setSizeOnCurrentStation( int sizeOnCurrentStation )
{
	this->sizeOnCurrentStation = sizeOnCurrentStation;
}


void CAgent::setIsActive( bool isActive )
{
	this->isActive = isActive;
}
