/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined Component_hpp
	#define Component_hpp





// #include "SimulationTypes.h"

#include ".//..//Constants.h"
#include "./../../Editor/ComponentType.h"  // Included from editor!
//class CComponentType;

#include <map>





// using namespace std;





/**
 * Objects of this class represent single simulated components that serve as data structures
 * for the simulation.
 */
class CComponent 
{
	public: 

		/**
		 * Creates a new component and initializes its (edge dependent) general attribute values.
		 * 
		 * @param componentType   the component's associated component type
		 */
		CComponent( CComponentType& componentType );

		/**
		 * Destroys the component.
		 */ 
		virtual ~CComponent();

		/** 
		 * Initializes the component's (edge dependent) general attribute values.
		 */
		void initialize();

		/** 
		 * Resets the component's values to start values at the beginning of a simulation.
		 * <p>
		 * ALL INITIALIZATION WORK OF VALUES HAS TO BE DONE HERE TO GUARANTEE THAT VALUES ARE 
		 * REINITIALIZED EXACLTY THE SAME WAY AS IN THE FIRST RUN WHEN SIMULATION RESTARTS.
		 */
		void reset();

		/** 
		 * Cleans up the component by clearing its containers (whose elements where created
		 * during simulation).
		 * <p>
		 * This method is called by simulation before a component is removed from simulation
		 * since it seems to make problems when an element is added to a container by 
		 * simulation DLL and this element is later removed by the editor DLL through the 
		 * component's destructor.
		 */
		void cleanUp();

		/** 
		 * Returns the component's number, identifying the component in the context of its component type. 
		 *
		 * @return   the number identifying the component in the context of its agent type
		 */
		virtual int getNumber() const = 0;

		/** 
		 * Returns the component type to which the component belongs.
		 *
		 * @return   the component's associated component type
		 */
		const CComponentType& getComponentType() const;

		/** 
		 * Returns whether the component is involved in an enter event (in the last calculated time step). 
		 *
		 * @return   true when the component is involved in an enter event, false otherwise
		 */
		virtual bool getHasEnterEvent() const = 0;

		/** 
		 * Returns whether the component already had an enter event during the simulation. 
		 *
		 * @return   true when the component already had an enter event during the simulation, false otherwise
		 */
		bool getHadEnterEventAlready() const;

		/** 
		 * Returns the time of the last enter event occuring on this component. 
		 *
		 * @return   the time of the last enter event occuring on this component
		 */
		int getLastEnterEventTime() const;

		/** 
		 * Returns the time of the enter event before the last one occuring on this component. 
		 *
		 * @return   the time of the enter event before the last one occuring on this component
		 */
		int getPreviousEnterEventTime() const;

		/** 
		 * Returns the number of enter events on this component. 
		 *
		 * @return   the number of enter events on this component
		 */
		int getEnterEventCount() const;

		/** 
		 * Returns the necessity counter for a given component that is connected by a visit-edge. 
		 *
		 * @param component   a component that is connected by a visit-edge
		 * @return            the current necessity counter for the given connected component
		 */
		int getNecessity( const CComponent& component ) const;

		/** 
		 * Returns if the necessity constraint of this component is fulfilled for all 
		 * components that are connected by visit-edges. 
		 *
		 * @return   true if the necessity is fulfilled for all connected components, false otherwise
		 */
		bool getIsNecessityFulfilled() const;

		/** 
		 * Returns whether this component already serves to fulfill a time constraint for another component.
		 *
		 * @return   true if this component already serves to fulfill a time constraint for another component
		 */
		bool getIsTimeEdgeConstraintFulfiller() const;

		/** 
		 * Returns the time unit, when this component became a time edge contraint fulfiller for another component.
		 *
		 * @return   the time unit, when this component became a time edge contraint fulfiller (invalid time is returned in case the component is not a time edge contraint fulfiller)
		 */
		int getTimeEdgeConstraintFulfillerTime() const;

		/** 
		 * Returns the time edge constraint fulfillers of this component.
		 *
		 * @return   the time edge constraint fulfillers of this component
		 */
		list<CComponent*>& getTimeEdgeConstraintFulfillers();

		/** 
		 * Sets the time of this component's last enter event. 
		 *
		 * @param time   the time when the last enter event occured on this component
		 */
		void setLastEnterEventTime( int time );

		/** 
		 * Sets the time of this component's enter event before the last one. 
		 *
		 * @param time   the time when the enter event before the last one occured on this component
		 */
		void setPreviousEnterEventTime( int time );

		/** 
		 * Sets the number of enter events on this component.
		 *
		 * @param cycle   the number of enter events on this component
		 */
		void setEnterEventCount( int count );

		/** 
		 * Sets the necessity counter for a given component that is connected by a visit-edge. 
		 *
		 * @param component   a component that is connected by a visit-edge
		 * @param necessity   the new necessity value to be set
		 */
		void setNecessity( const CComponent& component, int necessity );

		/** 
		 * Sets whether this component already serves to fulfill a time constraint for another component.
		 *
		 * @param isTimeEdgeContraintFulfiller   whether this component already serves to fulfill a time constraint for another component
		 * @param time                           the current time unit when the component becomes a fulfiller (default is invalid time in case "false" is given as first parameter)
		 */
		void setIsTimeEdgeConstraintFulfiller( bool isTimeEdgeContraintFulfiller, int time = INVALID_TIME );

		/** 
		 * Sets the time edge constraint fulfillers of this component.
		 *
		 * @param timeEdgeConstraintFulfillers   the time edge constraint fulfillers of this component
		 */
		void setTimeEdgeConstraintFulfillers( list<CComponent*> timeEdgeConstraintFulfillers );

		/**
		 * Initializes the necessity counters of the component.
		 *
		 * @param componentType   the connected component type for whose components the necessity counters should be initialized
		 */
		void initNecessities( const CComponentType& componentType );

	private:

		/** The component's associated component type. */
		CComponentType& componentType;

		/** 
		  * The current necessities for each connected component, representing how many visit 
		  * events have already taken place between the component and its connected components. 
		  */
		map<const CComponent*, int> necessities;

		/** The count of enter events this component was already involved in. */
		int enterEventCount;

		/** The time of the last enter event occuring on this component. */
		int lastEnterEventTime;

		/** The time of the previous enter event occuring on this component. */
		int previousEnterEventTime;

		/** Determines whether this component already serves to fulfill a time constraint for another component. */
		bool isTimeEdgeConstraintFulfiller;

		/** The time unit when this component became a time edge contraint fulfiller for another component. */
		int timeEdgeConstraintFulfillerTime;

		/** The time edge constraint fulfillers of this component. */
		list<CComponent*> timeEdgeConstraintFulfillers;
};





#endif
