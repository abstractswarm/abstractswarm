/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined Simulation_h
	#define Simulation_h





// Needed for platform dependent(!) visualization
#include "windows.h"

#include "SimulationTypes.h"
#include "./../Editor/Grid.h"
#include <list>
#include <string>
#include "./Calculation/ExternalJavaAgentInterface.hpp"





using namespace std;





class CSimulation 
{
	public: 

		/**
		 * Creates the simulation.
		 *
		 * @param sApplicationPath   the path where the executable file loading the simulation DLL is located
         * @param sAgentName         the name of the agent implementation folder to be loaded at the beginning
         * @param createConsole      whether or not the simulation should allocate its own console window (otherwise the console from which the application was started will be used)
         */
        CSimulation( string sApplicationPath, string sAgentName = "", bool createConsole = true );

		// Destruction
		virtual ~CSimulation();

		/**
		 * Sets the new mouse x and y position of the timetable.
		 *
		 * @param x   the new mouse x position
		 * @param y   the new mouse y position
		 */
		void mouseMove( int x, int y );

		/** 
		 * Sets the new scroll position in x direction.
		 *
		 * @param percentScrollX   the new scroll x position in percent
		 */
		void setScrollX( int percentScrollX );

		/** 
		 * Sets the new scroll position in y direction.
		 *
		 * @param percentScrollY   the new scroll y position in percent
		 */
		void setScrollY( int percentScrollY );
		
		/**
		 * Sets the new zoom factor and calculates the new scroll x/y positions depending on 
		 * the new zoom factor. Returns the new zoom factor.
		 *
		 * @param zoomFactor       the new zoom factor
		 * @param percentScrollX   the new scroll y position in percent
		 * @param percentScrollY   the new scroll y position in percent
		 * @return                 the new zoom factor
		 */
		double setZoom( double zoomFactor, int& percentScrollX, int& percentScrollY );

		/**
		 * Sets and reloads the agent file to be used for simulation. 
         * The java environment and the whole simulation will be reset.
		 *
		 * @param pFileName   the name of the agent file to be used for simulation
		 */
        void setAgentFile( const char* pFileName );

        /**
         * Reloads the agent java file by reinitializing the whole java environment.
         */
        void reloadAgentJavaFile();

        /**
		 * Sets the visible frame size according to the layout of the UI.
		 *
		 * @param width   the width of the visible area according to the layout of the UI
		 * @param width   the height of the visible area according to the layout of the UI
		 */
		void setVisibleFrameSize( int width, int height );

		/** 
		 * Returns the current simulation time step.
		 *
		 * @return the current simulation time step
		 */
		int getTimeStep() const;

		/** 
		 * Returns the agents contained in the simulation.
		 *
		 * @return the agents contained in the simulation
		 */
		const list<CAgent*>& getAgents() const;

		/** 
		 * Adds a station to the simulation. 
		 */
		void addStation( CStation* pStation );

		/** 
		 * Adds an agent to the simulation. 
		 */
		void addAgent( CAgent* pAgent );

		/** 
		 * Removes a station from the simulation. 
		 */
		void removeStation( CStation* pStation );

		/** 
		 * Removes an agent from the simulation. 
		 */
		void removeAgent( CAgent* pAgent );

		/**
		 * Calculates the next step of the simulation. Visualizes the current simulation state BEFORE the 
		 * leave events are calculated in case a graphic context is given.
		 *
		 * @param graphicContext   the graphic context where the simulation is visualized; NULL if no graphic context is given: no visualization is done in this case
		 */
		void simulateStep( HDC graphicContext = NULL );

		/**
		 * Returns if the simulation is currently finished.
		 *
		 * @return   true if the simulation is currently finished, false otherwise
		 */
		bool getIsFinished();

		/**
		 * Returns if the simulation is currently in a valid state, which means that all
		 * Frequency and Necessity attributes are fulfilled.
		 * <p>
		 * A timetable of a simulation that are finished but not valid can not be used, 
		 * even if it is better than previous ones. The function should be called only 
		 * after the simulation is finished.
		 *
		 * @return   true if the simulation is in a valid state, false otherwise
		 */
		bool getIsValid();

		/**
		 * Resets the simulation to a start state.
		 */
		void reset();

		/**
		 * Connects the simulation to the timetable module by storing the given function pointers.
		 * <p>
		 * Every time a component enters or leaves a station, a corresponding event is added to the 
		 * time table through the given function pointers.
		 * 
		 * @param fpAddAgentEnterEvent    pointer to the function that adds an agent enter event to the timetable
		 * @param TfpAddAgentLeaveEvent   pointer to the function that adds an agent leave event to the timetable
		 */
		void connectToTimetable( TfpAddAgentEnterEvent fpAddAgentEnterEvent, TfpAddAgentLeaveEvent fpAddAgentLeaveEvent );

        /**
         * @brief Flushes the console of the underlying process of the external agent interface to the AbstractSwarm console window.
         */
        void flushConsole();

		/** 
		 * Draws the simulation (platform dependent(!) interface for visualization).
		 * 
		 * @param graphicContext   the handle to the graphic device context where the simulation should be visualized
		 */
		void draw( HDC graphicContext );


	private:

		/** The currently known width of the visible area. */
		int visibleFrameWidth;

		/** The currently known height of the visible area. */
		int visibleFrameHeight;

		/** The current mouse x position. */
		int curMouseX;

		/** The current mouse y position. */
		int curMouseY;

		/** The current scroll position along the x axis. */
		int curScreenX;

		/** The current scroll position along the y axis. */
		int curScreenY;

		/** The current zoom factor. */
		double curZoomFactor;

		/** The simulation's grid to be visualized as background. */
		CGrid grid;

		/** The stations contained in the simulation. */
		list<CStation*> stations;

		/** The agents contained in the simulation. */
		list<CAgent*> agents;	

		/** The name of the agent file to be used for simulation. */
		string sAgentFile;

		/** The path where the executable file loading the simulation DLL is located. */
		string sApplicationPath;

		/** The global simulation time. */
		int timeStep;

		/** The function pointer to the function that adds an agent enter event to the timetable. */
		TfpAddAgentEnterEvent fpAddAgentEnterEvent;

		/** The function pointer to the function that adds an agent leave event to the timetable. */
		TfpAddAgentLeaveEvent fpAddAgentLeaveEvent;

        /** The process for external Java agent interface. */
        CExternalJavaAgentInterface externalJavaAgentInterface;

        /**
         * Initializes the Java interpreter and redirects its standard out to a string
		 * (since for some reasons the normal output is not written to the visible console).
		 */
        void initializeJava();

		/**
         * Maps all components of the simulation to corresponding Java objects
		 */
        void transferComponentsToJava();

		/**
         * Transfers all attributes of a station to the Java environment.
		 *
         * @param station           the station whose attributes should be transferred
         * @param ossJavaCode       the string stream to which the code to be transferred will be written
         */
        void transferAttributesToJava( const CStation& station, ostringstream& ossJavaCode );

		/**
         * Transfers all attributes of an agent to the Java environment.
		 *
         * @param agent         the agent whose attributes should be transferred
         * @param ossJavaCode   the string stream to which the code to be transferred will be written
         */
        void transferAttributesToJava( const CAgent& agent, ostringstream& ossJavaCode );

		/**
         * Calculates the local reward for the current time step and transfers it to Java (to all agents).
		 *
		 * @param agent   the agent to which the reward should be transferred
		 */
        void transferRewardToJava( const CAgent& agent );

		/**
		 * Returns if the simulation is currently finished for the given agent.
		 *
		 * @param pAgent   the agent to be checked
		 * @return         true if the simulation is currently finished for the given agent, false otherwise
		 */
		bool getIsFinished( CAgent* pAgent );

		/** 
		 * Transforms the given grid x position to the corresponding screen x position.
		 * <p>
		 * The input parameter is a double for more precise scrolling behavior (e.g. if clicked not exactly in the middle of a grid cell).
		 *
		 * @param x   the grid x position to be transformed
		 */
		int xToScreenX( double x ) const;

		/** 
		 * Transforms the given grid y position to the corresponding screen y position.
		 * <p>
		 * The input parameter is a double for more precise scrolling behavior (e.g. if clicked not exactly in the middle of a grid cell).
		 *
		 * @param y   the grid y position to be transformed
		 */
		int yToScreenY( double y ) const;

		/** 
		 * Transforms the given screen x position to the corresponding grid x position.
		 * <p>
		 * The return value is a double for more precise scrolling behavior (e.g. if the given position is not exactly in the middle of a grid cell).
		 *
		 * @param x   the screen x position to be transformed
		 */
		double screenXToX( int x ) const;

		/** 
		 * Transforms the given screen y position to the corresponding grid y position.
		 * <p>
		 * The return value is a double for more precise scrolling behavior (e.g. if the given position is not exactly in the middle of a grid cell).
		 *
		 * @param y   the screen y position to be transformed
		 */
		double screenYToY( int y ) const;
};





#endif
