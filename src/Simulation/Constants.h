/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined SimulationConstants_h
	#define SimulationConstants_h





//------------------------------------------------------------
// General
//------------------------------------------------------------


//#define USE_NATIVE_AGENT_INTERFACE
//#define ALLOW_FAST_FORWARDING  // This is an experimental feature that allows the simulation to fast-forward working and movement of agents if no important events occur meanwhile. If you experience any problems or weird simulation behavior: Comment out this line!
#define INVALID_TIME (-1)
#define NO_PATH (-1)


//------------------------------------------------------------
// Agent external interface (Java)
//------------------------------------------------------------

#define JAVA_AGENT_FILES_REL_PATH "Agents"
#define JAVA_AGENT_INTERFACE_FILE_NAME_NO_EXT "AbstractSwarmAgentInterface"
#define JAVA_AGENT_SERVER_FILE_NAME_NO_EXT "AbstractSwarmAgentInterfaceServer"
#define JAVA_AGENT_DEFAULT_NAME "AgentRandom"
#define CONSOLE_OUTPUT_NAME_EVALUATION_FUNCTION "EVALUATION"
#define CONSOLE_OUTPUT_NAME_COMMUNICATION_FUNCTION "COMMUNICATION"
#define CONSOLE_OUTPUT_NAME_REWARD_FUNCTION "REWARD"
#define MSG_ERROR_EXECUTING_EVALUATION_FUNCTION "ERROR WHILE EXCECUTING EVALUATION FUNCTION!"
#define MSG_ERROR_EXECUTING_COMMUNICATION_FUNCTION "ERROR WHILE EXCECUTING COMMUNICATION FUNCTION!"
#define MSG_ERROR_EXECUTING_REWARD_FUNCTION "ERROR WHILE EXCECUTING REWARD FUNCTION!"

#define EXTERNAL_AGENT_INTERFACE_DATA_TERMINATOR '$'
#define EXTERNAL_AGENT_INTERFACE_JAVA_POSITIVE_INFINITY "Infinity"
#define EXTERNAL_AGENT_INTERFACE_JAVA_NEGATIVE_INFINITY "-Infinity"
#define EXTERNAL_AGENT_INTERFACE_MSG_ERROR_NO_INIT "Java was not correctly initialized."
#define EXTERNAL_AGENT_INTERFACE_MSG_ERROR_EVALUATION_FUNCTION "Call to evaluation function failed; the Java external agent interface is not properly working."
#define EXTERNAL_AGENT_INTERFACE_MSG_ERROR_TIMEOUT "Timeout while waiting for Java external agent interface."
#define EXTERNAL_AGENT_INTERFACE_MSG_ERROR_LOADING_JAVA "Error loading Java. Is Java correctly installed? Path variable(s) set?"
#define EXTERNAL_AGENT_INTERFACE_JAVA_COMPILER_OUTPUT_ERROR_INDICATOR1 "error"
#define EXTERNAL_AGENT_INTERFACE_JAVA_COMPILER_OUTPUT_ERROR_INDICATOR2 "Error"
#define EXTERNAL_AGENT_INTERFACE_JAVA_COMPILER_OUTPUT_ERROR_INDICATOR3 "ERROR"


//------------------------------------------------------------
// Agent strategies
//------------------------------------------------------------

#define STRATEGY_RANDOM 0
#define STRATEGY_RULE_BASED 1
#define STRATEGY_JAVA 2

#define STR_POSITION_UNKNOWN "UNKNOWN"





#endif
