/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------

#include "./../../Editor/VisitEdge.h"
#include "./../../Editor/PlaceEdge.h"
#include "./../../Editor/TimeEdge.h"
#include "./../../Editor/ComponentType.h"
#include "./../Components/Agent.hpp" 
#include "./../Components/Station.hpp" 
#include <typeinfo>
//#pragma c-mol_hdrstop

//#include "CountVisitEdgeConnectedComponents.hmol"
#include "CountVisitEdgeConnectedComponents.hpp"





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MCountVisitEdgeConnectedComponents::<const CVisitEdge*>()
void MCountVisitEdgeConnectedComponents::handling( const CVisitEdge* that, METHOD_PARAMS_COUNTVISITEDGESCONNECTEDCOMPONENTS )
{
	// Determine opposite type
    const CComponentType* pOppositeType = nullptr;
	if( that->getConnector1().getIsConnected() && that->getConnector2().getIsConnected() )
	{
		pOppositeType = &(that->getConnector1().getComponentType());
		if( pOppositeType == (&componentType) )
			pOppositeType = &(that->getConnector2().getComponentType());
	}
	else
	{
		return;
	}

	// Add the number of components of opposite type to counter
	counter += pOppositeType->getNoOfComponents();

	// Add the number of components of opposite type that have a target in the given component type to counter for target station
	vector<CComponent*>::const_iterator itComponent;
	for( itComponent = pOppositeType->getComponents().begin(); itComponent != pOppositeType->getComponents().end(); itComponent++ )
	{
//		MCountVisitEdgeConnectedComponents( componentType, counter, counterTargetStation )<-itComponent();
        MCountVisitEdgeConnectedComponents::handling( *itComponent, componentType, counter, counterTargetStation );
    }
}


//void MCountVisitEdgeConnectedComponents::<const CPlaceEdge*, const CTimeEdge*, const CStation*>()
void MCountVisitEdgeConnectedComponents::handling( const CPlaceEdge* that, METHOD_PARAMS_COUNTVISITEDGESCONNECTEDCOMPONENTS )
{
}
void MCountVisitEdgeConnectedComponents::handling( const CTimeEdge* that, METHOD_PARAMS_COUNTVISITEDGESCONNECTEDCOMPONENTS )
{
}
void MCountVisitEdgeConnectedComponents::handling( const CStation* that, METHOD_PARAMS_COUNTVISITEDGESCONNECTEDCOMPONENTS )
{
}


//void MCountVisitEdgeConnectedComponents::<const CAgent*>()
void MCountVisitEdgeConnectedComponents::handling( const CAgent* that, METHOD_PARAMS_COUNTVISITEDGESCONNECTEDCOMPONENTS )
{
    if( (that->getTarget() != nullptr) && ((&(that->getTarget()->getStationType())) == &componentType) )
		counterTargetStation += 1;
}
