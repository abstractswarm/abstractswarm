/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------

#include "./../../Editor/VisitEdge.h"
#include "./../../Editor/PlaceEdge.h"
#include "./../../Editor/TimeEdge.h"
#include <typeinfo>
//#pragma c-mol_hdrstop

#include "CountVisitEdges.hpp"





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MCountVisitEdges::<const CVisitEdge*>()
void MCountVisitEdges::handling( const CVisitEdge* that, METHOD_PARAMS_COUNTVISITEDGES )
{
	counter++;
}


//void MCountVisitEdges::<const CPlaceEdge*, const CTimeEdge*>()
void MCountVisitEdges::handling( const CPlaceEdge* that, METHOD_PARAMS_COUNTVISITEDGES )
{
}
void MCountVisitEdges::handling( const CTimeEdge* that, METHOD_PARAMS_COUNTVISITEDGES )
{
}
