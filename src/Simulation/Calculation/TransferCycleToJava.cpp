/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------

#include "./../../Editor/VisitEdge.h"
#include "./../../Editor/PlaceEdge.h"
#include "./../../Editor/TimeEdge.h"
#include "./../Components/Component.hpp"
#include ".//..//Constants.h"
#include <sstream>
#include <typeinfo>
//#pragma c-mol_hdrstop

//#include "TransferCycleToJava.hmol"
#include "TransferCycleToJava.hpp"





using namespace std;





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MTransferCycleToJava::<const CComponent>()
void MTransferCycleToJava::handling( const CComponent& that, METHOD_PARAMS_TRANSFERCYCLETOTOJAVA )
{
    // Create emtpy hash map object
    ossJavaCode << pComponentName << "." << "cycles" << " = " << "new HashMap<TimeEdge, Integer>()" << ";\n";

	// Iterate over all edges connected to the component's type
	list<CEdgeConnector*>::const_iterator itEdgeConnector;
	for( itEdgeConnector = that.getComponentType().getEdgeConnectors().begin(); itEdgeConnector != that.getComponentType().getEdgeConnectors().end(); itEdgeConnector++ )
	{
		// Call the method for the edge
//        MTransferCycleToJava( ossJavaCode, pTypeName, pComponentName )°(const_cast<CEdge*>( &((*itEdgeConnector)->getEdge()) ))( that );
        MTransferCycleToJava::handling( const_cast<CEdge*>( &((*itEdgeConnector)->getEdge()) ), ossJavaCode, pTypeName, pComponentName, that );
    }
}


//void MTransferCycleToJava::<CVisitEdge*, CPlaceEdge*>( const CComponent& component )
void MTransferCycleToJava::handling( const CVisitEdge* that, METHOD_PARAMS_TRANSFERCYCLETOTOJAVA, const CComponent& component )
{
}
void MTransferCycleToJava::handling( const CPlaceEdge* that, METHOD_PARAMS_TRANSFERCYCLETOTOJAVA, const CComponent& component )
{
}


//void MTransferCycleToJava::<CTimeEdge*>( const CComponent& component )
void MTransferCycleToJava::handling( const CTimeEdge* that, METHOD_PARAMS_TRANSFERCYCLETOTOJAVA, const CComponent& component )
{
	// Determine whether the edge is incoming
	bool isIncoming = false;
	if(    (((&(that->getConnector1().getComponentType())) == (&(component.getComponentType()))) && that->getIsDirected1())
		|| (((&(that->getConnector2().getComponentType())) == (&(component.getComponentType()))) && that->getIsDirected2()) )
	{
		isIncoming = true;
	}

	// Determine the opposite component type and its name
    const CComponentType* pOppositeComponentType = nullptr;
	ostringstream ossOppositeComponentTypeName;
	if( (&(that->getConnector1().getComponentType())) == (&(component.getComponentType())) )
	{
		pOppositeComponentType = &(that->getConnector2().getComponentType());
		ossOppositeComponentTypeName << that->getConnector2().getComponentType().getName() << "_" << that->getConnector2().getComponentType().getId();
	}
	else
	{
		pOppositeComponentType = &(that->getConnector1().getComponentType());
		ossOppositeComponentTypeName << that->getConnector1().getComponentType().getName() << "_"  << that->getConnector1().getComponentType().getId();
	}

	// If incoming, add the cycle value
	if( isIncoming )
	{
		int remainingCycleValue = -1;
		if( pOppositeComponentType->getHasCycle() )
			remainingCycleValue = pOppositeComponentType->getCycle() - (component.getEnterEventCount() % pOppositeComponentType->getCycle());
        ossJavaCode << pComponentName << "." << "cycles.put( " << "edge" << "_" << "timeEdge" << "_" << pTypeName << "_" << "componentType_" << ossOppositeComponentTypeName.str() << ", " << remainingCycleValue << " );\n";
    }
}
