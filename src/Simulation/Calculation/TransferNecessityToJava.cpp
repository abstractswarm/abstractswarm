/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------

#include "./../../Editor/VisitEdge.h"
#include "./../../Editor/PlaceEdge.h"
#include "./../../Editor/TimeEdge.h"
#include "./../Components/Component.hpp"
#include ".//..//Constants.h"
#include <sstream>
#include <typeinfo>
//#pragma c-mol_hdrstop

//#include "TransferNecessityToJava.hmol"
#include "TransferNecessityToJava.hpp"





using namespace std;





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MTransferNecessityToJava::<const CComponent>()
void MTransferNecessityToJava::handling( const CComponent& that, METHOD_PARAMS_TRANSFERNECESSITYTOTOJAVA )
{
    //
    // Create emtpy hash map object
    //

    // First determine the type of the opposite components
    string typeOfOppositeComponents;
    if( string( pComponentName ).substr( 0, 5 ) == string( "agent" ) )
    {
        typeOfOppositeComponents = "Station";
    }
    else
    {
        typeOfOppositeComponents = "Agent";
    }

    ossJavaCode << pComponentName << "." << "necessities" << " = " << "new HashMap<" << typeOfOppositeComponents << ", Integer>()" << ";\n";


	// Iterate over all edges connected to the component's type
	list<CEdgeConnector*>::const_iterator itEdgeConnector;
	for( itEdgeConnector = that.getComponentType().getEdgeConnectors().begin(); itEdgeConnector != that.getComponentType().getEdgeConnectors().end(); itEdgeConnector++ )
	{
		// Call the method for the edge
//        MTransferNecessityToJava( ossJavaCode, pTypeName, pComponentName )°(const_cast<CEdge*>( &((*itEdgeConnector)->getEdge()) ))( that );
        MTransferNecessityToJava::handling( const_cast<CEdge*>( &((*itEdgeConnector)->getEdge()) ), ossJavaCode, pTypeName, pComponentName, that );
    }
}


//void MTransferNecessityToJava::<CVisitEdge*>( const CComponent& component )
void MTransferNecessityToJava::handling( const CVisitEdge* that, METHOD_PARAMS_TRANSFERNECESSITYTOTOJAVA, const CComponent& component )
{
    // First determine the type prefixes of the opposite component and component type
    string typePrefixOfOppositeComponent;
    string typePrefixOfOppositeComponentType;
    if( string( pComponentName ).substr( 0, 5 ) == string( "agent" ) )
    {
        typePrefixOfOppositeComponent = "station_";
        typePrefixOfOppositeComponentType = "stationType_";
    }
    else
    {
        typePrefixOfOppositeComponent = "agent_";
        typePrefixOfOppositeComponentType = "agentType_";
    }

	// Determine the opposite component type and its name
    const CComponentType* pOppositeComponentType = nullptr;
	ostringstream ossOppositeComponentTypeName;
	if( (&(that->getConnector1().getComponentType())) == (&(component.getComponentType())) )
	{
		pOppositeComponentType = &(that->getConnector2().getComponentType());
        ossOppositeComponentTypeName << typePrefixOfOppositeComponentType << that->getConnector2().getComponentType().getName() << "_" << that->getConnector2().getComponentType().getId();
	}
	else
	{
		pOppositeComponentType = &(that->getConnector1().getComponentType());
        ossOppositeComponentTypeName << typePrefixOfOppositeComponentType << that->getConnector1().getComponentType().getName() << "_"  << that->getConnector1().getComponentType().getId();
	}

	// Add the necessity values for all components
	vector<CComponent*>::const_iterator itOppositeComponent;
	for( itOppositeComponent = pOppositeComponentType->getComponents().begin(); itOppositeComponent != pOppositeComponentType->getComponents().end(); itOppositeComponent++ )
	{
		int remainingNecessityValue = -1;
		if( component.getComponentType().getHasNecessity() )
			remainingNecessityValue = component.getComponentType().getNecessity() - component.getNecessity( **itOppositeComponent );
        ossJavaCode << pComponentName << "." << "necessities.put( " << typePrefixOfOppositeComponent << ossOppositeComponentTypeName.str() << "_" << (*itOppositeComponent)->getNumber() << ", " << remainingNecessityValue << " );\n";
    }
}


//void MTransferNecessityToJava::<CPlaceEdge*, CTimeEdge*>( const CComponent& component )
void MTransferNecessityToJava::handling( const CPlaceEdge* that, METHOD_PARAMS_TRANSFERNECESSITYTOTOJAVA, const CComponent& component )
{
}
void MTransferNecessityToJava::handling( const CTimeEdge* that, METHOD_PARAMS_TRANSFERNECESSITYTOTOJAVA, const CComponent& component )
{
}
