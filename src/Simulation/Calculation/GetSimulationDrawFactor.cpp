/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "./../Constants.h"

#include "./../../Editor/StationType.h"
#include "./../../Editor/AgentType.h"
#include <typeinfo>
//#pragma c-mol_hdrstop

//#include "GetSimulationDrawFactor.hmol"
#include "GetSimulationDrawFactor.hpp"
//#include "CountVisitEdgeConnectedComponents.hmol"
#include "CountVisitEdgeConnectedComponents.hpp"
#include "./../Components/Agent.hpp"





using namespace std;





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MGetSimulationDrawFactor::<CStationType*>()
void MGetSimulationDrawFactor::handling( CStationType* that )
{
	// AN ALTERNATIVE APPROACH TO THIS FACTOR IS IMPLEMENTED IN REVISION 1058:
	// THERE, THE WORKLOAD OF THE STATIONS IS USED AS CRITERION. IN NEXT VERSIONS
	// MAY BE A USER BASED SELECTION FOR THE CRITERIONS OF VISUALIZATION WOULD BE 
	// A GOOD IDEA. BUT IN GENERAL THE CURRENT APPROACH SEEMS TO CONTAIN MOST OF 
	// THE INTERESTING INFORMATION, WHICH CANNOT BE SEEN DIRECTLY IN THE GANTT-CHART.
	// (AS IT IS THE CASE FOR THE ALTERNATIVE APPROACH WHICH SEEMS TO BE A BIT 
	// REDUNDANT TO WHAT CAN BE SEEN DIRECTLY IN THE GANTT-CHART.)

	// Count all agents connected by visit edges and count all agents 
	// connected by visit edges with target one of the stations
	int connectedAgents = 0;
	int connectedAgentsWithTargetStation = 0;
	list<CEdgeConnector*>::const_iterator itEdgeConnector;
	for( itEdgeConnector = that->getEdgeConnectors().begin(); itEdgeConnector != that->getEdgeConnectors().end(); itEdgeConnector++ )
	{
//		MCountVisitEdgeConnectedComponents( *that, connectedAgents, connectedAgentsWithTargetStation )°(const_cast<CEdge*>( &((*itEdgeConnector)->getEdge()) ))();
        MCountVisitEdgeConnectedComponents::handling( const_cast<CEdge*>( &((*itEdgeConnector)->getEdge()) ), *that, connectedAgents, connectedAgentsWithTargetStation );
    }

	// Set factor between [1.0..1.66] (1.0 means no agent has a station of that type as target and 1.66 means all possible agents a station of that type as target)
	if( connectedAgents > 0 )
		that->setDrawFactor( ((static_cast<double>( connectedAgentsWithTargetStation ) / connectedAgents) / 1.5) + 1 );
	else
		that->setDrawFactor( 1.0 );
}


//void MGetSimulationDrawFactor::<CAgentType*>()
void MGetSimulationDrawFactor::handling( CAgentType* that )
{
	// Get all agents of the component type
	int noOfAgents = that->getNoOfComponents();

	// Count all agents that are currently visiting a station
	int noOfVisitingAgents = 0;
	vector<CAgent*>::const_iterator itAgent;
	for( itAgent = that->getAgents().begin(); itAgent != that->getAgents().end(); itAgent++ )
	{
		if( (*itAgent)->getIsVisiting() )
			noOfVisitingAgents++;
	}

	// Set factor between [0.33..1.0] (0.33 means all agents are currently visiting and 1.0 means no agent is currently visiting)
	if( noOfAgents > 0 )
		that->setDrawFactor( 1.0 - ((static_cast<double>( noOfVisitingAgents ) / noOfAgents) / 1.5) );
	else
		that->setDrawFactor( 1.0 );
}
