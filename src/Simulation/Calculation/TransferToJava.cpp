/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------

#include "./../../Editor/VisitEdge.h"
#include "./../../Editor/PlaceEdge.h"
#include "./../../Editor/TimeEdge.h"
#include ".//..//Constants.h"
#include <sstream>
#include <typeinfo>
//#pragma c-mol_hdrstop

//#include "TransferToJava.hmol"
#include "TransferToJava.hpp"





using namespace std;





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MTransferToJava::<CVisitEdge*>()
void MTransferToJava::handling( const CVisitEdge* that, METHOD_PARAMS_TRANSFERTOJAVA )
{
    // Write connection
    ossJavaCode << "edge" << "_" << "visitEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "connectedType" << " = " << pOppositeTypeName << ";\n";

	// Add the type
    ossJavaCode << "edge" << "_" << "visitEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "type" << " = " << "\"VISIT_EDGE\"" << ";\n";

	// Set visit edge specific attributes
	if( that->getIsBold() )
        ossJavaCode << "edge" << "_" << "visitEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "bold" << " = " << "true" << ";\n";
	else
        ossJavaCode << "edge" << "_" << "visitEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "bold" << " = " << "false" << ";\n";

	// Add edge to corresponding edge tuple
    ossJavaCode << pTypeName << "." << "visitEdges.add( " << "edge" << "_" << "visitEdge" << "_" << pTypeName << "_" << pOppositeTypeName << " )" << ";\n";
}


//void MTransferToJava::<CPlaceEdge*>()
void MTransferToJava::handling( const CPlaceEdge* that, METHOD_PARAMS_TRANSFERTOJAVA )
{
    // Write connection
    ossJavaCode << "edge" << "_" << "placeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "connectedType" << " = " << pOppositeTypeName << ";\n";

    // Add the type
    ossJavaCode << "edge" << "_" << "placeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "type" << " = " << "\"PLACE_EDGE\"" << ";\n";

	// Set place edge specific attributes
	if( isIncoming )
        ossJavaCode << "edge" << "_" << "placeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "incoming" << " = " << "true" << ";\n";
	else
        ossJavaCode << "edge" << "_" << "placeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "incoming" << " = " << "false" << ";\n";
	if( isOutgoing )
        ossJavaCode << "edge" << "_" << "placeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "outgoing" << " = " << "true" << ";\n";
	else
        ossJavaCode << "edge" << "_" << "placeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "outgoing" << " = " << "false" << ";\n";

    // Set weight
    ossJavaCode << "edge" << "_" << "placeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "weight" << " = " << that->getValue() << ";\n";

    // Add edge to corresponding edge list
    ossJavaCode << pTypeName << "." << "placeEdges.add( " << "edge" << "_" << "placeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << " )" << ";\n";
}


//void MTransferToJava::<CTimeEdge*>()
void MTransferToJava::handling( const CTimeEdge* that, METHOD_PARAMS_TRANSFERTOJAVA )
{
    // Write connection
    ossJavaCode << "edge" << "_" << "timeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "connectedType" << " = " << pOppositeTypeName << ";\n";

    // Add the type
    ossJavaCode << "edge" << "_" << "timeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "type" << " = " << "\"TIME_EDGE\"" << ";\n";

	// Set time edge specific attributes
	if( isIncoming )
        ossJavaCode << "edge" << "_" << "timeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "incoming" << " = " << "true" << ";\n";
	else
        ossJavaCode << "edge" << "_" << "timeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "incoming" << " = " << "false" << ";\n";
	if( isOutgoing )
        ossJavaCode << "edge" << "_" << "timeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "outgoing" << " = " << "true" << ";\n";
	else
        ossJavaCode << "edge" << "_" << "timeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "outgoing" << " = " << "false" << ";\n";
	if( isAndOrigin )
        ossJavaCode << "edge" << "_" << "timeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "andOrigin" << " = " << "true" << ";\n";
	else
        ossJavaCode << "edge" << "_" << "timeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "andOrigin" << " = " << "false" << ";\n";
	if( isAndConnected )
        ossJavaCode << "edge" << "_" << "timeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "andConnected" << " = " << "true" << ";\n";
	else
        ossJavaCode << "edge" << "_" << "timeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "andConnected" << " = " << "false" << ";\n";

    // Set weight
    ossJavaCode << "edge" << "_" << "timeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << "." << "weight" << " = " << that->getValue() << ";\n";

    // Add edge to corresponding edge list
    ossJavaCode << pTypeName << "." << "timeEdges.add( " << "edge" << "_" << "timeEdge" << "_" << pTypeName << "_" << pOppositeTypeName << " )" << ";\n";
}
