/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined TransferToJava_hmol
    #define TransferToJava_hmol





#include <typeinfo>
#include "./../../Editor/VisitEdge.h"  // Included from editor!
#include "./../../Editor/PlaceEdge.h"  // Included from editor!
#include "./../../Editor/TimeEdge.h"  // Included from editor!
#include <iosfwd>





using namespace std;





/**
 * Creates a Java object as initialization code from the input object and adds it to the
 * edges tuple of the corresponding type.
 *
 * @param ossJavaCode         the string stream to which the Java code is written
 * @param pTypeName           the name of the component type to which the edge is connected
 * @param pOppositeTypeName   the name of the opposite component type to which the edge is connected
 * @param isIncoming          whether or not the edge is incoming
 * @param isOutgoing          whether or not the edge is outgoing
 * @param isAndOrigin         whether or not the edge has an and relation at its origin type
 * @param isAndConnected      whether or not the edge has an and relation at its connected type
 */
//method MTransferToJava( ostringstream& ossJavaCode, const char* pTypeName, const char* pOppositeTypeName, bool isIncoming, bool isOutgoing, bool isAndOrigin, bool isAndConnected )
class MTransferToJava
{
#define METHOD_PARAMS_TRANSFERTOJAVA ostringstream& ossJavaCode, const char* pTypeName, const char* pOppositeTypeName, bool isIncoming, bool isOutgoing, bool isAndOrigin, bool isAndConnected

public:

	/**
     * Creates a Java object as initialization code for visit edges.
	 */
//	virtual void <CVisitEdge*>();
    static void handling( const CVisitEdge* that, METHOD_PARAMS_TRANSFERTOJAVA );

	/**
     * Creates a Java object as initialization code for place edges.
	 */
//	virtual void <CPlaceEdge*>();
    static void handling( const CPlaceEdge* that, METHOD_PARAMS_TRANSFERTOJAVA );

	/**
     * Creates a Java object as initialization code for time edges.
	 */
//	virtual void <CTimeEdge*>();
    static void handling( const CTimeEdge* that, METHOD_PARAMS_TRANSFERTOJAVA );


    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline void handling( void* vp_that, METHOD_PARAMS_TRANSFERTOJAVA )
    {
        if( typeid( *(static_cast<const CVisitEdge*>( vp_that ))) == typeid( const CVisitEdge ) )
            handling( static_cast<const CVisitEdge*>( vp_that ), ossJavaCode, pTypeName, pOppositeTypeName, isIncoming, isOutgoing, isAndOrigin, isAndConnected );
        else if( typeid( *(static_cast<const CPlaceEdge*>( vp_that ))) == typeid( const CPlaceEdge ) )
            handling( static_cast<const CPlaceEdge*>( vp_that ), ossJavaCode, pTypeName, pOppositeTypeName, isIncoming, isOutgoing, isAndOrigin, isAndConnected );
        else if( typeid( *(static_cast<const CTimeEdge*>( vp_that ))) == typeid( const CTimeEdge ) )
            handling( static_cast<const CTimeEdge*>( vp_that ), ossJavaCode, pTypeName, pOppositeTypeName, isIncoming, isOutgoing, isAndOrigin, isAndConnected );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }
};





#endif
