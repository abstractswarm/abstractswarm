/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined ExternalJavaAgentInterface_hpp
    #define ExternalJavaAgentInterface_hpp

#include <QProcess>
#include <string>
#include "ProcessCommunicationAdapter.hpp"





using namespace std;





/**
 * @brief This class implements an adapter to communicate with Java (needed for implementing Agents in Java).
 */
class CExternalJavaAgentInterface
{
    public:

        /**
         * @brief Creates and starts the external Java agent interface.
         */
        CExternalJavaAgentInterface();

        /**
         * @brief Stops and destroys the external Java agent interface.
         */
        ~CExternalJavaAgentInterface();

        /**
         * @brief Sends a hello message to the external Java agent interface which returns the anwer.
         *
         * @return the answer to the hello message.
         */
        string hello();

        /**
         * @brief Sends a print command to the external Java agent interface which prints the text to standard out.
         *
         * @param text  the text to be printed to the standard out
         */
        void print( const string& text );

        /**
         * @brief Sends an update command to the external Java agent interface which updates all components according to what is contained in the provided data (in Java syntax).
         *
         * @param data  the components to be updated (in Java syntax)
         * @return the answer to the update command (which is a simple confirmation)
         */
        string update( const string& data );

        /**
         * @brief Sends an evaluation command to the external Java agent interface which calls the evaluation function for a given agent, time and station and returns the result.
         *
         * @param parameters  the parameters to be transferred to the interface, separated by blanks: agent (the agent doing the evaluation), time (the current simulation time unit) and station (the station to be evaluated)
         * @return the evaluation result
         */
        double evaluation( const string& parameters );

        /**
         * @brief Sends a communication command to the external Java agent interface which calls the communication method for a given agent, time, target station name, and target station evaluation (the latter three form the default data available to be returned in the communication function).
         *
         * @param parameters  the parameters to be transferred to the interface, separated by blanks: agent (the communicating agent), time (the current simulation time unit), target station name (the the name of the agent's target station) and target station evaluation (the agent's corresponding evaluation for the target station)
         */
        void communication( const string& parameters );

        /**
         * @brief Sends a reward command to the external Java agent interface which calls the reward method for a given agent, time and reward.
         *
         * @param parameters  the parameters to be transferred to the interface, separated by blanks: agent (the agent receiving the reward), time (the current simulation time unit) and reward (the reward to be passed to the agent)
         */
        void reward( const string& parameters );

        /**
         * @brief Sends a stop command to the external Java agent interface which stops the interface.
         *
         * @return the goodbye message
         */
        string stop();

        /**
         * @brief Initializes the external Java agent interface by compiling the agent server and file and establishing the connection (if possible).
         *
         * @param sAgentName  the name of the agent implementation used by the interface
         */
        void initialize( const string& sAgentName );

        /**
         * @brief Returns whether the external Java agent interface was properly initialized.
         *
         * @return whether the external Java agent interface was properly initialized
         */
        bool isInitialized();

        /**
         * @brief Returns whether the external Java agent interface is currently waiting for a response of the interface server.
         *        (Since this requeires internally the event loop to be processed, this method should be used in all events, e. g. the simulation timer,
         *        that rely on the waiting to be "blocking" to prevent such events from being executed by the manualy internal calls to the event loop.)
         *
         * @return
         */
        bool isWaiting();

        /**
         * @brief Updates the console with the text from standard out.
         */
        void flushConsole();


    private:

        /**
         * @brief The adapter realizing the communication with the external Java agent interface child process.
         */
        CProcessCommunicationAdapter* pProcessCommunicationAdapter;

        /** Counts the number of instances. */
        static int instanceCounter;

        /** The process for external Java agent interface. */
        QProcess* pJavaProcess;

        /** The name of the current agent implementation used by the agent interface. */
        string sAgentName;

        /**
         * @brief Waits until the external Java agent interface processed a command.
         *
         * @return the answer by the externel agent interface
         */
        string waitForResponse( int timeOut = -1 );

        /**
         * @brief Compiles the Java agent interface (should not be called when the interface is running).
         *
         * @return the output of the compilation (standard out and standard error) or an error indicating string in case of Java compiler sould not be loaded
         */
        string compile();

        /**
         * @brief Removes all class files in the folder of the external Java agent interface.
         */
        void cleanUp();
};





#endif

