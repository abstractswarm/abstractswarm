/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include ".//..//Components//Agent.hpp"
#include ".//..//Components//Station.hpp"
//#pragma c-mol_hdrstop

#include "Simulate.hpp"
#include "CheckPlaceEdgeConstraint.hpp"





//void MSimulate::<CStation*>()
void MSimulate::handling( CStation* that, METHOD_PARAMS_SIMULATE )
{
}


//void MSimulate::<CAgent*>()
void MSimulate::handling( CAgent* that, METHOD_PARAMS_SIMULATE )
{
	// Mark the agent as being inactive (to determine if there is agent activity during simulation - important for break condition)
	that->setIsActive( false );

	// Reset agent's flag for entering a station
	that->setHasEnterEvent( false );

	// If agent is not visiting a station and frequency or necessity constraint of the agent is fulfilled, agent is no more activ => return!
	if( (!that->getIsVisiting()) &&    ((that->getAgentType().getHasFrequency() && (that->getEnterEventCount() >= that->getAgentType().getFrequency()))
		                            || that->getIsNecessityFulfilled()) )
	{
		return;
	}

	// MOVE
	// If agent is not visiting a station and has a target and its target station is not reached yet
    if( (!that->getIsVisiting()) && (that->getTarget() != nullptr) && (that->getDistanceToTarget() != NO_PATH) && (that->getCoveredDistanceToTarget() < that->getDistanceToTarget()) )
	{
		// Move the agent to its target (according to its speed)
		that->setCoveredDistanceToTarget( that->getCoveredDistanceToTarget() + that->getAgentType().getSpeed() );
		
		// Mark the agent as being active (important for break condition)
		that->setIsActive( true );
	}
	else
	{
		// ENTER
		// If agent reached its target and all contraints are fulfilled (target station not full
		// and all time-edge dependencies fulfilled)
        if( (!that->getIsVisiting()) && (that->getTarget() != nullptr) && (that->getDistanceToTarget() != NO_PATH) && (that->getCoveredDistanceToTarget() >= that->getDistanceToTarget())
            /* directed place edge constraints satisfied? */
            && checkPlaceEdgeContraints( *that )
            /* target station not full: station type has no space attribute OR agent type has no size attribute and station has some space left OR agent type has size attribute and station has enough space left for the agent's size */
			&& ((!that->getTarget()->getStationType().getHasSpace()) || ((!that->getAgentType().getHasSize()) && (that->getTarget()->getSpace() < that->getTarget()->getStationType().getSpace())) || (that->getAgentType().getHasSize() && (that->getTarget()->getSpace() + that->getAgentType().getSize() <= that->getTarget()->getStationType().getSpace())) ) 
			/* target station's frequency not fulfilled (this should normally not happen, since a new target will be chosen before in MChooseAction!) */
			&& ((!that->getTarget()->getStationType().getHasFrequency()) || (that->getTarget()->getEnterEventCount() < that->getTarget()->getStationType().getFrequency()))
			/*&& (CHECK FOR TIME-EDGE-DEPENDENCIES HERE!) */ )
		{
			// Enter station
			const_cast<CStation*>( that->getTarget() )->addAgent( *that );
			that->setHasEnterEvent( true );
			that->setIsVisiting( true );
			
			// Store enter event time stamps for agent and station
			that->setPreviousEnterEventTime( that->getLastEnterEventTime() );
			that->setLastEnterEventTime( timeStep );
			const_cast<CStation*>( that->getTarget() )->setPreviousEnterEventTime( that->getTarget()->getLastEnterEventTime() );
			const_cast<CStation*>( that->getTarget() )->setLastEnterEventTime( timeStep );
		}

		// WORK
		// If agent currently visits a station 
		if( that->getIsVisiting() )
		{
			// Progress its visit time
			that->setTime( that->getTime() + 1 );

			// Mark the agent as being active (important for break condition) 
			// if the agent did not enter the same simulation step (since in this case the agent
			// would be active anyway if the enter event will not be reset by time edge constraint
			// algorithm)
			if( !that->getHasEnterEvent() )
				that->setIsActive( true );
		}
	}
}



//------------------------------------------------------------
// Helper
//------------------------------------------------------------

bool checkPlaceEdgeContraints( const CAgent& agent )
{
    // Iterate over all edges and check if the contraints are satisfied
    bool isPlaceEdgeContraintSatisfied = true;
    list<CEdgeConnector*>::const_iterator itEdgeConnector;
    for( itEdgeConnector = agent.getTarget()->getComponentType().getEdgeConnectors().begin(); itEdgeConnector != agent.getTarget()->getComponentType().getEdgeConnectors().end(); itEdgeConnector++ )
    {
        // Check whether it is a place edge and (in case it is) whether there is a station on the opposite connector
        // that was visited immidiately before by the given agent
        bool isPlaceEdge = false;
        bool isPlaceEdgeContraintOk = MCheckPlaceEdgeConstraint::handling( const_cast<CEdge*>( &(*itEdgeConnector)->getEdge() ), agent, isPlaceEdge );

        // If there is a place edge and it is directed to the agent's target station
        // and the place edge constraint is okay (i.e., there is a station on the opposite connector that was visited
        // immidiately before), leave
        if( isPlaceEdge && (*itEdgeConnector)->getIsDirected() && isPlaceEdgeContraintOk )
        {
            return( true );
        }

        // ...else if the contraint is not okay (i.e., no station on the opposite connector was visited
        // immidiately before), place edge represents a contraint and is not satisfied yet!
        else if( isPlaceEdge && (*itEdgeConnector)->getIsDirected() )
        {
            isPlaceEdgeContraintSatisfied = false;
        }
    }

    return( isPlaceEdgeContraintSatisfied );
}
