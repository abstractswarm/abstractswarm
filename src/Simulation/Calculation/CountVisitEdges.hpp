/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined CountVisitEdges_hmol
	#define CountVisitEdges_hmol


#include <typeinfo>
#include "./../../Editor/VisitEdge.h"  // Included from editor!
#include "./../../Editor/PlaceEdge.h"  // Included from editor!
#include "./../../Editor/TimeEdge.h"  // Included from editor!





/**
 * The method increases the given counter by 1 for a visit edge that is passed in.
 */
//method MCountVisitEdges( int& counter )
class MCountVisitEdges
{
#define METHOD_PARAMS_COUNTVISITEDGES int& counter

public:

	/**
	 * Increases the given counter by 1.
	 */
//	virtual void <const CVisitEdge*>();
    static void handling( const CVisitEdge* that, METHOD_PARAMS_COUNTVISITEDGES );

	/**
	 * Does not increase the counter (only visit edges are counted).
	 */
//	virtual void <const CPlaceEdge*, const CTimeEdge*>();
    static void handling( const CPlaceEdge* that, METHOD_PARAMS_COUNTVISITEDGES );
    static void handling( const CTimeEdge* that, METHOD_PARAMS_COUNTVISITEDGES );


    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline void handling( void* vp_that, METHOD_PARAMS_COUNTVISITEDGES )
    {
        if( typeid( *(static_cast<const CVisitEdge*>( vp_that ))) == typeid( const CVisitEdge ) )
            handling( static_cast<const CVisitEdge*>( vp_that ), counter );
        else if( typeid( *(static_cast<const CPlaceEdge*>( vp_that ))) == typeid( const CPlaceEdge ) )
            handling( static_cast<const CPlaceEdge*>( vp_that ), counter );
        else if( typeid( *(static_cast<const CTimeEdge*>( vp_that ))) == typeid( const CTimeEdge ) )
            handling( static_cast<const CTimeEdge*>( vp_that ), counter );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }
};





#endif
