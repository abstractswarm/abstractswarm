/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "./../Constants.h"
#include "./../Utilities.h"
#include "./../Components/Agent.hpp"
#include "./../Components/Station.hpp"
#include "./../../Editor/StationType.h"  // Included from editor!
#include <sstream>
#include <iostream>
#include <algorithm>
//#pragma c-mol_hdrstop

#include "ChooseAction.hpp"
#include "CountVisitEdges.hpp"
#include "ExternalJavaAgentInterface.hpp"





// Forward declarations for helper functions
double strategyRandom( const CAgent* that, const CStation* pStation, const vector<const CStation*>& possibleTargets );
double strategyRuleBased( const CAgent* that, const CStation* pStation, int time, const string& sPosition, vector<const CStation*>& possibleTargets );
double strategyJava( const CAgent* that, const CStation* pStation, int time, const string& sPosition, vector<const CStation*>& possibleTargets, CExternalJavaAgentInterface* pExternalJavaAgentInterface );





//void MChooseAction::<CStation*>()
void MChooseAction::handling( CStation* that, METHOD_PARAMS_CHOOSEACTION )
{
}


//void MChooseAction::<CAgent*>()
void MChooseAction::handling( CAgent* that, METHOD_PARAMS_CHOOSEACTION )
{
	// If the agent is not visiting a station and currently has a target station whose frequency is already fulfilled
	// (this case can occur for example if several agents decide to take the same station and must wait to
	// enter the station; then in the next simulation step the calculation order is changed and another agent
	// takes the station and fulfilles its frequency => the other agents have targets with fulfilled frequency)
    if( (!that->getIsVisiting()) && (that->getTarget() != nullptr) && (that->getTarget()->getStationType().getHasFrequency()) && (that->getTarget()->getEnterEventCount() >= that->getTarget()->getStationType().getFrequency()) )
	{
		// Reset it to null to let the agent choose another target
        that->setTarget( nullptr, 0, INVALID_TIME );
	}

	// If the agent has no target
    if( that->getTarget() == nullptr )
	{
		// Position as station name
		string sPosition = STR_POSITION_UNKNOWN;
        if( that->getPreviousTarget() != nullptr )
		{
			ostringstream ossStationNumber;
			ossStationNumber << that->getPreviousTarget()->getNumber();
			sPosition = that->getPreviousTarget()->getStationType().getName() + "." + ossStationNumber.str();
		}


		//
		// Possible target stations as vector
		//

		vector<const CStation*> possibleTargets;
		possibleTargets.clear();

		// Iterate over all connected station types
		list<CEdgeConnector*>::const_iterator itEdgeConnector;
		for( itEdgeConnector = that->getComponentType().getEdgeConnectors().begin(); itEdgeConnector != that->getComponentType().getEdgeConnectors().end(); itEdgeConnector++ )
		{
			// Count the visit edges
			int visitEdgeNumber = 0;
//			MCountVisitEdges( visitEdgeNumber )°(const_cast<CEdge*>(&((*itEdgeConnector)->getEdge())))();
            MCountVisitEdges::handling( const_cast<CEdge*>(&((*itEdgeConnector)->getEdge())), visitEdgeNumber );

			// If the the number of visit edges was increased
			if( visitEdgeNumber > 0 )
			{
				// Iterate over all stations
				const vector<CStation*>* stations = &(static_cast<const CStationType*>( &((*itEdgeConnector)->getLinkedConnector().getComponentType()) )->getStations());
				vector<CStation*>::const_iterator itStation;
				for( itStation = stations->begin(); itStation != stations->end(); itStation++ )
				{
					// If frequency of the station and necessity of the station for 
					// the visiting agent (or vice versa) are not fulfilled yet
					const CStation* targetStation = *itStation;
					if(    (!targetStation->getStationType().getHasFrequency() || (targetStation->getEnterEventCount() < targetStation->getStationType().getFrequency())) 
						&& ((!targetStation->getStationType().getHasNecessity()) || (targetStation->getNecessity( *that ) < targetStation->getStationType().getNecessity())) 
						&& ((!that->getAgentType().getHasNecessity()) || (that->getNecessity( *targetStation ) < that->getAgentType().getNecessity())) )
					{
						// Store the station as possible target
						possibleTargets.push_back( targetStation );
					}
				}
			}
		}

		// Do random permutation of the possible target stations
		random_shuffle( possibleTargets.begin(), possibleTargets.end() );

		// Do evaluation function call for all possible target stations
		list<pair<const CStation*, double> > evaluations;
		vector<const CStation*>::const_iterator itStation;
		for( itStation = possibleTargets.begin(); itStation !=  possibleTargets.end(); itStation++ )
		{
			// Get the agent's evaluation result depending on the given strategy
			double result = 0; 
			if( strategy == STRATEGY_RANDOM )
				result = strategyRandom( that, (*itStation), possibleTargets );
			else if( strategy == STRATEGY_RULE_BASED )
				result = strategyRuleBased( that, (*itStation), time, sPosition, possibleTargets );
            else if( strategy == STRATEGY_JAVA )
                result = strategyJava( that, (*itStation), time, sPosition, possibleTargets, pExternalJavaAgentInterface );

			// Add the result to objectives
			// (iterate over map and find the right position to insert new pair)
			list<pair<const CStation*, double> >::iterator itEvaluation;
			for( itEvaluation = evaluations.begin(); itEvaluation != evaluations.end(); itEvaluation++ )
			{
				if( itEvaluation->second < result )
					break;
			}
			evaluations.insert( itEvaluation, pair<const CStation*, double>( *itStation, result ) );
		}

		// Get the station with the best objective
        const CStation* selectedStation = nullptr;
		double evaluation = 0;
		if( !evaluations.empty() )
		{
			selectedStation = evaluations.front().first;
			evaluation = evaluations.front().second;
		}

		// Set the choosen station as target
		that->setTarget( selectedStation, evaluation, time );

		// Reset the covered distance to the target
		that->setCoveredDistanceToTarget( 0 );
	}
}



//------------------------------------------------------------
// Helper
//------------------------------------------------------------

double strategyRandom( const CAgent* pAgent, const CStation* pStation, const vector<const CStation*>& possibleTargets )
{
	// Randomly select a station from the given possible target stations
	int randomEvaluation = randomNumber( 0, 99 );
	return( randomEvaluation );
}


double strategyRuleBased( const CAgent* pAgent, const CStation* pStation, int time, const string& sPosition, vector<const CStation*>& possibleTargets )
{
	// Simple rule based implementation: select the station with most free space
	int freeSpace = pStation->getStationType().getSpace() - pStation->getSpace();
	return( freeSpace );
}


double strategyJava( const CAgent* pAgent, const CStation* pStation, int time, const string& sPosition, vector<const CStation*>& possibleTargets, CExternalJavaAgentInterface* pExternalJavaAgentInterface )
{
    //
    // JAVA INTERFACE
    //

    //
    // Do evaluation function call
    //
	
    // Create agent type name
    ostringstream ossAgentTypeName;
    ossAgentTypeName << "agentType_" << pAgent->getAgentType().getName() << "_" << pAgent->getAgentType().getId();
    const string& sAgentTypeName = ossAgentTypeName.str();

    // Create agent name
    ostringstream ossAgentName;
    ossAgentName << "agent_" << sAgentTypeName << "_" << pAgent->getNumber();
    const string& sAgentName = ossAgentName.str();
	
    // Create station type name
    ostringstream ossStationTypeName;
    ossStationTypeName << "stationType_" << pStation->getStationType().getName() << "_" << pStation->getStationType().getId();
    const string& sStationTypeName = ossStationTypeName.str();

    // Create station name
    ostringstream ossStationName;
    ossStationName << "station_" << sStationTypeName << "_" << pStation->getNumber();
    const string& sStationName = ossStationName.str();

    // Build the Java call code string
    ostringstream ossObjectiveFunctionCallParameters;
    ossObjectiveFunctionCallParameters << sAgentName << " " << time << " " << sStationName;
	
    // Run the evaluation method of the external java agent interface
    double result = pExternalJavaAgentInterface->evaluation( ossObjectiveFunctionCallParameters.str() );

    return( result );
}
