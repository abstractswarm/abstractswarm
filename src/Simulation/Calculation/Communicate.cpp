/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "./../Constants.h"
#include "./../Components/Agent.hpp"
#include "./../Components/Station.hpp"
#include <sstream>
#include <iostream>
//#pragma c-mol_hdrstop

#include "Communicate.hpp"
#include "ExternalJavaAgentInterface.hpp"



using namespace std;



//void MCommunicate::<CStation*>()
void MCommunicate::handling( CStation* that, METHOD_PARAMS_COMMUNICATE )
{
}


//void MCommunicate::<CAgent*>()
void MCommunicate::handling( CAgent* that, METHOD_PARAMS_COMMUNICATE )
{
#if !defined USE_NATIVE_AGENT_INTERFACE

    if( that->getTarget() != nullptr )
	{
		//
        // JAVA INTERFACE
		//

		//
		// Do communication function call
		//
		
		// Create agent type name
		ostringstream ossAgentTypeName; 
        ossAgentTypeName << "agentType_" << that->getAgentType().getName() << "_" << that->getAgentType().getId();
        const string& sAgentTypeName = ossAgentTypeName.str();

		// Create agent name
		ostringstream ossAgentName; 
        ossAgentName << "agent_" << sAgentTypeName << "_" << that->getNumber();
        const string& sAgentName = ossAgentName.str();

		// Create target station type name
		ostringstream ossTargetStationTypeName; 
        ossTargetStationTypeName << "stationType_" << that->getTarget()->getStationType().getName() << "_" << that->getTarget()->getStationType().getId();
        const string& sTargetStationTypeName = ossTargetStationTypeName.str();

		// Create target station name
		ostringstream ossTargetStationName; 
        ossTargetStationName << "station_" << sTargetStationTypeName << "_" << that->getTarget()->getNumber();
        const string& sTargetStationName = ossTargetStationName.str();

        // Create time when target station will be reached
        long timeWhenTargetStationWillBeReached = -1;
        if( that->getDistanceToTarget() != NO_PATH )
        {
            timeWhenTargetStationWillBeReached = timeStep + (that->getDistanceToTarget() - that->getCoveredDistanceToTarget());
        }

		// Create the corresponding parameters for the method call
        ostringstream ossCommunicationMethodCallParameters;
        ossCommunicationMethodCallParameters << sAgentName << " " << timeStep << " " << sTargetStationName << " " << timeWhenTargetStationWillBeReached << " ";
            // THE DEFAULT COMMUNICATION DATA CONSISTS OF sTargetStationName (the station object, not its name), timeWhenTargetStationWillBeReached (-1 for "never" if no path can be found) AND that->getTargetEvaluation() (SEE COMMAND COMMUNICATION IN EXETERNAL JAVA AGENT INTERFACE SERVER)
        // Convert positive/negative infinity correctly to a string representation that can be handled by Java
        if( that->getTargetEvaluation() == std::numeric_limits<double>::infinity() )
        {
            ossCommunicationMethodCallParameters << EXTERNAL_AGENT_INTERFACE_JAVA_POSITIVE_INFINITY;
        }
        else if( that->getTargetEvaluation() == -std::numeric_limits<double>::infinity() )
        {
            ossCommunicationMethodCallParameters << EXTERNAL_AGENT_INTERFACE_JAVA_NEGATIVE_INFINITY;
        }
        else
        {
            ossCommunicationMethodCallParameters << that->getTargetEvaluation();
        }

        // Run the communication method of the external java agent interface
        pExternalJavaAgentInterface->communication( ossCommunicationMethodCallParameters.str() );
    }
    else
    {
        //
        // Reset communication data entry in agents hash map
        //
		
        // Create agent type name
        ostringstream ossAgentTypeName;
        ossAgentTypeName << "agentType_" <<  that->getAgentType().getName() << "_" << that->getAgentType().getId();
        const string& sAgentTypeName = ossAgentTypeName.str();

        // Create agent name
        ostringstream ossAgentName;
        ossAgentName << "agent_" <<  sAgentTypeName << "_" << that->getNumber();
        const string& sAgentName = ossAgentName.str();
		
		// Create the corresponding parameters for the command
        ostringstream ossCommunicationMethodCallParameters;
        ossCommunicationMethodCallParameters << sAgentName << " " << "null";

        // Run the communication command of the external java agent interface which will set the data entry in agents hash map to null
        pExternalJavaAgentInterface->communication( ossCommunicationMethodCallParameters.str() );
    }

#endif
}
