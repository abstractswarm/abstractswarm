/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined CountVisitEdgeConnectedComponents_hmol
	#define CountVisitEdgeConnectedComponents_hmol


#include <typeinfo>
#include "./../../Editor/VisitEdge.h"  // Included from editor!
#include "./../../Editor/PlaceEdge.h"  // Included from editor!
#include "./../../Editor/TimeEdge.h"  // Included from editor!
#include "./../Components/Agent.hpp"  // Included from editor!
#include "./../Components/Station.hpp"  // Included from editor!





class CComponentType;





/**
 * The method increases the given counter by the number of components connected by visit edges to the given component type.
 *
 * @param componentType  the component type connected by an edge and whose visit edge conneceted components should be counted
 * @param counter        output parameter to which the number of components is added in case the opposite type is connected by a visit edge
 * @conterTargetStation  output parameter to which the number of components that have one of the components of the component type as target is added in case the opposite type is connected by a visit edge
 */
//method MCountVisitEdgeConnectedComponents( const CComponentType& componentType, int& counter, int& counterTargetStation )
class MCountVisitEdgeConnectedComponents
{
#define METHOD_PARAMS_COUNTVISITEDGESCONNECTEDCOMPONENTS const CComponentType& componentType, int& counter, int& counterTargetStation

public:

	/**
	 * Increases the given counter by the number of connected components.
	 */
//	virtual void <const CVisitEdge*>();
    static void handling( const CVisitEdge* that, METHOD_PARAMS_COUNTVISITEDGESCONNECTEDCOMPONENTS );

	/**
	 * Does not increase any of the counters (components are only counted if connected by visit edges and the counter for agents having the component type as target station is no increased for stations since only agents can have stations as target).
	 */
//	virtual void <const CPlaceEdge*, const CTimeEdge*, const CStation*>();
    static void handling( const CPlaceEdge* that, METHOD_PARAMS_COUNTVISITEDGESCONNECTEDCOMPONENTS );
    static void handling( const CTimeEdge* that, METHOD_PARAMS_COUNTVISITEDGESCONNECTEDCOMPONENTS );
    static void handling( const CStation* that, METHOD_PARAMS_COUNTVISITEDGESCONNECTEDCOMPONENTS );

	/**
	 * Increases the given counter for agents having the component type as target station by one.
	 */
//	virtual void <const CAgent*>();
    static void handling( const CAgent* that, METHOD_PARAMS_COUNTVISITEDGESCONNECTEDCOMPONENTS );


    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline void handling( void* vp_that, METHOD_PARAMS_COUNTVISITEDGESCONNECTEDCOMPONENTS )
    {
        if( typeid( *(static_cast<const CVisitEdge*>( vp_that ))) == typeid( const CVisitEdge ) )
            handling( static_cast<const CVisitEdge*>( vp_that ), componentType, counter, counterTargetStation );
        else if( typeid( *(static_cast<const CPlaceEdge*>( vp_that ))) == typeid( const CPlaceEdge ) )
            handling( static_cast<const CPlaceEdge*>( vp_that ), componentType, counter, counterTargetStation );
        else if( typeid( *(static_cast<const CTimeEdge*>( vp_that ))) == typeid( const CTimeEdge ) )
            handling( static_cast<const CTimeEdge*>( vp_that ), componentType, counter, counterTargetStation );
        else if( typeid( *(static_cast<const CStation*>( vp_that ))) == typeid( const CStation ) )
            handling( static_cast<const CStation*>( vp_that ), componentType, counter, counterTargetStation );
        else if( typeid( *(static_cast<const CAgent*>( vp_that ))) == typeid( const CAgent ) )
            handling( static_cast<const CAgent*>( vp_that ), componentType, counter, counterTargetStation );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }
};





#endif
