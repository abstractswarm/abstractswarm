/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined TransferNecessityToJava_hmol
    #define TransferNecessityToJava_hmol





#include <typeinfo>
#include "./../../Editor/VisitEdge.h"  // Included from editor!
#include "./../../Editor/PlaceEdge.h"  // Included from editor!
#include "./../../Editor/TimeEdge.h"  // Included from editor!
#include <iosfwd>





using namespace std;






class CComponent;





/**
 * Adds entries to Java hash map object for the necessity attribute belonging to the
 * corresponding input object. 
 *
 * @param ossJavaCode         the string stream to which the java code is written
 * @param pTypeName           the name of the component type to which the edge is connected
 * @param pComponentName      the name of the component to which the remaining cycle value is added
 */
//method MTransferNecessityToJava( ostringstream& ossJavaCode, const char* pTypeName, const char* pComponentName )
class MTransferNecessityToJava
{
#define METHOD_PARAMS_TRANSFERNECESSITYTOTOJAVA ostringstream& ossJavaCode, const char* pTypeName, const char* pComponentName

public:

    /**
     * Adds entries to Java hash map object for the cycle attribute belonging to that component.
	 * Note that the cycle attribute itself belongs to the type connected by a incoming time edge to that component's type.
	 */
//	void <const CComponent>();
    static void handling( const CComponent& that, METHOD_PARAMS_TRANSFERNECESSITYTOTOJAVA );

	/**
     * Adds an entry to Java hash map object for the necessity attribute belonging to that time edge.
	 */
//	virtual void <CVisitEdge*>( const CComponent& component );
    static void handling( const CVisitEdge* that, METHOD_PARAMS_TRANSFERNECESSITYTOTOJAVA, const CComponent& component );

	/**
	 * Does nothing (since those edges are not related to the necessity attribute).
	 */
//	virtual void <CPlaceEdge*, CTimeEdge*>( const CComponent& component );
    static void handling( const CPlaceEdge* that, METHOD_PARAMS_TRANSFERNECESSITYTOTOJAVA, const CComponent& component );
    static void handling( const CTimeEdge* that, METHOD_PARAMS_TRANSFERNECESSITYTOTOJAVA, const CComponent& component );


    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline void handling( void* vp_that, METHOD_PARAMS_TRANSFERNECESSITYTOTOJAVA, const CComponent& component )
    {
        if( typeid( *(static_cast<const CVisitEdge*>( vp_that ))) == typeid( const CVisitEdge ) )
            handling( static_cast<const CVisitEdge*>( vp_that ), ossJavaCode, pTypeName, pComponentName, component );
        else if( typeid( *(static_cast<const CPlaceEdge*>( vp_that ))) == typeid( const CPlaceEdge ) )
            handling( static_cast<const CPlaceEdge*>( vp_that ), ossJavaCode, pTypeName, pComponentName, component );
        else if( typeid( *(static_cast<const CTimeEdge*>( vp_that ))) == typeid( const CTimeEdge ) )
            handling( static_cast<const CTimeEdge*>( vp_that ), ossJavaCode, pTypeName, pComponentName, component );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }
};





#endif
