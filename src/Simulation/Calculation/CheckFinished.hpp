/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined CheckFinished_hmol
	#define CheckFinished_hmol


#include <typeinfo>
#include "./../../Editor/VisitEdge.h"  // Included from editor!
#include "./../../Editor/PlaceEdge.h"  // Included from editor!
#include "./../../Editor/TimeEdge.h"  // Included from editor!
#include "./../Components/Component.hpp"





/**
 * Checks for the given component if its simulation is finished according to the components
 * connected to it by visit edges.
 *
 * @param component   the component to check
 */
//method MCheckFinished( const CComponent& component )
class MCheckFinished
{
#define METHOD_PARAMS_CHECKFINISHED const CComponent& component

public:

	/**
	 * Returns true if the given component is finisehd according to all components connected by 
	 * that visit edge.
	 *
	 * @return   true if the given component is finisehd according to all components connected by that visit edge, false otherwise
	 */
//	virtual bool <CVisitEdge*>();
    static bool handling( CVisitEdge* that, METHOD_PARAMS_CHECKFINISHED );

	/**
	 * Returns always true since only visit edges can influence the end of simulation.
	 *
	 * @return   always true
	 */
//	virtual bool <CPlaceEdge*, CTimeEdge*>();
    static bool handling( CPlaceEdge* that, METHOD_PARAMS_CHECKFINISHED );
    static bool handling( CTimeEdge* that, METHOD_PARAMS_CHECKFINISHED );


    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline bool handling( void* vp_that, METHOD_PARAMS_CHECKFINISHED )
    {
        if( typeid( *(static_cast<CVisitEdge*>( vp_that ))) == typeid( CVisitEdge ) )
            return handling( static_cast<CVisitEdge*>( vp_that ), component );
        else if( typeid( *(static_cast<CPlaceEdge*>( vp_that ))) == typeid( CPlaceEdge ) )
            return handling( static_cast<CPlaceEdge*>( vp_that ), component );
        else if( typeid( *(static_cast<CTimeEdge*>( vp_that ))) == typeid( CTimeEdge ) )
            return handling( static_cast<CTimeEdge*>( vp_that ), component );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }
};





#endif
