/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------

#include "./../../Editor/VisitEdge.h"
#include "./../../Editor/PlaceEdge.h"
#include "./../../Editor/TimeEdge.h"
#include ".//..//Components//Component.hpp"
#include ".//..//Components//Station.hpp"
#include ".//..//Constants.h"
#include <algorithm>
#include <typeinfo>
//#pragma c-mol_hdrstop

#include "CheckPlaceEdgeConstraint.hpp"





using namespace std;





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//bool MCheckPlaceEdgeConstraint::<CPlaceEdge*>()
bool MCheckPlaceEdgeConstraint::handling( CPlaceEdge* that, METHOD_PARAMS_CHECKPLACEEDGECONSTRAINT, bool& isPlaceEdge )
{
    // When in this handling, it is a place edge
    isPlaceEdge = true;

    // Contraint cannot be unsatisfied if not both connectors are connected (when editing while simulating)
	if( !that->getConnector1().getIsConnected() || !that->getConnector2().getIsConnected() )
	{
        return( true );
	}

	// Determine the opponent connector
	CEdgeConnector* pOpponentConnector = &(that->getConnector1)();
	CEdgeConnector* pComponentsConnector = &(that->getConnector2());
    if( &(pOpponentConnector->getComponentType()) == &(agent.getTarget()->getComponentType()) )
	{
		pOpponentConnector = &(that->getConnector2());
		pComponentsConnector = &(that->getConnector1());
	}

    // If it is an incoming edge to the agent's target station
    if( pComponentsConnector->getIsDirected() )
    {
        // For all opposite stations
        vector<CComponent*>::const_iterator itComponent;
        for( itComponent = pOpponentConnector->getComponentType().getComponents().begin(); itComponent != pOpponentConnector->getComponentType().getComponents().end(); itComponent++ )
        {
            // If the agent's previous target was one of the opposite stations and there was an enter event already (otherwise the
            // previous target was initialized randomly in the agent's reset method), it is a constraint and it is satisfied, return true!
            if( (agent.getPreviousTarget() == (*itComponent)) && (agent.getLastEnterEventTime() != INVALID_TIME) )
            {
                return( true );
            }
        }

        // If it is an incoming edge to the agent's target station, and there was no station on the opposite connector found
        // that satisfies the contraint, return false!
        return( false );
    }

    // If reached here, the place edge was not a incoming directed to the agent's target station => return true!
    return( true );
}


//bool MCheckPlaceEdgeConstraint::<CVisitEdge*, CTimeEdge*>()
bool MCheckPlaceEdgeConstraint::handling( CVisitEdge* that, METHOD_PARAMS_CHECKPLACEEDGECONSTRAINT, bool& isPlaceEdge )
{
    // No place edge and thus no place edge contraint that could be unsatisfied
    isPlaceEdge = false;

    return( true );
}
bool MCheckPlaceEdgeConstraint::handling( CTimeEdge* that, METHOD_PARAMS_CHECKPLACEEDGECONSTRAINT, bool& isPlaceEdge )
{
    // No place edge and thus no place edge contraint that could be unsatisfied
    isPlaceEdge = false;

    return( true );
}

