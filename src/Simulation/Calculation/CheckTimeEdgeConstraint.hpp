/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined CheckTimeEdgeConstraint_hmol
	#define CheckTimeEdgeConstraint_hmol


#include <typeinfo>
#include "./../../Editor/VisitEdge.h"  // Included from editor!
#include "./../../Editor/PlaceEdge.h"  // Included from editor!
#include "./../../Editor/TimeEdge.h"  // Included from editor!
#include "./../Components/Component.hpp"





/**
 * Checks whether the contraint expressed by a given time edge is fulfilled.
 *
 * @param component   the component to check the time edge contraints for
 */
//method MCheckTimeEdgeConstraint( const CComponent& component )
class MCheckTimeEdgeConstraint
{
#define METHOD_PARAMS_CHECKTIMEEDGECONSTRAINT const CComponent& component

public:

	/**
	 * Returns true if contraint expressed by the time edge is fulfilled; false otherwise.
	 *
	 * @param isTimeEdge       output parameter that determines whether the edge passed as input to the method is a time-edge (always true for completely connected time-edges)
	 * @param isOrConstraint   output parameter that determines whether the constraint represented by the time edge is an or-constraint
	 * @param pFulfiller       output parameter that contains a pointer to the opposite component that fulfills the constraint (NULL if no opposite component was found to fulfill the constraint)
	 */
//	virtual bool <CTimeEdge*>( bool& isTimeEdge, bool& isOrConstraint, CComponent*& pFulfiller );
    static bool handling( CTimeEdge* that, METHOD_PARAMS_CHECKTIMEEDGECONSTRAINT, bool& isTimeEdge, bool& isOrConstraint, CComponent*& pFulfiller );

	/**
	 * Always returns true (a time edge contraint can only be not fulfilled for time edges).
	 *
	 * @param isTimeEdge       output parameter that determines whether the edge passed as input to the method is a time-edge (always false for all non-time edges)
	 * @param isOrConstraint   output parameter that determines whether the constraint is an or-constraint (always false for all non-time edges)
	 * @param pFulfiller       output parameter that contains a pointer to the opposite component that fulfills the constraint (always NULL for all non-time edges)
	 */
//	virtual bool <CVisitEdge*, CPlaceEdge*>( bool& isTimeEdge, bool& isOrConstraint, CComponent*& pFulfiller );
    static bool handling( CVisitEdge* that, METHOD_PARAMS_CHECKTIMEEDGECONSTRAINT, bool& isTimeEdge, bool& isOrConstraint, CComponent*& pFulfiller );
    static bool handling( CPlaceEdge* that, METHOD_PARAMS_CHECKTIMEEDGECONSTRAINT, bool& isTimeEdge, bool& isOrConstraint, CComponent*& pFulfiller );

	/**
	 * Returns true if constraint expressed by the time edge contraint is fullfilled by the component; false otherwise.
	 *
	 * @param timeEdge   the time edge that connects the given component with the input component
	 */
//	bool <CComponent*>( const CTimeEdge& timeEdge );
    static bool handling( CComponent* that, METHOD_PARAMS_CHECKTIMEEDGECONSTRAINT, const CTimeEdge& timeEdge );


    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline bool handling( void* vp_that, METHOD_PARAMS_CHECKTIMEEDGECONSTRAINT, bool& isTimeEdge, bool& isOrConstraint, CComponent*& pFulfiller )
    {
        if( typeid( *(static_cast<CVisitEdge*>( vp_that ))) == typeid( CVisitEdge ) )
            return handling( static_cast<CVisitEdge*>( vp_that ), component, isTimeEdge, isOrConstraint, pFulfiller );
        else if( typeid( *(static_cast<CPlaceEdge*>( vp_that ))) == typeid( CPlaceEdge ) )
            return handling( static_cast<CPlaceEdge*>( vp_that ), component, isTimeEdge, isOrConstraint, pFulfiller );
        else if( typeid( *(static_cast<CTimeEdge*>( vp_that ))) == typeid( CTimeEdge ) )
            return handling( static_cast<CTimeEdge*>( vp_that ), component, isTimeEdge, isOrConstraint, pFulfiller );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }
};





#endif
