/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined CheckPlaceEdgeConstraint_hmol
    #define CheckPlaceEdgeConstraint_hmol


#include <typeinfo>
#include "./../../Editor/VisitEdge.h"  // Included from editor!
#include "./../../Editor/PlaceEdge.h"  // Included from editor!
#include "./../../Editor/TimeEdge.h"  // Included from editor!
#include "./../Components/Agent.hpp"





/**
 * Checks whether the contraint expressed by a given place edge is satisfied.
 *
 * @param agent            the agent for which the place edge contraint is checked
 */
//method MCheckPlaceEdgeConstraint( const CAgent& agent )
class MCheckPlaceEdgeConstraint
{
#define METHOD_PARAMS_CHECKPLACEEDGECONSTRAINT const CAgent& agent

public:

	/**
     * Returns true if contraint expressed by the place edge is satisfied; false otherwise.
	 */
//	virtual bool <CPlaceEdge*>();
    static bool handling( CPlaceEdge* that, METHOD_PARAMS_CHECKPLACEEDGECONSTRAINT, bool& isPlaceEdge );

	/**
     * Always returns true (a place edge contraint can only be unsatisfied for place edges).
	 */
//	virtual bool <CVisitEdge*, CTimeEdge*>();
    static bool handling( CVisitEdge* that, METHOD_PARAMS_CHECKPLACEEDGECONSTRAINT, bool& isPlaceEdge );
    static bool handling( CTimeEdge* that, METHOD_PARAMS_CHECKPLACEEDGECONSTRAINT, bool& isPlaceEdge );


    // C-MOL DISPATCHER HANDLING FOR RUNTIME POLYMORPHISM
    // (DO NOT FORGET TO ADD/CHANGE IF-STATEMENTS HERE WHEN ADDING/CHANGING POLYMORPHIC HANDLINGS!)
    static inline bool handling( void* vp_that, METHOD_PARAMS_CHECKPLACEEDGECONSTRAINT, bool& isPlaceEdge )
    {
        if( typeid( *(static_cast<CVisitEdge*>( vp_that ))) == typeid( CVisitEdge ) )
            return handling( static_cast<CVisitEdge*>( vp_that ), agent, isPlaceEdge );
        else if( typeid( *(static_cast<CPlaceEdge*>( vp_that ))) == typeid( CPlaceEdge ) )
            return handling( static_cast<CPlaceEdge*>( vp_that ), agent, isPlaceEdge );
        else if( typeid( *(static_cast<CTimeEdge*>( vp_that ))) == typeid( CTimeEdge ) )
            return handling( static_cast<CTimeEdge*>( vp_that ), agent, isPlaceEdge );
        else
            throw( "C-mol runtime error: no polymorphic handling for that type" );
    }
};





#endif
