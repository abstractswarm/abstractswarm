/*
AbstractSwarm Agent that is able to collect simulation data
Copyright (C) 2020  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;


/**
 * This class provides the three methods of the AbstractSwarm agent interface 
 * for state perception and performing actions, communication with other agents
 * and the perception of rewards.<br />
 * <br />
 * 
 * The properties of the interface's objects are:<br />
 * <br />
 * 
 * AGENT:<br />
 * .name           the agent's name<br />
 * .type           the agent's AGENT_TYPE (see below)<br />
 * .frequency      the number of remaining visits; -1 if the agent's type has
 *                 no frequency attribute<br />
 * .necessities    the number of remaining visits for each connected station,
 *                 -1 if the agent's type has no necessity attribute<br />          
 * .cycles         the number of remaining cycles for each incoming visit edge;
 *                 -1 if agent's type has no cycle attribute<br />
 * .time           the remaining time on the target station, -1 if agent is
 *                 currently not visiting a station or if agent's type has no
 *                 time attribute<br />
 * .target         the agent's current target<br />
 * .visiting       whether the agent is currently visiting a station<br />
 * <br />
 * 
 * STATION:<br />
 * .name           the station's name<br />
 * .type           the station's STATION_TYPE (see below)<br />
 * .frequency      the number of remaining visits; -1 if the station's type
 *                 has no frequency attribute<br />
 * .necessities    the number of remaining visits for each connected agent,
 *                 -1 if the station's type has no necessity attribute<br />          
 * .cycles         the number of remaining cycles for each incoming visit edge;
 *                 -1 if station's type has no cycle attribute<br />
 * .space          the remaining space, -1 if the station's type has no space
 *                 attribute<br />
 * <br />
 * 
 * AGENT_TYPE:<br />
 * .name           the agent type's name as string<br />
 * .type           the type as string ("AGENT_TYPE")<br />
 * .components     the agent type's AGENTs (see above)<br />
 * .frequency      the agent type's frequency attribute; -1 if the agent type 
 *                 has no frequency attribute<br />
 * .necessity      the agent type's necessity attribute; -1 if the agent type 
 *                 has no necessity attribute<br />          
 * .cycle          the agent type's cycle attribute; -1 if agent type has no
 *                 cycle attribute<br />
 * .time           the agent type's time attribute, -1 if agent type has no
 *                 time attribute<br />
 * .size           the agent type's size attribute, -1 if agent type has no
 *                 size attribute<br />
 * .priority       the agent type's priority attribute, -1 if agent type has
 *                 no priority attribute<br />
 * .visitEdges     the agent type's VISIT_EDGEs as list (see below)<br />
 * .timeEdges      the agent type's TIME_EDGEs as list (see below)<br />
 * .placeEdges     the agent type's PLACE_EDGEs as list (see below)
 *                 (note that, by definition, agent types cannot have place
 *                 edges and thus this list will always be empty; this is only
 *                 for backwards compatibility/unification reasons)<br />
 * <br />
 *
 * STATION_TYPE:<br />
 * .name           the station type's name as string<br />
 * .type           the type as string ("STATION_TYPE")<br />
 * .components     the station type's stations<br />
 * .frequency      the station type's frequency attribute; -1 if the station
 *                 type has no frequency attribute<br />
 * .necessity      the station type's necessity attribute; -1 if the station
 *                 type has no necessity attribute<br />          
 * .cycle          the station type's cycle attribute; -1 if station type has
 *                 no cycle attribute<br />
 * .time           the station type's time attribute, -1 if station type has no
 *                 time attribute<br />
 * .space          the station type's space attribute, -1 if station type has
 *                 no space attribute<br />
 * .visitEdges     the station type's VISIT_EDGEs as list (see below)<br />
 * .timeEdges      the station type's TIME_EDGEs as list (see below)<br />
 * .placeEdges     the station type's PLACE_EDGEs as list (see below)<br />
 * <br />
 *
 * VISIT EDGE:<br />
 * .type           the edge's type as string ("VISIT_EDGE")<br />
 * .connectedType  the opposite component type connected by the edge<br />
 * .bold           whether the edge is bold<br />
 * <br />
 * 
 * TIME EDGE:<br />
 * .type           the edge's type as string ("TIME_EDGE")<br />
 * .connectedType  the opposite component type connected by the edge<br />
 * .incoming       whether the edge is incoming<br />
 * .outgoing       whether the edge is outgoing<br />
 * .andConnected   whether the edge is and-connected to the opposite type<br />
 * .andOrigin      whether the edge is and-connected at its origin type<br />
 * <br />
 * 
 * PLACE EDGE:<br />
 * .type         the edge's type as string ("PLACE_EDGE")<br />
 * .connectedType  the opposite component type connected by the edge<br />
 * .incoming       whether the edge is incoming<br />
 * .outgoing       whether the edge is outgoing<br />
 */
public class AbstractSwarmAgentInterface 
{
	/** The agent's random generator used for decisions. */
	private static Random random = new Random();
		
	/** The agent's current evaluations for each station. */
	private static Map<Station, Double> evaluatedStations = new HashMap<Station, Double>();
	
	/** The currently selected stations of each agent. */
	private static Map<Agent, Station> selectedStations = new HashMap<Agent, Station>();
	
	/** The global reward of all agents. */
	private static double globalReward = 0; 

	/** The previous time unit. */
	private static long previousTime = -1;
	
	/** The data to be collected */
	private static List<String[]> dataRows = new ArrayList<String[]>();
	
	
	/**
	 * This method allows an agent to perceive its current state and to perform
	 * actions by returning an evaluation value for potential next target
	 * stations. The method is called for every station that can be visted by
	 * the agent. 
	 * 
	 * @param me        the agent itself
	 * @param others    all other agents in the scenario with their currently
	 *                  communicated information
	 * @param stations  all stations in the scenario
	 * @param time      the current time unit
	 * @param station   the station to be evaluated as potential next target
	 * 
	 * @return          the evaluation value of the station
	 */
	public static double evaluation( Agent me, HashMap<Agent, Object> others, List<Station> stations, long time, Station station )
	{
		// If this is the very first call to evaluation function
		if( (time == 1) && (time != previousTime) )
		{
			globalReward = 0.0;
		}
		
		// Create random evaluation different from the ones already created
		// (to be sure which one will be selected as next target)
		double evaluation = -1; 
		do
		{
			evaluation = random.nextDouble();
		}
		while( evaluatedStations.containsValue( evaluation ) );
		
		// Store the new evaluation
		evaluatedStations.put( station, evaluation );
		
		// Remember previous time
		previousTime = time;
		
		return evaluation;
	}
	
	
	/**
	 * This method allows an agent to communicate with other agents by
	 * returning a communication data object.
	 * 
	 * @param me           the agent itself
	 * @param others       all other agents in the scenario with their
	 *                     currently communicated information
	 * @param stations     all stations in the scenario
	 * @param time         the current time unit
	 * @param defaultData  a triple (selected station, time unit when the 
	 *                     station is reached, evaluation value of the station)
	 *                     that can be used for default communication
	 * 
	 * @return             the communication data object
	 */
	public static Object communication( Agent me, HashMap<Agent, Object> others, List<Station> stations, long time, Object[] defaultData )
	{
		// If empty, communication function was called without prior call to evaluation function
		if( !evaluatedStations.isEmpty() )
		{
			// Restore which station was selected
			Station selectedStation = null;
			double maxEvaluation = Double.NEGATIVE_INFINITY;
			for( Station evaluatedStation : evaluatedStations.keySet() )
			{
				if( evaluatedStations.get( evaluatedStation ) > maxEvaluation )
				{
					selectedStation = evaluatedStation;
					maxEvaluation = evaluatedStations.get( evaluatedStation );
				}
			}
			
			// Store the selected station for each agent
			selectedStations.put( me, selectedStation );
						
			
			//
			// Collect agent type, previous and current selected target station type, distance
			//
			
			String myTypeName = me.type.name;
			String previousTargetTypeName = me.previousTarget.type.name;
			String targetTypeName = selectedStation.type.name;

			// Create (ordered) set of stations that still have to be visited
			Set<String> stationTypesToBeVisited = new HashSet<String>();
			for( Station evaluatedStation : evaluatedStations.keySet() )
			{
				// Initialize the free space with the current station's remaining space
				double freeSpace = evaluatedStation.space;

				// If station not limited in space, free space will be infinite
				if(	evaluatedStation.space == -1 )
				{
					freeSpace = Double.POSITIVE_INFINITY;
				}
				
				// Iterate over all other agents
				for( Agent other : others.keySet() )
				{
					// If other agent has communicated the current station as target
					if( (others.get( other ) != null) && ((Object[]) others.get( other ))[ 0 ] == evaluatedStation )
					{
						// If other agent has a limited size, subtract it from free space
						if( other.type.size != -1 )
						{
							freeSpace -= other.type.size;
						}
						
						// ...else remaining free space will be completely covered
						else if( freeSpace > 0 )
						{
							freeSpace = 0;
						}
					}
				}

				// Was station occupied?
				if( freeSpace > 0 /*evaluatedStation.space > 0 || evaluatedStation.type.space == -1*/ )
				{
					// If there is a full version of this station in the set, remove it, there is a free one now!
					if( stationTypesToBeVisited.contains( evaluatedStation.type.name + "_full" ) )
					{
						stationTypesToBeVisited.remove( evaluatedStation.type.name + "_full" );
					}

					stationTypesToBeVisited.add( evaluatedStation.type.name + "_free" );
				}
				else
				{
					// Only if there is no free version of this station in the set, add the full one
					if( !stationTypesToBeVisited.contains( evaluatedStation.type.name + "_free" ) )
					{
						stationTypesToBeVisited.add( evaluatedStation.type.name + "_full" );
					}
				}
			}
			List<String> lStationsToBeVisited = new ArrayList<String>( stationTypesToBeVisited );
			Collections.sort( lStationsToBeVisited );
			String sStationsToBeVisites = "";
			for( String s : lStationsToBeVisited )
			{
				sStationsToBeVisites += s + "|";
			}
			sStationsToBeVisites = sStationsToBeVisites.substring( 0, sStationsToBeVisites.length() - 1 );
			
			
			// Create a data row
			String[] dataRow = new String[]{ "time_" + time, myTypeName, "|"+ sStationsToBeVisites + "|", "at_" + previousTargetTypeName, "to_" + targetTypeName };
			dataRows.add( dataRow );
			
			// DEBUG
			// Write current data row to file
			BufferedWriter writer;
			try 
			{
				writer = new BufferedWriter( new FileWriter( "data_raw.log", true ) );
			    
				for( String s : dataRow )
				{
					writer.write( s );
					if( !dataRow[ dataRow.length - 1 ].equals( s ) )
					{
						writer.write( " " );
					}
				}
				writer.write( "\n" );
				
				writer.close();
			} 
			catch( IOException e ) 
			{
				e.printStackTrace();
			}
			
			
			// Clear evaluated stations for the next agent
			evaluatedStations.clear();
		}
		
		return defaultData;
	}

	
	/**
	 * This method allows an agent to perceive the local reward for its most
	 * recent action.
	 * 
	 * @param me           the agent itself
	 * @param others       all other agents in the scenario with their
	 *                     currently communicated information
	 * @param stations     all stations in the scenario
	 * @param time         the current time unit
	 * @param value        the local reward in [0, 1] for the agent's most
	 *                     recent action 
	 */
	public static void reward( Agent me, HashMap<Agent, Object> others, List<Station> stations, long time, double value )
	{
		// Count global reward for all agents
		globalReward += value;

		// Determine whether the simulation is successful
		// (Must be called before the current agent is removed from the selected stations)
		// (Note that the function changes the state of the objects passed to it!)
		boolean isSuccess = isSuccess( me, others, stations, selectedStations );
		
		// Remove the agent from the map of agents that selected a target station 
		// (i.e., agents that were asked for evaluation before)
		selectedStations.remove( me );
	
		// Determine whether there are no more visiting agents among the agents
		// that have still a target selected
		// (this is important to ensure that this is the last call to the reward function; 
		// depending on the model, it may happen that the simulation is already successful 
		// according to the frequency and necessity attributes but this is not the last
		// agent's reward function call and therefore not all agents already received
		// their reward)
		boolean isNotVisiting = isNotVisiting( selectedStations.keySet() );
		
		// Success and last call of the reward function?
		if( isSuccess && isNotVisiting )
		{
			// DEBUG
			System.out.println( "Simulation finished with success at time " + time + " (global reward: " + globalReward + ")." );

			// DO POST-PROCESSING OF THE DATA
			postProcessing( dataRows, time, globalReward );
		}
	}


	/**
	 * Returns the distance from previous target to target station.
	 *  
	 * @param previousTarget  the previous target station
	 * @param target          the current target station
	 * @return                the distance from previous target to target station, -1 if no path could be found or if previous target or target is null
	 */
	private static int distance( Station previousTarget, Station target )
	{
		// If previousTarget and target exist
	    if( (previousTarget != null) && (target != null) )
	    {
	        // Create open and closed lists
	        Map<StationType, Integer> openList = new HashMap<StationType, Integer>();
	        List<StationType> closedList = new ArrayList<StationType>();
	        Map<StationType, Integer> distances = new HashMap<StationType, Integer>();

	        // Initialize open list with first node
	        openList.put( previousTarget.type, 0 ); 
	        
	        do
	        {
	            // Get and remove node with minimal value
	            int minValue = 0;
	            StationType minStationType = null;
	            for( StationType stationType : openList.keySet() )
	            {
	                if( (minStationType == null) || openList.get( stationType ) < minValue )
	                {
	                    minValue = openList.get( stationType );
	                    minStationType = stationType;
	                }
	            }
	            openList.remove( minStationType );

	            // If goal found, set current distance and stop search
	            if( minStationType == target.type )
	            {
	            	int distanceToTarget = 0;
	                if( distances.containsKey( minStationType ) )
	                {
	                    distanceToTarget = distances.get( minStationType );
	                }
	                else
	                {
	                    distanceToTarget = 0;
	                }

	                return( distanceToTarget );
	            }

	            // If goal not found yet, put to closed list (will no longer be considered)
	            closedList.add( minStationType );

	            // Consider all neighbor nodes
	            for( PlaceEdge placeEdge : minStationType.placeEdges )
	            {
                    StationType neighbor = (StationType) placeEdge.connectedType;

                    // If the current considered node with minimal value is not in the closed list
                    if( !closedList.contains( neighbor ) )
                    {
                        // Calculate actual distance to the neighbor
                        int distance = 0;
                        if( distances.containsKey( minStationType ) )
                        {
                            distance = distances.get( minStationType );
                        }
                        distance += placeEdge.weight;

                        // If node is not in open list or the new distance will be better than the old one
                        if( !openList.containsKey( neighbor ) || (distance < distances.get( neighbor )) )
                        {
                            openList.put( neighbor, distance );  // TODO: Add an estimated value here (without overestimation) for the A* heuristic
                            distances.put( neighbor, distance );
                        }
                    }
	            }
	        }
	        while( !openList.isEmpty() );
	    }

	    // If reached to here no path could be found (or no target or previous target)
	    int distanceToTarget = -1;
	    return( distanceToTarget );
	}
	
	
	/**
	 * Returns whether none of the provided agents are visiting a station. 
	 * 
	 * @param agentsWithSelectedTarget  agents with a target station selected
	 * @return                          whether none of the provided agents are visiting a station
	 */
	private static boolean isNotVisiting( Set<Agent> agentsWithSelectedTarget )
	{
		boolean isNotVisiting = true;
		for( Agent agent : agentsWithSelectedTarget )
		{
			if( agent.visiting )
			{
				isNotVisiting = false;
				break;
			}
		}
		
		return isNotVisiting;
	}
	
	
	/**
	 * Calculates whether the current simulation run is successful.
	 * (Note that the function changes the state of the objects passed to it!)
	 * 
	 * @param me                the current agent
	 * @param others            all other agents
	 * @param stations          all stations
	 * @param selectedStations  all stations (values) currently selected by an agent (keys) 
	 * @return                  true if the current simulation run is successful, false otherwise
	 */
	private static boolean isSuccess( Agent me, HashMap<Agent, Object> others, List<Station> stations, Map<Agent, Station> selectedStations )
	{
		// Is successful end of simulation?
		boolean isSuccess = true;
		List<Agent> agents = new ArrayList<Agent>( others.keySet() );
		agents.add( me );

		// Get last selected station
		Station target = selectedStations.get( me );

		// Check whether this is the successful end of the simulation:
		// -1 here, since in reward function agents (mainly) see the state at time of action selection!
		// (Check min/max visit time, since only in case visit events of length one this correction must be done)
		if( (target != null) && ((Math.min( me.type.time, target.type.time ) == 1) || (Math.max( me.type.time, target.type.time ) == 1)) )
		{
			me.frequency = me.frequency - 1;  
			if( me.necessities != null )
			{
				me.necessities.put( target, me.necessities.get( target ) - 1 );
			}
			target.frequency = target.frequency - 1;
			if( target.necessities != null )
			{
				target.necessities.put( me, target.necessities.get( me ) - 1 );
			}
		}
		
		// Check whether there is an agent or station with frequency/necessities > 0
		// (if not => success!)
		for( Agent agent : agents )
		{
			if( agent.frequency > 0 ) 
			{
				isSuccess = false;
				break;
			}
			if( agent.necessities != null )
			{
				for( Station station : agent.necessities.keySet() )
				{
					if( agent.necessities.get( station ) > 0 )
					{
						isSuccess = false;
						break;
					}
				}
			}
		}
		for( Station station : stations )
		{
			if( station.frequency > 0 )
			{
				isSuccess = false;
				break;
			}
			if( station.necessities != null )
			{
				for( Agent agent : station.necessities.keySet() )
				{
					if( station.necessities.get( agent ) > 0 )
					{
						isSuccess = false;
						break;
					}
				}
			}
		}
	
		return isSuccess;
	}	
	
	
	/**
	 * Does the post-processing of the data after a successfully finished run.
	 * 
	 * @param dataRows      the raw data
	 * @param neededTime    the time needed to successfully perform the run
	 * @param globalReward  the global reward for the successful run    
	 */
	public static void postProcessing( List<String[]> dataRows, long neededTime, double globalReward )
	{
		// DEBUG
		System.out.println( "POST-PROCESSING DATA..." );

		// Write current data row to file
		BufferedWriter writerAll;
		BufferedWriter writerPatients;
		BufferedWriter writerDocs;
		BufferedWriter writerNurses;
		try 
		{
			writerAll = new BufferedWriter( new FileWriter( "data.log", true ) );
			writerPatients = new BufferedWriter( new FileWriter( "data_patients.log", true ) );
			writerDocs = new BufferedWriter( new FileWriter( "data_docs.log", true ) );
			writerNurses = new BufferedWriter( new FileWriter( "data_nurses.log", true ) );

			// Do weighting according to the global reward
			long weight = Math.round( (globalReward / 10.0) * (globalReward / 10.0) );
			for( int i = 0; i < weight; i++ )
			{
				for( String[] row : dataRows )
				{
					writerAll.write( row[ 1 ] + " " + row[ 2 ] + " " + row[ 3 ] + " " + row[ 4 ] + "\n" );
					if( row[ 1 ].startsWith( "Patient" ) )
					{
						writerPatients.write( row[ 2 ] + " " + row[ 3 ] + " " + row[ 4 ] + "\n" );
					}
					else if( row[ 1 ].startsWith( "Doc" ) )
					{
						writerDocs.write( row[ 2 ] + " " + row[ 3 ] + " " + row[ 4 ] + "\n" );
					}
					else if( row[ 1 ].startsWith( "Nurse" ) )
					{
						writerNurses.write( row[ 2 ] + " " + row[ 3 ] + " " + row[ 4 ] + "\n" );
					}
				}
			}
			
			writerAll.close();
			writerPatients.close();
			writerDocs.close();
			writerNurses.close();
		} 
		catch( IOException e ) 
		{
			e.printStackTrace();
		}
	}
}
