/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------

#include "./../../Editor/VisitEdge.h"
#include "./../../Editor/PlaceEdge.h"
#include "./../../Editor/TimeEdge.h"
#include ".//..//Components//Component.hpp"
#include ".//..//Constants.h"
#include <algorithm>
#include <typeinfo>
//#pragma c-mol_hdrstop

#include "CheckTimeEdgeConstraint.hpp"





using namespace std;





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//bool MCheckTimeEdgeConstraint::<CTimeEdge*>( bool& isTimeEdge, bool& isOrConstraint, CComponent*& pFulfiller )
bool MCheckTimeEdgeConstraint::handling( CTimeEdge* that, METHOD_PARAMS_CHECKTIMEEDGECONSTRAINT, bool& isTimeEdge, bool& isOrConstraint, CComponent*& pFulfiller )
{
	// Always true for completely connected time edges...
	isTimeEdge = true;

	// Contraint cannot be violated if not both connectors connected (when editing while simulating)
	if( !that->getConnector1().getIsConnected() || !that->getConnector2().getIsConnected() )
	{
		// Incompletly connected time edges do not count as time edges
		isTimeEdge = false;

		// Incompletly connected edges count as and-constraints, since they can not cause a fulfillment by itself
		isOrConstraint = false;

		return( true );
	}

	// Determine the opponent connector
	CEdgeConnector* pOpponentConnector = &(that->getConnector1)();
	CEdgeConnector* pComponentsConnector = &(that->getConnector2());
	if( &(pOpponentConnector->getComponentType()) == &(component.getComponentType()) )
	{
		pOpponentConnector = &(that->getConnector2());
		pComponentsConnector = &(that->getConnector1());
	}

	// Determine whether this edge represents an or-constraint
	if( pComponentsConnector->getIsAndConnection() )
		isOrConstraint = false;
	else
		isOrConstraint = true;

	// For all opponent components
	vector<CComponent*>::const_iterator itComponent;
	for( itComponent = pOpponentConnector->getComponentType().getComponents().begin(); itComponent != pOpponentConnector->getComponentType().getComponents().end(); itComponent++ )
	{
		// Check if the time edge constraint is okay
//		bool isTimeEdgeConstraintOk = MCheckTimeEdgeConstraint( component )<-itComponent( *that );
        bool isTimeEdgeConstraintOk = MCheckTimeEdgeConstraint::handling( *itComponent, component, *that );

		// Check if the opposite component is the fulfiller of the component to check
		bool isFulfillerOfComponentToCheck = (find( const_cast<CComponent&>( component ).getTimeEdgeConstraintFulfillers().begin(), const_cast<CComponent&>( component ).getTimeEdgeConstraintFulfillers().end(), *itComponent ) != const_cast<CComponent&>( component ).getTimeEdgeConstraintFulfillers().end());
		
		// Check if no other component of the type that contains the component to check
		// also has the opposite component as fulfiller
		bool hasOtherComponentTheSameFulfiller = false;
		vector<CComponent*>::const_iterator itOtherComponent;
		for( itOtherComponent = component.getComponentType().getComponents().begin(); itOtherComponent != component.getComponentType().getComponents().end(); itOtherComponent++ )
		{
			if( *itOtherComponent != &component )
			{
				hasOtherComponentTheSameFulfiller = (find( const_cast<CComponent&>( **itOtherComponent ).getTimeEdgeConstraintFulfillers().begin(), const_cast<CComponent&>( **itOtherComponent ).getTimeEdgeConstraintFulfillers().end(), *itComponent ) != const_cast<CComponent&>( **itOtherComponent ).getTimeEdgeConstraintFulfillers().end());
				if( hasOtherComponentTheSameFulfiller )
					break;
			}
		}

		// Check if fulfiller is used multiple times only due to an AND connection
		bool isMultipleFulfillerDueToAndTimeEdgeOnly = pOpponentConnector->getIsAndConnection() && (!hasOtherComponentTheSameFulfiller);
		
		// If time edge constraint is fulfilled by one connected component and that connected 
		// component is not a fulfiller yet or it is a fulfiller of the component to check or it
		// is already a fulfiller due to an AND time edge
		if( isTimeEdgeConstraintOk && (    (!(*itComponent)->getIsTimeEdgeConstraintFulfiller()) 
			                            || isFulfillerOfComponentToCheck 
										|| isMultipleFulfillerDueToAndTimeEdgeOnly) )
		{
			// Store the fulfiller of the constraint
			pFulfiller = *itComponent;

			// Return true
			return( true );
		}
	}

	// If reached here, no connected component fulfills the time edge contraint => return false
    pFulfiller = nullptr;
	return( false );
}


//bool MCheckTimeEdgeConstraint::<CVisitEdge*, CPlaceEdge*>( bool& isTimeEdge, bool& isOrConstraint, CComponent*& pFulfiller )
bool MCheckTimeEdgeConstraint::handling( CVisitEdge* that, METHOD_PARAMS_CHECKTIMEEDGECONSTRAINT, bool& isTimeEdge, bool& isOrConstraint, CComponent*& pFulfiller )
{
	// Only true time-edges...
	isTimeEdge = false;

	// Only time-edges represent or-time-constraints...
	isOrConstraint = false;

	// A fulfiller can only be determined in case of time edges
    pFulfiller = nullptr;

	return( true );
}
bool MCheckTimeEdgeConstraint::handling( CPlaceEdge* that, METHOD_PARAMS_CHECKTIMEEDGECONSTRAINT, bool& isTimeEdge, bool& isOrConstraint, CComponent*& pFulfiller )
{
    // Only true time-edges...
    isTimeEdge = false;

    // Only time-edges represent or-time-constraints...
    isOrConstraint = false;

    // A fulfiller can only be determined in case of time edges
    pFulfiller = nullptr;

    return( true );
}


//bool MCheckTimeEdgeConstraint::<CComponent*>( const CTimeEdge& timeEdge )
bool MCheckTimeEdgeConstraint::handling( CComponent* that, METHOD_PARAMS_CHECKTIMEEDGECONSTRAINT, const CTimeEdge& timeEdge )
{
	// Determine the time edge's direction
	bool isOutgoing = false;
	bool isIncoming = false;
	if(    (((&(timeEdge.getConnector1().getComponentType())) == (&(component.getComponentType()))) && timeEdge.getIsDirected1())
		|| (((&(timeEdge.getConnector2().getComponentType())) == (&(component.getComponentType()))) && timeEdge.getIsDirected2()) )
	{
		isIncoming = true;
	}
	if(    (((&(timeEdge.getConnector1().getComponentType())) == (&that->getComponentType())) && timeEdge.getIsDirected1())
		|| (((&(timeEdge.getConnector2().getComponentType())) == (&that->getComponentType())) && timeEdge.getIsDirected2()) )
	{
		isOutgoing = true;
	}

	// If there is no enter event on the component to be checked, no time edge can 
	// be violated => return true!
	if( !component.getHasEnterEvent() )
	{
		return( true );
	}

	// If the time edge directs only from the given component to the input component, 
	// there is no constraint => return true!
	if( isOutgoing && (!isIncoming) )
	{
		return( true );
	}

	// If the time edge is incoming and opposite component has a cycle attribute and had an enter 
	// event already and the cycle is not yet reached again and time edge weight is fulfilled => no time edge violation
	if(    isIncoming && that->getComponentType().getHasCycle() && that->getHadEnterEventAlready() && (component.getEnterEventCount() % that->getComponentType().getCycle() != 0) 
		&& ((component.getLastEnterEventTime() - that->getLastEnterEventTime()) >= timeEdge.getValue()) )
	{
		return( true );
	}

	// If time edge is incoming and opposite component has no cycle attribute and had an enter 
	// event already and time edge weight is fulfilled => always no time edge violation for the rest of the simulation
	if(    isIncoming && (!that->getComponentType().getHasCycle()) && that->getHadEnterEventAlready()
		&& ((component.getLastEnterEventTime() - that->getLastEnterEventTime()) >= timeEdge.getValue()) )
	{
		return( true );
	}

	// If time edge has zero weight, check if there is an enter event on the opposite component
	if( (timeEdge.getValue() == 0) && that->getHasEnterEvent() )
	{
		return( true );
	}

	// If the time edge directs from the input component to the given component, check if there 
	// was an enter event on the input component before a number of time units greater than or 
	// equal to the time edge's weight and if the cycle-attribute is satisfied
	if(    isIncoming && that->getHadEnterEventAlready() && ((component.getLastEnterEventTime() - that->getLastEnterEventTime()) >= timeEdge.getValue())
		&& (!that->getComponentType().getHasCycle() ||    ((component.getEnterEventCount() % that->getComponentType().getCycle() == 0) 
		                                               && ((component.getPreviousEnterEventTime() == INVALID_TIME) || (component.getPreviousEnterEventTime() < that->getLastEnterEventTime())))) )
	{
		return( true );
	}

	// If time edge is not directed and has weight greater than zero, check if there was an enter 
	// event on the input component before a number of time units greater than or equal to the 
	// time edge's weight
	if(    (!isIncoming) && (!isOutgoing) && (timeEdge.getValue() > 0)
		&& ((!that->getHadEnterEventAlready()) || ((component.getLastEnterEventTime() - that->getLastEnterEventTime()) >= timeEdge.getValue())) )
	{
		return( true );
	}

	// If reached to here, the contraint is not fulfilled!
	return( false );
}
