/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PROCESSCOMMUNICATIONADAPTER_HPP
#define PROCESSCOMMUNICATIONADAPTER_HPP


#include <QObject>
#include <QProcess>
#include <QMutex>
#include <string>





using namespace std;





/**
 * @brief Implements a simple interface for sending and receiving messages from/to a child process using the standard error channel.
 */
class CProcessCommunicationAdapter : public QObject
{
Q_OBJECT;

public:

    /**
     * @brief Creates the adapter for the provided child process and connects the child process to the adapter.
     * @param process  the process for which the adapater is created.
     */
    CProcessCommunicationAdapter( QProcess& process, char messageTerminator );

    /**
     * @brief Destructs the adapter disconnects the child process from the adapter.
     */
    ~CProcessCommunicationAdapter();

    /**
     * @brief Sends the proveded message to the child process.
     * @param message  the message to be sent
     */
    void sendMessage( string message );

    /**
     * @brief Waits until a message is received from the child process.
     * @return  the received message
     */
    string waitForResponse();

    /**
     * @brief Returns whether the adapter is currently waiting for a response of the child process.
     *        (This may be important in case the adapter is used together with other Qt events, e. g. timer events,
     *        since waiting for a response executes the event loop which may result in running events - which can
     *        contradict the idea of waiting for a response in a "blocking" way. Events that should not be excecuted
     *        while waiting for a response should return immediately when waiting is true.)
     * @return
     */
    bool getIsWaiting();

private:

    /**
     * @brief The process for which the adapter is created.
     */
    QProcess& process;

    /**
     * @brief The symbol terminating a message (for both send and receive).
     */
    char messageTerminator;

    /**
     * @brief Mutex object for making receiving messages thread safe.
     */
    QMutex mutex;

    /**
     * @brief The message that is currently received from the child process (but not finished yet).
     */
    string currentMessage;

    /**
     * @brief The last message from the child process that was completely received.
     */
    string lastCompletedMessage;

    /**
     * @brief Indicates whether the adapter is currently waiting for a response of the child process.
     */
    bool isWaiting;

    /**
     * @brief Returns the last message from the child process that was completely received.
     * @return  the last message from the child process that was completely received
     */
    string& getLastCompletedMessage();

    /**
     * @brief Resets the last message from the child process that was completely received to an empty string.
     */
    void resetLastCompletedMessage();

private slots:

    /**
     * @brief Callback that is called in case of output from the child process.
     */
    void onProcessOutput();
};


#endif // PROCESSCOMMUNICATIONADAPTER_HPP
