/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <fstream>
#include <QThread>
#include <QTime>
#include <QDir>
#include "ExternalJavaAgentInterface.hpp"
#include "./../Constants.h"





using namespace std;





//------------------------------------------------------------
// Construction
//------------------------------------------------------------

int CExternalJavaAgentInterface::instanceCounter = 0;

CExternalJavaAgentInterface::CExternalJavaAgentInterface()
{
    instanceCounter++;

    pJavaProcess = nullptr;

    pProcessCommunicationAdapter = nullptr;
}



//------------------------------------------------------------
// Destruction
//------------------------------------------------------------

CExternalJavaAgentInterface::~CExternalJavaAgentInterface()
{
    instanceCounter--;

    if( isInitialized() )
    {
        // Create command
        string message = stop();
        cout << message;

        // Do finishing of the process with the corresponding adapter
        pJavaProcess->close();
        delete pProcessCommunicationAdapter;
        pProcessCommunicationAdapter = nullptr;
        delete  pJavaProcess;
        pJavaProcess = nullptr;
    }

    // Do clean up
    cleanUp();
}



//------------------------------------------------------------
// Commands
//------------------------------------------------------------


string CExternalJavaAgentInterface::hello()
{
    // If the Java external agent interface was not correctly initialized, return immediately!
    if( !isInitialized() )
    {
        cout << EXTERNAL_AGENT_INTERFACE_MSG_ERROR_NO_INIT << endl;
        return string();
    }

    // Create command
    pProcessCommunicationAdapter->sendMessage( "hello" );

    // Wait until command is being processed
    string message = waitForResponse();

    return message;
}


string CExternalJavaAgentInterface::stop()
{
    // If the Java external agent interface was not correctly initialized, return immediately!
    if( !isInitialized() )
    {
        cout << EXTERNAL_AGENT_INTERFACE_MSG_ERROR_NO_INIT << endl;
        return string();
    }

    // Create command
    pProcessCommunicationAdapter->sendMessage( "stop" );

    // Wait until command is being processed
    string message = waitForResponse();

    return message;
}


void CExternalJavaAgentInterface::print( const string& text )
{
    // If the Java external agent interface was not correctly initialized, return immediately!
    if( !isInitialized() )
    {
        cout << EXTERNAL_AGENT_INTERFACE_MSG_ERROR_NO_INIT << endl;
        return;
    }

    // Create command
    string command = "print\n";
    command += text;
    pProcessCommunicationAdapter->sendMessage( command );

    // Wait until command is being processed
    string message = waitForResponse();
}


string CExternalJavaAgentInterface::update( const string& data )
{
    // If the Java external agent interface was not correctly initialized, return immediately!
    if( !isInitialized() )
    {
        cout << EXTERNAL_AGENT_INTERFACE_MSG_ERROR_NO_INIT << endl;
        return string();
    }


    //
    // Create command
    //

    string command = "update\n";

    // If data ends with newline(s), erase (terminator should be in the same line as the last transferred information)
    if( data[ data.length() - 1 ] == '\n' )
    {
        string dataWithoutNewlineAtTheEnd = data;
        do
        {
            dataWithoutNewlineAtTheEnd.erase( data.length() - 1, 1 );
        }
        while( dataWithoutNewlineAtTheEnd[ dataWithoutNewlineAtTheEnd.length() - 1 ] == '\n' );

        command += dataWithoutNewlineAtTheEnd;
    }
    else
    {
        command += data;
    }

    pProcessCommunicationAdapter->sendMessage( command );


    // Wait until command is being processed
    string message = waitForResponse();

    return message;
}


double CExternalJavaAgentInterface::evaluation( const string& parameters )
{
    // If the Java external agent interface was not correctly initialized, return immediately!
    if( !isInitialized() )
    {
        cout << EXTERNAL_AGENT_INTERFACE_MSG_ERROR_NO_INIT << endl;
        return -999;  // Return a remarkable number which is returned in case of error
    }

    // Create command
    string command = "evaluation\n";
    command += parameters;
    pProcessCommunicationAdapter->sendMessage( command );

    // Wait until command is being processed
    string message = waitForResponse( /* To limit the agent's evaluation time, put a timeout here! */ );

    // Convert response to double if possible
    // (Interestingly, the conversion seems to be able to handle the Java string representation of positive/negative infinity,
    // which is "Infinity"/"-Infinity"!)
    double result = -999;  // Initialize with a remarkable number which is returned in case of error
    try
    {
        string::size_type size;
        result = stod( message, &size );
    }
    catch( const std::invalid_argument e )
    {
        cout << EXTERNAL_AGENT_INTERFACE_MSG_ERROR_EVALUATION_FUNCTION << " Response message was: " << message << endl;
    }

    return result;
}


void CExternalJavaAgentInterface::reward( const string& parameters )
{
    // If the Java external agent interface was not correctly initialized, return immediately!
    if( !isInitialized() )
    {
        cout << EXTERNAL_AGENT_INTERFACE_MSG_ERROR_NO_INIT << endl;
    }

    // Create command
    string command = "reward\n";
    command += parameters;
    pProcessCommunicationAdapter->sendMessage( command );

    // Wait until command is being processed
    string message = waitForResponse( /* To limit the agent's reward processing time, put a timeout here! */ );
}


void CExternalJavaAgentInterface::communication( const string& parameters )
{
    // If the Java external agent interface was not correctly initialized, return immediately!
    if( !isInitialized() )
    {
        cout << EXTERNAL_AGENT_INTERFACE_MSG_ERROR_NO_INIT << endl;
    }

    // Create command
    string command = "communication\n";
    command += parameters;
    pProcessCommunicationAdapter->sendMessage( command );

    // Wait until command is being processed
    string message = waitForResponse( /* To limit the agent's communication time, put a timeout here! */ );
}


//------------------------------------------------------------
// Helper
//------------------------------------------------------------


string CExternalJavaAgentInterface::waitForResponse( int timeOut )
{
    return( pProcessCommunicationAdapter->waitForResponse() );
}


void CExternalJavaAgentInterface::initialize( const string& sAgentName )
{
    // If the process exists, shut down first!
    if( pJavaProcess != nullptr )
    {
        // Create command
        string message = stop();
// Not needed to show the stop reponse message when reinitializing!
//        cout << message << endl;

        // Do finishing of the process with the corresponding adapter
        pJavaProcess->close();
        delete pProcessCommunicationAdapter;
        pProcessCommunicationAdapter = nullptr;
        delete pJavaProcess;
        pJavaProcess = nullptr;

        // Do clean up
        cleanUp();
    }

    // Store path and file name
    this->sAgentName = sAgentName;

    // Compile the java agent interface and server
    string compileOutput = compile();

    if(    (compileOutput.find( EXTERNAL_AGENT_INTERFACE_JAVA_COMPILER_OUTPUT_ERROR_INDICATOR1 ) == string::npos)
        && (compileOutput.find( EXTERNAL_AGENT_INTERFACE_JAVA_COMPILER_OUTPUT_ERROR_INDICATOR2 ) == string::npos)
        && (compileOutput.find( EXTERNAL_AGENT_INTERFACE_JAVA_COMPILER_OUTPUT_ERROR_INDICATOR3 ) == string::npos) )
    {
        // Get agent directory
        QDir agentDir = QDir::current();
        agentDir.cd( JAVA_AGENT_FILES_REL_PATH );
        agentDir.cd( sAgentName.c_str() );

        // Create the classpath string with all .jar files (if there are any)
        QString classPathWithJarFiles = ".";
        QStringList fileList = agentDir.entryList( QDir::Files );
        for( int i = 0; i < fileList.size(); i++ )
        {
            if( fileList[ i ].endsWith( ".jar" ) || fileList[ i ].endsWith( ".JAR" ) )
            {
                classPathWithJarFiles += ";" + fileList[ i ];
            }
        }

        // Configure and create process and the corresponding communication adapter
        QStringList arguments;
        arguments << "-cp" << classPathWithJarFiles << JAVA_AGENT_SERVER_FILE_NAME_NO_EXT;
        pJavaProcess = new QProcess();
        pJavaProcess->setWorkingDirectory( agentDir.path() );
//      pJavaProcess->setProcessChannelMode( QProcess::ForwardedChannels );
//      pJavaProcess->setReadChannel( QProcess::ProcessChannel::StandardOutput );
        pProcessCommunicationAdapter = new CProcessCommunicationAdapter( *pJavaProcess, EXTERNAL_AGENT_INTERFACE_DATA_TERMINATOR );

        // Start process
        pJavaProcess->start( "java", arguments );
        if( !pJavaProcess->waitForStarted() )
        {
            cout << EXTERNAL_AGENT_INTERFACE_MSG_ERROR_LOADING_JAVA << endl;

            // Do finishing of the process with the corresponding adapter
            pJavaProcess->close();
            delete pProcessCommunicationAdapter;
            pProcessCommunicationAdapter = nullptr;
            delete  pJavaProcess;
            pJavaProcess = nullptr;
        }
        else
        {
            // Wait for ready
            string message = waitForResponse();

            // Print ready message
            print( message );
        }
    }
}


bool CExternalJavaAgentInterface::isInitialized()
{
    return( pJavaProcess != nullptr );
}


bool CExternalJavaAgentInterface::isWaiting()
{
    if( pProcessCommunicationAdapter == nullptr )
    {
        return( false );
    }
    else
    {
        return( pProcessCommunicationAdapter->getIsWaiting() );
    }
}


void CExternalJavaAgentInterface::flushConsole()
{
    if( pJavaProcess != nullptr )
    {
        cout << pJavaProcess->readAllStandardOutput().data();
    }
}


string CExternalJavaAgentInterface::compile()
{
    cout << "Compiling agent..." << endl;

    // Perform clean up (remove all class files and jar/java files without the agent server)
    cleanUp();

    // Copy the agent server to current selected agent directory
    // (Here are probably some issues with mixed native/non-native path separators)
    QString agentInterfaceServerFilename = QString( JAVA_AGENT_SERVER_FILE_NAME_NO_EXT ) + ".java";
    QDir agentServerDirectory = QDir::current();  /* This variable must not be named "agentServerDir", otherwise the debugger will provide an error everytime when stepping into the method "compile()". */
    QDir agentDir = QDir::current();
    agentDir.cd( JAVA_AGENT_FILES_REL_PATH );
    agentDir.cd( sAgentName.c_str() );
    QFile::copy( agentServerDirectory.path() + QDir::separator() + agentInterfaceServerFilename, agentDir.path() + QDir::separator() + agentInterfaceServerFilename );

    // Create the classpath string with all .jar files (if there are any)
    QString classPathWithJarFiles = ".";
    QStringList fileList = agentDir.entryList( QDir::Files );
    for( int i = 0; i < fileList.size(); i++ )
    {
        if( fileList[ i ].endsWith( ".jar" ) || fileList[ i ].endsWith( ".JAR" ) )
        {
            classPathWithJarFiles += ";" + fileList[ i ];
        }
    }

    // Configure process
    QStringList arguments;
    arguments << "-cp" << classPathWithJarFiles << agentInterfaceServerFilename;
    QProcess* pJavaCompilerProcess = new QProcess();
    pJavaCompilerProcess->setWorkingDirectory( agentDir.path() );
//    pJavaCompilerProcess->setProcessChannelMode( QProcess::ForwardedChannels );
//    pJavaCompilerProcess->setReadChannel( QProcess::ProcessChannel::StandardOutput );

    // Start process and wait until finished
    pJavaCompilerProcess->start( "javac", arguments );
    if( !pJavaCompilerProcess->waitForFinished() )
    {
        cout << EXTERNAL_AGENT_INTERFACE_MSG_ERROR_LOADING_JAVA << endl;

        // Close and delete the process
        pJavaCompilerProcess->close();
        delete pJavaCompilerProcess;
        pJavaCompilerProcess = nullptr;

        return string( EXTERNAL_AGENT_INTERFACE_JAVA_COMPILER_OUTPUT_ERROR_INDICATOR1 );
    }
    else
    {
        // Read compiler output
        string sStdOut = string( pJavaCompilerProcess->readAllStandardOutput().constData() );
        string sStdErr = string( pJavaCompilerProcess->readAllStandardError().constData() );

        // Write compiler output to console, if any
        if( (sStdOut + sStdErr) != "" )
        {
            cout << sStdOut << sStdErr << endl;
        }

        // Close and delete the process
        pJavaCompilerProcess->close();
        delete pJavaCompilerProcess;
        pJavaCompilerProcess = nullptr;

        return (sStdOut + sStdErr);
    }
}


void CExternalJavaAgentInterface::cleanUp()
{
    // Get the current agent directory
    QDir agentDir = QDir::current();
    agentDir.cd( JAVA_AGENT_FILES_REL_PATH );
    agentDir.cd( sAgentName.c_str() );

    // Remove old class files in the current agent directory
    QStringList fileExt;
    fileExt.append( "*.class" );
    QStringList classFiles =  agentDir.entryList( fileExt, QDir::Files );
    for( QStringList::iterator itString = classFiles.begin(); itString != classFiles.end(); itString++ )
    {
        QFile classFile( agentDir.path() + QDir::separator() + *itString );
        classFile.remove();
    }

    // Remove agent server
    QFile::remove( agentDir.path() + QDir::separator() + JAVA_AGENT_SERVER_FILE_NAME_NO_EXT + ".java" );
}

