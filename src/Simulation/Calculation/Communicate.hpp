/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined Communicate_hmol
	#define Communicate_hmol





class CStation;
class CAgent;

class CExternalJavaAgentInterface;





/**
 * The method implements the communacation of agents and stations for the next
 * simulation step.
 *
 * @param timeStep   the current time step
 */
//method MCommunicate( int timeStep )
class MCommunicate
{
#define METHOD_PARAMS_COMMUNICATE int timeStep, CExternalJavaAgentInterface* pExternalJavaAgentInterface

public:

	/**
	 * Implements the communacation of a station.
	 */
//	void <CStation*>();
    static void handling( CStation* that, METHOD_PARAMS_COMMUNICATE );

	/**
	 * Implements the communacation of an agent.
	 */
//	void <CAgent*>();
    static void handling( CAgent* that, METHOD_PARAMS_COMMUNICATE );
};





#endif
