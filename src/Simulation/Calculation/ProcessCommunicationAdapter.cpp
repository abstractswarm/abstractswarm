/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "ProcessCommunicationAdapter.hpp"
#include <QCoreApplication>
#include <iostream>





using namespace std;





CProcessCommunicationAdapter::CProcessCommunicationAdapter( QProcess& process, char messageTerminator ) : process( process )
{
    this->messageTerminator = messageTerminator;
    isWaiting = false;

    process.connect( &process, SIGNAL(readyReadStandardError()), this, SLOT(onProcessOutput()) );
}


CProcessCommunicationAdapter::~CProcessCommunicationAdapter()
{
    process.disconnect( &process, SIGNAL(readyReadStandardError()), this, SLOT(onProcessOutput()) );
}


string& CProcessCommunicationAdapter::getLastCompletedMessage()
{
    return lastCompletedMessage;
}



void CProcessCommunicationAdapter::resetLastCompletedMessage()
{
    lastCompletedMessage = "";
}


void CProcessCommunicationAdapter::sendMessage( string message )
{
    // Add a terminal symbol to the message and send it to the child process
    message += messageTerminator;
    process.write( message.c_str() );
}


string CProcessCommunicationAdapter::waitForResponse()
{
    isWaiting = true;

    // Wait until a message was completley received
    string message;
    while( message == "" )
    {
        // Processing the events is needed here since otherwise the callback where the output of the child process
        // is processed whill never be executed (ISWAITING CAN BE QUERIED EXTERNALLY TO AVOID OTHER EVENTS FROM BEING
        // BEING PROCESSED WHILE THE ADAPTER IS WAITING IN A BLOCKING WAY)
        QCoreApplication::processEvents();

        message = getLastCompletedMessage();
    }

    // Reset the recently received message internally to emply string
    resetLastCompletedMessage();

    isWaiting = false;

    // ...and return a copy of the message
    return message;
}


bool CProcessCommunicationAdapter::getIsWaiting()
{
    return( isWaiting );
}


void CProcessCommunicationAdapter::onProcessOutput()
{
    mutex.lock();

    // Add all available data
    currentMessage += process.readAllStandardError().data();

    // Get the position of the message terminal symbol
    std::size_t terminalPos = currentMessage.find( messageTerminator );

    // If the message is complete
    if( terminalPos != string::npos )
    {
        // Store is as new last received completed message
        lastCompletedMessage = currentMessage.substr( 0, terminalPos );

        // Remove line break (\r\n) which is remaining in some cases
        if( lastCompletedMessage.front() == '\r' )
        {
            lastCompletedMessage = lastCompletedMessage.substr( 2, lastCompletedMessage.size() );
        }

        // Reset the current message
        currentMessage = "";
    }

    mutex.unlock();
}
