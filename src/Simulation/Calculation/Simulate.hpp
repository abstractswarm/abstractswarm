/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined Simulate_hmol
	#define Simulate_hmol





class CStation;
class CAgent;

/** Type for pointer to function to add an enter event entry (agent starts visiting a station) to timetable. */
typedef void (*TfpAddEnterEvent)( const CAgent*, const CStation*, int );





/**
 * The method calculates a simulation step for a component every time it is called.
 *
 * @param timeStep the current time step that will be calculated
 */
//method MSimulate( int timeStep )
class MSimulate
{
#define METHOD_PARAMS_SIMULATE int timeStep

public:

	/**
	 * Calculates the next simulation step for a station.
	 */
//	void <CStation*>();
    static void handling( CStation* that, METHOD_PARAMS_SIMULATE );

	/**
	 * Calculates a simulation step for an agent.
	 */
//	void <CAgent*>();
    static void handling( CAgent* that, METHOD_PARAMS_SIMULATE );
};



//------------------------------------------------------------
// Helper
//------------------------------------------------------------


/**
 * @brief Returns true if the given agent is allowed to enter its current target station according to the contraints implied by place edges.
 * @param agent  the agent to be checked
 * @return       true if the given agent is allowed to enter its current target station, false otherwise
 */
bool checkPlaceEdgeContraints( const CAgent& agent );





#endif
