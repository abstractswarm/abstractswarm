/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined ChooseAction_hmol
    #define ChooseAction_hmol





class CStation;
class CAgent;

class CExternalJavaAgentInterface;





/**
 * The method implements the action selection behavior of agents and stations for the 
 * next simulation step.
 *
 * @param strategy                     the decision strategy (see Constants.h)
 * @param time                         the current time step
 * @param pExternalJavaAgentInterface  the external java agent interface to be called for an agent's decision (may be nullptr if not needed, e. g., if the given strategy is not Java or if MChooseAction is called for a station)
 */
//method MChooseAction( int strategy, int time )
class MChooseAction
{
#define METHOD_PARAMS_CHOOSEACTION int strategy, int time, CExternalJavaAgentInterface* pExternalJavaAgentInterface

public:

    /**
	 * Selects the next action for a station. 
	 */
//	void <CStation*>();
    static void handling( CStation* that, METHOD_PARAMS_CHOOSEACTION );

	/**
	 * Selects the next action for an agent. 
	 */
//	void <CAgent*>();
    static void handling( CAgent* that, METHOD_PARAMS_CHOOSEACTION );
};





#endif
