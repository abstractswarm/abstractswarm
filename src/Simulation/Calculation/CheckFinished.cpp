/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------

#include "./../../Editor/VisitEdge.h"
#include "./../../Editor/PlaceEdge.h"
#include "./../../Editor/TimeEdge.h"
#include ".//..//Components//Component.hpp"
#include ".//..//Constants.h"
#include <typeinfo>
//#pragma c-mol_hdrstop

#include "CheckFinished.hpp"





using namespace std;





//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//bool MCheckFinished::<CVisitEdge*>()
bool MCheckFinished::handling( CVisitEdge* that, METHOD_PARAMS_CHECKFINISHED )
{
	// Edge cannot falsify end of a simulation if not completely connected
	if( !that->getConnector1().getIsConnected() || !that->getConnector2().getIsConnected() )
	{
		return( true );
	}

	// Determine the opposite connector
	CEdgeConnector* pOppositeConnector = &(that->getConnector1)();
	CEdgeConnector* pComponentsConnector = &(that->getConnector2());
	if( &(pOppositeConnector->getComponentType()) == &(component.getComponentType()) )
	{
		pOppositeConnector = &(that->getConnector2());
		pComponentsConnector = &(that->getConnector1());
	}
	
	// For all opposite components
	vector<CComponent*>::const_iterator itComponent;
	for( itComponent = pOppositeConnector->getComponentType().getComponents().begin(); itComponent != pOppositeConnector->getComponentType().getComponents().end(); itComponent++ )
	{
		// Check if the given component is fished according to the current iterator element
		bool componentHasFrequency = (*itComponent)->getComponentType().getHasFrequency();
		bool componentFrequencyFulfilled = (*itComponent)->getEnterEventCount() >= (*itComponent)->getComponentType().getFrequency();
		bool componentHasNecessity = (*itComponent)->getComponentType().getHasNecessity();
		bool componentNecessityFulfilled = false;
		if( componentHasNecessity )
			componentNecessityFulfilled = (*itComponent)->getNecessity( component ) >= (*itComponent)->getComponentType().getNecessity();
		if(    (!componentHasFrequency && !componentHasNecessity) 
			|| (componentHasFrequency && !componentFrequencyFulfilled) 
			|| (componentHasNecessity && !componentNecessityFulfilled) )
		{
			// In this case, visit edge cannot state the end of the simulation
			return( false );
		}
	}

	return( true );
}


//bool MCheckFinished::<CPlaceEdge*, CTimeEdge*>()
bool MCheckFinished::handling( CPlaceEdge* that, METHOD_PARAMS_CHECKFINISHED )
{
	return( true );
}
bool MCheckFinished::handling( CTimeEdge* that, METHOD_PARAMS_CHECKFINISHED )
{
    return( true );
}
