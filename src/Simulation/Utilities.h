/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined Utilities_h
	#define Utilities_h





class CComponent;





/**
 * Generates a random number up to and including the given bounds.
 *
 * @param from   the lower bound
 * @param to     the upper bound
 * @return       random number up to and including the given bounds
 */
int randomNumber( int from, int to );


/**
 * Compares the priorities of two components and returns true if 
 * component1 has a higher priority than component2.
 * <p>
 * The function also returns the correct result in case only one component
 * or none of the two component's types has a priority attribute set.
 *
 * @param component1   the first component
 * @param component2   the second component
 * @return             true if component1 has a higher priority than component2, false otherwise
 */
bool comparePriorities( const CComponent* component1, const CComponent* component2 );





#endif
