/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Simulation.h"


namespace SimulationDll
{
    //------------------------------------------------------------
    // Construction/Destruction
    //------------------------------------------------------------

    void createSimulation( string sApplicationPath, string sAgentName = "", bool createConsole = true );


    void deleteSimulation();




    //------------------------------------------------------------
    // Adding/removing components
    //------------------------------------------------------------


    void addStation( CStation* pStation );


    void addAgent( CAgent* pAgent );


    void removeStation( CStation* pStation );


    void removeAgent( CAgent* pAgent );





    //------------------------------------------------------------
    // Simulation
    //------------------------------------------------------------

    void simulateStep( HDC graphicContext = nullptr );


    bool getIsFinished();


    bool getIsValid();


    void setAgentFile( const char* pFileName );


    void reloadAgentJavaFile();


    void reset();



    //------------------------------------------------------------
    // UI and advanced communcation
    //------------------------------------------------------------

    void connectToTimetable( TfpAddAgentEnterEvent fpAddAgentEnterEvent, TfpAddAgentLeaveEvent fpAddAgentLeaveEvent );


    void flushConsole();



    //------------------------------------------------------------
    // Events
    //------------------------------------------------------------

    void mouseMove( int x, int y );



    //------------------------------------------------------------
    // Layout independant functionality and state setters
    //------------------------------------------------------------

    void setScrollX( int percentScrollX );


    void setScrollY( int percentScrollY );


    double setZoom( double zoomFactor, int& percentScrollX, int& percentScrollY );


    void setVisibleFrameSize( int width, int height );



    //------------------------------------------------------------
    // Platform dependent(!) interface for visualization
    //------------------------------------------------------------

    void draw( HDC graphicContext );
}
