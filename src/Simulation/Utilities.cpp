/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Utilities.h"
#include <stdlib.h>

#include ".//Components//Component.hpp"





int randomNumber( int from, int to )
{
	// Generate a random number
	int randomNumber = rand() % (to - from + 1) + from;
	
	// Return the generated number
	return( randomNumber );
}


bool comparePriorities( const CComponent* component1, const CComponent* component2 )
{
	if( component1->getComponentType().getHasPriority() && component2->getComponentType().getHasPriority() )
		return( component1->getComponentType().getPriority() > component2->getComponentType().getPriority() );
	else if( component1->getComponentType().getHasPriority() )
		return( true );
	else
		return( false );
}
