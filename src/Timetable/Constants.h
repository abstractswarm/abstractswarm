/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined Constants_h
    #define Constants_h





//------------------------------------------------------------
// General
//------------------------------------------------------------


#define PI 3.1416
#define INVALID_TIME (-1)



//------------------------------------------------------------
// Timetable
//------------------------------------------------------------


/** The width of surface (TIMETABLE_WIDTH * GRID_XSIZE * TIMETABLE_HEIGHT * GRID_YSIZE may not exceed 14546596). */
#define TIMETABLE_WIDTH  1000

/** The height of surface (TIMETABLE_WIDTH * GRID_XSIZE * TIMETABLE_HEIGHT * GRID_YSIZE may not exceed 14546596). */
#define TIMETABLE_HEIGHT 1000

/** The minimum possible zoom factor. */
#define MIN_ZOOM_FACTOR 0.2

/** The maximum possible factor. */
#define MAX_ZOOM_FACTOR 5

/** The width of the first column */
#define FIRST_COLUMN_WIDTH 10



//------------------------------------------------------------
// Visualization
//------------------------------------------------------------


/** The background color's red component value. */
#define TIMETABLE_BACKGROUND_RED   150

/** The background color's green component value. */
#define TIMETABLE_BACKGROUND_GREEN 150

/** The background color's blue component value. */
#define TIMETABLE_BACKGROUND_BLUE  150

/** The surface color's red component value. */
#define SURFACE_RED   200

/** The surface color's green component value. */
#define SURFACE_GREEN 200

/** The surface color's blue component value. */
#define SURFACE_BLUE  200


/** The vertical cell size in grid. */
#define GRID_XSIZE 15

/** The horizontal cell size in grid. */
#define GRID_YSIZE 25	


/** The grid color's red component value. */
#define GRID_RED   220

/** The grid color's red component value. */
#define GRID_GREEN 220			  

/** The grid color's red component value. */
#define GRID_BLUE  220			  





#endif
