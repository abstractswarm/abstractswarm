/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "Timetable.h"
#include "Constants.h"
#include "./../Simulation/Components/Agent.hpp"  // Included from simulation!
#include "./../Simulation/Components/Station.hpp"  // Included from simulation!
#include "TimetableColor.hpp"
#include <fstream>   // TEST: Time table test code!

//#pragma c-mol_hdrstop

// Platform dependant drawing interface
//#include "./Draw/DrawWithStyles.hmol"
#include "./Draw/TimetableDrawWithStyles.hpp"





using namespace std;





//------------------------------------------------------------
// Construction
//------------------------------------------------------------


CTimetable::CTimetable() : grid( GRID_XSIZE, GRID_YSIZE )
{
	// Initialize the currently known width and height of the visible area
	// (maximum until set by the UI)
	visibleFrameWidth = TIMETABLE_WIDTH * GRID_XSIZE;
	visibleFrameHeight = TIMETABLE_HEIGHT * GRID_YSIZE;

	// Set default values for timetable
	curMouseX = 0;
	curMouseY = 0;
	curScreenX = 0;
	curScreenY = 0;
	curZoomFactor = 1;

	// Initialize waiting times
    waitingTimeToLastEnterEvent = 0;
    totalWaitingTime = 0;
	bestTotalWaitingTime = INVALID_TIME;

	// Initialize the mode
	mode = mCompleteRun;
}





//------------------------------------------------------------
// Destruction
//------------------------------------------------------------


CTimetable::~CTimetable()
{
}



//------------------------------------------------------------
// Events
//------------------------------------------------------------


void CTimetable::mouseMove( int x, int y )
{
	// Set the incoming mouse position as current mouse position
	curMouseX = x;
	curMouseY = y;
}



//------------------------------------------------------------
// Layout independant functionality and state setters
//------------------------------------------------------------


void CTimetable::setScrollX( double percentScrollX )
{
    curScreenX = static_cast<int>( TIMETABLE_WIDTH*GRID_XSIZE*curZoomFactor*(percentScrollX/100.0) );
}


void CTimetable::setScrollY( double percentScrollY )
{
    curScreenY = static_cast<int>( TIMETABLE_HEIGHT*GRID_YSIZE*curZoomFactor*(percentScrollY/100.0) );
}


double CTimetable::setZoom( double zoomFactor, double& percentScrollX, double& percentScrollY )
{
	// Check zoom factor validity and make it valid if not
	if( zoomFactor < MIN_ZOOM_FACTOR )
		zoomFactor = MIN_ZOOM_FACTOR;
	else if( zoomFactor > MAX_ZOOM_FACTOR )
		zoomFactor = MAX_ZOOM_FACTOR;
	
	// Get old real mouse position
	double mouseOldX = screenXToX( curMouseX );
	double mouseOldY = screenYToY( curMouseY );
	
	// Set the new zoom factor
	curZoomFactor = zoomFactor;

	// Now, get the new real mouse position
	double mouseNewX = screenXToX( curMouseX );
	double mouseNewY = screenYToY( curMouseY );

	// Correct scroll position due to offset between old and new real mouse positions
	curScreenX = curScreenX-(xToScreenX( mouseNewX )-xToScreenX( mouseOldX ));
	curScreenY = curScreenY-(yToScreenY( mouseNewY )-yToScreenY( mouseOldY ));

	// Update the given percent scroll variables for UI
	// (in earlier versions the results were cast to int here, since percentScrollX/Y wer int variables; 
	// this has been changed, since the timetable is usually larger then the editor and the simulation and therefore needs more fin-grained zooming and scrolling)
    percentScrollX = (static_cast<double>( curScreenX )/(TIMETABLE_WIDTH*GRID_XSIZE*curZoomFactor))*100.0;
    percentScrollY = (static_cast<double>( curScreenY )/(TIMETABLE_HEIGHT*GRID_YSIZE*curZoomFactor))*100.0;

	// Return the new zoom factor
	return( curZoomFactor );
}


void CTimetable::setVisibleFrameSize( int width, int height )
{
	visibleFrameWidth = width;
	visibleFrameHeight = height;
}


void CTimetable::setMode( ENMode mode )
{
	this->mode = mode;
}



//------------------------------------------------------------
// Timetable
//------------------------------------------------------------

void CTimetable::clear()
{
	plan.clear();

	// Reset current total waiting time
	totalWaitingTime = 0;

	// Reset waiting time to last enter event
	waitingTimeToLastEnterEvent = 0;
}


void CTimetable::reset()
{
	// Clear timetable
	clear();

	// Clear the best plan as well
	bestTotalWaitingTime = INVALID_TIME;
	bestPlan.clear();
}


void CTimetable::keepBestPlan()
{
	// If there is no completly calculated plan yet or the currently calculated plan 
	// is better than the best calculated plan
	if( (bestPlan.size() == 0) || (totalWaitingTime < bestTotalWaitingTime) )
	{
		// Store it as new best plan
		bestPlan = plan;

		// Store the total waiting time as new best total waiting time
		bestTotalWaitingTime = totalWaitingTime;
	}
	
	// else restore the better plan
	else
	{
		// Restore the best plan
		plan = bestPlan;

		// Restore the best total waiting time
		totalWaitingTime = bestTotalWaitingTime;
	}
}

int CTimetable::getTotalWaitingTime()
{
	return( totalWaitingTime );
}



//------------------------------------------------------------
// Interface to simulation
//------------------------------------------------------------

void CTimetable::addAgentEnterEvent( const CAgent* pAgent, const CStation* pStation, int time )
{
	// If agent not yet in map
	if( plan.find( pAgent ) == plan.end() )
	{
		// Create new entry list for the agent
		plan[ pAgent ] = list<CEntry>();
	}


	//
	// Count total waiting time:
	//

	// If there were already entries in the list
	if( plan[ pAgent ].size() > 0 )
	{
		// Sum up the difference of the new and the preivous entry
		waitingTimeToLastEnterEvent += (time - plan[ pAgent ].back().getEndTime() - 1);
	}
	else
	{
		// Sum up difference from the beginning of the simulation
		waitingTimeToLastEnterEvent += (time - 1);
	}

	// Total waiting time is current sum of waiting time up to this enter event
	totalWaitingTime = waitingTimeToLastEnterEvent;


	// Add entry to list
	plan[ pAgent ].push_back( CEntry( pStation, time ) );


	//
	// TEST: Time table test code!
	//

	// Open log file for appending
	ofstream* pofsOutputFile = new ofstream( "events.log", ios_base::app );

	// Write a line with enter event
	(*pofsOutputFile) << time << ": " << pAgent->getAgentType().getName() << "." << pAgent->getNumber() << " enters " << pStation->getStationType().getName() << "." << pStation->getNumber() << endl;

	// Close log file
	delete pofsOutputFile;
}


void CTimetable::addAgentLeaveEvent( const CAgent* pAgent, const CStation* pStation, int time )
{
	// Set end time to the last entry in the entry list for the given agent
	plan[ pAgent ].back().setEndTime( time );


	// 
	// Add the sum of waiting time up to this leave event
	//

	// Count the waiting time up to this leave event
	int waitingTimeToTheEnd = 0;
	map<const CAgent*, list<CEntry> >::iterator itEntryList;
	for( itEntryList = plan.begin(); itEntryList != plan.end(); itEntryList++ )
	{
		if( !itEntryList->second.back().getIsOpen() )
			waitingTimeToTheEnd += time - itEntryList->second.back().getEndTime();
	}

	// Total waiting time is the sum of waiting time up to the last enter event, 
	// plus the sum of waiting time up to this leave event
	// (NOTE THAT THE COUNTING OF TOTAL WAITING TIME IS NOT OBJECTIVE AND DEPENDS ON 
	// WHAT THE USER WANTS)
	totalWaitingTime = waitingTimeToLastEnterEvent + waitingTimeToTheEnd;


	//
	// TEST: Time table test code!
	//

	// Open log file for appending
	ofstream* pofsOutputFile = new ofstream( "events.log", ios_base::app );

	// Write a line with enter event
	(*pofsOutputFile) << time << ": " << pAgent->getAgentType().getName() << "." << pAgent->getNumber() << " leaves " << pStation->getStationType().getName() << "." << pStation->getNumber() << endl;

	// Close log file
	delete pofsOutputFile;
}


//------------------------------------------------------------
// Platform dependent(!) interface for visualization
//------------------------------------------------------------


void CTimetable::draw( HDC graphicContext )
{
	// Init double-buffering
	HDC hBackbuffer;
	hBackbuffer = CreateCompatibleDC( graphicContext );
	HBITMAP theBitmap;
	theBitmap = CreateCompatibleBitmap( graphicContext, visibleFrameWidth, visibleFrameHeight );
	SelectObject( hBackbuffer, theBitmap );
    
	// Draw background
	HBRUSH hbrush, hbrushold;
    hbrush = static_cast<HBRUSH>( CreateSolidBrush( RGB( TIMETABLE_BACKGROUND_RED, TIMETABLE_BACKGROUND_GREEN, TIMETABLE_BACKGROUND_BLUE ) ) );
    hbrushold = static_cast<HBRUSH>( SelectObject( hBackbuffer, hbrush ) );
	Rectangle( hBackbuffer, 0, 0, visibleFrameWidth, visibleFrameHeight );
	SelectObject( hBackbuffer, hbrushold );
	DeleteObject( hbrush );
	
	// Draw editable surface
	int red = SURFACE_RED;
	int green = SURFACE_GREEN; 
	int blue = SURFACE_BLUE; 
    hbrush = static_cast<HBRUSH>( CreateSolidBrush( RGB( red, green, blue ) ) );
    hbrushold = static_cast<HBRUSH>( SelectObject( hBackbuffer, hbrush ) );
	Rectangle( hBackbuffer, xToScreenX( 0 ), yToScreenY( 0 ), xToScreenX( TIMETABLE_WIDTH ), yToScreenY( TIMETABLE_HEIGHT ) );
	SelectObject( hBackbuffer, hbrushold );
	DeleteObject( hbrush );

	// Draw grid
	int screenX = xToScreenX( 0 );
	int screenY = yToScreenY( 0 );
    CTimetableColor penColor( GRID_RED, GRID_GREEN, GRID_BLUE );
    CTimetableColor brushColor( 0, 0, 0 );
    CTimetableColor textColor( TIMETABLE_BACKGROUND_RED, TIMETABLE_BACKGROUND_GREEN, TIMETABLE_BACKGROUND_BLUE );
	bool isSelected = false;
//	MDrawWithStyles( hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, textColor, isSelected )°grid( TIMETABLE_WIDTH, TIMETABLE_HEIGHT );
    MTimetableDrawWithStyles::handling( grid, hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, textColor, isSelected, TIMETABLE_WIDTH, TIMETABLE_HEIGHT );

	// Decide which plan to draw
	map<const CAgent*, list<CEntry> >* pPlanToDraw = &plan;
	if( mode == mCompleteRun )
		pPlanToDraw = &bestPlan;

	// Draw agent rows and entries
	int row = 1;
	map<const CAgent*, list<CEntry> >::iterator itEntryList;
	for( itEntryList = pPlanToDraw->begin(); itEntryList != pPlanToDraw->end(); itEntryList++ )
	{
		// Draw agent rows
		int screenX = xToScreenX( 0 );
		int screenY = yToScreenY( row );
		int entryRed = itEntryList->first->getComponentType().color.getRed();
		int entryGreen = itEntryList->first->getComponentType().color.getGreen();
		int entryBlue = itEntryList->first->getComponentType().color.getBlue();
        CTimetableColor penColor( 0, 0, 0 );
        CTimetableColor brushColor( TIMETABLE_BACKGROUND_RED, TIMETABLE_BACKGROUND_GREEN, TIMETABLE_BACKGROUND_BLUE );
        CTimetableColor textColor( entryRed, entryGreen, entryBlue );
		bool isSelected = false;
//		MDrawWithStyles( hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, textColor, isSelected )<-(itEntryList->first)( itEntryList->first->getComponentType().getName(), itEntryList->first->getNumber() );
        MTimetableDrawWithStyles::handling( *(itEntryList->first), hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, textColor, isSelected, itEntryList->first->getComponentType().getName(), itEntryList->first->getNumber() );

		// Draw entries
		list<CEntry>::iterator itEntry;
		for( itEntry = itEntryList->second.begin(); itEntry != itEntryList->second.end(); itEntry++ )
		{
			int screenX = xToScreenX( itEntry->getStartTime() + FIRST_COLUMN_WIDTH - 1 );
			int screenY = yToScreenY( row );
			int entryRed = itEntry->getStation()->getComponentType().color.getRed();
			int entryGreen = itEntry->getStation()->getComponentType().color.getGreen();
			int entryBlue = itEntry->getStation()->getComponentType().color.getBlue();
            CTimetableColor penColor( 0, 0, 0 );
            CTimetableColor brushColor( entryRed, entryGreen, entryBlue );
            CTimetableColor textColor( 0, 0, 0 );
			bool isSelected = false;
//			MDrawWithStyles( hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, textColor, isSelected )<-itEntry( itEntry->getStation()->getComponentType().getName(), itEntry->getStation()->getNumber() );
            MTimetableDrawWithStyles::handling( *itEntry, hBackbuffer, screenX, screenY, curZoomFactor, penColor, brushColor, textColor, isSelected, itEntry->getStation()->getComponentType().getName(), itEntry->getStation()->getNumber() );
        }

		// Increment row
		row++;
	}

	// Show back-buffer
	BitBlt( graphicContext, 0, 0, TIMETABLE_WIDTH * GRID_XSIZE, TIMETABLE_HEIGHT * GRID_YSIZE, hBackbuffer, 0, 0, SRCCOPY );

	// Delete back-buffer device context and bitmap
	DeleteDC( hBackbuffer );
	DeleteObject( theBitmap );
}



//------------------------------------------------------------
// Helper
//------------------------------------------------------------


int CTimetable::xToScreenX( double x ) const
{
	int scrollOffset = curScreenX;
	double gridSize = static_cast<double>( GRID_XSIZE );

    return( static_cast<int>( (x*gridSize*curZoomFactor) - scrollOffset ) );
}


int CTimetable::yToScreenY( double y ) const
{
	int scrollOffset = curScreenY;
	double gridSize = static_cast<double>( GRID_YSIZE );

    return( static_cast<int>( (y*gridSize*curZoomFactor) - scrollOffset ) );
}


double CTimetable::screenXToX( int x ) const
{
	int scrollOffset = curScreenX;
	double gridSize = static_cast<double>( GRID_XSIZE );
	
	return( (x + scrollOffset)/gridSize/curZoomFactor );
}


double CTimetable::screenYToY( int y ) const
{
	int scrollOffset = curScreenY;
	double gridSize = static_cast<double>( GRID_YSIZE );

	return( (y + scrollOffset)/gridSize/curZoomFactor );
}
