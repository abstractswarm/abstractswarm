/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined TimetableColor_hpp
    #define TimetableColor_hpp





/**
 * Objects of this class represent a changable RGB-color.
 */
class CTimetableColor
{
	public:

		/**
		 * Creates and initializes a RGB-color.
		 *
		 * @param red     the red component value of the color
		 * @param green   the green component value of the color
		 * @param blue    the blue component value of the color
		 */
        CTimetableColor( int red, int green, int blue );

		/** 
		 * Returns the red component value of the color.
		 *
		 * @return   the red component value of the color
		 */
		int getRed() const;

		/** 
		 * Returns the green component value of the color.
		 *
		 * @return   the green component value of the color
		 */
		int getGreen() const;

		/** 
		 * Returns the blue component value of the color.
		 *
		 * @return   the blue component value of the color
		 */
		int getBlue() const;

		/** 
		 * Sets the red component value of the color.
		 *
		 * @param red   the new red component value of the color
		 */
		void setRed( int red );

		/** 
		 * Sets the green component value of the color.
		 *
		 * @param green   the new red component value of the color
		 */
		void setGreen( int green );

		/** 
		 * Sets the blue component value of the color.
		 *
		 * @param blue   the new red component value of the color
		 */
		void setBlue( int blue );


	private:

		/** The red component value of the color. */
		int red;

		/** The green component value of the color. */
		int green;

		/** The blue component value of the color. */
		int blue;
};	





#endif
