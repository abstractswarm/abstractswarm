/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined Timetable_h
	#define Timetable_h





// Needed for platform dependent(!) visualization
#include "windows.h"

#include "TimetableTypes.h"
#include "Entry.hpp"
#include "TimetableGrid.h"
#include <map>
#include <list>





using namespace std;





class CTimetable 
{
	public: 

		// Construction
		CTimetable();

		// Destruction
		virtual ~CTimetable();

		/**
		 * Sets the new mouse x and y position of the timetable.
		 *
		 * @param x   the new mouse x position
		 * @param y   the new mouse y position
		 */
		void mouseMove( int x, int y );

		/** 
		 * Sets the new scroll position in x direction.
		 * (In contrast to editor/simulation the interface uses a double, since the timetable is larger than the editor/simulation and therefore needs more fine-grained zooming/scrolling.)
		 *
		 * @param percentScrollX   the new scroll x position in percent
		 */
		void setScrollX( double percentScrollX );

		/** 
		 * Sets the new scroll position in y direction.
		 * (In contrast to editor/simulation the interface uses a double, since the timetable is larger than the editor/simulation and therefore needs more fine-grained zooming/scrolling.)
		 *
		 * @param percentScrollY   the new scroll y position in percent
		 */
		void setScrollY( double percentScrollY );
		
		/**
		 * Sets the new zoom factor and calculates the new scroll x/y positions depending on 
		 * the new zoom factor. Returns the new zoom factor.
		 * (In contrast to editor/simulation the interface uses doubles, since the timetable is larger than the editor/simulation and therefore needs more fine-grained zooming/scrolling.)
		 *
		 * @param zoomFactor       the new zoom factor
		 * @param percentScrollX   the new scroll y position in percent
		 * @param percentScrollY   the new scroll y position in percent
		 * @return                 the new zoom factor
		 */
		double setZoom( double zoomFactor, double& percentScrollX, double& percentScrollY );

		/**
		 * Sets the visible frame size according to the layout of the UI.
		 *
		 * @param width   the width of the visible area according to the layout of the UI
		 * @param height   the height of the visible area according to the layout of the UI
		 */
		void setVisibleFrameSize( int width, int height );
		
		/**
		 * Sets the current running mode of the timetable (needed to decide whether the current or the best plan should be drawn).
		 *
		 * @param mode   the new mode in which the timetable (the simulation) is currently running
		 */
		void setMode( ENMode mode );

		/** 
		 * Adds an agent enter event based on the given data to the current timetable.
		 *
		 * @param pAgent     the agent that is invovled in the enter event
		 * @param pStation   the station that is invovled in the enter event
		 * @param time       the time when the enter event took place
		 */
		void addAgentEnterEvent( const CAgent* pAgent, const CStation* pStation, int time );

		/** 
		 * Adds an agent leave event based on the given data to the current timetable.
		 *
		 * @param pAgent     the agent that is invovled in the leave event
		 * @param pStation   the station that is invovled in the leave event
		 * @param time       the time when the leave event took place
		 */
		void addAgentLeaveEvent( const CAgent* pAgent, const CStation* pStation, int time );

		/**
		 * Clears the whole timetable.
		 */
		void clear();

		/**
		 * Resets the whole timetable (by clearing it and by clearing the best plan in addition).
		 */
		void reset();

		/**
		 * Returns the total waiting time contained in the current timetable.
		 *
		 * @return   the total waiting time contained in the current timetable
		 */
		int getTotalWaitingTime();

		/**
		 * Checks the best plan againt the currently calculated plan and keeps the best 
		 * one by either copying the current plan to the best plan or vice versa.
		 * <p>
		 * The method should be called only on finshed plans since an unfinished plan
		 * normally has lower total waiting time.
		 */
		void keepBestPlan();

		/** 
		 * Draws the timetable (platform dependent(!) interface for visualization).
		 * 
		 * @param graphicContext   the handle to the graphic device context where the timetable should be visualized
		 */
		void draw( HDC graphicContext );


	private:

		/** The currently known width of the visible area. */
		int visibleFrameWidth;

		/** The currently known height of the visible area. */
		int visibleFrameHeight;

		/** The current mouse x position. */
		int curMouseX;

		/** The current mouse y position. */
		int curMouseY;

		/** The current scroll position along the x axis. */
		int curScreenX;

		/** The current scroll position along the y axis. */
		int curScreenY;

		/** The current zoom factor. */
		double curZoomFactor;

		/** The mode in which the timetable is currently running (needed to decide whether the current or the best plan should be drawn). */
		ENMode mode;

		/** The timetable's grid to be visualized as background. */
        CTimetableGrid grid;

		/** The plan that contains for every agent an entry list of its visit events. */
		map<const CAgent*, list<CEntry> > plan;

		/** The best currently calculated plan with the least waiting time. */
		map<const CAgent*, list<CEntry> > bestPlan;

		/** The total waiting time up to the last enter event. */
		int waitingTimeToLastEnterEvent;

		/** The total waiting time for the current plan. */
		int totalWaitingTime;

		/** The total waiting time for the best currently calculated plan. */
		int bestTotalWaitingTime;

		/** 
		 * Transforms the given grid x position to the corresponding screen x position.
		 * <p>
		 * The input parameter is a double for more precise scrolling behavior (e.g. if clicked not exactly in the middle of a grid cell).
		 *
		 * @param x   the grid x position to be transformed
		 */
		int xToScreenX( double x ) const;

		/** 
		 * Transforms the given grid y position to the corresponding screen y position.
		 * <p>
		 * The input parameter is a double for more precise scrolling behavior (e.g. if clicked not exactly in the middle of a grid cell).
		 *
		 * @param y   the grid y position to be transformed
		 */
		int yToScreenY( double y ) const;

		/** 
		 * Transforms the given screen x position to the corresponding grid x position.
		 * <p>
		 * The return value is a double for more precise scrolling behavior (e.g. if the given position is not exactly in the middle of a grid cell).
		 *
		 * @param x   the screen x position to be transformed
		 */
		double screenXToX( int x ) const;

		/** 
		 * Transforms the given screen y position to the corresponding grid y position.
		 * <p>
		 * The return value is a double for more precise scrolling behavior (e.g. if the given position is not exactly in the middle of a grid cell).
		 *
		 * @param y   the screen y position to be transformed
		 */
		double screenYToY( int y ) const;
};





#endif
