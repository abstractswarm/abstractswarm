/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "TimetableDll.h"


//------------------------------------------------------------
// Construction
//------------------------------------------------------------

static CTimetable* pTimetable;


void TimetableDll::createTimetable()
{
    pTimetable = new CTimetable();
}


void TimetableDll::deleteTimetable()
{
    delete pTimetable;
}





//------------------------------------------------------------
// Events
//------------------------------------------------------------

void TimetableDll::mouseMove( int x, int y )
{
    pTimetable->mouseMove( x, y );
}





//------------------------------------------------------------
// Layout independant functionality and state setters
//------------------------------------------------------------

void TimetableDll::setScrollX( double percentScrollX )
{
    pTimetable->setScrollX( percentScrollX );
}


void TimetableDll::setScrollY( double percentScrollY )
{
    pTimetable->setScrollY( percentScrollY );
}


double TimetableDll::setZoom( double zoomFactor, double& percentScrollX, double& percentScrollY )
{
    return( pTimetable->setZoom( zoomFactor, percentScrollX, percentScrollY ) );
}


void TimetableDll::setVisibleFrameSize( int width, int height )
{
    pTimetable->setVisibleFrameSize( width, height );
}


void TimetableDll::setMode( ENMode mode )
{
    pTimetable->setMode( mode );
}





//------------------------------------------------------------
// Timetable
//------------------------------------------------------------

void TimetableDll::clear()
{
    pTimetable->clear();
}


void TimetableDll::reset()
{
    pTimetable->reset();
}


void TimetableDll::keepBestPlan()
{
    pTimetable->keepBestPlan();
}


int TimetableDll::getTotalWaitingTime()
{
    return( pTimetable->getTotalWaitingTime() );
}





//------------------------------------------------------------
// Communication with simulation
//------------------------------------------------------------

void TimetableDll::addAgentEnterEvent( const CAgent* pAgent, const CStation* pStation, int time )
{
    pTimetable->addAgentEnterEvent( pAgent, pStation, time );
}


void TimetableDll::addAgentLeaveEvent( const CAgent* pAgent, const CStation* pStation, int time )
{
    pTimetable->addAgentLeaveEvent( pAgent, pStation, time );
}





//------------------------------------------------------------
// Platform dependent(!) interface for visualization
//------------------------------------------------------------

void TimetableDll::draw( HDC graphicContext )
{
    pTimetable->draw( graphicContext );
}
