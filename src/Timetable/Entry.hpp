/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined Entry_hpp
	#define Entry_hpp

#include "TimetableTypes.h"





/**
 * Objects of this class represent an entry in the time table, thus a visit event when and how long a station was visited.
 */
class CEntry
{
	public:
		
		/**
		 * Creates and initializes an entry of the timetable.
		 * 
		 * @param pStation    the station which was visited during the given time
		 * @param startTime   the time when the visit event startet
		 */
		CEntry( const CStation* pStation, int startTime );

		/**
		 * Destroys an entry of the timetable.
		 */
		~CEntry();

		/**
		 * Returns the station that was visited during the time represented by this entry.
		 * 
		 * @return   the visited station
		 */
		const CStation* getStation() const;

		/**
		 * Returns the start time of the visit event reprented by this entry.
		 * 
		 * @return   the start time of the visit event
		 */
		int getStartTime() const;

		/**
		 * Returns the end time of the visit event reprented by this entry.
		 * 
		 * @return   the end time of the visit event (invalid value if the entry is not closed yet)
		 */
		int getEndTime() const;

		/**
		 * Returns whether the entry represents a visit event that is not finished yet.
		 * 
		 * @return   true if the entry represents a visit event that is not finished yet, false otherwise
		 */
		bool getIsOpen() const;


		/**
		 * Sets the end time of the visit event reprented by this entry.
		 * 
		 * @param   the end time of the visit event
		 */
		void setEndTime( int endTime );

	
	private:

		/** The station which was visited during the visit event represented by this entry. */
		const CStation* pStation;

		/** The start time of the visit event. */
		int startTime;

		/** The end time of the visit event */
		int endTime;

		/** Value that represents whether the visit event represented by this entry is finished yet. */
		bool isOpen;
};





#endif
