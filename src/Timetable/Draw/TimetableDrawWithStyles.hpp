/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined TimetableDrawWithStyles_hmol
    #define TimetableDrawWithStyles_hmol

#include "windows.h"
#include <string>





class CTimetableGrid;
class CEntry;
class CTimetableColor;
class CAgent;





using namespace std;





/**
 * Handlings of this method visualize all timetable related components. 
 *
 * @param graphicContext   the graphic context to where the timetable components are drawn
 * @param screenX          the x position on the screen where to draw the components
 * @param screenY          the y position on the screen where to draw the components
 * @param zoomFactor       the current zoom factor to draw the timetable components
 * @param penColor         the outline color that is used to draw the timetable components
 * @param brushColor       the fill color that is used to draw the timetable components
 * @param isSelected       determines whether the components are drawn in selected style
 */
//method MDrawWithStyles( HDC graphicContext,
//					    int screenX,
//						int screenY,
//						double zoomFactor,
//						const CColor& penColor,
//						const CColor& brushColor,
//						const CColor& textColor,
//						bool isSelected )
class MTimetableDrawWithStyles
{
#define METHOD_PARAMS_DRAWWITHSTYLES HDC graphicContext, int screenX, int screenY, double zoomFactor, const CTimetableColor& penColor, const CTimetableColor& brushColor, const CTimetableColor& textColor, bool isSelected

public:

	/** 
	 * Creates all graphic handles and sets the given styles.
	 */
//	MDrawWithStyles();
    static void prologue( /*that,*/ METHOD_PARAMS_DRAWWITHSTYLES );

	/** 
	 * Releases all graphic handles. 
	 */
//	~MDrawWithStyles();
    static void epilogue( /*that,*/ METHOD_PARAMS_DRAWWITHSTYLES );

	/** 
	 * Visualizes the background grid of the timetable. 
	 *
     * @param width    the number of cells to be drawn in x direction
     * @param height   the number of cells to be drawn in y direction
	 */
//	void <CGrid>( int width, int height );
    static void handling( CTimetableGrid& that, METHOD_PARAMS_DRAWWITHSTYLES, int width, int height );

	/** 
	 * Visualizes an agent row in the timetable. 
	 *
     * @param agentNumber   the number of the agent defining where the row should be visualized
	 */
//	void <const CAgent>( const string& sAgentName, int agentNumber );
    static void handling( const CAgent& that, METHOD_PARAMS_DRAWWITHSTYLES, const string& sAgentName, int agentNumber );

	/** 
	 * Visualizes an entry in the timetable. 
	 *
     * @param row   the row where the entry should be visualized
	 */
//	void <CEntry>( const string& sStationName, int stationNumber );
    static void handling( CEntry& that, METHOD_PARAMS_DRAWWITHSTYLES, const string& sStationName, int stationNumber );

	/** 
	 * Visualizes a time unit number in the timetable. 
	 */
//	void <int>();
    static void handling( int& that, METHOD_PARAMS_DRAWWITHSTYLES );
};





#endif
