//------------------------------------------------------------
// Includes
//------------------------------------------------------------


#include "./../Constants.h"

// Needed for platform dependent(!) visualization
#include "windows.h"

#include "./../TimetableGrid.h"
#include "./../Entry.hpp"
#include "./../TimetableColor.hpp"
#include <climits>
#include <string>
#include <sstream>
//#pragma c-mol_hdrstop

//#include "DrawWithStyles.hmol"
#include "TimetableDrawWithStyles.hpp"





//------------------------------------------------------------
// Static data
//------------------------------------------------------------


static HPEN hPen, hPenOld; 
static HBRUSH hBrush, hBrushOld;
static HFONT hFont, hFontOld;





//------------------------------------------------------------
// Prologue
//------------------------------------------------------------


//MDrawWithStyles::MDrawWithStyles()
void MTimetableDrawWithStyles::prologue( METHOD_PARAMS_DRAWWITHSTYLES )
{

	// Create the pen style

	int penRed = penColor.getRed();
	int penGreen = penColor.getGreen();
	int penBlue = penColor.getBlue();
	
	int penWidth = 1;
	
	// If the object is selected, draw it with red frame lines
	if( isSelected )
	{
		penRed = 255;
		penGreen = 0;
		penBlue = 0;

		penWidth = 2;
	}

	LOGPEN logPen;
	logPen.lopnColor = RGB( penRed, penGreen, penBlue );
	logPen.lopnStyle = PS_SOLID;
	logPen.lopnWidth.x = penWidth;
	
	hPen = CreatePenIndirect( &logPen );
    hPenOld = static_cast<HPEN>( SelectObject( graphicContext, hPen ) );
	
	
	// Create the brush style
	
	int brushRed = brushColor.getRed();
	int brushGreen = brushColor.getGreen();
	int brushBlue = brushColor.getBlue();
    hBrush = static_cast<HBRUSH>( CreateSolidBrush( RGB( brushRed, brushGreen, brushBlue ) ) );
    hBrushOld = static_cast<HBRUSH>( SelectObject( graphicContext, hBrush ) );


	// Create the font style

	LOGFONT logFont;
    
    logFont.lfHeight = static_cast<LONG>( 14*zoomFactor );		// height of font
    logFont.lfWidth = 0;										// average character width
    logFont.lfEscapement = 0;									// angle of escapement
    logFont.lfOrientation = 0;									// base-line orientation angle
    logFont.lfWeight = FW_NORMAL;  /* (400) */					// font weight
    logFont.lfItalic = FALSE;									// italic attribute option
    logFont.lfUnderline = FALSE;								// underline attribute option
    logFont.lfStrikeOut = FALSE;								// strikeout attribute option
    logFont.lfCharSet = ANSI_CHARSET;							// character set identifier
    logFont.lfOutPrecision = OUT_DEFAULT_PRECIS;				// output precision
    logFont.lfClipPrecision = CLIP_DEFAULT_PRECIS;				// clipping precision
    logFont.lfQuality = ANTIALIASED_QUALITY;					// output quality
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_DONTCARE;	// pitch and family
//	strcpy( logFont.lfFaceName, "Arial" );						// typeface name
    // WORKAROUND: typeface name (for WCHAR)
    logFont.lfFaceName[ 0 ] = 'A'; logFont.lfFaceName[ 1 ] = 'r'; logFont.lfFaceName[ 2 ] = 'i'; logFont.lfFaceName[ 3 ] = 'a'; logFont.lfFaceName[ 4 ] = 'l';  logFont.lfFaceName[ 5 ] = '\0';

	hFont = CreateFontIndirect( &logFont );
    hFontOld = static_cast<HFONT>( SelectObject( graphicContext, hFont ) );

	
	// Set the text color
	
	int textRed = textColor.getRed();
	int textGreen = textColor.getGreen();
	int textBlue = textColor.getBlue();
	SetTextColor( graphicContext, RGB( textRed, textGreen, textBlue ) );


	// Make the background transparent
	
	SetBkMode( graphicContext, TRANSPARENT );
}


//------------------------------------------------------------
// Epilogue
//------------------------------------------------------------


//MDrawWithStyles::~MDrawWithStyles()
void MTimetableDrawWithStyles::epilogue( METHOD_PARAMS_DRAWWITHSTYLES )
{
	// Release pen
	SelectObject( graphicContext, hPenOld );
	DeleteObject( hPen );

	// Release brush
	SelectObject( graphicContext, hBrushOld );
	DeleteObject( hBrush );

	// Release font
	SelectObject( graphicContext, hFontOld );
	DeleteObject( hFont );
}


//------------------------------------------------------------
// Handlings
//------------------------------------------------------------


//void MDrawWithStyles::<CGrid>( int width, int height )
void MTimetableDrawWithStyles::handling( CTimetableGrid& that, METHOD_PARAMS_DRAWWITHSTYLES, int width, int height )
{
    prologue( graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, textColor, isSelected );

	// Get the grid's size
	int gridXSize = that.getXSize();
	int gridYSize = that.getYSize();

	// Draw vertical grid lines
	int x = 0;
	for( x = 1; x < width; x++ )
	{
		// Calculate current grid position relative to the whole grid's position on screen
        int relGridXPos = static_cast<int>( x*gridXSize*zoomFactor );
        int relGridYPos = static_cast<int>( height*gridYSize*zoomFactor );

		// Draw line
        MoveToEx( graphicContext, screenX+relGridXPos, screenY+1, nullptr );
		LineTo( graphicContext, screenX+relGridXPos, screenY+relGridYPos-1 );


		//
		// Draw time unit number
		//

		// Save the graphic handles to restore them after station types and agent types are drawn
		HPEN hGridPen = hPen;
		HPEN hGridPenOld	= hPenOld;
		HBRUSH hGridBrush = hBrush;
		HBRUSH hGridBrushOld	= hBrushOld;
		HFONT  hGridFont = hFont;
		HFONT  hGridFontOld = hFontOld;

//		MDrawWithStyles( graphicContext, screenX + ((FIRST_COLUMN_WIDTH - 1) * gridXSize * zoomFactor) + relGridXPos, screenY, zoomFactor, penColor, brushColor, textColor, isSelected )°x();
        MTimetableDrawWithStyles::handling( x, graphicContext, static_cast<int>( screenX + ((FIRST_COLUMN_WIDTH - 1) * gridXSize * zoomFactor) + relGridXPos ), screenY, zoomFactor, penColor, brushColor, textColor, isSelected );

		// Restore graphic handles for perspective
		hPen = hGridPen;
		hPenOld = hGridPenOld;
		hBrush = hGridBrush;
		hBrushOld = hGridBrushOld;
		hFont = hGridFont;
		hFontOld = hGridFontOld; 
	}

	// Draw horizontal grid lines
	int y = 0;
	for( y = 1; y < height; y++ )
	{
		// Calculate current grid position relative to the whole grid's position on screen
        int relGridXPos = static_cast<int>( width*gridXSize*zoomFactor );
        int relGridYPos = static_cast<int>( y*gridYSize*zoomFactor );

		// Draw line
        MoveToEx( graphicContext, screenX+1, screenY+relGridYPos, nullptr );
		LineTo( graphicContext, screenX+relGridXPos-1, screenY+relGridYPos );
	}

    epilogue( graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, textColor, isSelected );
}


//void MDrawWithStyles::<const CAgent>( const string& sAgentName, int agentNumber )
void MTimetableDrawWithStyles::handling( const CAgent& that, METHOD_PARAMS_DRAWWITHSTYLES, const string& sAgentName, int agentNumber )
{
    prologue( graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, textColor, isSelected );

    // Save the graphic handles to restore them after station types and agent types are drawn
	HPEN hPerspectivePen = hPen;
	HPEN hPerspectivePenOld	= hPenOld;
	HBRUSH hPerspectiveBrush = hBrush;
	HBRUSH hPerspectiveBrushOld	= hBrushOld;
    HFONT  hPerspectiveFont = hFont;
	HFONT  hPerspectiveFontOld = hFontOld;

	// Create a special entry with no station to visualize the first agent column
    CEntry agentEntry( nullptr, 0 );
	agentEntry.setEndTime( FIRST_COLUMN_WIDTH - 1 );
//	MDrawWithStyles( graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, textColor, isSelected )°agentEntry( sAgentName, agentNumber );
    MTimetableDrawWithStyles::handling( agentEntry, graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, textColor, isSelected, sAgentName, agentNumber );

	// Restore graphic handles for perspective
	hPen = hPerspectivePen;
	hPenOld = hPerspectivePenOld;
	hBrush = hPerspectiveBrush;
	hBrushOld = hPerspectiveBrushOld;
    hFont = hPerspectiveFont;
	hFontOld = hPerspectiveFontOld; 

    epilogue( graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, textColor, isSelected );
}


//void MDrawWithStyles::<CEntry>( const string& sStationName, int stationNumber )
void MTimetableDrawWithStyles::handling( CEntry& that, METHOD_PARAMS_DRAWWITHSTYLES, const string& sStationName, int stationNumber )
{
    prologue( graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, textColor, isSelected );

    //
	// Calculate zoomed width and height
	//

	int zoomedWidth = 0;

	// If the entry is an open interval, set the width to maximum
	if( that.getIsOpen() )
        zoomedWidth = static_cast<int>( TIMETABLE_WIDTH * GRID_XSIZE * zoomFactor );
	
	// ...else simply calculate the width from start and end time
	else
        zoomedWidth = static_cast<int>( (that.getEndTime() - that.getStartTime() + 1) * GRID_XSIZE * zoomFactor );
	
	// Calculate the height
    int zoomedHeight = static_cast<int>( GRID_YSIZE * zoomFactor );

	
	// Draw the entry	
	Rectangle( graphicContext, screenX, screenY, screenX + zoomedWidth + 1, screenY + zoomedHeight + 1 ); 


	//
	// Draw the station name
	//
	
	// Convert station number to string
	ostringstream ossStationNumber; 
	ossStationNumber << stationNumber;
	string sStationNumber = ossStationNumber.str();

	// Create string with station type and number
	string sStationNameWithNumber = sStationName + "." + sStationNumber;
    wchar_t wcStationNameWithNumber[ 512 ];  // NOTE THAT THE STATION NAME WITH NUMBER IS NOT ALLLOWED TO BE LONGER THAN 512
    mbstowcs( wcStationNameWithNumber, sStationNameWithNumber.c_str(), 512 );

	// Calculate text size on screen
	SIZE textSize;
    GetTextExtentPoint32( graphicContext, wcStationNameWithNumber, static_cast<int>( sStationNameWithNumber.length() ), &textSize );

	// Shorten the station type name until it fits with the agent number in the rectangle
	// (replace the end with '...' (horizontal ellipse) and make station type name stepwise smaller)
	string sNewStationName = sStationName;
	while( (textSize.cx > zoomedWidth) && (sNewStationName.length() > 1) )
	{
		// Shorten the station type name
        sNewStationName = sNewStationName.substr( 0, sNewStationName.length() - 2 ) + "_";  // Should be "horizontal ellipse" ("…") instead of "underscore" ("_") here, but this leads to encoding problems which causes/may cause an infinite while loop here

		// Add agent number
		sStationNameWithNumber = sNewStationName + "." + sStationNumber;
        mbstowcs( wcStationNameWithNumber, sStationNameWithNumber.c_str(), 512 );

		// Calculate new size on screen in pixels
        GetTextExtentPoint32( graphicContext, wcStationNameWithNumber, static_cast<int>( sStationNameWithNumber.length() ), &textSize );
	}

	// If only '...' (horizontal ellipse) remains as station type name, use number only
	if( sNewStationName.length() <= 1 )
    {
		sStationNameWithNumber = sStationNumber;
        mbstowcs( wcStationNameWithNumber, sStationNameWithNumber.c_str(), 512 );
    }

	// Calculate size on screen in pixels
    GetTextExtentPoint32( graphicContext, wcStationNameWithNumber, static_cast<int>( sStationNameWithNumber.length() ), &textSize );

	// Draw station type and agent number
    TextOut( graphicContext, screenX + 2, screenY + (zoomedHeight / 2) - (textSize.cy / 2), wcStationNameWithNumber, static_cast<int>( sStationNameWithNumber.length() ) );

    epilogue( graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, textColor, isSelected );
}


//void MDrawWithStyles::<int>()
void MTimetableDrawWithStyles::handling( int& that, METHOD_PARAMS_DRAWWITHSTYLES )
{
    prologue( graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, textColor, isSelected );

    // Modify current selected font by angle
	HFONT hCurFont;
    hCurFont = static_cast<HFONT>( GetCurrentObject( graphicContext, OBJ_FONT ) );
	LOGFONT logFont;
	GetObject( hCurFont, sizeof( LOGFONT ), &logFont );
    logFont.lfEscapement = static_cast<LONG>( +(((PI / 2.0) * 180) / PI) * 10 );  // angle of escapement

	// Create an select new modified font
	HFONT hFont = CreateFontIndirect( &logFont );
    HFONT hFontOld = static_cast<HFONT>( SelectObject( graphicContext, hFont ) );

	// Prepeare number text
	ostringstream ossUnitNumber; 
	ossUnitNumber << that;
	string sUnitNumber( ossUnitNumber.str() );  
    wchar_t wcUnitNumber[ 512 ];  // NOTE THAT THE UNIT NUMBER IS NOT ALLLOWED TO BE LONGER THAN 512
    mbstowcs( wcUnitNumber, sUnitNumber.c_str(), 512 );

	// Draw the unit number text and restore previous text alignment
	UINT oldTextAlign = GetTextAlign( graphicContext );
	SetTextAlign( graphicContext, TA_RIGHT );
    TextOut( graphicContext, screenX + 1, screenY + 1, wcUnitNumber, static_cast<int>( sUnitNumber.length() ) );
	SetTextAlign( graphicContext, oldTextAlign );
	
	// Release new modified font
	SelectObject( graphicContext, hFontOld );
	DeleteObject( hFont );

    epilogue( graphicContext, screenX, screenY, zoomFactor, penColor, brushColor, textColor, isSelected );
}
