/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Timetable.h"


namespace TimetableDll
{
	//------------------------------------------------------------
	// Construction
	//------------------------------------------------------------

	void createTimetable();


	void deleteTimetable();





	//------------------------------------------------------------
	// Events
	//------------------------------------------------------------

	void mouseMove( int x, int y );





	//------------------------------------------------------------
	// Layout independant functionality and state setters
	//------------------------------------------------------------

    void setScrollX( double percentScrollX );  // (In contrast to editor/simulation the interface uses a double, since the timetable is larger than the editor/simulation and therefore needs more fine-grained zooming/scrolling.)



    void setScrollY( double percentScrollY );  // (In contrast to editor/simulation the interface uses a double, since the timetable is larger than the editor/simulation and therefore needs more fine-grained zooming/scrolling.)


    double setZoom( double zoomFactor, double& percentScrollX, double& percentScrollY );  // (In contrast to editor/simulation the interface uses doubles, since the timetable is larger than the editor/simulation and therefore needs more fine-grained zooming/scrolling.)


	void setVisibleFrameSize( int width, int height );


	void setMode( ENMode mode );





	//------------------------------------------------------------
	// Timetable
	//------------------------------------------------------------

	void clear();


	void reset();


	void keepBestPlan();


	int getTotalWaitingTime();





	//------------------------------------------------------------
	// Communication with simulation
	//------------------------------------------------------------

	void addAgentEnterEvent( const CAgent* pAgent, const CStation* pStation, int time );


	void addAgentLeaveEvent( const CAgent* pAgent, const CStation* pStation, int time );





	//------------------------------------------------------------
	// Platform dependent(!) interface for visualization
	//------------------------------------------------------------

	void draw( HDC graphicContext );
}
