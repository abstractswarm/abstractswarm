/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "graphicsviewtimetable.h"
#include "mainwindow.h"
#include <QMouseEvent>



GraphicsViewTimetable::GraphicsViewTimetable( QWidget* parent, QWidget* mainWindow, int width, int height ) : GraphicsViewGDI( parent )
{
    // Set the size of this widget
    setGeometry( 0, 0, width, height );

    // Enable mouse move events without a mouse button being pressed
    setMouseTracking( true );

    // Init previous coordinates
    previousPressedX = 0;
    previousPressedY = 0;

    // Init the widget to which the callbacks will be delegated
    this->mainWindow = mainWindow;
}


GraphicsViewTimetable::~GraphicsViewTimetable()
{
}


void GraphicsViewTimetable::mouseMoveEvent( QMouseEvent* event )
{
    // TODO This delegation should be removed in the context of the refactoring!
    static_cast<MainWindow*>( mainWindow )->timetableMouseMoveEvent( event );
}


void GraphicsViewTimetable::mouseReleaseEvent( QMouseEvent* event )
{
    // TODO This delegation should be removed in the context of the refactoring!
    static_cast<MainWindow*>( mainWindow )->timetableMouseReleaseEvent( event );
}


void GraphicsViewTimetable::drawGDI( HDC hdc )
{
    // TODO This delegation should be removed in the context of the refactoring!
    static_cast<MainWindow*>( mainWindow )->timetableDrawGDI( hdc );
}
