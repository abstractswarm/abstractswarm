/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GRAPHICSVIEWEDITOR_H
#define GRAPHICSVIEWEDITOR_H


#include "graphicsviewgdi.h"
#include "./Editor/EditorTypes.h"


/**
 * @brief The class for displaying the native GDI content of the AbstractSwarm editor.
 */
class GraphicsViewEditor : public GraphicsViewGDI
{
    public:

        /**
         * @brief Creates a graphics view for the editor and loads the editor DLL for this purpose.
         *
         * @param parent  the widget where the graphics view should be visualized on
         * @param mainWindow  the widget to which the callbacks will be delegated
         * @param width  the graphics view's width
         * @param height  the graphics view's height
         */
        GraphicsViewEditor( QWidget* parent, QWidget* mainWindow, int width, int height );

        /**
         * @brief Frees the editor library.
         */
        virtual ~GraphicsViewEditor() Q_DECL_OVERRIDE;


    protected:

        /**
         * @brief keyPressEvent Processes key press events
         * @param event the key event to be processed
         */
       virtual void keyPressEvent( QKeyEvent* event ) override;

        /**
        * @brief keyReleaseEvent
        * @param event
        */
       virtual void keyReleaseEvent( QKeyEvent* event ) override;

        /**
         * @brief Processes mouse click events (i. e., a mouse press event
         *        followed by a mouse release event with the same x and y
         *        coordinates). The event is simulated by the release event.
         *
         * @param event  the mouse event to be processed
         */
        virtual void mouseClickEvent( QMouseEvent* event );

        /**
         * @brief Processes mouse double click events.
         *
         * @param event  the mouse event to be processed
         */
        virtual void mouseDoubleClickEvent( QMouseEvent* event ) Q_DECL_OVERRIDE;

        /**
         * @brief Processes mouse press events.
         *
         * @param event  the mouse event to be processed
         */
        virtual void mousePressEvent( QMouseEvent* event ) Q_DECL_OVERRIDE;

        /**
         * @brief Processes mouse move events when a mouse button is pressed
         *        (if mouse move events should be processed without a mouse button being
         *        pressed, setMouseTracking() must be set to "true" for the component).
         *
         * @param event  the mouse event to be processed
         */
        virtual void mouseMoveEvent( QMouseEvent* event ) Q_DECL_OVERRIDE;

        /**
         * @brief Processes mouse release events.
         *
         * @param event  the mouse event to be processed
         */
        virtual void mouseReleaseEvent( QMouseEvent* event ) Q_DECL_OVERRIDE;

        /**
         * @brief Draws the editor.
         *
         * @param hdc  the native device context to draw on
         */
        virtual void drawGDI( HDC hdc ) Q_DECL_OVERRIDE;


    private:

        /**
         * @brief Handle to the editor DLL.
         */
        HINSTANCE hEditorDLLHandle;

        /**
         * @brief previousPressedX  The previous x position pointed to by the mouse when a button was pressed.
         */
        int previousPressedX;

        /**
         * @brief previousPressedY  The previous y position pointed to by the mouse when a button was pressed.
         */
        int previousPressedY;

        /**
         * @brief mainWindow  The widget to which the callbacks are delegated.
         */
        QWidget* mainWindow;
};


#endif // GRAPHICSVIEWEDITOR_H
