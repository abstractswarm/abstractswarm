/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "graphicsviewgdi.h"

#include <QEvent>
#include <QPaintEvent>
#include "windows.h"


GraphicsViewGDI::GraphicsViewGDI( QWidget* parent ) : QGraphicsView( parent )
{
    // Set the graphics view as native element
    setAttribute( Qt::WA_NativeWindow );

    // Get native window handle and device context
    hwnd = reinterpret_cast<HWND>( viewport()->winId() );
    hdc = GetDC( hwnd );
}


GraphicsViewGDI::~GraphicsViewGDI()
{
    // Release native device context
    ReleaseDC( hwnd, hdc );
}


bool GraphicsViewGDI::event( QEvent* event )
{
    // If paint event, call GDI drawing routine...
    if( event->type() == QEvent::Paint )
    {
        bool result = QGraphicsView::event( event );

        drawGDI( hdc );

        return result;
    }

    return( QGraphicsView::event( event ) );
}


HDC GraphicsViewGDI::getHDC() const
{
    return( hdc );
}
