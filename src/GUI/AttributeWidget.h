/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2019  Lars Hadidi (lahadidi@uni-mainz.de)
                    (partly auto-generated by Qt Creator)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ATTRIBUTEWIDGET_H
#define ATTRIBUTEWIDGET_H


#include <QObject>
#include <QVariant>

namespace GUI {
    enum ENAttributeCode
    {
        NAME = 0, COUNT = 1, COLOR = 2,
        PRIORITY = 13, FREQUENCY= 14, NECESSITY = 15,
        TIME = 16, CYCLE = 17, ITEM = 18,
        SPACE = 19, CAPACITY = 20, SIZE = 21,
        SPEED = 22, BOLD = 23, DIRECTED_1 = 24,
        DIRECTED_2 = 25, AND_1 = 27, AND_2 = 28,
        UNDIRECTED = 30, AND_1_2 = 31, AND_0 = 32
    };

    class AttributeWidget : public QObject
    {
        Q_OBJECT

        public:
            void setAttributeCode( ENAttributeCode attributeCode )
            {
                this->attributeCode = attributeCode;
            }
            ENAttributeCode getAttributeCode()
            {
                return this->attributeCode;
            }

        public slots:
            void setValue( const QVariant &value )
            {
                emit valueChanged( attributeCode, value );
            }

        signals:
            void valueChanged( ENAttributeCode, const QVariant& );

        private:
            ENAttributeCode attributeCode;
    };
}


#endif // ATTRIBUTEWIDGET_H
