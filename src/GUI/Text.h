/*
AbstractSwarm - Graphical multi-agent modeling/simulation environment
Copyright (C) 2020  Daan Apeldoorn (daan.apeldoorn@uni-mainz.de)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if !defined Text_h
    #define Text_h





// Application title and version
#define APP_TITLE "AbstractSwarm"
#define APP_VERSION "0.24.1"


// File name
#define FILE_NEW "New"


// Messages
#define MSG_OPEN_ERROR "Invalid graph file!"
#define MSG_NO_FILE "No such file!"
#define MSG_SAVE_ERROR "The graph file could not be saved correctly!"
#define MSG_SAVE_CONFIRMATION "Do you want to save?"
#define MSG_OVERWRITE "This file already exists! Do you want to overwrite it?"
#define MSG_NOT_ENOUGH_SPACE_FOR_PERSPECTIVE "There is not enough space to add this perspective!"
#define MSG_NOT_ENOUGH_SPACE_FOR_COMPONENTTYPE "There is not enough space to add this component type! Please enlarge perspective."
#define MSG_NOT_ENOUGH_SPACE_FOR_ATTRIBUTE "There is not enough space to add this attribute! Please enlarge perspective."


// Status bar
#define STATUS_READY "Ready"
#define STATUS_RUNNING "Running..."
#define STATUS_RUNNING_COUNT "Running... (%n runs remaining)"
#define STATUS_RUNS_COMPLETED "Run(s) completed (total waiting time: %n)"
#define STATUS_RUNS_COMPLETED_NO_SOLUTION "Run(s) completed (AGENTS WERE NOT ABLE TO FIND A SOLUTION TO THE PROBLEM!)"





#endif
 
