#include "mainwindow.h"
#include <QApplication>
#include <QFile>
#include <QTimer>
#include <QDir>
#include <QSplashScreen>

#include <fstream>

#include "Editor/EditorDll.h"
#include "Simulation/SimulationDll.h"
#include "Timetable/TimetableDll.h"
#include "GUI/Text.h"

using namespace std;


// A timer constantly flushing the console to make the agent Java output visible
class FlushConsoleTimer : public QWidget
{
public:
    FlushConsoleTimer()
    {
        // Do the first flushing directly when the timer is created
        // (without event queue)
        SimulationDll::flushConsole();

        startTimer( 1 );
    }
    ~FlushConsoleTimer()
    {
        killTimer( 1 );
    }
private slots:
    void timerEvent( QTimerEvent *event ) Q_DECL_OVERRIDE;
};
void FlushConsoleTimer::timerEvent( QTimerEvent *event )
{
    SimulationDll::flushConsole();
}


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // Only create ui, if no additional parameters are provided
    if( argc <= 1 )
    {
        // Create and show splash screen
        QPixmap pixmap( ":/logo.png" );
        QSplashScreen splashScreen( pixmap, Qt::WindowStaysOnTopHint );
        splashScreen.show();

        MainWindow mainWindow;

        QFile styleFile( ":/styles/mainStyle" );
        styleFile.open( QFile::ReadOnly );

        QString style( styleFile.readAll() );
        app.setStyleSheet( style );

        mainWindow.show();

        // Remove splash screen
        splashScreen.finish( &mainWindow );

        return app.exec();
    }
    else
    {
        // Load editor
        EditorDll::createEditor();

        // Load simulation
        if( argc > 2 )
        {
            SimulationDll::createSimulation( "", argv[ 2 ], false );
        }
        else
        {
            cout << "Too few arguments: Missing agent implementation." << endl;
        }

        // Load timetable
        TimetableDll::createTimetable();


        // Connect editor to simulation
        EditorDll::connectToSimulation( SimulationDll::addStation, SimulationDll::removeStation, SimulationDll::addAgent, SimulationDll::removeAgent );

        // Connect simulation to timetable
        SimulationDll::connectToTimetable( TimetableDll::addAgentEnterEvent, TimetableDll::addAgentLeaveEvent );

        // Create a timer continuously flushing the console to see agent output to standard out directy
        FlushConsoleTimer flushConsoleTimer;

        // Load provided scenario (parameter 2)
        if( argc > 1 )
        {
            // Extract the path and the file name from the provided relative path
            QStringList scenarioPathList;
            if( QString( argv[ 1 ] ).contains( QDir::separator() ) )
            {
                scenarioPathList = QString( argv[ 1 ] ).split( QDir::separator() );
            }
            else if( QString( argv[ 1 ] ).contains( '/' ) )
            {
                scenarioPathList = QString( argv[ 1 ] ).split( '/' );
            }
            else if( QString( argv[ 1 ] ).contains( '\\' ) )
            {
                scenarioPathList = QString( argv[ 1 ] ).split( '\\' );
            }
            QString scenarioFilename = scenarioPathList.last();
            scenarioPathList.pop_back();
            QString scenarioPath;
            for( int i = 0; i < scenarioPathList.size(); i++ )
            {
                scenarioPath = scenarioPath + scenarioPathList[ i ] + QDir::separator();
            }

            // Convert relative to absolute path
            scenarioPath = QDir( scenarioPath ).absolutePath();

            // Create the abslute path with filename
            QString scenarioPathAndFilename = scenarioPath + QDir::separator() + scenarioFilename;

            // Load scenario
            EditorDll::load( const_cast<char *>( scenarioPathAndFilename.toStdString().c_str() ) );
        }
        else
        {
            cout << "Too few arguments: Missing scenario graph." << endl;
        }

        // Set the number of runs (parameter 4)
        int simulationRunsCount = 1;
        if( argc > 3 )
        {
            // Set simulation run counter
            simulationRunsCount = strtol( argv[ 3 ], nullptr, 10 );
        }


        // Provide message that the simulation is running depending on if an infinite number of runs
        // has been chosen or not
        if( simulationRunsCount == -1 )
        {
            cout << STATUS_RUNNING << endl;
        }
        else
        {
            string statusMessage = STATUS_RUNNING_COUNT;
            statusMessage.replace( statusMessage.find( "%n" ), 2, to_string( simulationRunsCount ) );
            cout << statusMessage << endl;
        }


        // Reset variables for calculating simulation time
        double calcTimeSum = 0;

        // At the beginning there is no best valid total waiting time
        int bestValidTotalWaitingTime = -1;

        // Run the simulation (where i counts the current simulation STEP)
        int i = 0;
        while( true )
        {
            // Take the clock start to measure calculation time for one step
            clock_t clockStart = clock();

            // If there are more runs to perform and simulation not finished yet
            if( (simulationRunsCount != 0) && (!SimulationDll::getIsFinished()) )
            {
                // Simulate step
                SimulationDll::simulateStep( nullptr );

// THIS MIGHT BE AN OPTION FOR NON-EPISODIC SIMULATIONS
//                // If infinitely many runs, provide how many STEPS have been calculated
//                if( simulationRunsCount == -1 )
//                {
//                    cout << STATUS_RUNNING << " (" << i << " steps)" << endl;
//                }
            }

            // else if there are still runs to perform but simulation is finished, reset simulation
            else if( simulationRunsCount != 0 )
            {
                // Decrement simulation runs counter
                if( simulationRunsCount > 0 )
                    simulationRunsCount--;

                // If not infinitely many runs and not zero, show the remaining number of runs
                if( simulationRunsCount > 0 )
                {
                    string statusMessage = STATUS_RUNNING_COUNT;
                    statusMessage.replace( statusMessage.find( "%n" ), 2, to_string( simulationRunsCount ) );
                    cout << statusMessage << endl;
                }

                // Check if all Frequency and Necessity attributes are fulfilled
                bool isValid = SimulationDll::getIsValid();


                //
                // WRITE LOG FILE OF TIMETABLES
                //

                // Get path to .exe
                string sExePath = ".\\";

                // Open log file for appending
                ofstream* pofsOutputFile = new ofstream( (sExePath + "timetables.log").c_str(), ios_base::app );

                // Write a line with total waiting time if valid, else write empty line for missing value
                if( isValid )
                    (*pofsOutputFile) << TimetableDll::getTotalWaitingTime() << endl;
                else
                    (*pofsOutputFile) << endl;

                // Close log file
                delete pofsOutputFile;


                // If all Frequency and Necessity attributes are fulfilled
                if( isValid )
                {
                    // Keep the best plan
                    TimetableDll::keepBestPlan();

                    // Store the best valid total waiting time
                    bestValidTotalWaitingTime = TimetableDll::getTotalWaitingTime();
                }

                // Reset simulation
                SimulationDll::reset();

                // If there are no more simulation runs to be calculated
                if( simulationRunsCount == 0 )
                {
                     // Provide the (best) valid total waiting time, if there was a solution
                     if( bestValidTotalWaitingTime != -1 )
                     {
                         string statusMessage = STATUS_RUNS_COMPLETED;
                         statusMessage.replace( statusMessage.find( "%n" ), 2, to_string( bestValidTotalWaitingTime ) );
                         cout << statusMessage << endl;
                     }
                     else
                     {
                         cout << STATUS_RUNS_COMPLETED_NO_SOLUTION << endl;
                     }
                }

                // Clear timetable if simulation continues
                if( simulationRunsCount != 0 )
                {
                    TimetableDll::clear();
                }
                else
                {
                    // Unload everything and do break, if number of runs is reached
                    EditorDll::deleteEditor();
                    SimulationDll::deleteSimulation();
                    TimetableDll::deleteTimetable();
                    break;
                }
            }

            // Calculate the time that was needed for this step
            clock_t clockEnd = clock();
            double calcTime = ((clockEnd - clockStart) + 1) / CLOCKS_PER_SEC;
            calcTimeSum += calcTime;

            i++;
        }
    }

    return 0;
}
