#-------------------------------------------------
#
# Project created by QtCreator 2019-07-23T17:24:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AbstractSwarm
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11
CONFIG += rtti

SOURCES += \
        Editor/AgentType.cpp \
        Editor/CalcEdgeDrawOffset.cpp \
        Editor/CheckPosition.cpp \
        Editor/Color.cpp \
        Editor/ComponentType.cpp \
        Editor/CorrectPosition.cpp \
        Editor/CorrectSize.cpp \
        Editor/Drag.cpp \
        Editor/DragEdge.cpp \
        Editor/DragGraphObj.cpp \
        Editor/DrawComponentType.cpp \
        Editor/DrawEdge.cpp \
        Editor/DrawWeightEdge.cpp \
        Editor/DrawWithStyles.cpp \
        Editor/Drop.cpp \
        Editor/DropEdge.cpp \
        Editor/DropGraphObj.cpp \
        Editor/Edge.cpp \
        Editor/EdgeConnector.cpp \
        Editor/Editor.cpp \
        Editor/EditorDll.cpp \
        Editor/EditorObj.cpp \
        Editor/GetDraggedObjOwnerData.cpp \
        Editor/GetEdgeLabel.cpp \
        Editor/GetEditorObjData.cpp \
        Editor/GetEditorObjType.cpp \
        Editor/GraphObj.cpp \
        Editor/Grid.cpp \
        Editor/LoadXmlFile.cpp \
        Editor/Perspective.cpp \
        Editor/SetEditorObjData.cpp \
        Editor/StationType.cpp \
        Editor/TimeEdge.cpp \
        Editor/VisitEdge.cpp \
        Editor/WeightEdge.cpp \
        Editor/WriteXmlAttribs.cpp \
        Editor/WriteXmlCloseTag.cpp \
        Editor/WriteXmlComponentTypeTagContent.cpp \
        Editor/WriteXmlEditorObjAttribs.cpp \
        Editor/WriteXmlFile.cpp \
        Editor/WriteXmlOpenTag.cpp \
        Editor/WriteXmlSpecialEdgeAttribs.cpp \
        Editor/WriteXmlSpecialGraphObjAttribs.cpp \
        Editor/WriteXmlSpecialWeightEdgeAttribs.cpp \
        Editor/WriteXmlTagContent.cpp \
        GUI/AttributeDeleteButton.cpp \
        Simulation/Calculation/CheckFinished.cpp \
        Simulation/Calculation/CheckPlaceEdgeConstraint.cpp \
        Simulation/Calculation/CheckTimeEdgeConstraint.cpp \
        Simulation/Calculation/ChooseAction.cpp \
        Simulation/Calculation/Communicate.cpp \
        Simulation/Calculation/CountVisitEdgeConnectedComponents.cpp \
        Simulation/Calculation/CountVisitEdges.cpp \
        Simulation/Calculation/ExternalJavaAgentInterface.cpp \
        Simulation/Calculation/GetSimulationDrawFactor.cpp \
        Simulation/Calculation/ProcessCommunicationAdapter.cpp \
        Simulation/Calculation/Simulate.cpp \
        Simulation/Calculation/TransferCycleToJava.cpp \
        Simulation/Calculation/TransferNecessityToJava.cpp \
        Simulation/Calculation/TransferToJava.cpp \
        Simulation/Components/Agent.cpp \
        Simulation/Components/Component.cpp \
        Simulation/Components/InitEdgeDependentComponentValues.cpp \
        Simulation/Components/InitEdgeDependentStationValues.cpp \
        Simulation/Components/Station.cpp \
        Simulation/Simulation.cpp \
        Simulation/SimulationDll.cpp \
        Simulation/Utilities.cpp \
        Timetable/Draw/TimetableDrawWithStyles.cpp \
        Timetable/Entry.cpp \
        Timetable/Timetable.cpp \
        Timetable/TimetableColor.cpp \
        Timetable/TimetableDll.cpp \
        Timetable/TimetableGrid.cpp \
        GUI/AttributeAction.cpp \
        GUI/AttributeSpinBox.cpp \
        graphicsvieweditor.cpp \
        graphicsviewgdi.cpp \
        graphicsviewsimulation.cpp \
        graphicsviewtimetable.cpp \
        main.cpp \
        mainwindow.cpp

HEADERS += \
        Editor/AgentType.h \
        Editor/CalcEdgeDrawOffset.hpp \
        Editor/CheckPosition.hpp \
        Editor/Color.h \
        Editor/ComponentType.h \
        Editor/Constants.h \
        Editor/CorrectPosition.hpp \
        Editor/CorrectSize.hpp \
        Editor/Drag.hpp \
        Editor/DragEdge.hpp \
        Editor/DragGraphObj.hpp \
        Editor/DrawComponentType.hpp \
        Editor/DrawEdge.hpp \
        Editor/DrawWeightEdge.hpp \
        Editor/DrawWithStyles.hpp \
        Editor/Drop.hpp \
        Editor/DropEdge.hpp \
        Editor/DropGraphObj.hpp \
        Editor/Edge.h \
        Editor/EdgeConnector.h \
        Editor/Editor.h \
        Editor/EditorDll.h \
        Editor/EditorObj.h \
        Editor/EditorTypes.h \
        Editor/GetDraggedObjOwnerData.hpp \
        Editor/GetEdgeLabel.hpp \
        Editor/GetEditorObjData.hpp \
        Editor/GetEditorObjType.hpp \
        Editor/GraphObj.h \
        Editor/Grid.h \
        Editor/LoadXmlFile.hpp \
        Editor/Perspective.h \
        Editor/PlaceEdge.h \
        Editor/SetEditorObjData.hpp \
        Editor/StationType.h \
        Editor/TimeEdge.h \
        Editor/VisitEdge.h \
        Editor/WeightEdge.h \
        Editor/WriteXmlAttribs.hpp \
        Editor/WriteXmlCloseTag.hpp \
        Editor/WriteXmlComponentTypeTagContent.hpp \
        Editor/WriteXmlEditorObjAttribs.hpp \
        Editor/WriteXmlFile.hpp \
        Editor/WriteXmlOpenTag.hpp \
        Editor/WriteXmlSpecialEdgeAttribs.hpp \
        Editor/WriteXmlSpecialGraphObjAttribs.hpp \
        Editor/WriteXmlSpecialWeightEdgeAttribs.hpp \
        Editor/WriteXmlTagContent.hpp \
        GUI/AttributeDeleteButton.h \
        GUI/Text.h \
        Simulation/Calculation/CheckFinished.hpp \
        Simulation/Calculation/CheckPlaceEdgeConstraint.hpp \
        Simulation/Calculation/CheckTimeEdgeConstraint.hpp \
        Simulation/Calculation/ChooseAction.hpp \
        Simulation/Calculation/Communicate.hpp \
        Simulation/Calculation/CountVisitEdgeConnectedComponents.hpp \
        Simulation/Calculation/CountVisitEdges.hpp \
        Simulation/Calculation/ExternalJavaAgentInterface.hpp \
        Simulation/Calculation/GetSimulationDrawFactor.hpp \
        Simulation/Calculation/ProcessCommunicationAdapter.hpp \
        Simulation/Calculation/Simulate.hpp \
        Simulation/Calculation/TransferCycleToJava.hpp \
        Simulation/Calculation/TransferNecessityToJava.hpp \
        Simulation/Calculation/TransferToJava.hpp \
        Simulation/Components/Agent.hpp \
        Simulation/Components/Component.hpp \
        Simulation/Components/InitEdgeDependentComponentValues.hpp \
        Simulation/Components/InitEdgeDependentStationValues.hpp \
        Simulation/Components/Station.hpp \
        Simulation/Constants.h \
        Simulation/Simulation.h \
        Simulation/SimulationDll.h \
        Simulation/SimulationTypes.h \
        Simulation/Utilities.h \
        Timetable/Constants.h \
        Timetable/Draw/TimetableDrawWithStyles.hpp \
        Timetable/Entry.hpp \
        Timetable/Timetable.h \
        Timetable/TimetableColor.hpp \
        Timetable/TimetableDll.h \
        Timetable/TimetableGrid.h \
        Timetable/TimetableTypes.h \
        GUI/AttributeAction.h \
        GUI/AttributeSpinBox.h \
        GUI/AttributeWidget.h \
        graphicsvieweditor.h \
        graphicsviewgdi.h \
        graphicsviewsimulation.h \
        graphicsviewtimetable.h \
        mainwindow.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32: LIBS += -lgdi32

RESOURCES += \
    qres.qrc

# No unused parameters warnings, since these do often appear in the ported C-mol code
QMAKE_CXXFLAGS += -Wno-unused

# Copy the external java agent interface to the output directory
win32
{
    source = $${PWD}/Simulation/Calculation/AbstractSwarmAgentInterface.java
    source ~= s,/,\\,g
    destination = $${OUT_PWD}
    destination ~= s,/,\\,g
    system( copy /y $$source $$destination )

    source2 = $${PWD}/Simulation/Calculation/AbstractSwarmAgentInterfaceServer.java
    source2 ~= s,/,\\,g
    destination2 = $${OUT_PWD}
    destination2 ~= s,/,\\,g
    system( copy /y $$source2 $$destination2 )

    source3 = $${PWD}/Simulation/Calculation/Agents
    source3 ~= s,/,\\,g
    destination3 = $${OUT_PWD}/Agents
    destination3 ~= s,/,\\,g
#    system( rd /s /q $$destination3 )
    system( xcopy /i /y /e $$source3 $$destination3 )
}

unix|win32: LIBS += -L$$PWD/../res/lib/xercesc/compiled/ -llibxerces-c.dll

INCLUDEPATH += $$PWD/../res/lib/xercesc/include
DEPENDPATH += $$PWD/../res/lib/xercesc/include
